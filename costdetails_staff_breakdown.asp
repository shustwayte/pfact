

 	  
<link rel="stylesheet" href="UOE/include/tabs.css" TYPE="text/css" MEDIA="screen">
<script type="text/javascript" src="UOE/include/tabber.js"></script>	  

<style>

body {
	background-color: #F1EAE0;
	font-size:11px;
	font-family: Arial; 
}


.tdblink
{
	background-color:red;
	color:white;
	font-family:'Helvetica Neue',Arial,sans-serif;
	text-align:right;
}

.butCL { 
	cursor:pointer;
	box-shadow:rgba(0,0,0,0.2) 0 1px 0 0;
	border-bottom-color:#333;
	border:1px solid #61c4ea;
	background-color:#7cceee;
	border-radius:5px;
	color:#333;
	font-family:'Helvetica Neue',Arial,sans-serif;
	font-size:14px;
	height:20px;
	font-weight:700;
	text-shadow:#b2e2f5 0 1px 0;
	padding:0px 16px;
 }   
 .butCL:hover {
 background-color:#7ccccc;
 
 }
 
 
.td3 {
	font-size:12px;
	border-left:1pt solid black;
	background-color: white;
	font-family: Arial; 
}

.td1 {
	font-weight: bold;
	color: black;
	font-family: Arial;
	background-color: #F1EAE0;
	font-size:12px;
	border-top:1pt solid black;
	border-left:1pt solid black;
	font-family: Arial; 
}

.tdjoined {
	font-weight: bold;
	color: white;
	font-family: Arial;
	background-color: green;
	font-size:12px;
	border-top:1pt solid black;
	border-left:1pt solid black;
	font-family: Arial; 
}

.td2 {
	background-color: lightblue;
	font-size:12px;
	border-left:1pt solid black;
	font-family: Arial; 
}

</style>
 
<div id="slowload" style="display:block;"> 	

	<LINK rel=stylesheet href=Images/Gray/PFATStyleSheet.css type=text/css>

<%
dim prjid
prjid = request.querystring("prjid")

	Set objConn = Server.CreateObject("ADODB.Connection")
    objConn.Open "Driver={SQL Server};Server=VMMSSQLTEST01\DB_01_12TE;Initial Catalog=pfact_t1;User Id=Pfact_dbo;Password=4ctu4l1ty84515;Connection Timeout=10;Application Name=UOE_PFACTCostdetails;Connection Lifetime=10;"
	
	objConn.CommandTimeout = 120 
	
	set uoe_details = Server.CreateObject("ADODB.Recordset")
	uoe_details.CursorLocation = 3  ' adUseClient
	'response.write "  exec [pfact_t1].[dbo].[ProjectStaffCostDetailed_HOURS_REPORT] '" & prjid & "' "
	uoe_details.Open "  exec [pfact_t1].[dbo].[ProjectStaffCostDetailed_HOURS_REPORT] '" & prjid & "' ", objConn
	
%>


<span class="LabelCaption"><u>Proposal Staff Hours</u></span></br></br>

<div class="tabber" id="tab1">

<%		
dim tot
tot = 0

dim tot12
tot12 = 0
dim tot13
tot13 = 0
dim tot14
tot14 = 0
dim tot15
tot15 = 0
dim tot16
tot16 = 0
dim tot17
tot17 = 0
dim tot18
tot18 = 0
dim tot19
tot19 = 0
dim tot20
tot20 = 0

dim tot21
tot21 = 0
dim tot22
tot22 = 0
dim tot23
tot23 = 0
dim tot24
tot24 = 0
dim tot25
tot25 = 0

dim sname

Do While Not uoe_details.EOF	
if uoe_details.EOF = false then
%>			


<div class="tabbertab">
 <h2><% = uoe_details(1) %></h2>
 
<TABLE style="border: 1px solid black;" cellspacing="0">
<TR>
<td class="td1" width=450 colspan=4></td>
<td class="td1" width=150 style="text-align:right">Total Proposal Hours</td>
<td class="td1" width=150 style="text-align:right">2012/13</td>
<td class="td1" width=150 style="text-align:right">2013/14</td>
<td class="td1" width=150 style="text-align:right">2014/15</td>
<td class="td1" width=150 style="text-align:right">2015/16</td>
<td class="td1" width=150 style="text-align:right">2016/17</td>
<td class="td1" width=150 style="text-align:right">2017/18</td>
<td class="td1" width=150 style="text-align:right">2018/19</td>
<td class="td1" width=150 style="text-align:right">2019/20</td>
<td class="td1" width=150 style="text-align:right">2020/21</td>


<td class="td1" width=150 style="text-align:right">2021/22</td>
<td class="td1" width=150 style="text-align:right">2022/23</td>
<td class="td1" width=150 style="text-align:right">2023/24</td>
<td class="td1" width=150 style="text-align:right">2024/25</td>
<td class="td1" width=150 style="text-align:right">2025/26</td>

</tr>

<tr>
<td class="td3" width=150px colspan=4><font size=2><B><% = uoe_details(1) %>: Proposal <% = prjid %>  Hours</B></font></td>
<td class="td3" style="text-align:right"><% = formatnumber(uoe_details(2),2) %></td>
<td class="td3" style="text-align:right"><% = formatnumber(uoe_details(3),2) %></td>
<td class="td3" style="text-align:right"><% = formatnumber(uoe_details(4),2) %></td>
<td class="td3" style="text-align:right"><% = formatnumber(uoe_details(5),2) %></td>
<td class="td3" style="text-align:right"><% = formatnumber(uoe_details(6),2) %></td>
<td class="td3" style="text-align:right"><% = formatnumber(uoe_details(7),2) %></td>
<td class="td3" style="text-align:right"><% = formatnumber(uoe_details(8),2) %></td>
<td class="td3" style="text-align:right"><% = formatnumber(uoe_details(9),2) %></td>
<td class="td3" style="text-align:right"><% = formatnumber(uoe_details(10),2) %></td>
<td class="td3" style="text-align:right"><% = formatnumber(uoe_details(11),2) %></td>

<td class="td3" style="text-align:right"><% = formatnumber(uoe_details(12),2) %></td>
<td class="td3" style="text-align:right"><% = formatnumber(uoe_details(13),2) %></td>
<td class="td3" style="text-align:right"><% = formatnumber(uoe_details(14),2) %></td>
<td class="td3" style="text-align:right"><% = formatnumber(uoe_details(15),2) %></td>
<td class="td3" style="text-align:right"><% = formatnumber(uoe_details(16),2) %></td>

</tr>
<%
tot = tot + uoe_details(2)
tot12 = tot12 + uoe_details(3)
tot13 = tot13 + uoe_details(4)
tot14 = tot14 + uoe_details(5)
tot15 = tot15 + uoe_details(6)
tot16 = tot16 + uoe_details(7)
tot17 = tot17 + uoe_details(8)
tot18 = tot18 + uoe_details(9)
tot19 = tot19 + uoe_details(10)
tot20 = tot20 + uoe_details(11)

tot21 = tot21 + uoe_details(12)
tot22 = tot22 + uoe_details(13)
tot23 = tot23 + uoe_details(14)
tot24 = tot24 + uoe_details(15)
tot25 = tot25 + uoe_details(16)



'response.write mid(uoe_details(1),1, InStr(uoe_details(1)," ")-1)
'if InStr(uoe_details(1)," ") > 0 then
'sname = mid(uoe_details(1),1, InStr(uoe_details(1)," ")-1)
'else
'sname = uoe_details(1)
'end if

sname = uoe_details("staffid")
'response.write sname

'response.write "</br>" & mid(sname,1,InStr(sname,"_")-1)
sname = mid(sname,1,InStr(sname,"_")-1)

if sname <> "" then	

	Set objConnPAC = Server.CreateObject("ADODB.Connection")
    'objConn.Open "Driver={SQL Server};Server=VMTESTSYMREP;Databse=UOE_Purchasing;UID=UOE_Purchasing;PWD=fbUS786290Mdfs"
	'objConnPAC.Open "Driver={SQL Server};Server=VMSQLPR02A\DB_01_16;Databse=pac;UID=MiCRM_Admin;PWD=MiCRM_p@55w0rd"
	objConnPAC.Open "Driver={SQL Server};Server=DB0116PRLIS,64007;Databse=pac;UID=MiCRM_Admin;PWD=MiCRM_p@55w0rd"
	
	objConnPAC.CommandTimeout = 120 
		
	set hrtot = Server.CreateObject("ADODB.Recordset")
	hrtot.CursorLocation = 3  ' adUseClient
	'response.write "  exec [pace].[dbo].[UOE_P_PRJ_HOURS_CALC_GET_TOTAL] '', '" & sname & "' , '" & prjid & "'   "
	hrtot.Open "  exec [pac].[dbo].[UOE_P_PRJ_HOURS_CALC_GET_TOTAL] '', '" & sname & "' , '" & prjid & "'   ", objConnPAC

	set saved_req = Server.CreateObject("ADODB.Recordset")
	saved_req.CursorLocation = 3  ' adUseClient
	'response.write "  exec [pac].[dbo].[UOE_P_PRJ_HOURS_CALC_GET] '', '" & sname & "'   "
	saved_req.Open "  exec [pac].[dbo].[UOE_P_PRJ_HOURS_CALC_GET] '', '" & sname & "'   ", objConnPAC

	if saved_req.eof then	%>
	
	
	</table>
	</br>
	No PAC Award records.
	</div>
	<%
	ELSE
	%>
<tr>	
				  <TD class=td1>PAC Project</TD>
				  <TD class=td1>Status</T widTD=150H>
				  <TD class=td1 widTD=250>Description</TD>
				  <TD class=td1 >Joined</TD>

				  <TD class=td1 widTD=150 style="text-align:right"></TD>
				  
				  
				  <TD class=td1 widTD=150 style="text-align:right"></TD>
				  <TD class=td1 widTD=150 style="text-align:right"></TD>
				  <TD class=td1 widTD=150 style="text-align:right"></TD>
				  <TD class=td1 widTD=150 style="text-align:right"></TD>
				  <TD class=td1 widTD=150 style="text-align:right"></TD>
				  <TD class=td1 widTD=150 style="text-align:right"></TD>
				  <TD class=td1 widTD=150 style="text-align:right"></TD>
				  <TD class=td1 widTD=150 style="text-align:right"></TD>
				  <TD class=td1 widTD=150 style="text-align:right"></TD>
				  
				  <TD class=td1 widTD=150 style="text-align:right"></TD>
				  <TD class=td1 widTD=150 style="text-align:right"></TD>
				  <TD class=td1 widTD=150 style="text-align:right"></TD>
				  <TD class=td1 widTD=150 style="text-align:right"></TD>
				  <TD class=td1 widTD=150 style="text-align:right"></TD>
				  
				</TR>	
	

<%		
Do While Not saved_req.EOF	
%>			
<TR>
<td class="td3"><a target="_new" href="http://vmpac:90/PAC_Test/micrm/BUDGET/UOE_PCF_Live.asp?project_id=<% = saved_req(0) %>"><% = saved_req(0) %></a></td>
<td class="td3"><% = saved_req(1) %></td>
<td class="td3"><% = saved_req(2) %></td>
<% if saved_req("pfactid") <> "" then %>
<td class="tdjoined"><% = saved_req("pfactid") %></td>
<% else %>
<td class="td3"><% = saved_req("pfactid") %></td>
<% end if %>
<td class="td3" style='text-align:right'><% = FormatNumber(saved_req(6)) %></td>


<%

	if saved_req(9) > 1650 then
	response.write  "<td class =tdblink><b>" & FormatNumber(saved_req(9)) & "</b></td>"
	else
	response.write  "<td class=td3  style='text-align:right'>" & FormatNumber(saved_req(9)) & "</td>"
	end if 
	if saved_req(10) > 1650 then
	response.write  "<td class =tdblink><b>" & FormatNumber(saved_req(10)) & "</b></td>"
	else
	response.write  "<td class=td3  style='text-align:right'>" & FormatNumber(saved_req(10)) & "</td>"
	end if 
	if saved_req(11) > 1650 then
	response.write  "<td class =tdblink><b>" & FormatNumber(saved_req(11)) & "</b></td>"
	else
	response.write  "<td class=td3 style='text-align:right'>" & FormatNumber(saved_req(11)) & "</td>"
	end if 
	if saved_req(12) > 1650 then
	response.write  "<td class =tdblink><b>" & FormatNumber(saved_req(12)) & "</b></td>"
	else
	response.write  "<td class=td3 style='text-align:right'>" & FormatNumber(saved_req(12)) & "</td>"
	end if 
	if saved_req(13) > 1650 then
	response.write  "<td class =tdblink><b>" & FormatNumber(saved_req(13)) & "</b></td>"
	else
	response.write  "<td class=td3 style='text-align:right'>" & FormatNumber(saved_req(13)) & "</td>"
	end if 
	if saved_req(14) > 1650 then
	response.write  "<td class =tdblink><b>" & FormatNumber(saved_req(14)) & "</b></td>"
	else
	response.write  "<td class=td3 style='text-align:right'>" & FormatNumber(saved_req(14)) & "</td>"
	end if 
	if saved_req(15) > 1650 then
	response.write  "<td class =tdblink><b>" & FormatNumber(saved_req(15)) & "</b></td>"
	else
	response.write  "<td class=td3 style='text-align:right'>" & FormatNumber(saved_req(15)) & "</td>"
	end if 
	if saved_req(16) > 1650 then
	response.write  "<td class =tdblink><b>" & FormatNumber(saved_req(16)) & "</b></td>"
	else
	response.write  "<td class=td3 style='text-align:right'>" & FormatNumber(saved_req(16)) & "</td>"
	end if 
	if saved_req(17) > 1650 then
	response.write  "<td class =tdblink><b>" & FormatNumber(saved_req(17)) & "</b></td>"
	else
	response.write  "<td class=td3 style='text-align:right'>" & FormatNumber(saved_req(17)) & "</td>"
	end if 
	
	
	
	if saved_req(18) > 1650 then
	response.write  "<td class =tdblink><b>" & FormatNumber(saved_req(18)) & "</b></td>"
	else
	response.write  "<td class=td3 style='text-align:right'>" & FormatNumber(saved_req(18)) & "</td>"
	end if 
	if saved_req(19) > 1650 then
	response.write  "<td class =tdblink><b>" & FormatNumber(saved_req(19)) & "</b></td>"
	else
	response.write  "<td class=td3 style='text-align:right'>" & FormatNumber(saved_req(19)) & "</td>"
	end if 
	if saved_req(20) > 1650 then
	response.write  "<td class =tdblink><b>" & FormatNumber(saved_req(20)) & "</b></td>"
	else
	response.write  "<td class=td3 style='text-align:right'>" & FormatNumber(saved_req(20)) & "</td>"
	end if 
	if saved_req(21) > 1650 then
	response.write  "<td class =tdblink><b>" & FormatNumber(saved_req(21)) & "</b></td>"
	else
	response.write  "<td class=td3 style='text-align:right'>" & FormatNumber(saved_req(21)) & "</td>"
	end if 
	if saved_req(22) > 1650 then
	response.write  "<td class =tdblink><b>" & FormatNumber(saved_req(22)) & "</b></td>"
	else
	response.write  "<td class=td3 style='text-align:right'>" & FormatNumber(saved_req(22)) & "</td>"
	end if 
	
	
%>	


</TR>

<%
saved_req.MoveNext
if saved_req.EOF = false then
%>

<TR>
<td class=td3><a target="_new" href="http://vmpac:90/PAC_Test/micrm/BUDGET/UOE_PCF_Live.asp?project_id=<% = saved_req(0) %>"><% = saved_req(0) %></a></td>
<td class=td3><% = saved_req(1) %></td>
<td class=td3><% = saved_req(2) %></td>
<% if saved_req("pfactid") <> "" then %>
<td class="tdjoined"><% = saved_req("pfactid") %></td>
<% else %>
<td class="td3"><% = saved_req("pfactid") %></td>
<% end if %>
<td class=td3 style='text-align:right'><% = FormatNumber(saved_req(6)) %></td>


<%
	if saved_req(9) > 1650 then
	response.write  "<td class =tdblink><b>" & FormatNumber(saved_req(9)) & "</b></td>"
	else
	response.write  "<td class=td3 style='text-align:right'>" & FormatNumber(saved_req(9)) & "</td>"
	end if 
	if saved_req(10) > 1650 then
	response.write  "<td class =tdblink><b>" & FormatNumber(saved_req(10)) & "</b></td>"
	else
	response.write  "<td class=td3 style='text-align:right'>" & FormatNumber(saved_req(10)) & "</td>"
	end if 
	if saved_req(11) > 1650 then
	response.write  "<td class =tdblink><b>" & FormatNumber(saved_req(11)) & "</b></td>"
	else
	response.write  "<td class=td3 style='text-align:right'>" & FormatNumber(saved_req(11)) & "</td>"
	end if 
	if saved_req(12) > 1650 then
	response.write  "<td class =tdblink><b>" & FormatNumber(saved_req(12)) & "</b></td>"
	else
	response.write  "<td class=td3 style='text-align:right'>" & FormatNumber(saved_req(12)) & "</td>"
	end if 
	if saved_req(13) > 1650 then
	response.write  "<td class =tdblink><b>" & FormatNumber(saved_req(13)) & "</b></td>"
	else
	response.write  "<td class=td3 style='text-align:right'>" & FormatNumber(saved_req(13)) & "</td>"
	end if 
	if saved_req(14) > 1650 then
	response.write  "<td class =tdblink><b>" & FormatNumber(saved_req(14)) & "</b></td>"
	else
	response.write  "<td class=td3 style='text-align:right'>" & FormatNumber(saved_req(14)) & "</td>"
	end if 
	if saved_req(15) > 1650 then
	response.write  "<td class =tdblink><b>" & FormatNumber(saved_req(15)) & "</b></td>"
	else
	response.write  "<td class=td3 style='text-align:right'>" & FormatNumber(saved_req(15)) & "</td>"
	end if 
	if saved_req(16) > 1650 then
	response.write  "<td class =tdblink><b>" & FormatNumber(saved_req(16)) & "</b></td>"
	else
	response.write  "<td class=td3 style='text-align:right'>" & FormatNumber(saved_req(16)) & "</td>"
	end if 
	if saved_req(17) > 1650 then
	response.write  "<td class =tdblink><b>" & FormatNumber(saved_req(17)) & "</b></td>"
	else
	response.write  "<td class=td3 style='text-align:right'>" & FormatNumber(saved_req(17)) & "</td>"
	end if 
	
	
	
	if saved_req(18) > 1650 then
	response.write  "<td class =tdblink><b>" & FormatNumber(saved_req(18)) & "</b></td>"
	else
	response.write  "<td class=td3 style='text-align:right'>" & FormatNumber(saved_req(18)) & "</td>"
	end if 
	if saved_req(19) > 1650 then
	response.write  "<td class =tdblink><b>" & FormatNumber(saved_req(19)) & "</b></td>"
	else
	response.write  "<td class=td3 style='text-align:right'>" & FormatNumber(saved_req(19)) & "</td>"
	end if 
	if saved_req(20) > 1650 then
	response.write  "<td class =tdblink><b>" & FormatNumber(saved_req(20)) & "</b></td>"
	else
	response.write  "<td class=td3 style='text-align:right'>" & FormatNumber(saved_req(20)) & "</td>"
	end if 
	if saved_req(21) > 1650 then
	response.write  "<td class =tdblink><b>" & FormatNumber(saved_req(21)) & "</b></td>"
	else
	response.write  "<td class=td3 style='text-align:right'>" & FormatNumber(saved_req(21)) & "</td>"
	end if 
	if saved_req(22) > 1650 then
	response.write  "<td class =tdblink><b>" & FormatNumber(saved_req(22)) & "</b></td>"
	else
	response.write  "<td class=td3 style='text-align:right'>" & FormatNumber(saved_req(22)) & "</td>"
	end if 
	
	
%>
</TR>

<%
saved_req.MoveNext
	end if
							
loop
%>		


		
<TR>
<td class=td2 colspan=4></td>
<td class=td2 style="text-align:right"><b>Total Hours</b></td>

<!--<td><b><% = hrtot(1)+tot %></b></td>-->
<% 
	if hrtot(2)+tot12 > 1650 then
	response.write  "<td class =tdblink><b>" & FormatNumber(hrtot(2)+tot12) & "</b></td>"
	else
	response.write  "<td class=td2 style='text-align:right'><b>" & FormatNumber(hrtot(2)+tot12) & "</b></td>"
	end if 
	if hrtot(3)+tot13 > 1650 then
	response.write  "<td class =tdblink><b>" & FormatNumber(hrtot(3)+tot13) & "</b></td>"
	else
	response.write  "<td class=td2 style='text-align:right'><b>" & FormatNumber(hrtot(3)+tot13) & "</b></td>"
	end if 
	if hrtot(4)+tot14 > 1650 then
	response.write  "<td class =tdblink><b>" & FormatNumber(hrtot(4)+tot14) & "</b></td>"
	else
	response.write  "<td class=td2 style='text-align:right'><b>" & FormatNumber(hrtot(4)+tot14) & "</b></td>"
	end if 
	if hrtot(5)+tot15 > 1650 then
	response.write  "<td class =tdblink><b>" & FormatNumber(hrtot(5)+tot15) & "</b></td>"
	else
	response.write  "<td class=td2 style='text-align:right'><b>" & FormatNumber(hrtot(5)+tot15) & "</b></td>"
	end if 
	if hrtot(6)+tot16 > 1650 then
	response.write  "<td class =tdblink><b>" & FormatNumber(hrtot(6)+tot16) & "</b></td>"
	else
	response.write  "<td class=td2 style='text-align:right'><b>" & FormatNumber(hrtot(6)+tot16) & "</b></td>"
	end if 
	if hrtot(7)+tot17 > 1650 then
	response.write  "<td class =tdblink><b>" & FormatNumber(hrtot(7)+tot17) & "</b></td>"
	else
	response.write  "<td class=td2 style='text-align:right'><b>" & FormatNumber(hrtot(7)+tot17) & "</b></td>"
	end if 
	if hrtot(8)+tot18 > 1650 then
	response.write  "<td class =tdblink><b>" & FormatNumber(hrtot(8)+tot18) & "</b></td>"
	else
	response.write  "<td class=td2 style='text-align:right'><b>" & FormatNumber(hrtot(8)+tot18) & "</b></td>"
	end if 
	if hrtot(9)+tot19 > 1650 then
	response.write  "<td class =tdblink><b>" & FormatNumber(hrtot(9)+tot19) & "</b></td>"
	else
	response.write  "<td class=td2 style='text-align:right'><b>" & FormatNumber(hrtot(9)+tot19) & "</b></td>"
	end if 
	if hrtot(10)+tot20 > 1650 then
	response.write  "<td class =tdblink><b>" & FormatNumber(hrtot(10)+tot20) & "</b></td>"
	else
	response.write  "<td class=td2 style='text-align:right'><b>" & FormatNumber(hrtot(10)+tot20) & "</b></td>"
	end if 
	
	

	if hrtot(11)+tot21 > 1650 then
	response.write  "<td class =tdblink><b>" & FormatNumber(hrtot(11)+tot21) & "</b></td>"
	else
	response.write  "<td class=td2 style='text-align:right'><b>" & FormatNumber(hrtot(11)+tot21) & "</b></td>"
	end if 
	if hrtot(12)+tot22 > 1650 then
	response.write  "<td class =tdblink><b>" & FormatNumber(hrtot(12)+tot22) & "</b></td>"
	else
	response.write  "<td class=td2 style='text-align:right'><b>" & FormatNumber(hrtot(12)+tot22) & "</b></td>"
	end if 
	if hrtot(13)+tot23 > 1650 then
	response.write  "<td class =tdblink><b>" & FormatNumber(hrtot(13)+tot23) & "</b></td>"
	else
	response.write  "<td class=td2 style='text-align:right'><b>" & FormatNumber(hrtot(13)+tot23) & "</b></td>"
	end if 
	if hrtot(14)+tot24 > 1650 then
	response.write  "<td class =tdblink><b>" & FormatNumber(hrtot(14)+tot24) & "</b></td>"
	else
	response.write  "<td class=td2 style='text-align:right'><b>" & FormatNumber(hrtot(14)+tot24) & "</b></td>"
	end if 
	if hrtot(15)+tot25 > 1650 then
	response.write  "<td class =tdblink><b>" & FormatNumber(hrtot(15)+tot25) & "</b></td>"
	else
	response.write  "<td class=td2 style='text-align:right'><b>" & FormatNumber(hrtot(15)+tot25) & "</b></td>"
	end if 
	
	
	%>
</TR>

		</TABLE>	

</br>


</div>
			
<% 
	end if 
	end if 	

tot = 0
tot12 = 0
tot13 = 0
tot14 = 0
tot15 = 0
tot16 = 0
tot17 = 0
tot18 = 0
tot19 = 0
tot20 = 0

tot21 = 0
tot22 = 0
tot23 = 0
tot24 = 0
tot25 = 0

uoe_details.MoveNext

	end if
							
loop
%>


</div>

<input type="hidden" id="last_tab_id">

