<%@ Reference Page="~/PromptBeforeDelete.aspx" %>
<%@ page language="VB" autoeventwireup="false" inherits="PFACT.NewProjectProposalStep3, pFACT_Deploy" enablesessionstate="True" %>
<%@ Register Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" TagPrefix="aspToolkit" %>
<%--<!DOCTYPE html PUBLIC "-//W3C//DTD html 4.0 Transitional//EN">--%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/tr/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>pFACT</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR"/>
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE"/>
		<meta content="JavaScript" name="vs_defaultClientScript"/>
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema"/>
		<%=styleSheetVar%>
		<script type="text/javascript"  language="javascript" src="ClientScripts.js"></script>
		<script type="text/javascript" language="javascript">
			var isPostback=false;
			function funderFilterClicked()
			{
//				document.frmStep3.txtFunFilterClicked.value="OK"  							
				document.frmStep3.submit()	
			}			
			function incFilterClicked()
			{
				
//				document.frmStep3.txtIncFilterClicked.value="OK"  							
				document.frmStep3.submit()	
			}
			//PID:12917 || Modified By sumathy on 13-Sep-2005 ||Comments:User changed text is reflected on alert messages. 
			function addClick()
			{
			
			    
			    /*if (document.frmStep3.ddFundSource.value=="0")
				{
					alert("Please select a "+document.frmStep3.hdnUserFundingSource.value +" from the list.")
					document.frmStep3.ddFundSource.focus()
					return false
				}
				*/
				if (document.frmStep3.ddFunder.value=="0")
				{
					alert("Please select a "+document.frmStep3.hdnUserFunder.value +" from the list.")
					document.frmStep3.ddFunder.focus()
					return false
				}
				
				if(document.frmStep3.ddIncomeTemplate.value=="0")
				{
					var income=document.frmStep3.hdnUserIncome.value
					alert("Please select a "+income+" template from the list")
					document.frmStep3.ddIncomeTemplate.focus()
					return false
				}
				/*Added validation for Amount filed PID:23912*/
				if(document.frmStep3.hdnAmount.value=="True")
				{
				
				    if(document.frmStep3.txtAmount.value=="")
				    {
					    alert("Please enter amount")
					    document.frmStep3.txtAmount.focus()
					    return false
					}
				}
				if(document.frmStep3.txtEuActivity.value=="True")
				{
				    if(document.frmStep3.ddEuActivity.value=="0")
				    {
				        alert("Please select an EuActivity from the list")
				        document.frmStep3.ddEuActivity.focus()
				        return false
				    }
				    
				}
				var ProjIncTempID="<%=session("ProjPropIncomeTemp")%>"
				if(document.frmStep3.hdnPrimaryFunder.value!="")
				{
				    if(document.frmStep3.chkPrimaryFunder.checked)
				    {
				        document.frmStep3.hdnRemovePrimary.value=""
				        var ddFunderText=document.frmStep3.ddFunder
				        var FunderText=ddFunderText.options[ddFunderText.selectedIndex].text
				        var endindex=FunderText.indexOf("-")
				        var TrimFunderText=FunderText.substring(0,endindex)
				        if(TrimFunderText=="")
				        {
				            TrimFunderText = FunderText
				        }
				        if (document.frmStep3.hdnSetValues.value != ProjIncTempID)
				        {
				            if(document.frmStep3.hdnPrimaryFunder.value != TrimFunderText)
				            {
						        var msg="Funder " + document.frmStep3.hdnPrimaryFunder.value + " is already set as primary funder. Click OK to make " + TrimFunderText + " as primary funder?"
						        var name=confirm(msg)
						        if (name==true)
							    {
							        document.frmStep3.hdnRemovePrimary.value="Save"
							    }
						        else
							        return false
					        }
					    }
				    }
				}
				if(document.frmStep3.hdnSetValues.value==ProjIncTempID)
				{
				    if(document.frmStep3.hdnCount.value>2 && document.frmStep3.chkPrimaryFunder.checked==false)
				    {
				        document.frmStep3.hdnRemovePrimary.value="Remove"
				    }
				}
				if (document.frmStep3.hdnPrimaryFunder.value != "" && document.frmStep3.chkPrimaryFunder.checked==false && document.frmStep3.hdnSetValues.value==ProjIncTempID && document.frmStep3.hdnCount.value==1)
				{
				    alert('You cannot remove the selection of PrimaryFunder, \n because this is the only funder for this project')
				    return false
				}
				
							
				if (document.getElementById('hdnCount').value+1 >1)
				{
				    if ((document.getElementById('txtFundedPercentage').value!="") && (document.getElementById('txtFundedPercentage').value!="0"))
				    {
				        if (!isNum(document.getElementById('txtFundedPercentage').value))
				        {
				            alert('Please enter a valid Funded Percentage')
				            document.getElementById('txtFundedPercentage').focus()
				            return false
				        }
				        if (ProjIncTempID!=0)
				        {
				            //If Edit
				            var PercentageTotal
				            var PercentageGiven
				            var PercentageSelected
				            var PercentageMinus
				            PercentageTotal=document.getElementById('hdnFundedPercentage').value
				            PercentageGiven=document.getElementById('txtFundedPercentage').value
				            PercentageSelected=document.getElementById('hdnSelectedFunPer').value
				            PercentageMinus = (PercentageTotal - PercentageSelected)
				            
				            Percentage = (parseInt(PercentageMinus) + parseInt(PercentageGiven))
				            
				            if (Percentage >100)
				            {
				                var msg=confirm('Funded Percentage exceeds 100 \n Do you want to continue')
				                if (msg==false)
				                {
				                    return false
				                }
				            }
				        }
				        else
				        {
				      
				            var PercentageTotal
				            var PercentageGiven
				            var Percentage
				            PercentageTotal=document.getElementById('hdnFundedPercentage').value
				            PercentageGiven=document.getElementById('txtFundedPercentage').value
				            Percentage=(parseInt(PercentageTotal) + parseInt(PercentageGiven))
				            if (Percentage >100)
				            {
				                var msg=confirm('Funded Percentage exceeds 100 \n Do you want to continue')
				                if (msg==false)
				                {
				                    return false
				                }
				            }
				        }
				    }
				}
                document.body.style.cursor='wait'
				document.frmStep3.txtButton.value="Add"
				document.frmStep3.submit()
			}
			
			function RemovePrimaryClicked(strfrom,Id,IncomeTemplate)
			{
			    var win
			    var String
			    var msg = 'You are about to delete a funder which is set as primary funder for the project \n Do you wish to delete it anyway?'
			    var result=confirm(msg)
			    if (result==true)
			    {
			        var Count=document.frmStep3.hdnCount.value
			        if (Count>2)
			        {
//			            win=window.open('AssignFunderForProject.aspx?For=Remove&ProjPropIncTemp=' + Id + '&FunderFrom=' + strfrom + '&From=' + IncomeTemplate ,'Details','toolbar=no,scrollbars=yes')
                        String=strfrom + '|' + Id + '|' + IncomeTemplate
                        document.frmStep3.hdnIDs.value=String
                        document.frmStep3.hdnAction.value="ShowAndRemovePrimary"
                        document.frmStep3.txtButton.value=""
				        document.frmStep3.submit()
			        }
			        if (Count==2)
			        {
			            var ProjPropID=document.frmStep3.hdnNextValue.value.split("|")[0]
			            var FunderID=document.frmStep3.hdnNextValue.value.split("|")[1]
			            String=FunderID + '|' + ProjPropID + '|' + strfrom + '|' + Id + '|' + IncomeTemplate
                        document.frmStep3.hdnIDs.value=String
                        document.frmStep3.hdnAction.value="RemovePrimary"
                        document.frmStep3.txtButton.value=""
				        document.frmStep3.submit()
//			            win=window.open('AssignFunderForProject.aspx?For=Remove&Count=2&ProjPropIncTemp=' + sendPrjPropID + '&FunderFrom=' + strfrom + '&From=' + IncomeTemplate + '&FunderID=' + FunderID ,'Details','toolbar=no,scrollbars=yes')
			        }
			        if (Count<2)
			        {
			            win=window.open('PromptBeforeDelete.aspx?For=Remove&FunderFrom=' + strfrom + '&ID=' + Id + '&From=' + IncomeTemplate,'Details','toolbar=no,scrollbars=yes')
			        }
			    }
			    else
			        return false
			}
			
		    function IncTempclicked()
			{
			document.frmStep3.txtButton.value ="Income Template Clicked"
			document.frmStep3.submit()  
			}
			function FundSourceClicked()
			{
			 document.frmStep3.txtButton.value ="Funding Source Clicked"
			 document.frmStep3.submit()
			}
			
			function SetDefaults()
			{
			    SetFunderVisibility()
				if (window.opener != null && !window.opener.closed)
				{
				//Required window size
				//PID:14493||Modified by Pravitha on 5 June 2006 ||comments:window height changed to remove scrollbar		
				windowWidth = 800;
				windowHeight= 600;	
	
				//If IE then, calculate centre screen by screen max size,
				if (screen)
				{
					x = (screen.availWidth - windowWidth)/2;
					y = (screen.availHeight - windowHeight)/2;
					//Give offset to roughly align within the main frame.
					x = x + 50;
					y = y + 50;
				}

				window.resizeTo(windowWidth, windowHeight)
				window.moveTo(x, y)
				
				self.focus();
				}
			}
			//Added by Preetha on 04 Aug 2008||PID:23271||handling  when form is closed using window close button('X') of the form		
			function HandleOnClose()
			{
			if(!isPostback) 
			{
			 if (event.clientY < 0 && event.clientX>(windowWidth-50))
                {
             CloseRefreshParent('ProjectProposalSummary.aspx');
         
                }
             }
			}	
			
		    //Modification 1 | By Lakshmanan | 10-Nov-08 | Modified Count by concatination '0' if count < 10 else get the original count
		    //This change is done because xhtmlConformance is removed from web.config file
			//Functions for selecting Primary Funder
		    function SetFunderVisibility()
		    {
			        var idRoot='dgFunders_ctl'
			        var idPrjIncTemp='_lblProjectIncTempID'
			        var idFunderName='_lblFunderName'
                    var idFunder='_lblFunderID'
                    var TempCount='02'

			        // The first grid row will have an index of 2.
			        var Count=2
			        var lblCtrl=null
			        // Get the first ID of project from label lblId
			        lblCtrl=document.getElementById(idRoot + TempCount + idFunderName)
			        while(lblCtrl!=null)
			        {
				        //Get the Archive label control
			            if (Count < 10)
			            {
                            document.getElementById(idRoot + '0' + Count + idPrjIncTemp).style.display="none"
			            }
			            if (Count > 9)
			            {
                            document.getElementById(idRoot + Count + idPrjIncTemp).style.display="none"
			            }
				        //Get the next ID from label lblID
			            if (Count < 10)
			            {
                            document.getElementById(idRoot + '0' + Count + idFunder).style.display="none"
			            }
			            if (Count > 9)
			            {
                            document.getElementById(idRoot + Count + idFunder).style.display="none"
			            }
			            Count++
			            if (Count < 10)
			            {
			                lblCtrl=document.getElementById(idRoot + '0' + Count + idFunderName)
			            }
			            if (Count > 9)
			            {
			                lblCtrl=document.getElementById(idRoot + Count + idFunderName)
			            }
			        }
		    }
		    //Modification 1 | By Lakshmanan | 10-Nov-08 | Modified Count by concatination '0' if count < 10 else get the original count
		    //This change is done because xhtmlConformance is removed from web.config file
		    function GetSelectedFunder(objCheckBox)
		    {
		        if(objCheckBox.checked==true)
		        {
			        var idRoot='dgFunders_ctl'
                    var idCheck='_chkFunder'
		            var objGrid= document.getElementById('dgFunders')
		            var length=objGrid.rows.length-1
		            var lenghtPlus=length=length+2
                    var iCount
                    for(iCount=2; iCount<lenghtPlus; iCount++)
                    {
			            if (iCount < 10)
			            {
                            var chk=document.getElementById(idRoot + '0' + iCount + idCheck)
			            }
			            if (iCount > 9)
			            {
                            var chk=document.getElementById(idRoot + iCount + idCheck)
			            }
                        if(chk.id!=objCheckBox.id)
                        {
                            chk.checked=false
                        }
                    }
                    document.frmStep3.hdnSelectedCheckbox.value=objCheckBox.id
                }
                else
                {
                    objCheckBox.checked=true
                }
                return false
		    }
		    function AddFunderClick()
		    {
		        var objGrid= document.getElementById('dgFunders')
		        var startindex
		        var endindex
		        var index
		        var ProjIncTempID
		        var FunderID
		        if (document.frmStep3.hdnSelectedCheckbox.value!="")
		        {
    		        var chkboxID=document.frmStep3.hdnSelectedCheckbox.value
			        //This gives the starting index of row
			        startindex = chkboxID.indexOf("ctl") + 3
			        //This gives the ending index of row
			        endindex = chkboxID.indexOf("_chk")
			        //This gives the index of the selected row
			        index=chkboxID.substring(startindex,endindex) - 1
			        var arr
			        var ProjIncTempID=getTextContent(objGrid.rows[index].cells[0])
//		            arr=objGrid.rows[index].cells[0].textContent
//		            ProjIncTempID=arr.split(" ")[1]
//		            FunderID=arr.split(" ")[2]
		            document.frmStep3.hdnPrjPropIncTempID.value=ProjIncTempID
		         // document.frmStep3.hdnFunderID.value=FunderID		
		            document.frmStep3.txtButton.value="Save Primary Funder"
		            document.frmStep3.submit()
		        }
		        else
		        {
		            alert('Please select an funder from the list')
		            return false
		        }
		    }
		    
		    function getTextContent(el)
            {
                if (el.textContent) return el.textContent;
                if (el.innerText) return (el.innerText);

                var cNode, cNodes = el.childNodes;
                var txt = '';
                for (var i=0, len=cNodes.length; i<len; ++i)
                {
                    cNode = cNodes[i];
                    if (1 == cNode.nodeType) 
                    {
                        txt += getTextContent(cNode);
                    }
                    if (3 == cNode.nodeType)
                    {
                        txt += cNode.data;
                    }
                }
                return txt;
                }
			function hideModal() 
			{
                $find('MsgPopup').hide();
            }

        </script>
	</head>
	<body class="Body" onload="javascript: SetDefaults()" onunload="javascript: HandleOnClose()">
		<!--Modified by Pravitha on 12 oct 2006||Comments:tabindex modified-->
		<form id="frmStep3" runat="server" method="post" onsubmit ="isPostback =true;">
		<div>
		<aspAjax:ScriptManager ID="scProjectProposalStep3" runat="server" />
		<table id="Table1" cellspacing="1" cellpadding="1" width="100%" border="0">
			<tr>
				<td align="center"><asp:label id="lblHeader" runat="server" Width="560px" CssClass="Header"></asp:label></td>
			</tr>
			<tr>
				<td align="center"><asp:label id="lblErrorMessage" runat="server" Width="560px" CssClass="ErrorLabel"></asp:label></td>
			</tr>
			<tr>
				<td align="center">
					<table id="Table2" cellspacing="1" cellpadding="1" border="0">
						<tr>
							<td style="WIDTH: 172px; HEIGHT: 15px" align="left"><asp:label id="lblFundSource" runat="server" CssClass="LabelCaption" Height="13px" >Funding Source</asp:label></td>
							<td style="HEIGHT: 15px" align="left"><asp:dropdownlist id="ddFundSource" tabindex="1" runat="server" Width="451" Height="22px" AutoPostBack="True"></asp:dropdownlist></td>
						</tr>
						<tr valign="middle">
							<td style="WIDTH: 172px" align="left" valign ="middle"><asp:label id="lblsearchfunder" runat="server" CssClass="LabelCaption">Search For Funder</asp:label></td>
							<td align="left">
							<asp:textbox id="txtFilterFunder" tabindex="2" runat="server" ></asp:textbox>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
							<img   src="" id="imgFilter1" onmouseover="javascript: toggleImage('imgFilter1',frmStep3.txtImagePath.value+'Filter_mover.jpg')"
									style="vertical-align:text-bottom;  WIDTH: 72px; CURSOR: hand" onclick=" Javascript:funderFilterClicked();" tabindex="3"
									onmouseout="javascript: toggleImage('imgFilter1',frmStep3.txtImagePath.value+'Filter.jpg')"
									alt="" name="imgFilter1" runat="server"/>
							</td>
						</tr>
						<tr>
							<td style="WIDTH: 172px; HEIGHT: 17px" align="left"><asp:label id="lblFunder" runat="server" CssClass="LabelCaption" Height="13px" >Funder*</asp:label></td>
							<td style="HEIGHT: 15px" align="left"><asp:dropdownlist id="ddFunder" tabindex="4" runat="server" Width="451px" Height="22px" AutoPostBack="True"></asp:dropdownlist></td>
						</tr>
						<tr>
							<td style="WIDTH: 172px; HEIGHT: 17px" align="left"><asp:label id="lblFundingScheme" runat="server" CssClass="LabelCaption" Height="13px" >Funding Scheme</asp:label></td>
							<td style="HEIGHT: 15px" align="left"><asp:dropdownlist id="ddFundingScheme" tabindex="5" runat="server" Width="451px" Height="22px"></asp:dropdownlist></td>
						</tr>
						<tr>
							<td style="WIDTH: 172px; HEIGHT: 15px" align="left"><asp:label id="lblSearchInctemp" runat="server" CssClass="LabelCaption" >Search For Income Template</asp:label></td>
							<td style="HEIGHT: 15px" align="left"><asp:textbox id="txtFilterIncTemp" tabindex="6" runat="server" ></asp:textbox>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<img id="Imgfilter2" src="" onmouseover="javascript: toggleImage('imgFilter2',frmStep3.txtImagePath.value+'Filter_mover.jpg')"
									style=" vertical-align:text-bottom; WIDTH: 72px; CURSOR: hand; HEIGHT: 29px" onclick=" Javascript:incFilterClicked();"
									 onmouseout="javascript: toggleImage('imgFilter2',frmStep3.txtImagePath.value+'Filter.jpg')"
									height="29" alt="" width="72"  name="imgFilter2" runat="server"/>
							</td>
						</tr>
						<tr>
							<td style="WIDTH: 172px; HEIGHT: 25px" align="left"><asp:label id="lblIncomeTemplate" runat="server" Width="140px" CssClass="LabelCaption" Height="13px"></asp:label></td>
							<td style="HEIGHT: 15px" align="left"><asp:dropdownlist id="ddIncomeTemplate" tabindex="8" runat="server" Width="451px" Height="22px" AutoPostBack="True"></asp:dropdownlist></td>
						</tr>
						<asp:Panel id="pnlEuactivity" runat ="server">
						<tr>
							<td style="WIDTH: 172px; HEIGHT: 25px" align="left"><asp:label id="lblEuActivity" runat="server" Width="120px" CssClass="LabelCaption" Height="13px">EU Activity*</asp:label></td>
							<td style="HEIGHT: 15px" align="left"><asp:dropdownlist id="ddEuActivity" tabindex="9" runat="server" Width="451px" Height="22px" ></asp:dropdownlist>
							</td>
						</tr>
						</asp:Panel> 
						<asp:Panel ID="pnlMarkup" runat="server">
						<tr>
							<td style="WIDTH: 172px; HEIGHT: 25px" align="left"><asp:label id="lblMarkup" runat="server" Width="120px" CssClass="LabelCaption"  Height="13px">Markup %*</asp:label></td>
							<td style="HEIGHT: 15px" align="left"><asp:TextBox ID ="txtAmount" runat="server"  /></td>
						</tr>
						</asp:Panel>
						
											
						<asp:Panel  runat="server" id="pnlisPrimaryFunder">
						<tr>							    
							<td style="WIDTH: 172px; HEIGHT: 25px" align="left"><asp:label ID="lblPrimaryFunder" runat="server" Width="160px" CssClass="LabelCaption" Height="28px" >Set as Primary Funder</asp:label></td>
							<td style="HEIGHT: 15px" valign="top"   align="left"><asp:CheckBox ID="chkPrimaryFunder" runat="server" /></td>
						</tr>
						</asp:Panel>
											
						<tr>
						    <td align="left" style="width: 172px;">
						        <asp:label ID="lblFundedPercentage" runat="server"  CssClass="LabelCaption">Funded Percentage</asp:label>
						    </td>
						    <td align="left">
						      <asp:TextBox ID="txtFundedPercentage" runat="server" CssClass="TextBox" Text="0"/>
						       &nbsp;
						      <asp:Label ID="lblFundedPercentageMsg" Visible ="false" runat ="server" Text="Leave this at 100%, unless intending to use Multiple Funders"
						      CssClass ="LabelSmallSize"/>
						    </td>
						    </tr>
							
                       					    
					</table>
				</td>
			</tr>
			<tr>
				<td align="center">&nbsp; <img id="Add1" onmouseover="javascript: toggleImage('Add1',frmStep3.txtImagePath.value+'Add_new_mover.jpg')"
						style="WIDTH: 40px; CURSOR: hand; HEIGHT: 29px" onclick="addClick();" onmouseout="javascript: toggleImage('Add1',frmStep3.txtImagePath.value+'Add_new.jpg')"
						height="31" alt="" src="" width="25" runat="server"/> <img id="Cancel" onmouseover="javascript: toggleImage('Cancel',frmStep3.txtImagePath.value+'Close_mover.jpg')"
						style="WIDTH: 71px; CURSOR: hand; HEIGHT: 29px" onclick="javascript: CloseRefreshParent('ProjectProposalSummary.aspx');" onmouseout="javascript: toggleImage('Cancel',frmStep3.txtImagePath.value+'Close.jpg')"
						height="31" alt="" src="" width="25" runat="server"/>
				</td>
			</tr>
			<tr>
				<td style="HEIGHT: 77px" align="center"><asp:panel id="Panel2" runat="server">
						<table id="Table4" cellspacing="5" cellpadding="1" align="center" border="0">
							<tr>
							<%--Modified by Phani on 16-Dec-2008||PID:24384||Comments:Added OnClientClick property to Image buttons inorder to work with script manager--%>
								<td align="center">
									<asp:imagebutton id="ImgStaffcost" tabindex="10" runat="server" ImageAlign="AbsMiddle" ImageUrl="" OnClientClick="this.disabled=true;__doPostBack(this.name,'');"></asp:imagebutton></td>
								<td align="center">
									<asp:imagebutton id="ImgEquipmentcost" tabindex="11" runat="server" ImageAlign="AbsMiddle" ImageUrl="" OnClientClick="this.disabled=true;__doPostBack(this.name,'');"></asp:imagebutton></td>
								<td align="center">
									<asp:imagebutton id="ImgNonstaffcost" tabindex="12" runat="server" ImageUrl="" OnClientClick="this.disabled=true;__doPostBack(this.name,'');"></asp:imagebutton></td>
								<td align="center">
									<asp:imagebutton id="ImgFECcost" tabindex="13" runat="server" ImageAlign="AbsMiddle" ImageUrl="" OnClientClick="this.disabled=true;__doPostBack(this.name,'');"></asp:imagebutton></td>
								<td align="center">
									<asp:imagebutton id="ImgIncome" tabindex="14" runat="server" ImageAlign="AbsMiddle" ImageUrl="" OnClientClick="this.disabled=true;__doPostBack(this.name,'');"></asp:imagebutton></td>
								<td align="center">
									<asp:imagebutton id="ImgInternalsource" tabindex="15" runat="server" ImageAlign="AbsMiddle" ImageUrl="" OnClientClick="this.disabled=true;__doPostBack(this.name,'');"></asp:imagebutton></td>
							</tr>
							<tr>
								<td align="center">
									<asp:label id="lblStaffcost" runat="server" CssClass="LabelCaption" Width="100px">Staff cost</asp:label></td>
								<td align="center">
									<asp:label id="lblEquipmentcost" runat="server" CssClass="LabelCaption" Width="175px">Facilities & Equipment cost</asp:label></td>
								<td align="center">
									<asp:label id="lblNonstaffcost" runat="server" CssClass="LabelCaption" Width="100px">Nonstaff cost</asp:label></td>
								<td align="center">
									<asp:label id="lblFECcost" runat="server" CssClass="LabelCaption" Width="150px">FEC Related Cost</asp:label></td>
								<td align="center">
									<asp:label id="lblIncome" runat="server" CssClass="LabelCaption" Width="100px">Income</asp:label></td>
								<td align="center">
									<asp:label id="lblInternalsource" runat="server" CssClass="LabelCaption" Width="100px">Internal Sources</asp:label></td>
							</tr>
						</table>
					</asp:panel>
					<asp:panel id="Panel1" runat="server">
						<table id="Table3" style="WIDTH: 468px; HEIGHT: 87px" cellspacing="2" cellpadding="1" align="center"
							border="0">
							<tr>
								<td style="WIDTH: 110px" align="center">
									<asp:imagebutton id="ImgClinicalTrial" runat="server" ImageAlign="AbsMiddle"  OnClientClick="this.disabled=true;__doPostBack(this.name,'');"></asp:imagebutton></td>
								<td style="WIDTH: 110px" align="center">
									<asp:imagebutton id="ImgDetails" tabindex="10" runat="server" ImageAlign="AbsMiddle" OnClientClick="this.disabled=true;__doPostBack(this.name,'');"></asp:imagebutton></td>
								<td style="WIDTH: 110px" align="center">
									<asp:imagebutton id="ImgDept" tabindex="11" runat="server" ImageAlign="AbsMiddle" OnClientClick="this.disabled=true;__doPostBack(this.name,'');"></asp:imagebutton></td>
								<td style="WIDTH: 110px" align="center">
									<asp:imagebutton id="ImgCoinvestigators" tabindex="12" runat="server" OnClientClick="this.disabled=true;__doPostBack(this.name,'');"></asp:imagebutton></td>
								<td style="WIDTH: 110px" align="center">
									<asp:imagebutton id="ImgInstitution" tabindex="13" runat="server" ImageAlign="AbsMiddle" OnClientClick="this.disabled=true;__doPostBack(this.name,'');"></asp:imagebutton></td>
							</tr>
							<tr style="HEIGHT: 50px"  >
								<td style="WIDTH: 110px" align="center">
									<asp:label id="lblClinicalTrial" runat="server" CssClass="LabelCaption" Width="108">Show Clinical Trial</asp:label></td>
								<td style="WIDTH: 110px" align="center">
									<asp:label id="lblDetails" runat="server" CssClass="LabelCaption" Width="108px">Details</asp:label></td>
								<td style="WIDTH: 110px" align="center">
									<asp:label id="lblDepartments" runat="server" CssClass="LabelCaption" Width="108px">Collaborating Departments</asp:label></td>
								<td style="WIDTH: 110px" align="center">
									<asp:label id="lblCoinvestigators" runat="server" CssClass="LabelCaption" Width="108px">Coinvestigators</asp:label></td>
								<td style="WIDTH: 110px" align="center">
									<asp:label id="LblInstitution" runat="server" CssClass="LabelCaption" Width="108px">Other Institutions</asp:label></td>
							</tr>
						</table>
					</asp:panel>
				</td>
			</tr>
			<tr>
				<td align="center" style="height: 42px"><asp:table id="tblStaff" runat="server" Width="630px" Height="18px" cellpadding="0" cellspacing="0"
						GridLines="Both"></asp:table>
				</td>
			</tr>
		</table>
			    <input id="txtImagePath" style="Z-INDEX: 102; LEFT: 752px; POSITION: absolute; TOP: 688px"
				type="hidden" name="txtImagePath" runat="server"/> <input id="txtFunFilterClicked" style="Z-INDEX: 103; LEFT: 576px; POSITION: absolute; TOP: 760px"
				type="hidden" name="txtFunFilterClicked"/> <input id="txtIncFilterClicked" style="Z-INDEX: 104; LEFT: 400px; POSITION: absolute; TOP: 760px"
				type="hidden" name="txtIncFilterClicked"/><input id="hdnUserFunder" style="Z-INDEX: 105; LEFT: 576px; POSITION: absolute; TOP: 688px"
				type="hidden" name="hdnUserFunder" runat="server"/><input id="hdnUserFundingSource" style="Z-INDEX: 106; LEFT: 400px; POSITION: absolute; TOP: 696px"
				type="hidden" name="hdnUserFundingSource" runat="server"/><input id="hdnUserIncome" style="Z-INDEX: 107; LEFT: 576px; POSITION: absolute; TOP: 728px"
				type="hidden" name="hdnUserIncome" runat="server"/> <input id="hdnfrom" style="Z-INDEX: 108; LEFT: 592px; POSITION: absolute; TOP: 1048px" type="hidden"
				name="Hidden1" runat="server"/> <input id="txtEuActivity" style="Z-INDEX: 109; LEFT: 408px; POSITION: absolute; TOP: 1048px"
				type="hidden" name="txtEuActivity" runat="server"/><input id="hdnPrimaryFunder" style="Z-INDEX: 109; LEFT: 592px; POSITION: absolute; TOP: 1008px"
				type="hidden" name="hdnPrimaryFunder" runat="server"/><input id="hdnSetValues" style="Z-INDEX: 109; LEFT: 400px; POSITION: absolute; TOP: 1008px"
				type="hidden" name="hdnSetValues" runat="server"/><input id="hdnCount" style="Z-INDEX: 109; LEFT: 600px; POSITION: absolute; TOP: 960px"
				type="hidden" name="hdnCount" runat="server"/><input id="hdnRemovePrimary" style="Z-INDEX: 109; LEFT: 408px; POSITION: absolute; TOP: 968px"
				type="hidden" name="hdnRemovePrimary" runat="server"/><input id="hdnPrimaryValue" style="Z-INDEX: 109; LEFT: 584px; POSITION: absolute; TOP: 928px"
				type="hidden" name="hdnPrimaryValue" runat="server"/><input id="hdnNextValue" style="Z-INDEX: 109; LEFT: 400px; POSITION: absolute; TOP: 928px"
				type="hidden" name="hdnNextValue" runat="server"/><input id="hdnAmount" style="Z-INDEX: 109; LEFT: 592px; POSITION: absolute; TOP: 888px"
				type="hidden" name="hdnAmount" runat="server"/><input id="hdnFundedPercentage" style="Z-INDEX: 109; LEFT: 592px; POSITION: absolute; TOP: 888px"
				type="hidden" name="hdnFundedPercentage" runat="server"/>

            <asp:LinkButton ID="lbPrimaryFunder" runat="server" CausesValidation="false" />
            <asp:Panel ID="pnlPrimaryFunder" CssClass="modalPopup" runat="server" style="display:none;" Height="310px" Width="350px" >
    	    <table>
    	        <tr>
    	            <td>
    	                <asp:Label ID="lblPopupHeader" runat="server" CssClass="LabelCaption" Text="You have deleted/edited a funder which is set as primary funder, Please select a Primary funder from the list"/>
    	            </td>
    	        </tr>
    	        <tr align="center">
    	            <td align="center">
						<img id="Add2" onmouseover="javascript: toggleImage('Add2',frmStep3.txtImagePath.value+'Add_new_mover.jpg')"
							style="WIDTH: 40px; CURSOR: hand; HEIGHT: 29px" onclick="javascript: AddFunderClick()" onmouseout="javascript: toggleImage('Add2',frmStep3.txtImagePath.value+'Add_new.jpg')"
							height="31" alt="" src="" width="25" runat="server"/>
						<img id="Cancel2" onmouseover="javascript: toggleImage('Cancel2',frmStep3.txtImagePath.value+'Close_mover.jpg')"
							style="WIDTH: 71px; CURSOR: hand; HEIGHT: 29px" onclick="javascript: hideModal()" onmouseout="javascript: toggleImage('Cancel2',frmStep3.txtImagePath.value+'Close.jpg')"
							height="31" alt="" src="" width="25" runat="server"/>
					</td>
    	        </tr>
    	        <tr align="center">
    	            <td align="center">
    	                <asp:datagrid id="dgFunders" AutoGenerateColumns="False" Runat="server">
				            <ItemStyle CssClass="TableData"></ItemStyle>
					        <HeaderStyle Height="10px" CssClass="TableHeader"></HeaderStyle>
					        <Columns>
						        <asp:TemplateColumn>
							        <ItemTemplate>
                                        <input id="chkFunder" name="chkFunder" runat="server" title="Select this Funder for selected project" onclick="javascript:GetSelectedFunder(this);" type="checkbox" value="1"/>
                                        <asp:Label ID="lblProjectIncTempID" runat="server" Text='<%# Trim(DataBinder.Eval(Container.DataItem, "PrjIncTempID")) %>' />
                                        <asp:Label ID="lblFunderID" runat="server" Text='<%# Trim(DataBinder.Eval(Container.DataItem, "FunderID")) %>' />
							        </ItemTemplate>
						        </asp:TemplateColumn>
						        <asp:TemplateColumn HeaderText="Funder">
							        <ItemTemplate>
                                        <asp:Label ID="lblFunderName" runat="server" Text='<%# Trim(DataBinder.Eval(Container.DataItem, "Funder")) %>'>
                                        </asp:Label>
							        </ItemTemplate>
						        </asp:TemplateColumn>
						        <asp:TemplateColumn HeaderText="Funding Source">
							        <ItemTemplate>
                                        <asp:Label ID="lblFundingourceName" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "FundingSource") %>'>
                                        </asp:Label>
							        </ItemTemplate>
						        </asp:TemplateColumn>
					        </Columns>
				        </asp:datagrid>
    	            </td>
    	        </tr>
    	    </table>
            </asp:Panel>
		        <input style="WIDTH: 79px; HEIGHT: 21px" type="hidden" name="txtButton" id="txtButton"/>&nbsp;
            <aspToolkit:ModalPopupExtender ID="modalPrimaryFunder" X="180" Y="200" BehaviorID="MsgPopup" BackgroundCssClass="modalBackground" runat="server" PopupControlID="pnlPrimaryFunder" TargetControlID="lbPrimaryFunder" DropShadow="true" CancelControlID="Cancel2"/>    
		</div>
    	    <input id="txtSelectedFunder" style="Z-INDEX: 103; LEFT: 752px; WIDTH: 75px; POSITION: absolute; TOP: 720px; HEIGHT: 22px" type="hidden" name="txtImagePath" runat="server"/>
    	    <input id="hdnPrjPropIncTempID" style="Z-INDEX: 109; LEFT: 400px; POSITION: absolute; TOP: 888px" type="hidden" name="hdnPrjPropIncTempID" runat="server"/>
    	    <input id="hdnFunderID" style="Z-INDEX: 109; LEFT: 592px; POSITION: absolute; TOP: 840px" type="hidden" name="hdnFunderID" runat="server"/>
    	    <input id="hdnSelectedCheckbox" style="Z-INDEX: 109; LEFT: 400px; POSITION: absolute; TOP: 840px" type="hidden" name="hdnSelectedCheckbox" runat="server"/>
    	    <input id="hdnAction" style="Z-INDEX: 109; LEFT: 584px; POSITION: absolute; TOP: 800px" type="hidden" name="hdnAction" runat="server"/>
    	    <input id="hdnIDs" style="Z-INDEX: 109; LEFT: 400px; POSITION: absolute; TOP: 800px" type="hidden" name="hdnIDs" runat="server"/>
    	    <input id="hdnParameters" style="Z-INDEX: 109; LEFT: 400px; POSITION: absolute; TOP: 728px" type="hidden" name="hdnParameters" runat="server"/>
    	    <input id="hdnFunPerVisible" style="Z-INDEX: 109; LEFT: 400px; POSITION: absolute; TOP: 728px" type="hidden" name="hdnFunPerVisible" runat="server"/>
    	    <input id="hdnSelectedFunPer" style="Z-INDEX: 109; LEFT: 400px; POSITION: absolute; TOP: 728px" type="hidden" name="hdnSelectedFunPer" runat="server"/>
		</form>
	</body>

</html>
