<%@ Reference Page="~/Logout.aspx" %>
<%@ page language="vb" autoeventwireup="false" inherits="PFACT.ProjectProposalSummary, pFACT_Deploy" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>ProjectProposalSummary</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR"/>
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE"/>
		<meta content="JavaScript" name="vs_defaultClientScript"/>
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema"/>
		<%=styleSheetVar%>
		<script language="Javascript" src="ClientScripts.js"></script>
		<script language="javascript">
		
		
			var win
			//PID:14018 created by sumathy
			function selectAttachmentImage()
			{
			if(document.frmProjectSummary.txtattached.value=="No")   
				{
				document.frmProjectSummary.ImgAttachments.src =document.frmProjectSummary.txtImagePath.value + "Attachments_dsbl.jpg" 
				}
				else
				{
				document.frmProjectSummary.ImgAttachments.src =document.frmProjectSummary.txtImagePath.value + "Attachments.jpg" 
				}			
			}
			//Pid:14131 on mouse over the attachment button changes			
				function selectAttachmentImageOnMouseOver()
				{
				
					if(document.frmProjectSummary.txtattached.value=="No")   
					{
						document.frmProjectSummary.ImgAttachments.src =document.frmProjectSummary.txtImagePath.value + "Attachments_m_dsbl.jpg" 
					}
					else
					{
						document.frmProjectSummary.ImgAttachments.src =document.frmProjectSummary.txtImagePath.value + "Attachments_m.jpg" 
					}			
				}
			//PId:13684
			function SelectNotesImage()
			{
			if(document.frmProjectSummary.txtNotesAttached.value=="No")   
				{
				document.frmProjectSummary.notes.src =document.frmProjectSummary.txtImagePath.value + "notes_dsbl.jpg" 
				}
				else
				{
				document.frmProjectSummary.notes.src =document.frmProjectSummary.txtImagePath.value + "Notes.jpg" 
				}			
			}
			function selectNotesImageOnMouseOver()
			{
			if(document.frmProjectSummary.txtNotesAttached.value=="No")   
				{
				document.frmProjectSummary.notes.src =document.frmProjectSummary.txtImagePath.value + "notes_mover_dsbl.jpg" 
				}
				else
				{
				document.frmProjectSummary.notes.src=document.frmProjectSummary.txtImagePath.value + "notes_mover.jpg" 
				}			
			}
			
			function ClosePopup()
			
			{
			    if (win && win.open && !win.closed) win.close();
			}
			//PID:14493||Modofied by Pravitha on 29 May 2006||maximize option enabled
			function popupWindow(url)
			{
				win=window.open(url,'Details','toolbar=no,scrollbars=yes,status=yes,resizable=yes')
			}
			
			/*function openReportWindow()
			{
				win=window.open('Reports.aspx?From=ProjectProposalSummary&strProjIdList=' + document.frmProjectSummary.txtProjectID.value,'Report','toolbar=no,scrollbars=yes,status=yes')
			}*/
			function openReportWindow()
			{
				win=window.open('ProjectFinancialReports.aspx?From=ProjectProposalSummary&strProjIdList=' + document.frmProjectSummary.txtProjectID.value,'Report','toolbar=no,scrollbars=yes,status=yes')
			}
			//PID:14493||Modofied by Pravitha on 29 May 2006||maximize option enabled
			function OpenSubmissionReports()
			{
			win=window.open('Reports.aspx?IsSubmissionReport=Yes&From=ProjectProposalSummary&strProjIdList=' + document.frmProjectSummary.txtProjectID.value,'Report','toolbar=no,scrollbars=yes,status=yes,resizable=yes')
			}
			
			//PID:13700 || Modified by Sumathy on 17-NOV-2005 ||Comments: When Replicate button is pressed a confirmation msg is given. 
			function openReplicateProjectWindow()
			{
			var name=confirm("Do you want to replicate this project?")
				if (name==true)
				{
				document.body.style.cursor='wait'
				win=window.open('ProjectProposalSummary.aspx?For=Template','main','toolbar=no')
				}
				else
				{
				return false
				}
			}
			
			function okClicked(from)
			{
				if (document.frmProjectSummary.ddStatus.value=="0")
				{
					alert("Please select a status from the list")	
					document.frmProjectSummary.ddStatus.focus()
					return false	
				}
    		    document.body.style.cursor='wait'
				document.frmProjectSummary.txtButton.value="Edit"
				document.frmProjectSummary.submit()
			}
			
			function bandingClicked()
			{
			    document.frmProjectSummary.txtButton.value="BAND"
				document.frmProjectSummary.submit()				
			}
			
			function VATChecked()
			{
				document.frmProjectSummary.chkVAT.checked=!document.frmProjectSummary.chkVAT.checked
			}
			
			function reCalculate()
			{
			    document.body.style.cursor = 'wait';
				document.frmProjectSummary.txtButton.value="RECALCULATE"
				document.frmProjectSummary.submit()
			}
			
			function infClicked(btn)
			{
				if (btn=='WITH')
					document.frmProjectSummary.txtButton.value="INF"
				else
					document.frmProjectSummary.txtButton.value="NOINF"
					
				document.frmProjectSummary.submit()			
			
			}
			function UserPermissionClicked()
			{
				win=window.open('AddUserPermissionToProject.aspx?ProjectName=' + document.frmProjectSummary.txtName.value,'UserPermission','toolbar=no,scrollbars=yes,status=yes,resizable=yes')
			}
			function ProjectListClicked(url)
			{
				win=window.open(url,'Details','toolbar=no,scrollbars=yes,status=yes,resizable=yes')
			}
			function LogoutClick()
			{
			 window.parent.close()
			} 
			 function ConsultancyIncome(url)
            {
				if(document.frmProjectSummary.hdnFunder.value=="")
				{
					alert("Add a Funder to this project")
					return false
				}
				else
				{
					popupWindow(url)
				}
			}
			
			//PID:21516.Changes For Manchester.Called In page Onload Event.
			function SetCostForManchesterProjects()
            {
	        if ( document.frmProjectSummary.hdnManc.value=="Manchester")
	           {
	           document.frmProjectSummary.submit()
	           }
            }
            
            function OnPostAwardsClick()
            {
                win=window.open('ProjectPostAward.aspx','PostAwards','toolbar=no,scrollbars=yes,status=yes,resizable=yes')
            }
		</script>
	</HEAD>
	<body class="Body" onunload="ClosePopup()" onload="javascript: SetCostForManchesterProjects();">
		<form id="frmProjectSummary" method="post" runat="server">
			<TABLE id="Table2" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr>
					<%--<td style="WIDTH: 38px" align="center"></td>--%>
					<td align="center"><asp:label id="lblHeader" runat="server" CssClass="Header" Height="9px" Width="616px"></asp:label></td>
				</tr>
				<tr>
					<%--<td style="WIDTH: 38px; HEIGHT: 6px" align="center"></td>--%>
					<td style="HEIGHT: 6px" align="center"><asp:label id="lblErrorMessage" runat="server" CssClass="ErrorLabel" Height="9px" Width="616px"></asp:label></td>
				</tr>
				<tr>
					<td style="WIDTH: 38px; HEIGHT: 6px" align="center"></td>
					<%--<td align="center" colspan="2"></td>--%>
				</tr>
				<tr>
				    <td>
					    <asp:panel id="Panelsummary" Runat="server">
					        <table>
					            <tr>
						            <td style="WIDTH: 38px; HEIGHT: 216px" valign="top"></td>
						            <td valign="top" align="center" rowSpan="1">
							            <TABLE id="Table1" style="HEIGHT: 423px; width: 71%;" cellSpacing="0" cellPadding="0" border="0">
								            <tr>
									            <td></td>
									            <td style="WIDTH: 297px" colspan="2" rowSpan="1">
										            <asp:label id="lblCreated" runat="server" CssClass="labelcaption"></asp:label></td>
									            <td style="WIDTH: 376px" align="right" colspan="2">
										            <asp:label id="lblModified" runat="server" CssClass="LabelCaption" Height="9px" Width="270px"></asp:label></td>
									            <td></td>
								            </tr>
								            <tr>
									            <td style="HEIGHT: 25px"></td>
									            <td style="WIDTH: 118px; HEIGHT: 25px">
										            <asp:label id="lblProjName" runat="server" CssClass="LabelCaption" Height="9px" Width="88px">Project Name</asp:label></td>
									            <td style="WIDTH: 581px; HEIGHT: 25px" align="left" colspan="3"><input class="Text" id="txtName" style="WIDTH: 504px; HEIGHT: 20px" tabindex="1" readOnly
											            type="text" maxLength="100" name="txtName" runat="server"></td>
									            <td style="WIDTH: 247px; HEIGHT: 25px" align="left">
                                                    &nbsp;<asp:label id="lblProjectID" runat="server" CssClass="LabelCaption" Height="9px" Width="120px">Project ID</asp:label>
										        </td>
								            </tr>
								            <tr>
									            <td style="HEIGHT: 1px"></td>
									            <td style="WIDTH: 118px; HEIGHT: 1px">
										            <asp:label id="lblCode" runat="server" CssClass="LabelCaption" Height="9px" Width="6px">Code</asp:label></td>
									            <td style="WIDTH: 195px; HEIGHT: 1px"><input class="Text" id="txtCode" style="WIDTH: 176px; HEIGHT: 20px" tabindex="2" readOnly
											            type="text" maxLength="100" size="28" name="txtCode" runat="server"></td>
									            <td style="WIDTH: 118px; HEIGHT: 1px" align="left">
										            <asp:label id="lblDept" runat="server" CssClass="LabelCaption" Width="104px">Department</asp:label></td>
									            <td style="WIDTH: 205px; HEIGHT: 1px"><input class="Text" id="txtDept" style="WIDTH: 222px; HEIGHT: 20px" tabindex="3" readOnly
											            type="text" maxLength="100" name="txtDept" runat="server"></td>
									            <td style="WIDTH: 193px; HEIGHT: 1px"><input id="chkVAT" style="HEIGHT: 20px" onclick="javascript: VATChecked()" type="checkbox"
											            size="20" name="chkVAT" runat="server">
										            <asp:label id="lblVATChecked" runat="server" CssClass="LabelCaption" Height="14px" Width="116px">VAT Checked</asp:label></td>
								            </tr>
								            <tr>
									            <td style="HEIGHT: 26px"></td>
									            <td style="WIDTH: 118px; HEIGHT: 26px">
										            <asp:label id="lblAct" runat="server" CssClass="LabelCaption" Height="9px" Width="6px">Activity</asp:label></td>
									            <td style="WIDTH: 195px; HEIGHT: 26px"><input class="Text" id="txtActivity" style="WIDTH: 176px; HEIGHT: 20px" tabindex="4" readOnly
											            type="text" maxLength="100" size="22" name="txtActivity" runat="server"></td>
									            <td style="WIDTH: 118px; HEIGHT: 26px" align="left">
										            <asp:label id="lblSubAct" runat="server" CssClass="LabelCaption" Width="104px">Sub Activity</asp:label></td>
									            <td style="WIDTH: 205px; HEIGHT: 26px"><input class="Text" id="txtSubActivity" style="WIDTH: 222px; HEIGHT: 20px" tabindex="5"
											            readOnly type="text" maxLength="100" name="txtSubActivity" runat="server"></td>
									            <td style="WIDTH: 193px; HEIGHT: 26px"><img id="imgEdit" onmouseover="javascript: toggleImage('imgEdit',frmProjectSummary.txtImagePath.value+'Edit_mover.jpg')"
											            title="" style="WIDTH: 158px; CURSOR: hand" onclick="popupWindow('NewProjectProposalStep1.aspx?From=Summary');" onmouseout="javascript: toggleImage('imgEdit',frmProjectSummary.txtImagePath.value+'Edit.jpg')"
											            height="25" width="53" runat="server"></td>
								            </tr>
								            <tr>
									            <td style="HEIGHT: 23px"></td>
									            <td style="WIDTH: 118px; HEIGHT: 23px">
										            <asp:label id="lblStartDate" runat="server" CssClass="LabelCaption" Height="9px" Width="64px">Start Date</asp:label></td>
									            <td style="WIDTH: 195px; HEIGHT: 23px;" align="left"><input class="Text" id="txtStartDate" style="WIDTH: 176px; HEIGHT: 20px" tabindex="6" readOnly
											            type="text" maxLength="100" name="txtStartDate" runat="server"></td>
									            <td style="WIDTH: 118px; HEIGHT: 23px" align="left">
										            <asp:label id="lblEndDate" runat="server" CssClass="LabelCaption" Width="104px">End Date</asp:label></td>
									            <td style="WIDTH: 205px; HEIGHT: 23px"><input class="Text" id="txtEndDate" style="WIDTH: 222px; HEIGHT: 20px" tabindex="7" readOnly
											            type="text" maxLength="100" name="txtEndDate" runat="server"></td>
									            <td style="WIDTH: 193px; HEIGHT: 23px"><img id="imgLongTitle" onmouseover="javascript: toggleImage('imgLongTitle',frmProjectSummary.txtImagePath.value+'long_title_mover.jpg')"
											            title="" style="WIDTH: 158px; CURSOR: hand" onclick="javascript: popupWindow('EditProjLongTitle.aspx') " onmouseout="javascript: toggleImage('imgLongTitle',frmProjectSummary.txtImagePath.value+'long_title.jpg')"
											            height="25" width="114" align="top" runat="server"></td>
								            </tr>
								            <tr>
									            <td style="HEIGHT: 25px"></td>
									            <td style="WIDTH: 118px; HEIGHT: 25px">
										            <asp:label id="lblLeader" runat="server" CssClass="LabelCaption" Height="9px" Width="100px">PI</asp:label></td>
									            <td style="WIDTH: 581px; HEIGHT: 25px" align="left" colspan="3"><input class="Text" id="txtLeader" style="WIDTH: 504px; HEIGHT: 20px" tabindex="8" readOnly
											            type="text" maxLength="100" name="txtLeader" runat="server"></td>
									            <td style="WIDTH: 528px; HEIGHT: 25px"><img id="notes" onmouseover="selectNotesImageOnMouseOver()" title="" style="WIDTH: 158px; CURSOR: hand"
											            onclick="javascript: popupWindow('ProjectProposalNotes.aspx') " onmouseout="SelectNotesImage()" height="25"
											            width="62" runat="server"></td>
								            </tr>
								            <tr>
									            <td style="HEIGHT: 17px"></td>
									            <td style="WIDTH: 118px; HEIGHT: 17px">
										            <asp:label id="lblContact" runat="server" CssClass="LabelCaption" Height="9px" Width="100px">Contact Person</asp:label></td>
									            <td style="WIDTH: 195px; HEIGHT: 17px"><input class="Text" id="txtContact" style="WIDTH: 176px; HEIGHT: 20px" tabindex="9" readOnly
											            type="text" maxLength="100" size="22" name="txtContact" runat="server"></td>
									            <td style="WIDTH: 118px; HEIGHT: 17px">
										            <asp:label id="lblPhone" runat="server" CssClass="LabelCaption" Width="88px">Telephone</asp:label></td>
									            <td style="WIDTH: 205px; HEIGHT: 17px"><input class="Text" id="txtPhone" style="WIDTH: 222px; HEIGHT: 20px" tabindex="10" readOnly
											            type="text" maxLength="100" name="txtPhone" runat="server"></td>
									            <td style="WIDTH: 528px; HEIGHT: 17px"><img id="change_start_date" onmouseover="javascript: toggleImage('change_start_date',frmProjectSummary.txtImagePath.value+'Change_Start_Date_mover.jpg')"
											            title="" style="WIDTH: 158px; CURSOR: hand" onclick="javascript: popupWindow('ProjectSlipping.aspx?From=ChangeStartDate') "
											            onmouseout="javascript: toggleImage('change_start_date',frmProjectSummary.txtImagePath.value+'Change_Start_Date.jpg')"
											            height="25" width="62" runat="server"></td>
								            </tr>
								            <tr>
									            <td style="HEIGHT: 19px"></td>
									            <td style="WIDTH: 118px; HEIGHT: 19px">
										            <asp:label id="lblEmail" runat="server" CssClass="LabelCaption" Height="9px" Width="64px">E-Mail</asp:label></td>
									            <td style="WIDTH: 195px; HEIGHT: 19px"><input class="Text" id="txtEmail" style="WIDTH: 176px; HEIGHT: 20px" tabindex="11" readOnly
											            type="text" maxLength="100" size="24" name="txtEmail" runat="server"></td>
									            <td style="WIDTH: 118px; HEIGHT: 19px" align="left">
										            <asp:label id="lblStatus" runat="server" CssClass="LabelCaption" Width="88px">Status</asp:label></td>
									            <td style="WIDTH: 205px; HEIGHT: 19px" align="left"><input class="Text" id="txtStatus" style="WIDTH: 222px; HEIGHT: 20px" tabindex="12" readOnly
											            type="text" maxLength="100" name="txtStatus" runat="server"></td>
									            <td style="WIDTH: 193px; HEIGHT: 19px"><img id="change_End_date" onmouseover="javascript: toggleImage('change_End_date',frmProjectSummary.txtImagePath.value+'Change_End_Date_mover.jpg')"
											            title="" style="WIDTH: 158px; CURSOR: hand" onclick="javascript: popupWindow('ProjectSlipping.aspx?From=ChangeEndDate') "
											            onmouseout="javascript: toggleImage('change_End_date',frmProjectSummary.txtImagePath.value+'Change_End_Date.jpg')" height="25"
											            width="62" runat="server"></td>
								            </tr>
								            <tr>
									            <td style="HEIGHT: 25px"></td>
									            <td style="WIDTH: 118px; HEIGHT: 25px">
										            <asp:label id="lblDeadLine" runat="server" CssClass="labelcaption" Width="136px"></asp:label></td>
									            <td style="WIDTH: 195px; HEIGHT: 25px"><input class="text" id="txtDeadLine" style="WIDTH: 176px; HEIGHT: 20px" tabindex="13" type="text"
											            maxLength="11" size="24" name="txtDeadLine" runat="server"></td>
									            <td style="WIDTH: 118px; HEIGHT: 25px" align="left">
										            <asp:label id="lblSubmittedOn" runat="server" CssClass="LabelCaption" Width="104px">Submitted On:</asp:label></td>
									            <td style="WIDTH: 205px; HEIGHT: 25px" align="left">
										            <asp:Label id="lblSubmittedValue" runat="server" CssClass="labelcaption" Width="104px">Label</asp:Label></td>
									            <td style="WIDTH: 193px; HEIGHT: 25px"><img id="imgReport" onmouseover="javascript: toggleImage('imgReport',frmProjectSummary.txtImagePath.value+'FinancialReports_m.jpg')"
											            title="Reports which focus on an individual project.All costs measured using appropriate University inflation rates.Useful for Finance,HoD,Research Admin."
											            style="WIDTH: 158px; CURSOR: hand" onclick="openReportWindow();" onmouseout="javascript: toggleImage('imgReport',frmProjectSummary.txtImagePath.value+'FinancialReports.jpg')"
											            height="25" width="53" runat="server"></td>
								            </tr>
								            <tr>
									            <td style="HEIGHT: 22px"></td>
									            <td style="WIDTH: 297px; HEIGHT: 22px" colspan="2" rowSpan="1">
										            <asp:label id="lblIncTemplate" style="CURSOR: hand" onclick="popupWindow('NewProjectProposalStep3.aspx?From=Summary')"
											            runat="server" Height="1px" Width="224px" ToolTip='Funding rules are laid down by different potential funders and are based on various formulae incorporating different proportions of cost, lump sums, etc.These formulae are incorporated in income templates by Finance staff.                Clicking here will allow you to select potential funders for your project, displaying the related funding income in the Income line below.In this way you can ""model"" different potential funders for your project and compare the related income which will accrue from each.The formulae are based on the funder only and are not related to the nature of the project in any way.Certain templates will include other additional funding for the project such as fees and consultancy income.Here both the rate and number of students, days, etc are controlled by the project.'
											            Font-Underline="True" Font-Size="10pt" Font-Names="Arial" Font-Bold="True" ForeColor="MediumBlue">Funders</asp:label></td>
									            <td style="WIDTH: 376px; HEIGHT: 22px" align="right" colspan="2" rowSpan="1">
										            <asp:label id="lblFirstFunder" runat="server" CssClass="LabelCaptionRight" Height="16px" Width="224px"
											            EnableViewState="False"></asp:label></td>
									            <td style="WIDTH: 92px; HEIGHT: 22px"><img id="imgRepl" onmouseover="javascript: toggleImage('imgRepl',frmProjectSummary.txtImagePath.value+'replicate_this_project_mover.jpg')"
											            title="" style="WIDTH: 158px; CURSOR: hand" onclick="openReplicateProjectWindow();" onmouseout="javascript: toggleImage('imgRepl',frmProjectSummary.txtImagePath.value+'replicate_this_project.jpg')"
											            height="25" width="53" runat="server"></td>
									            <td style="WIDTH: 202px; HEIGHT: 22px" align="right"></td>
									            <td style="WIDTH: 193px; HEIGHT: 22px"></td>
								            </tr>
								            <tr>
									            <td style="HEIGHT: 18px"></td>
									            <td style="WIDTH: 297px; HEIGHT: 18px" colspan="2">
										            <asp:label id="lblStaffCost" runat="server" Height="16" Width="263px" Font-Size="10pt" Font-Names="Arial">Staff Cost</asp:label></td>
									            <td style="WIDTH: 376px" align="right" colspan="2">
										            <div class="LabelCaptionRight" id="divStaffCost" style="DISPLAY: inline; WIDTH: 165px; HEIGHT: 16px; TEXT-ALIGN: right"
											            nowrap runat="server">Label</div>
									            </td>
									            <td style="WIDTH: 92px; HEIGHT: 18px" align="center"><NOBR><img id="imgUseSpine" onmouseover="javascript: toggleImage('imgUseSpine',frmProjectSummary.txtImagePath.value+'use_spine_points_mover.jpg')"
												            title="" style="WIDTH: 158px; CURSOR: hand" onclick="javascript: bandingClicked()" onmouseout="javascript: toggleImage('imgUseSpine',frmProjectSummary.txtImagePath.value+'use_spine_points.jpg')"
												            height="25" width="150"  runat="server"><img id="imgUseBanding" onmouseover="javascript: toggleImage('imgUseBanding',frmProjectSummary.txtImagePath.value+'use_bandings_mover.jpg')"
												            title="" style="WIDTH:  158px;vertical-align :bottom;   CURSOR: hand" onclick="javascript: bandingClicked()" onmouseout="javascript: toggleImage('imgUseBanding',frmProjectSummary.txtImagePath.value+'use_bandings.jpg')"
												            height="25" width="150" runat="server">
											            <asp:label id="lblIsSpineUsed" runat="server" CssClass="LabelCaption" Height="13px" Width="149"
												            Visible="False"></asp:label></NOBR></td>
								            </tr>
								            <tr>
									            <td style="HEIGHT: 27px"></td>
									            <td style="WIDTH: 297px; HEIGHT: 27px" colspan="2">
										            <asp:label id="lblEquCost" runat="server" Height="1px" Width="263px" Font-Size="10pt" Font-Names="Arial">Equipment Cost</asp:label></td>
									            <td style="WIDTH: 376px; height: 27px;" align="right" colspan="2">
										            <div class="LabelCaptionRight" id="divEquCost" style="DISPLAY: inline; WIDTH: 165px; HEIGHT: 16px; TEXT-ALIGN: right"
											            nowrap runat="server">Label</div>
									            </td>
									            <td style="HEIGHT: 27px" valign="middle" align="left"><NOBR><img id="Status" onmouseover="javascript: toggleImage('Status',frmProjectSummary.txtImagePath.value+'Change_Status_mover.jpg')"
												            title="" style="WIDTH: 158px; CURSOR: hand" onclick="popupWindow('AllProjectProposalStatus.aspx');" onmouseout="javascript: toggleImage('Status',frmProjectSummary.txtImagePath.value+'Change_Status.jpg')"
												            height="25" width="134" align="top" runat="server"></NOBR></td>
								            </tr>
								            <tr>
									            <td style="HEIGHT: 27px"></td>
									            <td style="WIDTH: 297px; HEIGHT: 27px" colspan="2">
										            <asp:label id="lblNonStaff" runat="server" Height="1px" Width="263px" Font-Size="10pt" Font-Names="Arial">Non Staff Cost</asp:label></td>
									            <td style="WIDTH: 376px; height: 27px;" align="right" colspan="2">
										            <div class="LabelCaptionRight" id="divNonStaff" style="DISPLAY: inline; WIDTH: 165px; HEIGHT: 16px; TEXT-ALIGN: right"
											            runat="server">Label</div>
									            </td>
									            <!--
												<td style="HEIGHT: 27px" valign="bottom" align="left"><img id="imgFinalAdj" onmouseover="javascript: toggleImage('imgFinalAdj',frmProjectSummary.txtImagePath.value+'final_adjustment_mover.jpg')"
											            title="" style="WIDTH: 158px; CURSOR: hand" onclick="popupWindow('ProjectIncomeAdjustment.aspx')" onmouseout="javascript: toggleImage('imgFinalAdj',frmProjectSummary.txtImagePath.value+'final_adjustment.jpg')"
											            height="25" width="62" align="top" name="imgFinalAdj" runat="server">
										            <IMG id="imgConsultancyIncome" onmouseover="javascript: toggleImage('imgConsultancyIncome',frmProjectSummary.txtImagePath.value+'Consultancy_Income_mover.jpg')"
											            title="" style="WIDTH: 158px; CURSOR: hand" onclick="ConsultancyIncome('ConsultancyIncome.aspx')"
											            onmouseout="javascript: toggleImage('imgConsultancyIncome',frmProjectSummary.txtImagePath.value+'Consultancy_Income.jpg')"
											            height="27" width="62" align="top" name="imgConsultancyIncome"
											            runat="server"></TD>								
												-->		
														</TR>
								            <TR>
									            <TD style="HEIGHT: 22px"></TD>
									            <TD style="WIDTH: 297px; HEIGHT: 22px" colSpan="2">
										            <asp:label id="lblIndirectCost" runat="server" Height="1px" Width="263px" Font-Size="10pt"
											            Font-Names="Arial">Indirect Cost</asp:label></td>
									            <td style="WIDTH: 376px" align="right" colspan="2">
										            <div class="LabelCaptionRight" id="divIndCost" style="DISPLAY: inline; WIDTH: 165px; HEIGHT: 16px; TEXT-ALIGN: right"
											            runat="server">Label</div>
									            </td>
									            <td style="WIDTH: 92px; HEIGHT: 22px" valign="bottom"><img id="imgCurrency" onmouseover="javascript: toggleImage('imgCurrency',frmProjectSummary.txtImagePath.value+'currency_mover.jpg')"
											            title="" style="WIDTH: 158px; CURSOR: hand" onclick="javascript: popupWindow('ProjectCurrency.aspx') " onmouseout="javascript: toggleImage('imgCurrency',frmProjectSummary.txtImagePath.value+'currency.jpg')"
											            height="25" width="62" align="top" runat="server"></td>
								            </tr>
								            <tr>
									            <td style="HEIGHT: 27px"></td>
									            <td style="WIDTH: 297px; HEIGHT: 27px" colspan="2">
										            <asp:label id="lblFEC" runat="server" CssClass="LabelCaption" Height="1px" Width="263px">Full Economic Cost</asp:label></td>
									            <td style="WIDTH: 376px" align="right" colspan="2">
										            <div class="LabelCaptionRight" id="divFEC" style="DISPLAY: inline; WIDTH: 165px; HEIGHT: 16px; TEXT-ALIGN: right"
											            runat="server">Label</div>
									            </td>
									            <td style="WIDTH: 92px; HEIGHT: 27px"><img id="imgRecalculate" onmouseover="javascript: toggleImage('imgRecalculate',frmProjectSummary.txtImagePath.value+'recalculate_mover.jpg')"
											            title="" style="WIDTH: 158px; CURSOR: hand" onclick="reCalculate()" onmouseout="javascript: toggleImage('imgRecalculate',frmProjectSummary.txtImagePath.value+'recalculate.jpg')"
											            height="25" width="62" align="top" name="imgRecalculate" runat="server"></td>
								            </tr>
								            <tr>
									            <td style="HEIGHT: 27px"></td>
									            <td style="WIDTH: 283px; HEIGHT: 27px" colspan="2">
										            <asp:label id="lblIncome" runat="server" Height="1px" Width="263px" Font-Size="10pt">Income</asp:label></td>
									            <td style="WIDTH: 376px" align="right" colspan="2">
										            <div class="LabelCaptionRight" id="divIncome" style="DISPLAY: inline; WIDTH: 165px; HEIGHT: 16px; TEXT-ALIGN: right"
											            runat="server">Label</div>
									            </td>
									            <td style="WIDTH: 92px; HEIGHT: 27px"><img id="imgFields" onmouseover="javascript: toggleImage('imgFields',frmProjectSummary.txtImagePath.value+'Additional_Fields_mover.jpg')"
											            title="" style="WIDTH: 158px; CURSOR: hand" onclick="javascript: popupWindow('ProjPropAditionalFields.aspx')" onmouseout="javascript: toggleImage('imgFields',frmProjectSummary.txtImagePath.value+'Additional_Fields.jpg')"
											            height="25" width="62" align="top" name="imgRecalculate" runat="server"></td>
								            </tr>
								            <tr>
									            <td style="HEIGHT: 15px"></td>
									            <td style="WIDTH: 283px; HEIGHT: 15px" colspan="2">
										            <asp:label id="lblSurplus" runat="server" CssClass="LabelCaption" Height="1px" Width="263">Surplus/Deficit</asp:label></td>
									            <td style="WIDTH: 376px; HEIGHT: 15px" align="right" colspan="2">
										            <div class="LabelCaptionRight" id="divSurplus" style="DISPLAY: inline; WIDTH: 165px; HEIGHT: 16px; TEXT-ALIGN: right"
											            runat="server">Label</div>
									            </td>
									            <td style="WIDTH: 92px; HEIGHT: 15px"><img id="imgSubmissionReport" onmouseover="javascript: toggleImage('imgSubmissionReport',frmProjectSummary.txtImagePath.value+'Submission_Reports_mover.jpg')"
											            title="Contains data required for input to all submission forms." style="WIDTH: 158px; CURSOR: hand" onclick="OpenSubmissionReports()"
											            onmouseout="javascript: toggleImage('imgSubmissionReport',frmProjectSummary.txtImagePath.value+'Submission_Reports.jpg')"
											            height="25" width="62" align="top" name="imgSubmissionReport" runat="server">
								            </TR>
								            <TR>
									            <TD style="HEIGHT: 26px"></TD>
									            <TD style="WIDTH: 283px; HEIGHT: 26px" colSpan="2">
										            <asp:label id="lblInternalSources" runat="server" CssClass="LabelCaption" Height="1px" Width="263">Internal Sources</asp:label></TD>
									            <TD style="WIDTH: 376px" align="right" colSpan="2">
										            <DIV class="LabelCaptionRight" id="divInternalSources" style="DISPLAY: inline; WIDTH: 165px; HEIGHT: 16px; TEXT-ALIGN: right"
											            runat="server">Label</DIV>
									            </TD>
										            <TD style="WIDTH: 92px; HEIGHT: 26px"><IMG id="ImgAttachments" onmouseover="selectAttachmentImageOnMouseOver()" title="Project Attachments"
											            style="WIDTH: 158px; CURSOR: hand" onclick="popupWindow('ProjectProposalAttachments.aspx');" onmouseout="selectAttachmentImage()"
											            height="25" width="62" align="top" name="imgAttachments" runat="server"></td>
								            </tr>
								            <tr>
									            <td style="HEIGHT: 26px"></td>
									            <td style="WIDTH: 283px; HEIGHT: 26px" colspan="2">
										            <asp:label id="lblShortfall" runat="server" CssClass="LabelCaption" Height="1px" Width="263">Shortfall</asp:label></td>
									            <td style="WIDTH: 376px" align="right" colspan="2">
										            <div class="LabelCaptionRight" id="divShortfall" style="DISPLAY: inline; WIDTH: 165px; HEIGHT: 16px; TEXT-ALIGN: right"
											            runat="server">Label</div>
									            </td>
									            <!-- Modified by Lakshmanan | 10-Nov-08 | PID:24042 | Added PostAward image and removed Submission image-->
									            <td style="WIDTH: 92px; HEIGHT: 26px">
									                <img id="imgPostAwards" onmouseover="javascript: toggleImage('imgPostAwards',frmProjectSummary.txtImagePath.value+'Post_Award_mover.jpg')"
										            title="Post Awards" style="WIDTH: 158px; CURSOR: hand" onclick="javascript: OnPostAwardsClick();"
										            onmouseout="javascript: toggleImage('imgPostAwards',frmProjectSummary.txtImagePath.value+'Post_Award.jpg')" height="25"
										            alt="" src="" width="62" align="top" runat="server">
							                    </td>
								            </tr>
								            <tr>
									            <td style="HEIGHT: 26px"></td>
									            <td style="WIDTH: 283px; HEIGHT: 26px" colspan="2"></td>
									            <td style="WIDTH: 376px" align="right" colspan="2"></td>
									            <td style="WIDTH: 92px; HEIGHT: 26px"><img id="ImgUserPermission" onmouseover="javascript: toggleImage('ImgUserPermission',frmProjectSummary.txtImagePath.value+'User_Permission_m.jpg')"
											            title="User Permission" style="WIDTH: 158px; CURSOR: hand" onclick="javascript: UserPermissionClicked();" onmouseout="javascript: toggleImage('ImgUserPermission',frmProjectSummary.txtImagePath.value+'User_Permission.jpg')"
											            height="25" alt="" src="" width="62" align="top" name="ImgUserPermission" runat="server"></td>
								            </tr>


<style>


.butCL { 
	cursor:pointer;
	box-shadow:rgba(0,0,0,0.2) 0 1px 0 0;
	border-bottom-color:#333;
	border:1px solid #61c4ea;
	background-color:#7cceee;
	border-radius:5px;
	color:#333;
	font-family:'Helvetica Neue',Arial,sans-serif;
	font-size:14px;
	height:20px;
	font-weight:700;
	text-shadow:#b2e2f5 0 1px 0;
	padding:0px 16px;
 }   
 .butCL:hover {
 background-color:#7ccccc;
 
 }
 
body #fancybox-overlay {
  //position: fixed;
  padding:0px 0px;

  top: 5%;
  right: 0%;
  left: 5%;
  width: 90%;
  height: 90% !important;
}

body #fancybox-wrap {
  //position: fixed;
}
 
</style> 


<a id="fancygoogle" target="main" href="https://google.com/"></a> 

<a id="fancygooglebydept" target="_fred" href="https://google.com/"></a> 

<script>
function prjbydept()
{
var prj_id = document.frmProjectSummary.txtProjectID.value;

	var link = document.getElementById("fancygooglebydept");
	link.href = "pfact_prj_dept.asp?frompfact=frompfact&prj_id=" + prj_id;
	document.getElementById('fancygooglebydept').click();
	
}

function prjfiles()
{
var prj_id = document.frmProjectSummary.txtProjectID.value;

/*
var t = window.document.name.getElementById('lblLoginMsg').outerHTML;
alert(t.innerHTML);
alert(window.parent.document.getElementById('lblLoginMsg').outerHTML);
var lblLoginMsg = parent.document.getElementById("lblLoginMsg");
alert(lblLoginMsg);
*/

var userval = parent.frames[0].user;
//alert(userval);


	var link = document.getElementById("fancygoogle");
	link.href = "pfact_files.asp?project_id=" + prj_id + "&user=" + userval;
	document.getElementById('fancygoogle').click();
	
}
</script>


	
<script type="text/javascript" src="../UOE/js/jquery.min.js"></script>
<script type="text/javascript" src="../UOE/js/jquery.fancybox-1.3.4.pack.js"></script>
<link rel="stylesheet" type="text/css" href="../UOE/js/jquery.fancybox-1.3.4.css" media="screen" />
<!--	Fancybox settings	-->
<script type="text/javascript">
	$(document).ready(function() { 
				
	$(".iframe").fancybox({
				'enableEscapeButton' : false ,
				'showCloseButton' : false ,
				'width'				: '100%',
				'height'			: '100%',
				'autoScale'			: false,
				//'autoSize'	        :  false,
				'transitionIn'		: 'fade',
				'transitionOut'		: 'none',
				'type'				: 'iframe',
				'padding' : 0,
				'onComplete' : function(){
					//$('#fancybox-content').removeAttr('style').css({ 'height' : $(window).height()+300, 'margin' : '0 auto', 'width' : $(window).width() });
					//$('#fancybox-wrap').removeAttr('style').css({ 'height' : $(window).height()+300, 'margin' : '0 auto', 'width' : $(window).width() }).show();

					//$.fancybox.resize();
					//$.fancybox.center();
					
					}

				});
				
	$(".framebig").fancybox({ 
            type: 'iframe', 
            width: '100%', 
            height: '95%', 
            openEffect: 'fade',
            closeEffect: 'fade',
			padding    : 2,
				margin     : 12,
			
		});

		
	});			
	
	</script>
<!--	Fancybox settings	-->	

									<tr>
										<td style="HEIGHT: 26px"></td>
									        <td style="WIDTH: 283px; HEIGHT: 26px" colspan="2"></td>
									        <td style="WIDTH: 376px" align="right" colspan="2"></td>
									        <td style="WIDTH: 50px; HEIGHT: 26px;display: inline-block;" align="middle"><button type="button" class="butCL" style="width:100px;display: inline-block;" onclick="prjbydept();">T1 Build</button></td>
											<td style="WIDTH: 10px; HEIGHT: 26px;display: inline-block;" align="middle"><a href="help.asp?which=bud" target="pfactrepwin"><img src="include/q.jpg" style="display: inline-block;border:0;" width=20px height=20px></img></a></td>
								
								            </tr>



									<tr>
										<td style="HEIGHT: 26px"></td>
									        <td style="WIDTH: 283px; HEIGHT: 26px" colspan="2"></td>
									        <td style="WIDTH: 376px" align="right" colspan="2"></td>
									        <td style="WIDTH: 50px; HEIGHT: 26px" align="middle"><button type="button" class="butCL" style="width:100px;" onclick="prjfiles();">Files</button></td>
											<td style="WIDTH: 10px; HEIGHT: 26px;display: inline-block;" align="middle"></td>
								            </tr>
											
											
									<tr>
										<td style="HEIGHT: 26px"></td>
									        <td style="WIDTH: 283px; HEIGHT: 26px" colspan="2"></td>
									        <td style="WIDTH: 376px" align="right" colspan="2"></td>
									        <td style="WIDTH: 50px; HEIGHT: 26px" align="middle">
											<SCRIPT>
{
document.write("<iframe scrolling='no' frameborder='0' id='t1bid' style='width:150px;height:20px' src='t1bidnum.asp' ></iframe>");
}
									</SCRIPT>	
											</td>
											<td style="WIDTH: 10px; HEIGHT: 26px;display: inline-block;" align="middle"></td>
								    </tr>		

									<tr>
										<td style="HEIGHT: 26px"></td>
									        <td style="WIDTH: 283px; HEIGHT: 20px" colspan="2"></td>
									        <td style="WIDTH: 376px" align="right" colspan="2"></td>
									        <td align="left"><SCRIPT>
{
document.write("<iframe scrolling='no' frameborder='0' id='retrofrm' style='width:150px;height:20px' src='retrocosting.asp' ></iframe>");
}
									</SCRIPT>	</td>
									</tr>


								            <tr>
									            <td style="HEIGHT: 26px"></td>
									            <td style="WIDTH: 283px; HEIGHT: 26px" colspan="2"></td>
									            <td style="WIDTH: 376px" align="right" colspan="2"></td>
									            <td style="WIDTH: 92px; HEIGHT: 26px"><img id="imgProjectList" onmouseover="javascript: toggleImage('imgProjectList',frmProjectSummary.txtImagePath.value+'Project_List_m.jpg')"
											            title="Project List" style="WIDTH: 158px; CURSOR: hand" onclick="javascript: ProjectListClicked('ProjectList.aspx');"
											            onmouseout="javascript: toggleImage('imgProjectList',frmProjectSummary.txtImagePath.value+'Project_List.jpg')" height="25"
											            alt="" src="" width="62" align="top" runat="server"></td>
								            </tr>
								            <tr>
									            <td style="HEIGHT: 26px"></td>
									            <td style="WIDTH: 283px; HEIGHT: 26px" colspan="2"></td>
									            <td style="WIDTH: 376px" align="right" colspan="2">
										            <asp:hyperlink id="lnkErrors" runat="server" Font-Bold="True" ForeColor="Blue" Visible="False"
											            NavigateUrl="PPAMErrorInfo.aspx" Target="Details">Errors</asp:hyperlink></td>
									            <td style="WIDTH: 92px; HEIGHT: 26px"><img id="Logout" onmouseover="javascript: toggleImage('Logout',frmProjectSummary.txtImagePath.value+'log_out_m.jpg')"
											            title="Logout" style="WIDTH: 158px; CURSOR: hand" onclick="LogoutClick()" onmouseout="javascript: toggleImage('Logout',frmProjectSummary.txtImagePath.value+'log_out.jpg')"
											            height="25" width="62" align="top" name="Logout" runat="server"></td>
								            </tr>
							            </TABLE>
						            </td>
						            <td style="HEIGHT: 216px" valign="top" rowSpan="1"></td>
					            </tr>
					        </table>
					    </asp:panel>
				    </td>
				</tr>
			</TABLE>
			<INPUT id="txtProjectID" style="Z-INDEX: 101; LEFT: 656px; POSITION: absolute; TOP: 664px" type="hidden" name="txtProjectID" runat="server">
			<INPUT id="txtExpTemp" style="Z-INDEX: 102; LEFT: 16px; WIDTH: 108px; POSITION: absolute; TOP: 656px; HEIGHT: 21px" type="hidden" size="12" name="txtExpTemp" runat="server">
			<INPUT id="txtImagePath" style="Z-INDEX: 103; LEFT: 304px; POSITION: absolute; TOP: 656px" type="hidden" name="txtImagePath" runat="server">
			<INPUT id="txtImage" style="Z-INDEX: 104; LEFT: 824px; WIDTH: 77px; POSITION: absolute; TOP: 624px; HEIGHT: 25px" type="hidden" size="7" name="txtImage" runat="server"> 
			<INPUT style="Z-INDEX: 105; LEFT: 824px; WIDTH: 77px; POSITION: absolute; TOP: 664px; HEIGHT: 25px" type="hidden" name="txtButton"> 
			<INPUT id="txtattached" style="Z-INDEX: 106; LEFT: 136px; POSITION: absolute; TOP: 656px" type="hidden" value="s" name="txtattached" runat="server"> 
			<INPUT id="txtRHEattached" style="Z-INDEX: 107; LEFT: 136px; POSITION: absolute; TOP: 656px" type="hidden" value="s" name="txtRHEattached" runat="server">
			<INPUT id="txtNotesAttached" style="Z-INDEX: 108; LEFT: 480px; POSITION: absolute; TOP: 664px" type="hidden" name="txtNotesAttached" runat="server">
			<INPUT id="hdnID" style="Z-INDEX: 109; LEFT: 563px; POSITION: absolute; TOP: 629px" type="hidden" runat="server">
			<INPUT id="hdnFunder" style="Z-INDEX: 110; LEFT: 19px; POSITION: absolute; TOP: 624px" type="hidden" runat="server" name="hdnFunder">
			<INPUT id="hdnIsRHUL" runat="server" type="hidden" name="hdnIsRHUL" style="z-index: 112; left: 366px; position: absolute; top: 624px" />
			<INPUT id="hdnManc" runat="server" type="hidden" style="z-index: 111; left: 193px; position: absolute; top: 624px" />
		</form>

<SCRIPT>
{
document.write("<iframe scrolling='yes' frameborder='0' style='width:800px;' src='costdetails_uoe_split.asp?prjid=" + document.frmProjectSummary.txtProjectID.value + "' ></iframe>");
}
</SCRIPT>	

<SCRIPT>
{
document.write("<iframe scrolling='yes' frameborder='0' style='height:900px;width:100%;' src='costdetails_staff_breakdown.asp?prjid=" + document.frmProjectSummary.txtProjectID.value + "' ></iframe>");
}

var retrofrm = document.getElementById("retrofrm");
retrofrm.src = "retrocosting.asp?prjid=" + document.frmProjectSummary.txtProjectID.value;


var t1bid = document.getElementById("t1bid");
t1bid.src = "t1bidnum.asp?prjid=" + document.frmProjectSummary.txtProjectID.value;




</SCRIPT>		
	</body>
</HTML>
