<%@ Page Language="C#" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
    <title>Cost Details</title>

</head>

<body>

<style>

.FooterStyle
{
    background-color: #a33;
    color: White;
    text-align: right;
}

input.temp { width: 300px; }

input.nomhead { width: 600px; }

body
{
background-color: #F1EAE0; 
}

.butCL { 
	cursor:pointer;
	box-shadow:rgba(0,0,0,0.2) 0 1px 0 0;
	border-bottom-color:#333;
	border:1px solid #61c4ea;
	background-color:#7cceee;
	border-radius:5px;
	color:#333;
	font-family:'Helvetica Neue',Arial,sans-serif;
	font-size:14px;
	height:20px;
	font-weight:700;
	text-shadow:#b2e2f5 0 1px 0;
	padding:0px 16px;
 }   
 .butCL:hover {
 background-color:#7ccccc;
 
 }
 
  .butCLclose { 
	cursor:pointer;
	box-shadow:rgba(0,0,0,0.2) 0 1px 0 0;
	border-bottom-color:#333;
	border:1px solid #61c4ea;
	background-color:#FFFF66;
	border-radius:5px;
	color:#333;
	font-family:'Helvetica Neue',Arial,sans-serif;
	font-size:15px;
	height:25px;
	font-weight:700;
	text-shadow:#b2e2f5 0 1px 0;
	padding:0px 16px;
 }   
 .butCLclose:hover {
 background-color:#FFFFCC;
 
 } 

</style>


    <form id="form1" runat="server">
	
<table width=800>
	<tr>
	<td width=600><h2>PFACT and T1 Nominal mapping list</h2></td>
		<td style="text-align:right;"><asp:button
		id="Button1"
		runat="server"
		text="New Mapping"
		onclick="InsertDetail" 
		ControlStyle-CssClass="butCL" />
		</td>
	</tr>
	<tr>
		<td width=600>Pfact categories mapped to T1 Budget Headings.</td>
	</tr>
</table>

    <div>

     <asp:GridView  runat="server"
            id="GridView1"
			Width="1400px"
            Font-Names="Arial" Font-Size="Smaller" 
            AutoGenerateColumns="False" 
            AutoGenerateEditButton="True" 
			Showfooter="True"
            DataSourceID="SqlDataSource1" 
            DataKeyNames="id"
            EnableEventValidation = "false"
			onrowdatabound="GridView1_RowDataBound"
            >
			                       
            <Columns>
			
				 <%--DATA BOUND COLUMNS--%>
		

                 <asp:BoundField DataField="id" HeaderText="id" SortExpression="id" 
                    ReadOnly="true" Visible = "false" />
                
				 <asp:TemplateField HeaderText="pFact Template" ControlStyle-CssClass="temp" SortExpression="PFACT_Template">
                    <EditItemTemplate>
                        <asp:DropDownList ID="DropDownList4" runat="server" 
                            DataSourceID="SqlDataSource4" DataTextField="PFACT_Template" DataValueField="PFACT_Template" 
                            SelectedValue='<%# Bind("PFACT_Template") %>'>
                        </asp:DropDownList>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label4" runat="server" Text='<%# Bind("PFACT_Template") %>'></asp:Label>
                    </ItemTemplate>    
				</asp:TemplateField> 
				
				 <asp:BoundField DataField="Nominal_Heading" ControlStyle-CssClass="nomhead"  HeaderText="Nominal Heading" SortExpression="Nominal_Heading" />
				 
				
				 <asp:BoundField DataField="T1_Bud_Temp_Heading" HeaderText="T1 Template Heading" SortExpression="T1_Bud_Temp_Heading" />
               				 
				 <asp:BoundField DataField="def_Nominal" HeaderText="Default Nominal" SortExpression="def_Nominal" />
				 
				 <asp:BoundField DataField="nominal" HeaderText="Nominal" SortExpression="nominal" />
				 
				 
               <asp:TemplateField HeaderText="Template Code" SortExpression="T1_Template_Code">
                    <EditItemTemplate>
                        <asp:DropDownList ID="DropDownList5" runat="server" 
                            DataSourceID="SqlDataSource5" DataTextField="T1_Template_Code" DataValueField="T1_Template_Code" 
                            SelectedValue='<%# Bind("T1_Template_Code") %>'>
                        </asp:DropDownList>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label5" runat="server" Text='<%# Bind("T1_Template_Code") %>'></asp:Label>
                    </ItemTemplate>    
				</asp:TemplateField> 

					

             <asp:TemplateField>
                    <ItemTemplate>
                    
                        <%--ADD THE DELETE LINK BUTTON--%>
                        <asp:LinkButton Runat="server" OnClientClick ="return confirm('Are you sure you?');"
                            CommandName="Delete">Delete</asp:LinkButton>
                            
                    </ItemTemplate>

                </asp:TemplateField>
			
              
                
            </Columns>
           
<FooterStyle BackColor="#cccccc" Font-Bold="True" ForeColor="Black" HorizontalAlign="Left" />

			<AlternatingRowStyle BackColor="lightblue" />
            <EditRowStyle BackColor="#E1EDFD" />
            <SelectedRowStyle BackColor="#00FFCC" />
            
        </asp:GridView>
        
        
        <%--THE SQL DATA SOURCE CONNECTED WITH THE GRIDVIEW--%>
        <asp:SqlDataSource 
            ID="SqlDataSource1" runat="server" 
            ConnectionString="Data Source=VMMSSQLTEST01\DB_01_12TE;Initial Catalog=pfact_t1;User Id=Pfact_dbo;Password=4ctu4l1ty84515;Connection Timeout=10;Application Name=UOE_API;Connection Lifetime=10;" 
            SelectCommand=" SELECT id, PFACT_Template, Nominal_Heading, T1_Bud_Temp_Heading, def_Nominal, nominal, T1_Template_Code FROM  T1_CSV_Budget_Heading_Map "
            UpdateCommand=" update T1_CSV_Budget_Heading_Map set PFACT_Template=@PFACT_Template, Nominal_Heading=@Nominal_Heading, T1_Bud_Temp_Heading=@T1_Bud_Temp_Heading, def_Nominal=@def_Nominal, nominal=@nominal, T1_Template_Code=@T1_Template_Code where id = @id "
			InsertCommand=" INSERT INTO [T1_CSV_Budget_Heading_Map] ( PFACT_Template, Nominal_Heading, T1_Bud_Temp_Heading, def_Nominal, nominal, T1_Template_Code ) VALUES ('', '', '', '', '', '') "
            DeleteCommand=" delete from T1_CSV_Budget_Heading_Map where id = @id ">
           
		   
            <UpdateParameters>
				<asp:Parameter Name="PFACT_Template" />
				<asp:Parameter Name="Nominal_Heading" />
				<asp:Parameter Name="T1_Bud_Temp_Heading" />
				<asp:Parameter Name="def_Nominal" />
				<asp:Parameter Name="nominal" />
				<asp:Parameter Name="T1_Template_Code" />
            </UpdateParameters>
			
			
             <DeleteParameters> <asp:Parameter Name="id" /> </DeleteParameters>
            
        </asp:SqlDataSource>
		
		
		
		<asp:SqlDataSource ID="SqlDataSource4" runat="server" 
        ConnectionString="Data Source=VMMSSQLTEST01\DB_01_12TE;Initial Catalog=pfact_t1;User Id=Pfact_dbo;Password=4ctu4l1ty84515;Connection Timeout=10;Application Name=UOE_API;Connection Lifetime=10;" 
        SelectCommand=" select PFACT_Template from (select '' as PFACT_Template union all SELECT DISTINCT PFACT_Template FROM T1_CSV_Budget_Heading_Map) d order by PFACT_Template ">
		</asp:SqlDataSource>
		
		
		<asp:SqlDataSource ID="SqlDataSource5" runat="server" 
        ConnectionString="Data Source=VMMSSQLTEST01\DB_01_12TE;Initial Catalog=pfact_t1;User Id=Pfact_dbo;Password=4ctu4l1ty84515;Connection Timeout=10;Application Name=UOE_API;Connection Lifetime=10;" 
        SelectCommand=" select T1_Template_Code from (select '' as T1_Template_Code union all SELECT DISTINCT T1_Template_Code FROM T1_CSV_Budget_Heading_Map ) a order by T1_Template_Code">
		</asp:SqlDataSource>
		

    </div>
	
	
	<br />
	
<table>
	<tr>
		<td><asp:button
		id="Button2"
		runat="server"
		text="New Category"
		onclick="InsertDetail" 
		ControlStyle-CssClass="butCL" />
		</td>
	</tr>
</table>	

 
 </form>



 
 <script runat="server">
  
 
private void InsertDetail (object source, EventArgs e) {
  SqlDataSource1.Insert();
  GridView1.EditIndex = GridView1.Rows.Count; 
  
}


protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
	{
	    //HttpContext.Current.Response.Write("<script>alert('here');<" + "/script>");
		try
            {
				
			 }

            catch (Exception ee)
            {
                //Response.Write(ee.Message);
            }
   }
</script>
	
	
</body>
</html>


