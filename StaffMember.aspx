<%@ page language="vb" autoeventwireup="false" inherits="PFACT.StaffMember, pFACT_Deploy" maintainscrollpositiononpostback="true" enableeventvalidation="false" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/tr/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<HEAD>
		<title>pFACT</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR"/>
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE"/>
		<meta content="JavaScript" name="vs_defaultClientScript"/>
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema"/>
		<%=styleSheetVar%>
		<script language="javascript" src="Calendar1-82.js" type="text/javascript" ></script>
		<script language="javascript" src="ClientScripts.js" type="text/javascript" ></script>
		<script language="javascript" type ="text/javascript" >
		var isPostback=false;
		/*Modified by phani on 18-12-2008||PID:24404||Comments:Added script functions to fill the Spine details in Payscale grades by using web methods and names are chenged to lowercase for dropdowns*/
        function OnPayGradeClick()
        {
            var dd = document.getElementById('ddpaygradesub');
            if ( dd.options[dd.selectedIndex].value !='' && document.getElementById('txtActionDtsub').value !='')
            {
                ValidateSpineDateForPayGrade();
                PageMethods.GetSpine(dd.value,document.getElementById('txtActionDtsub').value, onSuccessPayGradeSelection,onFailed);
            }
        }
        //PID:28280//
        /* dont remove this function */
        function DoNothing()
        {
        
        }
        
        function onSuccessPayGradeSelection(result, userContext, methodName)
        {
            var SpinePoints = result;
            var Spine = SpinePoints[0];
            var SpineDP = SpinePoints[1];
            var i;
            var ddSpine = document.getElementById('ddspinesub');
            var ddSpineDP = document.getElementById('dddispointsub');
            ddSpine.options.length=0;
            ddSpineDP.options.length=0;
            for(i=0;i<Spine.length;i++)
            {
                var Code = Spine[i].split('|');
                oOption = new Option(Code[1],Code[0]);
                ddSpine.options.add(oOption);
            }
            for(i=0;i<SpineDP.length;i++)
            {
                var Code = SpineDP[i].split('|');
                oOption = new Option(Code[1],Code[0]);
                ddSpineDP.options.add(oOption);
            }
            
        }
        function onFailed(error,userContext,methodName)
        {
            alert('An error occurred');
            var ddSpine = document.getElementById('ddspinesub');
            var ddSpineDP = document.getElementById('dddispointsub');
            ddSpine.options.length=0;
            ddSpineDP.options.length=0;
        }

        function getSelectedValues()
        {
	        document.getElementById('hdnPayGradeSpineDP').value = document.getElementById('dddispointsub').value;
	        document.getElementById('hdnPayGradeSpine').value = document.getElementById('ddspinesub').value;
        }
        
        function ValidateSpineDateForPayGrade()
		{
			if (Trim(document.getElementById('txtActionDtsub').value)=="") 
			{	
				alert("Please specify on which date is this staff member on this point")
				document.getElementById('txtActionDtsub').focus()
				return false
			}
			if (Trim(document.getElementById('txtActionDtsub').value)!="") 
			{
				retCheckDate= CheckDate(document.getElementById('txtActionDtsub').value)
				if (retCheckDate!="OK")
				{	alert(retCheckDate)
					document.getElementById('txtActionDtsub').focus()
					return false
				}
				else
				{
					document.getElementById('txtActionDtsub').value= ConvertDateToDDMMMYYYY(document.getElementById('txtActionDtsub').value)	
				}
			}	
			
		}

		
        function clearFields()
        {
            document.frmStaffMember.hdnPayscalechanged.value="CLEARSPINE"
            document.frmStaffMember.hdnSpineOnDatechanged.value ="CLEARSPINE"
            clearSucessMessage()	
            document.frmStaffMember.submit()
        }
		
        function fillSpine()
        {
            if (Trim(document.frmStaffMember.txtSpineDate.value)=="") 
            {	
                alert("Please specify on which date is this staff member on this point")
                document.frmStaffMember.txtSpineDate.focus()
                return false
            }
            if (Trim(document.frmStaffMember.txtSpineDate.value)!="") 
            {
                retCheckDate= CheckDate(document.frmStaffMember.txtSpineDate.value)
                if (retCheckDate!="OK")
                {	
                    alert(retCheckDate)
                    document.frmStaffMember.txtSpineDate.focus()
                    return false
                }
                else
                {
                    document.frmStaffMember.txtSpineDate.value= ConvertDateToDDMMMYYYY(document.frmStaffMember.txtSpineDate.value)	
                }
            }		

            if (Trim(document.frmStaffMember.txtOriginalSpDate.value) != Trim(document.frmStaffMember.txtSpineDate.value))
            {////PID:24315 
                submitFormStaff()
            }
        }
        //PID:24315 
        function submitFormStaff()
        {
            if (Trim(document.frmStaffMember.txtSpineDate.value)=="") 
            {						
                return false
            }
            if (Trim(document.frmStaffMember.txtSpineDate.value)!="") 
            {
                retCheckDate= CheckDate(document.frmStaffMember.txtSpineDate.value)
                if (retCheckDate!="OK")
                {	
                    alert(retCheckDate)
                    document.frmStaffMember.txtSpineDate.focus()
                    return false
                }
                else
                {
                    document.frmStaffMember.txtSpineDate.value= ConvertDateToDDMMMYYYY(document.frmStaffMember.txtSpineDate.value)	
                }
            }
            document.frmStaffMember.submit()
        }
			
			function okClicked()
			{ 
			    //PID:13428
				if (document.frmStaffMember.ddDepartment.value=="0") 
				{	
				
				     //Modified by Sakshi on 29th Sept09||PID:28208||Comments:displaying the Changed text for Department in the message

					alert("Please select a "+document.getElementById("hdnDept").value+" from the list")
					
					document.frmStaffMember.ddDepartment.focus()
					return false
				}
				
				if (Trim(document.frmStaffMember.txtSurName.value)=="") 
				{	
					alert("Please enter the Sur name")
					document.frmStaffMember.txtSurName.focus()
					return false
				}
				
				if (Trim(document.frmStaffMember.txtFName.value)=="") 
				{	
					alert("Please enter the first name")
					document.frmStaffMember.txtFName.focus()
					return false
				}
				//PID:18074 'valdated by sumathy on 9 Jan 2007
				if (document.frmStaffMember.txtEmail.value!="")
				{
					if (!(validateEmail(Trim(document.frmStaffMember.txtEmail.value))))
					{
						alert("Please enter a valid E-Mail address")
						document.frmStaffMember.txtEmail.focus()
						return false
					}
				}			
				//PID:12136
				if(document.frmStaffMember.chkOA.checked==true) 
				{
					if (document.frmStaffMember.txtOA.value=="") 
						{	
					        alert("Please enter the other allowances")
					        document.frmStaffMember.txtOA.focus()
					        return false
						}
						if (!validateDecimalInteger(document.frmStaffMember.txtOA.value))
						{
					        alert("Please enter a valid value")
					        document.frmStaffMember.txtOA.focus()
					        return false
						}
				}
                //If Edit then no need to validate Payscale, IncDate, SpineDate, Spine
                if (document.getElementById('hdnIsEdit').value=="")
                {
				    //PID:14015 
				    if (document.frmStaffMember.ddPayScale.value==0) 
				    {	
					    alert("Please select a payscale from the list")
					    document.frmStaffMember.ddPayScale.focus()
					    return false
				    }
				    if (Trim(document.frmStaffMember.txtSpineDate.value)=="") 
				    {	
					    alert("Please specify on which date is this staff member on this point")
					    document.frmStaffMember.txtSpineDate.focus()
					    return false
				    }
				    if (Trim(document.frmStaffMember.txtSpineDate.value)!="") 
				    {
				        retCheckDate= CheckDate(document.frmStaffMember.txtSpineDate.value)
				        if (retCheckDate!="OK")
				        {	alert(retCheckDate)
					        document.frmStaffMember.txtSpineDate.focus()
					        return false
				        }
				        else
				        {
					        document.frmStaffMember.txtSpineDate.value= ConvertDateToDDMMMYYYY(document.frmStaffMember.txtSpineDate.value)	
				        }
				    }
				    //PID:11805
				    if (document.frmStaffMember.ddSpine.value=="-501") 
				    {	
					    alert("Please select a value")
					    document.frmStaffMember.ddSpine.focus()
					    return false
				    }		
				    if (document.frmStaffMember.txtIncDate.value!="") 
				    {
				        retCheckDate= CheckDate(document.frmStaffMember.txtIncDate.value)
					    if (retCheckDate!="OK")
					    {	alert(retCheckDate)
						    document.frmStaffMember.txtIncDate.focus()
						    return false
					    }
					    else
					    {
						    document.frmStaffMember.txtIncDate.value= ConvertDateToDDMMMYYYY(document.frmStaffMember.txtIncDate.value)	
					    }
				    }
				}
				
				if (document.frmStaffMember.txtStaffNo.value=="")
				{
					alert("Please enter the staff number")
					document.frmStaffMember.txtStaffNo.focus()
					return false
				}	
				
				if (Trim(document.frmStaffMember.txtStartDate.value)!="")
				{
					retCheckDate= CheckDate(document.frmStaffMember.txtStartDate.value)
					
					if (retCheckDate!="OK")
					{	alert(retCheckDate)
						document.frmStaffMember.txtStartDate.focus()
						return false
					}
					else
					{
						document.frmStaffMember.txtStartDate.value= ConvertDateToDDMMMYYYY(document.frmStaffMember.txtStartDate.value)	
					}
				}
										
				if (Trim(document.frmStaffMember.txtEndDate.value)!="")
				{
					retCheckDate= CheckDate(document.frmStaffMember.txtEndDate.value)
					
					if (retCheckDate!="OK")
					{	alert(retCheckDate)
						document.frmStaffMember.txtEndDate.focus()
						return false
					}
					else
					{
						document.frmStaffMember.txtEndDate.value= ConvertDateToDDMMMYYYY(document.frmStaffMember.txtEndDate.value)	
					}
				}
					
				if ((Trim(document.frmStaffMember.txtStartDate.value)!="") && (Trim(document.frmStaffMember.txtEndDate.value)!=""))
				{											
					stdt=(document.frmStaffMember.txtStartDate.value).split("-")
					dtStartDate= new Date(convertYear(stdt[2]), convertMonth(stdt[1])-1, stdt[0],00,00,00)
					
					stdt=(document.frmStaffMember.txtEndDate.value).split("-")
					dtEndDate= new Date(convertYear(stdt[2]), convertMonth(stdt[1])-1, stdt[0],00,00,00)
					
					if (dtStartDate>dtEndDate)
					{
						alert("Start date cannot be a date after end date")
						document.frmAddUser.txtStartDate.focus()
						return false
					}
				}
				
				if (document.frmStaffMember.txtShowDOB.value=="TRUE")
				{
					if (Trim(document.frmStaffMember.txtDOB.value)!="")
					{
						retCheckDate= CheckDate(document.frmStaffMember.txtDOB.value)
					
						if (retCheckDate!="OK")
						{	alert(retCheckDate)
							document.frmStaffMember.txtDOB.focus()
							return false
						}
						else
						{
							d = new Date()
							stdt=(document.frmStaffMember.txtDOB.value).split("-")
							dtDOB= new Date(convertYear(stdt[2]), convertMonth(stdt[1])-1, stdt[0],00,00,00)
							if (d<dtDOB) 
							{
								alert("Date of birth cannot be greater than today")
								return false
							}
							document.frmStaffMember.txtDOB.value= ConvertDateToDDMMMYYYY(document.frmStaffMember.txtDOB.value)	
						}
					}
				}
				document.frmStaffMember.txtButton.value="OK"
				document.frmStaffMember.submit()
			}
			
			//pid:12006
			function cancelClicked()
			{
				var win=window.open('AllStaffMembers.aspx?'+document.frmStaffMember.txtFilterLink.value,'main','target=main')
				self.close()
			}
			
			function validateDecimalInteger( strValue )
			{

				var RefString  =  /(^-?\d*\.?\d+$)/;

				return RefString.test(strValue);
			}
			
		
		
		
			function SetDefaults()
			{
				//Required window size
				//PID:14493||Modified by Pravitha on 5 June 2006 ||comments:window height changed to remove scrollbar		
//				windowWidth = 800;
//				windowHeight= 620;
	            windowWidth =1024;
				windowHeight= 750;					
				//If IE then, calculate centre screen by screen max size,
				if (screen)
				{
					x = (screen.availWidth - windowWidth)/2;
					y = (screen.availHeight - windowHeight)/2;
					//Give offset to roughly align within the main frame.
//					x = x + 50;
//					y = y + 50;
				}

				window.resizeTo(windowWidth, windowHeight)
				window.moveTo(x, y)
				
				self.focus();
			}
			
			// If add or edit is sucessfull close the popup window
			function CheckWhetherToCloseWindow()
			{
				if (document.frmStaffMember.txtCloseWindow.value=="CLOSE")
				{
					var win=window.open('AllStaffMembers.aspx','main','target=main')
					self.close()
				}	
				else	
					SetDefaults()
			}	
			
			function CheckOA()
			{
				document.frmStaffMember.txtOA.value=""
				if(document.frmStaffMember.chkOA.checked==true) 
					{
					document.frmStaffMember.txtOA.disabled=false
					}
				else
					{
					document.frmStaffMember.txtOA.disabled=true
					}
			}	
			
			function GetJesPersonIDClicked()
			{
				//document.getElementById("lblErrorMessage").innerText="Connecting to JeS.Please wait..."
				if (Trim(document.frmStaffMember.txtSurName.value)=="") 
				{	
					alert("Please enter the Sur name")
					document.frmStaffMember.txtSurName.focus()
					return false
				}
				
				if (Trim(document.frmStaffMember.txtFName.value)=="") 
				{	
					alert("Please enter the first name")
					document.frmStaffMember.txtFName.focus()
					return false
				}
				
				document.frmStaffMember.txtButton.value="GetJesPersonID"
				document.frmStaffMember.submit()
			}		
		
			//Added by Lakshmanan on 05 -Sep-08 | PID:23271 | handling  when form is closed using window close button('X') of the form		
			function HandleOnClose()
			{
			if(!isPostback) 
			{
			    if (event.clientY < 0 && event.clientX>(windowWidth-50)) 
                {
                            
				    cancelClicked()
            }    }
			}			
//Scripts taken from StaffPaysacleGrades page, by Lakshmanan | PID:24178
//For Payscale			
			function SavePayscaleGradeClick()
			{
				if (document.frmStaffMember.ddpaygradesub.value==0)
				{
					alert("Please select a payscale")
					document.frmStaffMember.ddpaygradesub.focus()
					return false
				}
				if (Trim(document.frmStaffMember.txtActionDtsub.value)=="") 
				{	
					alert("Please enter the action date for the payscale")
					document.frmStaffMember.txtActionDtsub.focus()
					return false
				}
				if (Trim(document.frmStaffMember.txtActionDtsub.value)!="") 
				{
					retCheckDate= CheckDate(document.frmStaffMember.txtActionDtsub.value)
					if (retCheckDate!="OK")
					{	alert(retCheckDate)
						document.frmStaffMember.txtActionDtsub.focus()
						return false
					}
					else
					{
						document.frmStaffMember.txtActionDtsub.value= ConvertDateToDDMMMYYYY(document.frmStaffMember.txtActionDtsub.value)	
					}
				}
				if (Trim(document.frmStaffMember.txtIncDatesub.value)!="") 
				{
					retCheckDate= CheckDate(document.frmStaffMember.txtIncDatesub.value)
					if (retCheckDate!="OK")
					{	alert(retCheckDate)
						document.frmStaffMember.txtIncDatesub.focus()
						return false
					}
					else
					{
						document.frmStaffMember.txtIncDatesub.value= ConvertDateToDDMMMYYYY(document.frmStaffMember.txtIncDatesub.value)	
					}
				}
				if (document.frmStaffMember.ddspinesub.value=="-501") 
				{	
					alert("Please select a value")
					document.frmStaffMember.ddspinesub.focus()
					return false
				}
				getSelectedValues()
			    document.getElementById('txtButton').value="Save Payscale"
			    document.frmStaffMember.submit()
			}
			function AddPayscaleGradeClick()
			{
			    document.getElementById('hdnpayscalegradeid').value=""
			    document.getElementById('txtButton').value="Show Payscale"
			    document.frmStaffMember.submit()
			}
			function CancelPayscaleGradeClick()
			{
			    $find('modalPayscaleGrade').hide();
			}
			function OnPayscaleClick(PayscaleID)
			{
			    document.getElementById('hdnpayscalegradeid').value=PayscaleID
			    document.getElementById('txtButton').value="Show Payscale"
			    document.frmStaffMember.submit()
			}
            function clearSpine()
			{
		     document.frmStaffMember.hdnPayscalechangedsub.value ="CLEARSPINE"
		     document.getElementById('lblPayGradeError').innerText=""
		     document.getElementById('txtButton').value="Date Change"
		    }
			
			function submitForm()
			{
				if (Trim(document.getElementById('txtActionDtsub').value)=="") 
				{						
					return false
				}
				if (Trim(document.getElementById('txtActionDtsub').value)!="") 
				{
					retCheckDate= CheckDate(document.getElementById('txtActionDtsub').value)
					if (retCheckDate!="OK")
					{	alert(retCheckDate)
						document.getElementById('txtActionDtsub').focus()
						return false
					}
					else
					{
						document.getElementById('txtActionDtsub').value= ConvertDateToDDMMMYYYY(document.getElementById('txtActionDtsub').value)	
					}
				}
				document.getElementById('txtButton').value="FillSpine"
				document.frmStaffMember.submit()
			}
			function openWindow(openString)
			{				
				win=window.open(openString,'Details','toolbar=no,scrollbars=yes')
			}
			
//Scripts taken from StaffDepartments page, by Lakshmanan | PID:24178
//For Department
            function ClearMessage()
            {
                document.getElementById('lblDeptErrLbl').value=""
            }		
            function OnDepartmentClick(StaffDeptID)
            {
			    document.getElementById('hdnStaffDeptID').value=StaffDeptID
			    document.getElementById('txtButton').value="Show Department"
			    document.frmStaffMember.submit()
            }	
			function AddDepartmentClick()
			{
			    document.getElementById('hdnStaffDeptID').value=""
			    document.getElementById('txtButton').value="Show Department"
			    document.frmStaffMember.submit()
			}           
            function SaveDeptClick()
            {
                if(document.frmStaffMember.ddDepartmentsub.value==0)
                {
                    alert("Select a Department from the list")
                    document.frmStaffMember.ddDepartmentsub.focus()
                    return false
                }
        		 
		        if(document.frmStaffMember.txtFtePercentsub.value!="")
		        {
                    if (!isNumDec(document.frmStaffMember.txtFtePercentsub.value) )
                    {
                        alert("Only numbers are allowed as FtePercent.")
                        document.frmStaffMember.txtFtePercentsub.focus()
                        return false
                    }
                    FtePercent=new Number(document.frmStaffMember.txtFtePercentsub.value)
                    //Modification Error message changed
                    if((FtePercent<0)||(FtePercent>100))
                    {
                        alert("Ftepercent Should be between 0 to 100")
                        document.frmStaffMember.txtFtePercentsub.focus()
                        return false
                    }
                    FtePercent=FtePercent.toFixed(2) 
                    document.frmStaffMember.txtFtePercentsub.value=FtePercent 
		        }
		        document.frmStaffMember.txtButton.value="Save Department"
		        document.frmStaffMember.submit() 
            }
		    function CancelDeptClick()
		    {
		        $find('modalDepartment').hide();
		    }
		    
		   
		    
        </script>
	</HEAD>
	<body class="Body" onload="javascript: CheckWhetherToCloseWindow()" onbeforeunload="HandleOnClose()">
		<form id="frmStaffMember" method="post" runat="server" onsubmit ="isPostback =true;">
            <aspAjax:ScriptManager ID="smStaffMembers" runat="server" EnablePageMethods="true" AsyncPostBackTimeout="3600">
            </aspAjax:ScriptManager>
			<INPUT id="txtTitle" type="hidden" name="txtTitle" runat="server">
			<INPUT id="hdnPayscalechanged" type="hidden" name="hdnPayscalechanged" runat="server">
			<INPUT id="txtInitials" type="hidden" name="txtInitials" runat="server"> 
			<INPUT id="txtCloseWindow" type="hidden" name="txtCloseWindow" runat="server">
			<INPUT id="txtButton" type="hidden" name="txtButton" runat="server">
			<INPUT id="txtShowDOB" type="hidden" name="txtShowDOB" runat="server"> 
            <aspAjax:UpdatePanel ID="upStaffDetails" runat="server" UpdateMode="Always">
                <ContentTemplate>
			        <!--Modified by Pravitha on 10 oct 2006||Comments:Tabindex modified-->
			        <table cellSpacing="1" cellPadding="1" width="100%" border="0">
				        <tr>
					        <td align="center"><asp:label id="lblHeader" runat="server" Width="683px" CssClass="Header"></asp:label>&nbsp;
					        </td>
				        </tr>
				        <tr>
					        <td align="center"><asp:label id="lblErrorMessage" runat="server" Width="683px" CssClass="ErrorLabel"></asp:label></td>
				        </tr>
				        <tr>
					        <td align="center">
						        <table style="WIDTH: 659px" cellSpacing="1" cellPadding="1" width="659" border="0">
							        <tr>
							        <!--Modified by sumathy on 6 mar 2009|Pid:25033||width of department modified to 160-->
								        <td style="WIDTH: 148px" align="left"><asp:label id="lblDepartment" runat="server" Width="160px" CssClass="LabelCaption">Department</asp:label></td>
								        <td style="WIDTH: 480px" colSpan="3">
									        <!-- Modified by phani: Changed html select to WEb control dro[p down -->
									        <asp:dropdownlist id="ddDepartment" tabIndex="1" runat="server" Width="486px"></asp:dropdownlist>
								        </td>
							        </tr>
							        <tr>
								        <td style="WIDTH: 148px" align="left"><asp:label id="lblSurName" runat="server" Width="160px" CssClass="LabelCaption"> Surname*</asp:label></td>
								        <td style="WIDTH: 480px" colSpan="3" align="left"><INPUT class="Text" onkeypress="return submitEnter(this,event,'okClicked()')" id="txtSurName"
										        style="WIDTH: 482px" tabIndex="2" type="text" maxLength="30" size="70" name="txtSurName" runat="server"></td>
							        </tr>
							        <tr>
								        <td style="WIDTH: 148px" align="left"><asp:label id="lblFName" runat="server" Width="160px" CssClass="LabelCaption">First name*</asp:label></td>
								        <td style="WIDTH: 480px" colSpan="3" align="left"><INPUT class="Text" onkeypress="return submitEnter(this,event,'okClicked()')" id="txtFName"
										        style="WIDTH: 482px" tabIndex="3" type="text" maxLength="30" size="70" name="txtFName" runat="server"></td>
							        </tr>
							        <tr>
								        <td style="WIDTH: 152px" align="left"><asp:label id="lblPayScale" runat="server" Width="160px" CssClass="LabelCaption">Pay Scale*</asp:label></td>
								        <td style="WIDTH: 480px" colSpan="3" align="left"><asp:dropdownlist id="ddPayScale" tabIndex="4" runat="server" Width="486px" AutoPostBack="True"></asp:dropdownlist></td>
							        </tr>
							        <tr>
								        <td style="WIDTH: 152px" align="left"><asp:label id="lblSpineDate" runat="server" Width="160px" CssClass="LabelCaption">On Date*</asp:label></td>
								        <td style="WIDTH: 184px" align="left">
								            <INPUT class="Text" id="txtSpineDate" onblur="javascript: submitFormStaff();" onmouseover="return submitEnter(this,event,'okClicked()')" style="WIDTH: 114px" tabIndex="5" type="text" maxLength="11" size="9" name="txtSpineDate" runat="server">
									        <IMG id="imgOnDate" onmouseover="javascript: toggleImage('imgOnDate',frmStaffMember.txtImagePath.value+'Calendar_mOver.jpg')"
										        style="WIDTH: 24px; CURSOR: hand; HEIGHT: 22px" onclick="show_calendar('frmStaffMember.txtSpineDate',null,'','DD-MON-YYYY','','Title=Spine On Date;AllowWeekends=Yes;PopupX=300;PopupY=290')"
										        onmouseout="javascript: toggleImage('imgOnDate',frmStaffMember.txtImagePath.value+'Calendar.jpg')" height="22" alt="" src="Images\Calendar.jpg" width="24" align="absMiddle" name="imgOnDate" runat="server">
								        </td>
								        <td style="WIDTH: 173px" align="left"><asp:label id="lblSpine" runat="server" Width="90px" CssClass="LabelCaption"></asp:label></td>
								        <td style="WIDTH: 182px" align="left"><asp:dropdownlist id="ddSpine" tabIndex="6" runat="server" Width="159px"></asp:dropdownlist></td>
							        </tr>
							        <tr>
								        <td style="WIDTH: 148px" align="left"><asp:label id="lblIncDate" runat="server" Width="160px" CssClass="LabelCaption">Increment Date</asp:label></td>
								        <td style="WIDTH: 184px" align="left"><INPUT class="Text" id="txtIncDate" onmouseover="return submitEnter(this,event,'okClicked()')"
										        style="WIDTH: 114px" tabIndex="7" type="text" maxLength="11" size="12" name="txtIncDate" runat="server">
									        <IMG id="imgIncDate" onmouseover="javascript: toggleImage('imgIncDate',frmStaffMember.txtImagePath.value+'Calendar_mOver.jpg')"
										        style="WIDTH: 24px; CURSOR: hand; HEIGHT: 22px" onclick="show_calendar('frmStaffMember.txtIncDate',null,'','DD-MON-YYYY','','Title=Increment Date;AllowWeekends=Yes;PopupX=300;PopupY=290')"
										        onmouseout="javascript: toggleImage('imgIncDate',frmStaffMember.txtImagePath.value+'Calendar.jpg')"
										        height="22" width="24" align="absMiddle" name="imgIncDate"
										        runat="server"></td>
								        <td style="WIDTH: 173px" align="left"><asp:label id="lblDP" runat="server" Width="152px" CssClass="LabelCaption">Maximum Spine</asp:label></td>
								        <td style="WIDTH: 182px" align="left"><asp:dropdownlist id="ddDP" tabIndex="15" runat="server" Width="159px"></asp:dropdownlist></td>
							        </tr>
							        <tr>
								        <td style="WIDTH: 148px; height: 26px;" align="left"><asp:label id="lblStaffNo" runat="server" Width="160px" CssClass="LabelCaption">Staff Number*</asp:label></td>
								        <td style="WIDTH: 184px; height: 26px;" align="left"><INPUT class="Text" onkeypress="return submitEnter(this,event,'okClicked()')" id="txtStaffNo"
										        style="WIDTH: 113px" tabIndex="9" type="text" maxLength="12" size="18" name="txtStaffNo" runat="server">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
								        <td style="WIDTH: 173px; height: 26px;" align="left"><asp:label id="lblNI" runat="server" Width="90px" CssClass="LabelCaption">NI Number</asp:label></td>
								        <td style="WIDTH: 182px; height: 26px;" align="left"><INPUT class="Text" onkeypress="return submitEnter(this,event,'okClicked()')" id="txtNINo"
										        style="WIDTH: 154px" tabIndex="10" type="text" maxLength="12" name="txtNINo" runat="server"></td>
							        </tr>
							        <tr>
								        <td style="WIDTH: 148px" align="left"><asp:label id="lblStartDate" runat="server" Width="160px" CssClass="LabelCaption">Start Date</asp:label></td>
								        <td style="WIDTH: 184px" align="left"><INPUT class="Text" id="txtStartDate" style="WIDTH: 114px" tabIndex="11"
										        type="text" maxLength="12" size="12" name="txtStartDate" runat="server">&nbsp;<IMG id="imgStartDate" onmouseover="javascript: toggleImage('imgStartDate',frmStaffMember.txtImagePath.value+'Calendar_mOver.jpg')"
										        style="WIDTH: 24px; CURSOR: hand; HEIGHT: 22px" onclick="show_calendar('frmStaffMember.txtStartDate',null,'','DD-MON-YYYY','','Title=Start Date;AllowWeekends=Yes;PopupX=300;PopupY=290')"
										        onmouseout="javascript: toggleImage('imgStartDate',frmStaffMember.txtImagePath.value+'Calendar.jpg')" height="22" alt="" src="Images\Calendar.jpg" width="24" align="absMiddle"
										        name="imgStartDate" runat="server"></td>
								        <td style="WIDTH: 173px" align="left"><asp:label id="lblEndDate" runat="server" Width="90px" CssClass="LabelCaption">End Date</asp:label></td>
								        <td style="WIDTH: 182px" align="left"><INPUT class="Text" onkeypress="return submitEnter(this,event,'okClicked()')" id="txtEndDate"
										        style="WIDTH: 114px" tabIndex="12" type="text" maxLength="12" size="14" name="txtEndDate"
										        runat="server">&nbsp;<IMG id="imgEndDate" onmouseover="javascript: toggleImage('imgEndDate',frmStaffMember.txtImagePath.value+'Calendar_mOver.jpg')"
										        style="WIDTH: 24px; CURSOR: hand; HEIGHT: 22px" onclick="show_calendar('frmStaffMember.txtEndDate',null,'','DD-MON-YYYY','','Title=End Date;AllowWeekends=Yes;PopupX=300;PopupY=290')"
										        onmouseout="javascript: toggleImage('imgEndDate',frmStaffMember.txtImagePath.value+'Calendar.jpg')" height="22"
										        width="24" align="absMiddle" name="imgEndDate" runat="server"></td>
							        </tr>
							        <tr>
								        <td style="WIDTH: 148px" align="left"><asp:label id="lblEmail" runat="server" Width="160px" CssClass="LabelCaption"> Email</asp:label></td>
								        <td style="WIDTH: 480px" colSpan="3" align="left"><INPUT class="Text" onkeypress="return submitEnter(this,event,'okClicked()')" id="txtEmail"
										        style="WIDTH: 480px" tabIndex="13" type="text" maxLength="40" size="68" name="txtEmail" runat="server"></td>
							        </tr>
							        <tr>
								        <td style="WIDTH: 148px" align="left"><asp:label id="lblLW" runat="server" Width="160px" CssClass="LabelCaption">Use London Weighting</asp:label></td>
								        <td style="WIDTH: 184px" align="left"><INPUT id="chkLW" style="WIDTH: 16px" tabIndex="14" type="checkbox" name="chkLW"
										        runat="server"></td>
								        <td style="WIDTH: 173px" align="left"><asp:label id="lblPhone" runat="server" Width="90px" CssClass="LabelCaption">Position Number</asp:label></td>
								        <td style="WIDTH: 182px" align="left"><INPUT class="Text" onkeypress="return submitEnter(this,event,'okClicked()')" id="txtPhone"
										        style="WIDTH: 154px" tabIndex="8" type="text" maxLength="50" size="17" name="txtPhone" runat="server"></td>
							        </tr>
							        <tr>
								        <td style="WIDTH: 148px; height: 26px;" align="left"><asp:label id="lblOA" runat="server" Width="160px" CssClass="LabelCaption">Use Other Allowances</asp:label></td>
								        <td style="WIDTH: 184px; height: 26px;" align="left"><INPUT id="chkOA" style="WIDTH: 16px; HEIGHT: 18px"  tabIndex="16" type="checkbox" name="chkOA" runat="server"></td>
								        <td style="WIDTH: 173px; height: 26px;" align="left"><asp:label id="lblOtherAllowances" runat="server" Width="118px" Height="3px" CssClass="LabelCaption">Other Allowances</asp:label></td>
								        <td style="WIDTH: 182px; height: 26px;" align="left"><INPUT class="Text" onkeypress="return submitEnter(this,event,'okClicked()')" id="txtOA"
										        style="WIDTH: 154px" tabIndex="17" type="text" maxLength="12" size="17" name="txtOA" runat="server"></td>
							        </tr>
							        <tr>
								        <td style="WIDTH: 148px" align="left"><asp:label id="lblDOB" runat="server" Width="160px" CssClass="LabelCaption">Date Of Birth</asp:label></td>
								        <td style="WIDTH: 184px" align="left"><INPUT class="Text" id="txtDOB" style="WIDTH: 114px; HEIGHT: 20px" tabIndex="18" type="text"
										        maxLength="11" size="9" name="txtDOB" runat="server">&nbsp;<IMG id="imgDOB" onmouseover="javascript: toggleImage('imgDOB',frmStaffMember.txtImagePath.value+'Calendar_mOver.jpg')"
										        style="WIDTH: 24px; CURSOR: hand" onclick="show_calendar('frmStaffMember.txtDOB',null,'','DD-MON-YYYY','','Title=Date Of Birth;AllowWeekends=Yes;PopupX=300;PopupY=290')"
										        onmouseout="javascript: toggleImage('imgDOB',frmStaffMember.txtImagePath.value+'Calendar.jpg')" height="22" alt="" src="Images\Calendar.jpg" width="24"
										        align="absMiddle" name="imgDOB" runat="server"></td>
								        <td style="WIDTH: 173px" align="left"><asp:label id="lblTowardsFTE" runat="server" Width="115px" CssClass="LabelCaption">Towards FTE</asp:label></td>
								        <td style="WIDTH: 182px" align="left"><INPUT id="chkFTE" style="WIDTH: 19px; HEIGHT: 15px" type="checkbox" runat="server"></td>
							        </tr>
							        <!--Modified by Pravitha on 15 Dec 2006||PID:17859||Comments||Maxlength set to 50-->
							        <tr>
								        <td style="WIDTH: 148px" align="left"><asp:label id="lblJeSID" runat="server" Width="160px" CssClass="LabelCaption">JeS ID</asp:label></td>
								        <td style="WIDTH: 272px" colSpan="2" align="left"><asp:textbox id="txtJesID" tabIndex="20" runat="server" Width="112px" MaxLength="50"></asp:textbox>&nbsp;<IMG id="imgUpdateJesID" onmouseover="javascript: toggleImage('imgUpdateJesID',frmStaffMember.txtImagePath.value+'Get_m.jpg')"
										        style="CURSOR: hand; HEIGHT: 29px" onclick="GetJesPersonIDClicked()" tabIndex="20" onmouseout="javascript: toggleImage('imgUpdateJesID',frmStaffMember.txtImagePath.value+'Get.jpg')" height="24" src="Images\Submit.jpg" width="71" align="absMiddle"
										        runat="server"></td>
								        <td style="WIDTH: 182px"></td>
							        </tr>
						        </table>
					        </td>
				        </tr>
				        <tr>
					        <td align="center">&nbsp;<IMG id="Submit" onmouseover="javascript: toggleImage('Submit',frmStaffMember.txtImagePath.value+'Submit_mover.jpg')"
							        style="CURSOR: hand; HEIGHT: 29px" onclick="javascript: okClicked();" tabIndex="21" onmouseout="javascript: toggleImage('Submit',frmStaffMember.txtImagePath.value+'Submit.jpg')"
							        height="24" width="71" runat="server"> <IMG id="Cancel" onmouseover="javascript: toggleImage('Cancel',frmStaffMember.txtImagePath.value+'Close_mover.jpg')"
							        style="CURSOR: hand; HEIGHT: 29px" onclick="javascript: cancelClicked();" tabIndex="22" onmouseout="javascript: toggleImage('Cancel',frmStaffMember.txtImagePath.value+'Close.jpg')"
							        height="24" width="71" runat="server">
                                </td>
				        </tr>
                        <tr>
                            <td align="left">
                                <asp:LinkButton ID="lnkPayscaleGrade" runat="server" CssClass="LinkStyle"/>
                            </td>
                        </tr>
				        <tr>
				            <td align="center">
				                <asp:Panel ID="pnlPayscaleGrade" runat="server" ScrollBars="Vertical">
				                    <table align="center">
				                        <tr>
				                            <td align="right">
			                                    <IMG class="image" id="imgPayscaleGrades" onmouseover="javascript: toggleImage('imgPayscaleGrades',frmStaffMember.txtImagePath.value+'Add_new_normal_mover.jpg')"
						                        style="CURSOR: hand" onclick="javascript: AddPayscaleGradeClick();" onmouseout="javascript: toggleImage('imgPayscaleGrades',frmStaffMember.txtImagePath.value+'Add_new_normal.jpg')"
						                        src="" runat="server">
				                            </td>
				                        </tr>
				                        <tr>
				                            <td>
                                                <asp:GridView id="gvPayscaleGrades" runat="server" Width="50%" AutoGenerateColumns="False" RowStyle-CssClass ="TableData" HeaderStyle-CssClass="TableHeader">
							                    <%--<ItemStyle CssClass="TableData"></ItemStyle>
							                    <HeaderStyle CssClass="TableHeader"></HeaderStyle>--%>
							                        <Columns>
							                        
							                        
								                        <asp:TemplateField>
									                        <ItemTemplate>
										                        <asp:Label id="lblRemove" runat="server" CssClass='<%# DataBinder.Eval(Container.DataItem, "CssRow") %>' Width="79px" Text='<%# DataBinder.Eval(Container.DataItem, "RemoveLink") %>'>
										                        </asp:Label>
									                        </ItemTemplate>
								                        </asp:TemplateField>
								                        <asp:TemplateField  HeaderText="Payscale">
									                        <ItemTemplate>
										                        <asp:Label id="lblPayscale" runat="server" CssClass='<%# DataBinder.Eval(Container.DataItem, "CssRow") %>' Width="197px" Text='<%# DataBinder.Eval(Container.DataItem, "Payscale") %>'>
										                        </asp:Label>
									                        </ItemTemplate>
								                        </asp:TemplateField>
								                        <asp:TemplateField HeaderText="Action Date">
									                        <ItemTemplate>
										                        <asp:Label id="lblActionDate" runat="server" Width="102px" CssClass='<%# DataBinder.Eval(Container.DataItem, "CssRow") %>' Text='<%# format(DataBinder.Eval(Container.DataItem, "ActionStart"),"dd-MMM-yyyy") %>'>
										                        </asp:Label>
									                        </ItemTemplate>
								                        </asp:TemplateField>
								                        <asp:TemplateField HeaderText="Spine Point">
									                        <ItemTemplate>
										                        <asp:Label id="lblSpine" runat="server" Width="78px" CssClass='<%# DataBinder.Eval(Container.DataItem, "CssRow") %>' Text='<%# DataBinder.Eval(Container.DataItem, "Spine") %>'>
										                        </asp:Label>
									                        </ItemTemplate>
								                        </asp:TemplateField>
								                        <asp:TemplateField HeaderText="Maximum Spine">
									                        <ItemTemplate>
										                        <asp:Label id="lblMaxSpine" runat="server" Width="133px" CssClass='<%# DataBinder.Eval(Container.DataItem, "CssRow") %>' Text='<%# DataBinder.Eval(Container.DataItem, "MaxSpine") %>'>
										                        </asp:Label>
									                        </ItemTemplate>
								                        </asp:TemplateField>
								                        <asp:TemplateField HeaderText="Increment Date">
									                        <ItemTemplate>
									                            <!--Modified by Lakshmanan | 17-Sep-08 | PID:23670 | Modified the column name-->
										                        <asp:Label id="lblIncrementDate" runat="server" Width="133px" CssClass='<%# DataBinder.Eval(Container.DataItem, "CssRow") %>' Text='<%# format(ChkNull(DataBinder.Eval(Container.DataItem, "IncrementDate")),"dd-MMM-yyyy") %>'>
										                        </asp:Label>
									                        </ItemTemplate>
								                        </asp:TemplateField>
							                        </Columns>
						                        </asp:GridView>				                        
				                            </td>
				                        </tr>
				                    </table>
				                </asp:Panel>
                                <aspToolkit:CollapsiblePanelExtender ID="cpePayscaleGrade" runat="server" TextLabelID="lnkPayscaleGrade" ExpandedText="Hide Payscale Grades" CollapsedText="Show Payscale Grades" TargetControlID="pnlPayscaleGrade" ExpandControlID="lnkPayscaleGrade" CollapseControlID="lnkPayscaleGrade" CollapsedSize="0" ExpandedSize="0" AutoCollapse="false" AutoExpand="false" ExpandDirection="Vertical"></aspToolkit:CollapsiblePanelExtender>
				            </td>
				        </tr>
                        <tr>
                            <td align="left">
                                <asp:LinkButton ID="lnkDepartment" runat="server" CssClass="LinkStyle"/>
                            </td>
                        </tr>
				        <tr>
				            <td align="center">
				                <asp:Panel ID="pnlShowDepartments" runat="server" ScrollBars="Vertical" >
				                    <table align="center">
				                        <tr>
				                            <td align="right">
			                                    <IMG class="image" id="imgDepartment" onmouseover="javascript: toggleImage('imgDepartment',frmStaffMember.txtImagePath.value+'Add_new_normal_mover.jpg')"
						                        style="CURSOR: hand" onclick="javascript: AddDepartmentClick();" onmouseout="javascript: toggleImage('imgDepartment',frmStaffMember.txtImagePath.value+'Add_new_normal.jpg')"
						                        src="" runat="server">
				                            </td>
				                        </tr>
				                        <tr>
					                        <td >
					                            <asp:datagrid id="dgDepartment" runat="server" Width="90%" AutoGenerateColumns="False">
							                        <ItemStyle CssClass="TableData"></ItemStyle>
							                        <HeaderStyle CssClass="TableHeader"></HeaderStyle>
							                        <Columns>
								                        <asp:TemplateColumn>
									                        <ItemTemplate>
										                        <asp:Label id="lblDepRemove" runat="server" CssClass='<%# DataBinder.Eval(Container.DataItem, "CssRow") %>' Text='<%# DataBinder.Eval(Container.DataItem, "RemoveLink") %>'>
										                        </asp:Label>
									                        </ItemTemplate>
								                        </asp:TemplateColumn>
								                        <asp:TemplateColumn HeaderText="Department">
									                        <ItemTemplate>
										                        <asp:Label id=lblDepDepartment runat="server" Width="350px" CssClass='<%# DataBinder.Eval(Container.DataItem, "CssRow") %>' Text='<%# DataBinder.Eval(Container.DataItem, "DepartmentLink") %>'>
										                        </asp:Label>
									                        </ItemTemplate>
								                        </asp:TemplateColumn>
								                        <asp:TemplateColumn HeaderText="FTEPercent">
									                        <ItemTemplate>
										                        <asp:Label id="lblDepFTE" runat="server" Width="100px" CssClass='<%# DataBinder.Eval(Container.DataItem, "CssRow") %>' Text='<%# DataBinder.Eval(Container.DataItem, "FtePercent") %>'>
										                        </asp:Label>
									                        </ItemTemplate>
								                        </asp:TemplateColumn>
							                        </Columns>
						                        </asp:datagrid>&nbsp;&nbsp;
					                        </td>
				                        </tr>
				                    </table>
				                </asp:Panel>
                                <aspToolkit:CollapsiblePanelExtender ID="cpeDepartment" runat="server" TextLabelID="lnkDepartment" ExpandedText="Hide Department" CollapsedText="Show Department" TargetControlID="pnlShowDepartments" ExpandControlID="lnkDepartment" CollapseControlID="lnkDepartment" CollapsedSize="0" ExpandedSize="0" AutoCollapse="false" AutoExpand="false" ExpandDirection="Vertical"></aspToolkit:CollapsiblePanelExtender>
				            </td>
				        </tr>
			        </table>

	                <asp:LinkButton ID="linkPayscaleGrade" runat="server" />
                    <asp:Panel ID="pnlPayscale" runat="server" CssClass="modalPopup" style="display:none; width:auto;">
                        <table>
				            <tr>
					            <td align="center" colspan="3"><asp:label id="lblPayGradeHeader" runat="server" Width="536px" CssClass="Header"></asp:label></td>
				            </tr>
				            <tr>
					            <td align="center" colspan="3"><asp:label id="lblPayGradeError" runat="server" Width="520px" CssClass="ErrorLabel"></asp:label></td>
				            </tr>
				            <tr>
					            <td align="center" colspan="3">
						            <table id="Table2" style="WIDTH: 568px; HEIGHT: 138px" border="0">
							            <tr>
								            <td style="WIDTH: 101px" align="left"><asp:label id="lblPayGradesub" runat="server" Width="120px" CssClass="LabelCaption">Pay Scale*</asp:label></td>
								            <td style="WIDTH: 411px" colspan="3" align="left"><asp:dropdownlist id="ddpaygradesub" tabIndex="1" runat="server" Width="450px" AutoPostBack="false" ></asp:dropdownlist></td>
							            </tr>
							            <tr>
								            <td style="WIDTH: 101px" align="left"><asp:label id="lblActionDtsub" runat="server" Width="120px" CssClass="LabelCaption">Action Date*</asp:label></td>
								            <td style="WIDTH: 467px" align="left">
								                <asp:textbox id="txtActionDtsub" tabIndex="2" runat="server" Width="104px" AutoPostBack="false" />
								                <!--Modification 1 Modified by sumathy on 01-June-2009||PID:26303||comments:CallFunction used -->
								                <img id="imgActionDatesub" onmouseover="javascript: toggleImage('imgActionDatesub',frmStaffMember.txtImagePath.value+'Calendar_mOver.jpg')"
								                 style="WIDTH: 24px; CURSOR: hand; HEIGHT: 22px" onclick="show_calendar('frmStaffMember.txtActionDtsub',null,'','DD-MON-YYYY','','Title=Spine On Date;AllowWeekends=Yes;PopupX=300;PopupY=290;CallFunction=OnPayGradeClick;')" onmouseout="javascript: toggleImage('imgActionDatesub',frmStaffMember.txtImagePath.value+'Calendar.jpg')"
										        height="22" width="24" align="absMiddle" name="imgActionDate" runat="server"/>
								            </td>
								            <td style="WIDTH: 123px" align="left"><asp:label id="lblSpinesub" runat="server" Width="160px" CssClass="LabelCaption"></asp:label></td>
								            <td align="left"><asp:dropdownlist id="ddspinesub" tabIndex="3" runat="server" Width="136px"></asp:dropdownlist></td>
							            </tr>
							            <tr>
								            <td style="WIDTH: 101px" width="101" align="left"><asp:label id="lblDisPointsub" runat="server" Width="120px" Height="13px" CssClass="LabelCaption">Maximum Spine</asp:label></td>
								            <td style="HEIGHT: 1px; width: 467px;" align="left"><asp:dropdownlist id="dddispointsub" tabIndex="4" runat="server" Width="130px"></asp:dropdownlist></td>
								            <td style="WIDTH: 123px" align="left"><asp:Label ID="lblIncDatesub" runat="server" CssClass="LabelCaption" Width="160px">Increment Date</asp:Label></td>
								            <td style="WIDTH: 411px" align="left">
								            <!--PID:28120-->
								            <asp:textbox id="txtIncDatesub" tabIndex="2" runat="server" Width="104px" />
								            <img id="imgIncDatesub" onmouseover="javascript: toggleImage('imgIncDatesub',frmStaffMember.txtImagePath.value+'Calendar_mOver.jpg')"
										            style="WIDTH: 24px; CURSOR: hand; HEIGHT: 22px" onclick="show_calendar('frmStaffMember.txtIncDatesub',null,'','DD-MON-YYYY','','Title=Spine On Date;AllowWeekends=Yes;PopupX=300;PopupY=290;CallFunction=DoNothing;')" onmouseout="javascript: toggleImage('imgIncDatesub',frmStaffMember.txtImagePath.value+'Calendar.jpg')"
										            height="22" width="24" align="absMiddle" name="imgIncDatesub" runat="server"/></td>
							            </tr>
							            <tr>
								            <td align="center" colspan="4">
								                <img id="imgAddPayscale" onmouseover="javascript: toggleImage('imgAddPayscale',frmStaffMember.txtImagePath.value+'Add_new_mover.jpg');"
										        style="WIDTH: 40px; CURSOR: hand; HEIGHT: 29px" onclick="javascript: SavePayscaleGradeClick();" onmouseout="javascript: toggleImage('imgAddPayscale',frmStaffMember.txtImagePath.value+'Add_new.jpg');"
										        height="25" width="31" runat="server">
										        <img id="ImgClosePayscale" onmouseover="javascript: toggleImage('ImgClosePayscale',frmStaffMember.txtImagePath.value+'Close_mover.jpg');"
										        style="WIDTH: 71px; CURSOR: hand; HEIGHT: 29px" onclick="javascript: CancelPayscaleGradeClick();" onmouseout="javascript: toggleImage('ImgClosePayscale',frmStaffMember.txtImagePath.value+'Close.jpg');"
										        height="24" width="62" runat="server">
								            </td>
							            </tr>
						            </table>
					            </td>
				            </tr>
                        </table>
                    </asp:Panel>
		            <aspToolkit:ModalPopupExtender ID="modalPayscaleGrade" X="250" Y="300" runat="server" TargetControlID="linkPayscaleGrade" PopupControlID="pnlPayscale" BackgroundCssClass="modalBackground" DropShadow="true"/>        

	                <asp:LinkButton ID="lnkDepartments" runat="server" />
                    <asp:Panel ID="pnlDepartment" runat="server" CssClass="modalPopup" style="display:none; width:auto;">
			            <table>
				            <tr>
					            <td align="center"><asp:label id="lblDeptHeader" runat="server" CssClass="Header" Width="336px"></asp:label></td>
				            </tr>
				            <tr>
					            <td align="center"><asp:label id="lblDeptErrLbl" runat="server" CssClass="ErrorLabel" Width="524px"></asp:label></td>
				            </tr>
				            <tr>
					            <td align="center" colspan="2">
						            <table border="0">
							            <tr>
								            <td align="left" colSpan="2"><asp:label id="lblDepartmentsub" runat="server" CssClass="LabelCaption">Department*</asp:label></td>
								            <td align="left"><asp:dropdownlist id="ddDepartmentsub" tabIndex="1" runat="server" CssClass="labeltext"></asp:dropdownlist></td>
							            </tr>
							            <tr>
								            <td align="left" colSpan="2"><asp:label id="lblFtePersub" runat="server" CssClass="LabelCaption">FTEPercent</asp:label></td>
								            <td align="left"><asp:textbox id="txtFtePercentsub" tabIndex="2" Width="120px" runat="server" CssClass="labeltext" MaxLength="6"></asp:textbox></td>
							            </tr>
							            <tr>
								            <td align="center" colSpan="3">
								                <IMG id="imgAddDept" onmouseover="javascript: toggleImage('imgAddDept',frmStaffMember.txtImagePath.value+'Add_new_mOver.jpg')"
									            style="WIDTH: 40px; CURSOR: hand; HEIGHT: 29px" onclick="javascript: SaveDeptClick();" onmouseout="javascript: toggleImage('imgAddDept',frmStaffMember.txtImagePath.value+'Add_new.jpg')"
									            height="25" width="31" runat="server"> 
									            <IMG id="imgCloseDept" onmouseover="javascript: toggleImage('imgCloseDept',frmStaffMember.txtImagePath.value+'Close_mover.jpg')"
									            style="CURSOR: hand" onclick="javascript: CancelDeptClick();" onmouseout="javascript: toggleImage('imgCloseDept',frmStaffMember.txtImagePath.value+'Close.jpg')"
									            height="29" width="71" runat="server">
								            </td>
							            </tr>
						            </table>
                                    </td>
				            </tr>
			            </table>
                    </asp:Panel>
		            <aspToolkit:ModalPopupExtender ID="modalDepartment" X="100" Y="110" runat="server" TargetControlID="lnkDepartments" PopupControlID="pnlDepartment" BackgroundCssClass="modalBackground" DropShadow="true"/>        
			        
			        <input id="txtImagePath" type="hidden" name="txtImagePath" runat="server" /> 
			        <input id="txtFilterLink" type="hidden" name="txtFilterLink" runat="server" /> 
			        <input id="txtOriginalSpDate" type="hidden" name="txtOriginalSpDate" runat="server" />
                    <input id="hdnIsEdit" type="hidden" name="hdnIsEdit" runat="server" />
                    <input id="hdnpayscalegradeid" type="hidden" name="hdnpayscalegradeid" runat="server" />
                    <input id="hdnStaffDeptID" type="hidden" name="hdnStaffDeptID" runat="server" />
                    <input id="hdnPayscalechangedsub" type="hidden" name="hdnPayscalechanged" runat="server" />
                    <input id="txtOriginalSpDatesub" type="hidden" name="txtOriginalSpDatesub" runat="server"/>		
                    <INPUT id="txtFilterVal" name="txtFilterVal" type="hidden" runat="server">
                    <INPUT id="hdnPayGradeSpine" name="hdnPayGradeSpine" type="hidden" runat="server">                
                    <INPUT id="hdnPayGradeSpineDP" name="hdnPayGradeSpineDP" type="hidden" runat="server">                
                    <INPUT id="txtFilterBy" name="txtFilterBy" type="hidden" runat="server">                
                    <INPUT id="hdnSpineOnDatechanged" runat="server" type="hidden" name="hdnSpineOnDatechanged"><INPUT style="Z-INDEX: 107; LEFT: 216px; POSITION: absolute; TOP: 8px" type="hidden" name="hdnPayscalechanged">
			    </ContentTemplate>
            </aspAjax:UpdatePanel>
            
            <!--Modified by Sakshi on 29 Sept 2009||Comments:Hidden control added for storing the value of Department-->
            <input id="hdnDept" type="hidden" runat="server"/>
		</form>
	</body>
</html>
