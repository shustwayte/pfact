USE [pfact_t1]
GO
/****** Object:  StoredProcedure [dbo].[p_rpt_Je_S_Summary_Prj_Partners_Overall_Total]    Script Date: 12/09/2018 15:26:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER PROC [dbo].[p_rpt_Je_S_Summary_Prj_Partners_Overall_Total]
(
@prj_id int
)
		--		exec [dbo].[p_rpt_Je_S_Summary_Prj_Partners_Overall_Total] 10440

as

begin

	declare @res table
	(
	partner varchar(300),
	costtype varchar(300),
	fec decimal(20,2),
	rcContribution decimal(20,2)
	)

	insert into @res
	select 
	name, 
	costtype,
		Cost, 
		income
	
		FROM [pfact_t1].[dbo].[vw_UOE_P_PRJ_Budget_Table_costdetail_uoe_RPT] (nolock) 
		where ProjectID = @prj_id
	--	order by 1,2

		insert into @res
		SELECT DISTINCT 
		'UOE','',
		--StaffTypeDesc, 
		convert(decimal(20,2),sum(val)) as val, 
		convert(decimal(20,2),sum(sponval)) as sponval

		 from 
		 (
			Select
			rowid,
			prj_id,
			rectype,
			val,
				case
				when matchedfund = 'on' then 0
				WHEN ISNULL(finaladjust,'') = '' THEN 
				subincome
				else
				Sponval end as Sponval,
				ChargeType,
				StaffTypeDesc
			from
			UOE_T1_CSV_DATA  (nolock) 
			where 
			prj_id =@prj_id
		)
		 a


			where prj_id  = @prj_id 
		
	--	select * from @res
	if ( select COUNT(*) from @res ) > 1
		begin

		select SUM(fec) as fec, SUM(rcContribution) as rcContribution
		from @res

		end



			

		
			

end



