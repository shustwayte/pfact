USE [pfact_t1]
GO
/****** Object:  StoredProcedure [dbo].[p_T1_CSV_FEC_SUMM]    Script Date: 12/09/2018 15:28:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



ALTER PROC [dbo].[p_T1_CSV_FEC_SUMM]
(
@prjid int,
@username varchar(100)
)
AS

begin

	---		 exec [p_T1_CSV_FEC_SUMM] 10477 , 'sh444'

	DECLARE @RES TABLE
	(
		[Id] [int] NULL,
		[Code] [varchar](20) NULL,
		[Project_Name] [varchar](100) NULL,
		[DeadLine] [datetime] NULL,
		[Status] [int] NULL,
		[StatusDesc] [varchar](25) NULL,
		[Start_Date] [datetime] NULL,
		[End_Date] [datetime] NULL,
		[VAT_Checked] [bit] NULL,
		[CrossFaculty] [bit] NULL,
		[Contact_Person] [varchar](100) NULL,
		[Telephone] [varchar](25) NULL,
		[Email] [varchar](40) NULL,
		[Department] [int] NULL,
		[DepartmentDesc] [varchar](60) NULL,
		[SubActivity] [int] NULL,
		[SubActivityDesc] [varchar](50) NULL,
		[Activity] [int] NULL,
		[ActivityDesc] [varchar](50) NULL,
		[PIID] [int] NULL,
		[PIStaffType] [int] NULL,
		[PIName] [varchar](100) NULL,
		[JesDocID] [varchar](25) NULL,
		[FacultyCode] [varchar](15) NULL,
		[FacultyDesc] [varchar](50) NULL,
		[EXT] [varchar](100) NULL,
		[CREATEDBY] [varchar](101) NULL,
		[Created_On] [datetime] NULL,
		[MODIFIEDBY] [varchar](101) NULL,
		[Modified_On] [datetime] NULL,
		[ClinicalTrial] [int] NULL,
		[Faculty] [int] NULL,
		[usespinepoint] [int] NULL,
		[Long_Title] [varchar](1000) NULL
	)

	insert into @RES
	EXEC ProjectDetails @prjid,0



	DECLARE @TEMPFUND TABLE(
		[Id] [int] NULL,
		[Funder] [varchar](100) NULL,
		[FunderID] [int] NULL,
		[Address] [varchar](203) NULL,
		[FunderAddress] [varchar](152) NULL,
		[PostCode] [varchar](50) NULL,
		[FunderScheme] [int] NULL,
		[FunderCode] [varchar](20) NULL,
		[FundingSource] [varchar](50) NULL,
		[ExceptionCost] [float] NULL,
		[Income_Template] [int] NULL,
		[TemplateCode] [varchar](40) NULL,
		[TemplateDesc] [varchar](50) NULL,
		[FundingSchemeDesc] [varchar](6000) NULL,
		[jesSchemeID] [varchar](50) NULL,
		[jesDocumentSubType] [varchar](100) NULL,
		[EU_Activity] [varchar](200) NULL,
		[blnAllowDiscretionaryPoint] [bit] NULL,
		[blnAllowMulptiplier] [bit] NULL,
		[Amount] [float] NULL,
		[IsPrimaryFunder] [bit] NULL,
		[Income_Type] [int] NULL,
		[Recategorisetoconsumable] [bit] NULL,
		[FUNDEDPERCENTAGE] [float] NULL,
		[INCOMETEMPLATEPERCENTAGE] [float] NULL
	)
	
	INSERT INTO @TEMPFUND
		EXEC ProjectIncomeTemplates @prjid,0

		/*
		declare @fig table
		(
		price varchar(100),
		income varchar(100)
		)
		*/
		
		--	exec [pfact_t1].[dbo].[UOE_P_PRJ_Budget_Table_costdetail_uoe_TOTAL]  10388
		--exec [pfact_t1].[dbo].[UOE_P_PRJ_Budget_Table_costdetail_TOTAL] @prjid

		declare @sumtot table
		(
		staffcost float,
		facilitiesequip float,
		nonstff float,
		indirectcosts float,
		intersourcesum float,
		income float,
		surplusdef float
		)

		insert into @sumtot
		EXEC ProjectSummaryTotal @prjid,1,1,1

/*
		insert into @fig
		select 
		
		convert(varchar,isnull(dbo.UFFormatToMoney(
			sum(ft)
				),0))
				, 
		convert(varchar,isnull(dbo.UFFormatToMoney(				
			SUM(income)
			),0))
		
		from (
			
			select 
			ISNULL(staffcost,0) +
			ISNULL(facilitiesequip,0) +
			ISNULL(nonstff,0) +
			ISNULL(indirectcosts,0) as ft, 
			ISNULL(income,0) as income
			FROM @sumtot

			union all

			Select b.Cost, c.income
			FROM otherinstitutioncosttype a
				INNER JOIN ProjectOtherInstitutionCost b
					ON 
					a.ID=b.otherinstitutioncosttype 
					LEFT OUTER JOIN [dbo].[UOE_costdetails_UOE_Other_Income] c
						on c.rec = b.id
					WHERE ProjectID= @prjid
			) a
			*/
	--------------------------------------
	declare @rownum table (
		which varchar(20),
		rownum int
	)

	insert into @rownum
	select which, rownum	
		 from T1_CSV_ROW_COUNT
		 where prj_id = @prjid


--------------------------------------
			declare @FUNDING_TEMP VARCHAR(400)

			SET @FUNDING_TEMP = (
			                             
							 SELECT 
							 /*
							 Funders.Description as Funder,
							 ISNULL(Project_Proposal_Income_Template.Funding_Scheme,'') as FunderScheme, 
							 ISnull( Funders.Code,'') as FunderCode,                                    
							 LUP_Funding_source.Description as FundingSource,
							*/
							 LUP_Income_Template.TemplateCode
							 FROM Org_Details, Project_Proposal_Income_Template                                      
							 INNER JOIN Income_Template_Funders ON Project_Proposal_Income_Template.Income_Template_Funder=Income_Template_Funders.ID                                      
							 INNER JOIN LUP_Income_Template ON Income_Template_Funders.Income_Template=LUP_Income_Template.ID                                      
							 INNER JOIN Funders ON Income_Template_Funders.Funder=Funders.ID                                     
							 INNER JOIN LUP_Funding_Source ON Funders.FundingSource= LUP_Funding_source.ID                                     
							WHERE Project_Proposal_Income_Template.Project_Proposal= @prjid       

						
							) 




			select 
				'BID' +

						RIGHT('00000000' +
						replace(
							b.t1bid 
							, 'BID',''),6)
				
							as BIDNUMBER,
						A.id as PFACT_ID,

						'01' as ENTITY,

			( select convert(decimal(20,2),fec) from T1_CSV_TOTALS where prj_id = @prjid ) as FEC,
			( select convert(decimal(20,2),price) from T1_CSV_TOTALS where prj_id = @prjid ) AS PRICE,
			( select convert(decimal(20,2),sponcont) from T1_CSV_TOTALS where prj_id = @prjid ) AS INCOME,

			( select SUM(match_fund) from T1_CSV_FILE_TOTALS where pfact_id = @prjid ) AS MATCHED_FUND,
			--( select SUM(cc_cost) from T1_CSV_FILE_TOTALS where pfact_id = @prjid ) AS COLL_CONT,

			case
			when 
				( select convert(decimal(20,2),sponcont) from T1_CSV_TOTALS where prj_id = @prjid ) --AS INCOME
				>
				( select convert(decimal(20,2),fec) from T1_CSV_TOTALS where prj_id = @prjid ) --as FEC
				then 
				abs( ( select convert(decimal(20,2),COLL_CONT) from  T1_FEC_SUMM_FIG where pfact_id = @prjid ) ) * -1
				else
					abs( ( select convert(decimal(20,2),COLL_CONT) from  T1_FEC_SUMM_FIG where pfact_id = @prjid ) ) end AS COLL_CONT,
			
			--( select convert(decimal(20,2),fec) from T1_CSV_TOTALS where prj_id = @prjid ) AS FEC_CONT,
			( select convert(decimal(20,2),FEC_CONT) from  T1_FEC_SUMM_FIG where pfact_id = @prjid ) AS FEC_CONT,

			ISNULL( ( Select 
			case
			when rownum between -0.50 and 0.50 then 0.00 
			else rownum end
				from @rownum where which = 'COST_CALCS' ) ,0) AS COST_CALCS_ROWS,
			ISNULL( ( Select rownum from @rownum where which = 'ODI_PRD' ) ,0)  AS ODI_PRD_ROWS,
			ISNULL( ( Select rownum from @rownum where which = 'FAC_PRD' ) ,0)  AS FAC_PRD_ROWS,
			ISNULL( ( Select rownum from @rownum where which = 'CP_PRD' ) ,0)  AS CP_PRD_ROWS,
			ISNULL( ( Select rownum from @rownum where which = 'PT_PRD' ) ,0)  AS PT_PRD_ROWS,
			ISNULL( ( Select rownum from @rownum where which = 'STU_PRD' ) ,0)  AS STU_PRD_ROWS,

			@username AS CRUSER,
			

			convert(varchar,deadline,103) AS SUBMISSION_DATE,
			convert(varchar,start_date,103) AS EST_STARTDATE,
			convert(varchar,end_date,103) AS EST_ENDDATE,

			( select 
					RTRIM(SUBSTRING(b.StaffNumber,
					CHARINDEX('_',b.StaffNumber,1)+1,100)) + '_' + SUBSTRING(b.StaffNumber, 1, CHARINDEX('_',b.StaffNumber,1)-1)
					 from 
					Project_Proposal_Co_investigators a  
						left outer join [dbo].[Staff_Details] b
								on a.Co_Investigator = b.ID
					where Project_Proposal = @prjid
					and a.Id = PIID	) AS [PI],
			--PIID AS [PI],
			case
			when ISNULL(PIID,'') = '' then 
			PINAME else null end AS PI_TBA_NAME,
			case
			when ISNULL(PIID,'') = '' then 
			[DepartmentDesc] else null end AS PI_TBA_DEPT,

			( 
			 SELECT 
				 --Project_Proposal_Status.ID,
				 --A.Description as Status_From, 
				 --B.Description AS Status_TO,  
				-- Project_Proposal_Status.Created_On, 
				 convert(varchar,MAX(Project_Proposal_Status.Date_Of_Change),103) AS Date_Of_Change
				 --Project_Proposal_Status.Target_Date,   
				 --Project_Proposal_Status.Notes,  
				 --D.SurName + ' '+ D.FirstName AS SubmittedTo,  
				 --LUP_Users.User_Name AS Created_BY  
				 /*
				  CASE WHEN (LUP_Users.Name IS NULL OR LUP_USERS.NAME ='') AND (LUP_Users.Staff_ID IS NOT NULL)  THEN   
							 E.SurName + ' ' +E.FirstName  
					   WHEN (LUP_Users.Name IS NOT NULL OR LUP_USERS.NAME<>'') AND (LUP_Users.Staff_ID IS NULL ) THEN   
						 LUP_Users.Name  
					Else LUP_Users.User_Name 
					 END AS Created_By 
				   */                       
				 FROM Project_Proposal_Status   
				 INNER JOIN LUP_Status AS A ON  Project_Proposal_Status.Status_From = A.ID  
				 INNER JOIN LUP_Status AS B ON  Project_Proposal_Status.Status_TO = B.ID  
				 INNER JOIN LUP_Users ON LUP_Users.ID= Project_Proposal_Status.Created_By  
				 LEFT JOIN Staff_Details As E on E.Id = Lup_Users.staff_ID  
				 LEFT JOIN Staff_Details AS D ON Project_Proposal_Status.SubmittedTo=D.ID  
				 WHERE Project_Proposal= @prjid 
				 AND B.Description = 'Application Submitted'
				)   AS APP_SUBMIT_DATE,
			--FUNDER AS FUNDING_TEMP,
			CASE
				WHEN @FUNDING_TEMP = 'RCUK' THEN '01'
				WHEN @FUNDING_TEMP = 'Research Council' THEN '01'
				WHEN @FUNDING_TEMP = 'General' THEN '02'
				WHEN @FUNDING_TEMP = 'NIHR' THEN '03'
				WHEN @FUNDING_TEMP = 'Leverhulme Trust' THEN '04'
				WHEN @FUNDING_TEMP = 'British Academy' THEN '05'
				WHEN @FUNDING_TEMP = 'Royal Society' THEN '06'
				WHEN @FUNDING_TEMP = 'Welcome Trust' THEN '07'
				WHEN @FUNDING_TEMP = 'Wellcome Trust' THEN '07'
				WHEN @FUNDING_TEMP = 'EC - H2020' THEN '08'
				WHEN @FUNDING_TEMP = 'EC - H2021' THEN '08'
				WHEN @FUNDING_TEMP = 'EC - H2022' THEN '08'
				WHEN @FUNDING_TEMP = 'EC - H2023' THEN '08'
				WHEN @FUNDING_TEMP = 'EC - H2024' THEN '08'
				WHEN @FUNDING_TEMP = 'EC - H2025' THEN '08'
				WHEN @FUNDING_TEMP = 'EC - H2026' THEN '08'
				WHEN @FUNDING_TEMP = 'EC - H2027' THEN '08'
				WHEN @FUNDING_TEMP = 'EC - H2028' THEN '08'
				WHEN @FUNDING_TEMP = 'EC - H2029' THEN '08'

				WHEN @FUNDING_TEMP = 'EC_H2020' THEN '08'
				WHEN @FUNDING_TEMP = 'EC_H2021' THEN '08'
				WHEN @FUNDING_TEMP = 'EC_H2022' THEN '08'
				WHEN @FUNDING_TEMP = 'EC_H2023' THEN '08'
				WHEN @FUNDING_TEMP = 'EC_H2024' THEN '08'
				WHEN @FUNDING_TEMP = 'EC_H2025' THEN '08'
				WHEN @FUNDING_TEMP = 'EC_H2026' THEN '08'
				WHEN @FUNDING_TEMP = 'EC_H2027' THEN '08'
				WHEN @FUNDING_TEMP = 'EC_H2028' THEN '08'
				WHEN @FUNDING_TEMP = 'EC_H2029' THEN '08'

				WHEN @FUNDING_TEMP = 'ESIF/ESF 25%' THEN '08'

				WHEN @FUNDING_TEMP = 'ESIF/ESF 15%' THEN '08'
				WHEN @FUNDING_TEMP = 'ESIF / ESF 15%' THEN '08'
				
				
				WHEN @FUNDING_TEMP = 'EC-MSCA' THEN '09'
				WHEN @FUNDING_TEMP = 'EC_MSCA' THEN '09'
				--ELSE @FUNDING_TEMP END 
				ELSE '02' END 
				AS FUNDING_TEMP,
			
			
			
			ROW_NUMBER() OVER (ORDER BY 
						@username
						
						)  as rownum
						from 
						@RES a
						left outer join 
									UOE_PFACT_T1_BID_TBL b
										on a.ID = b.pfactid,
						@TEMPFUND
							





end


