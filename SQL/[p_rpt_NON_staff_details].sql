USE [pfact_t1]
GO
/****** Object:  StoredProcedure [dbo].[p_rpt_NON_staff_details]    Script Date: 12/09/2018 15:27:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER PROC [dbo].[p_rpt_NON_staff_details]
(
@prj_id int
)

as
			--		exec p_rpt_NON_staff_details 10440
begin
		 select recType, rowid, descr, @prj_id as prj_id
		  from  [dbo].[UOE_P_PFACT_Prj_Dept_Split]
			where prj_id  = @prj_id
			and recType <> 'staff'
			and recType <> 'FTE Related'

END

