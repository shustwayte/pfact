USE [pfact_t1]
GO
/****** Object:  StoredProcedure [dbo].[p_UOE_P_PRJ_Budget_Table_costdetail_uoe_RPT]    Script Date: 12/09/2018 15:28:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROC [dbo].[p_UOE_P_PRJ_Budget_Table_costdetail_uoe_RPT]
(
@prjid int
)

as 

begin

		Select 
		'Project Partners' as cat,
		ProjectID
		,b.name 
		, b.Cost,
		case
		when 
		isnull(c.income,null) = null then '0.00'
		else
		c.income
		end as income
	
		FROM otherinstitutioncosttype a
			INNER JOIN ProjectOtherInstitutionCost b
				ON 
				a.ID=b.otherinstitutioncosttype 
				LEFT OUTER JOIN 
				
				[dbo].[UOE_costdetails_UOE_Other_Income] c
					on c.rec = b.id
				--WHERE ISNULL(C.rec,'') <> ''
		where ProjectID = @prjid

end


