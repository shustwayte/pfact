USE [pfact_t1]
GO
/****** Object:  StoredProcedure [dbo].[UOE_P_PRJ_Proposal_DEPT_Split_Load]    Script Date: 12/09/2018 15:29:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



ALTER PROCedure [dbo].[UOE_P_PRJ_Proposal_DEPT_Split_Load]
(
@prj_id int
)

as

begin
-------------------------------- SPELL CHANGE END ADDED 2016/01/22

-- EXEC PROJECTSTAFFSPELLDETAILS 8406,'01-Jan-1900','01-Jan-1900',1,0,1,'NONE',0

		--	exec [dbo].[UOE_P_PRJ_Proposal_DEPT_Split_Load] 10388

	delete from [dbo].[UOE_P_PFACT_Prj_Dept_Split]
	where prj_id  = @prj_id

		
--		DECLARE @prj_id int
--		SET @prj_id = 1147
		
		declare @st datetime
		declare @ed datetime
		
		set @st = ( select Start_Date from dbo.Project_Proposal where id = @prj_id )
		set @ed = ( select End_Date from dbo.Project_Proposal where id = @prj_id )
		
		-- exec UOE_P_PRJ_Budget_Table 1260
		declare @ProjectStaffCostDetailed table                   
		 (             
		  num int identity(1,1),
		  PRJ_ID INT,       
		  ID INT NULL,                    
		  Staff_Type INT NULL,                    
		  StaffType VARCHAR (50) NULL,                    
		  Co_Investigator INT NULL,                    
		  NAME VARCHAR(100),                    
		  Sub_Element INT NULL,                    
		  CODE VARCHAR (30) NULL,                    
		  Description VARCHAR (100) NULL,                    
		  Tbl INT NULL,            
		  FTE FLOAT NULL,                    
		  Category VARCHAR(500) NULL,                    
		  CATID INT NULL,                    
		  Sequence_NO INT NULL,                    
		  StartDate DATETIME NULL,                    
		  EndDate DATETIME NULL,                    
		  TOTAL FLOAT NULL,                    
		  EditedFTE FLOAT NULL,                    
		  RoleSequence INT NULL,                      
		  StaffTypeDesc VARCHAR(100) NULL,                    
		  SpinePoint INT NULL,                    
		  SpineOnDate DATETIME NULL,                    
		  IncrementDate DATETIME NULL,                    
		  DiscretionaryPoint INT NULL,                    
		  Payscale  INT NULL,              
		  Department VARCHAR(100) NULL,        
		  ChargeType VARCHAR (100), --modification 11        
		  ChargeTypeID INT NULL ,  --modification 11     
		  [AnnualContractHours] FLOAT NULL,    
		  [AnnualContractDays] FLOAT NULL,    
		  [WeeksInYear] FLOAT NULL               
		 )                    
		   

		declare @i int
		set @i = 1

			insert into @ProjectStaffCostDetailed
			(
			ID , 
			  Staff_Type ,
			  StaffType ,
			  Co_Investigator ,
			  NAME ,
			  Sub_Element ,
			  CODE ,
			  [Description] ,
			  Tbl ,          
			  FTE ,
			  Category ,
			  CATID ,
			  Sequence_NO ,
			  StartDate ,
			  EndDate ,
			  TOTAL ,
			  EditedFTE ,
			  RoleSequence ,
			  StaffTypeDesc ,
			  SpinePoint ,
			  SpineOnDate ,
			  IncrementDate ,
			  DiscretionaryPoint ,
			  Payscale  ,
			  Department ,
			  ChargeType ,
			  ChargeTypeID ,
			  [AnnualContractHours] ,
			  [AnnualContractDays] ,
			  [WeeksInYear] 
			  )
			EXEC ProjectStaffCostDetailed @prj_id,1,1,0,''
			
			UPDATE @ProjectStaffCostDetailed
			SET PRJ_ID = @prj_id
			
			
			-------------- FTE START
			
			declare @ftei int
			set @ftei = 1
			
			declare @ftezero table
			(
			id int identity(1,1),
			row_id int
			)
	
			insert into @ftezero
			select id from 
			@ProjectStaffCostDetailed
			where FTE = 0
			
			declare @ftzerores table
			(
			removelink varchar(8000),
			sdate varchar(8000),
			eddate datetime,
			units float, --decimal(5,2),
			unit_type varchar(100),
			cssrow varchar(400),
			row_id int
			)
			
			declare @fteid int
			
			while @ftei <= ( select MAX(id) from @ftezero )
			begin
			
				
				set @fteid = ( select row_id from @ftezero where id = @ftei )
			
				insert into @ftzerores
				(
				removelink ,
				sdate ,
				eddate ,
				units ,
				unit_type ,
				cssrow
				)
				EXEC PROJECTSTAFFSPELLDETAILS @fteid,'01-Jan-1900','01-Jan-1900',1,0,1,'NONE',0
				
					update @ftzerores
					set row_id = @fteid
					where 
					ISNULL(row_id,'') = ''
			
				set @ftei = @ftei + 1
				
			end
		

		--	select * from @ftzerores
/*

FTE/Annum       =1 x The number of days between the period 01/07/16 – 31/12/20 divided by 365.
					1 (FTE/Annum) x 1644 days / 365 days = 4.5041

Hours           =FEC FTE = Hours/1650                                

Days			=Number of days x 7.5 = Hours				FEC FTE = Hours/1650 

Hours/Week      =No. of weeks in project = Number of days from start to end date of staff/365 x 44
							Hours = No. of weeks on project x hours per week
							FEC FTE = Hours/1650

Months          =Number of Months x 137.5 = Hours			FEC FTE = Hours/1650

*/
			update @ProjectStaffCostDetailed
			set FTE = 
			 --select 
			 case 
				 when unit_type = 'FTE/Annum' then
							 b.units * 
							 (( DATEDIFF(D,a.StartDate, dateadd(d,1,a.EndDate) )*1.00
							 / 365 ))-- * AnnualContractHours
							 
				when unit_type = 'Hours' then			 
							 (b.units / 1650)*1.00
							 
				when unit_type = 'Days' then			 
							 ( ( (b.units * 7.5) *1.00 )	/ 1650 )
							 
				when unit_type = 'Hours/Week' then	
							( b.units * ( ( DATEDIFF(d,a.StartDate,dateadd(d,1,a.EndDate) )/ 365 ) * 44 ) ) / 1650
							 
				when unit_type = 'Months' then				
							( ( b.units * 137.5 ) * 1.00 ) / 1650
							
			  else 0
			  end
			 from @ProjectStaffCostDetailed a
			 inner join @ftzerores b
					on a.ID = b.row_id
			-- where FTE = 0
			 
			
--			 select * from @ProjectStaffCostDetailed
			 -------------- FTE END
			 
			
			
			declare @staff_cost_a table 
			(
			-------------------------------- SPELL CHANGE END
			RECID INT IDENTITY(1,1),
			id int,
			[Spell] [int] NOT NULL,
			[StartDate] [datetime] NOT NULL,
			[EndDate] [datetime] NOT NULL,
			[Reason] [varchar](100) NULL,
			[Spine] [int] NULL,
			[Basic] [float] NOT NULL,
			[NI] [float] NOT NULL,
			[NIOUT] [float] NOT NULL,
			[Super] [float] NOT NULL,
			[Total] [float] NOT NULL,
			[CoinID] [int] NOT NULL,
			[StaffType] [int] NOT NULL,
			[Other_Allowances] [float] NULL,
			[PayScale] [int] NULL,
			[PayScaleDesc] [varchar](80) NULL,
			[SpineAmount] [decimal](22, 2) NULL
			)
			
			declare @max_prj_line int
			set @max_prj_line = ( select MAX(num) from @ProjectStaffCostDetailed )
			
			set @i = 1
			
			declare @staff_row_id int
			declare @tbl_id int
			
			
			while @i <= @max_prj_line
			begin
			
			set @staff_row_id = ( select id from @ProjectStaffCostDetailed where num = @i )
			set @tbl_id = ( select Tbl from @ProjectStaffCostDetailed where num = @i )
			
			insert into @staff_cost_a
			(
			[Spell] ,
			[StartDate] ,
			[EndDate] ,
			[Reason] ,
			[Spine] ,
			[Basic] ,
			[NI] ,
			[NIOUT] ,
			[Super] ,
			[Total] ,
			[CoinID] ,
			[StaffType] ,
			[Other_Allowances] ,
			[PayScale] ,
			[PayScaleDesc] ,
			[SpineAmount]
			)
			
			
			EXEC StaffCostFromTable 0,1,1,@staff_row_id,@tbl_id
			
			
			update @staff_cost_a
			set id = @staff_row_id
			where ISNULL(id,0) = 0
			
			set @i = @i + 1 
			end
			
	
			-------------------------------- SPELL CHANGE BEGIN
	--if @prj_id = 4960
	--begin
			update @staff_cost_a
				set 
				StartDate = q.startdate,
				EndDate = q.EndDate,
				Basic = q.basic,
				NI = q.ni,
				NIOUT = q.niout,
				Super = q.super,
				Total = q.total
			from
			@staff_cost_a a
				inner join (
			
					select 
					--'data new', 
					case
					when DATEPART(month,startdate) >=8 then DATEPART(year,startdate) 
					when DATEPART(month,startdate) <8 then DATEPART(year,startdate)-1
					end as yr
					,id,
					MIN(StartDate) as startdate,
					MAX(EndDate) as EndDate,
					SUM(Basic) as basic,
					SUM(NI) as ni,
					SUM(NIOUT) as niout,
					SUM(super) as super,
					SUM(total) as total
					from @staff_cost_a
					--where id = 16626
					group by
					case
					when DATEPART(month,startdate) >=8 then DATEPART(year,startdate) 
					when DATEPART(month,startdate) <8 then DATEPART(year,startdate)-1
					end ,
					id
				) q
					on a.StartDate = q.startdate
					and a.id = q.id
				--where a.id = 16626


			delete from @staff_cost_a
			where recid not in (
					Select a.recid
					from
					@staff_cost_a a
						inner join (
					
							select 
							--'data new', 
							case
							when DATEPART(month,startdate) >=8 then DATEPART(year,startdate) 
							when DATEPART(month,startdate) <8 then DATEPART(year,startdate)-1
							end as yr
							,id,
							MIN(StartDate) as startdate,
							MAX(EndDate) as EndDate,
							SUM(Basic) as basic,
							SUM(NI) as ni,
							SUM(NIOUT) as niout,
							SUM(super) as super,
							SUM(total) as total
							from @staff_cost_a
							--where id = 16626
							group by
							case
							when DATEPART(month,startdate) >=8 then DATEPART(year,startdate) 
							when DATEPART(month,startdate) <8 then DATEPART(year,startdate)-1
							end ,
							id
						) q
							on a.StartDate = q.startdate
							and a.id = q.id
						--where a.id = 16626
				)
			
	--end	
			-------------------------------- SPELL CHANGE END
			
			
			declare @staff_cost table 
			(
			id int,
			[Spell] [int] NOT NULL,
			VaribleValue int,
			[StartDate] [datetime] NOT NULL,
			[EndDate] [datetime] NOT NULL,
			[Reason] [varchar](100) NULL,
			[Spine] [int] NULL,
			[Basic] [float] NOT NULL,
			[NI] [float] NOT NULL,
			[NIOUT] [float] NOT NULL,
			[Super] [float] NOT NULL,
			[Total] [float] NOT NULL,
			[CoinID] [int] NOT NULL,
			[StaffType] [int] NOT NULL,
			[Other_Allowances] [float] NULL,
			[PayScale] [int] NULL,
			[PayScaleDesc] [varchar](80) NULL,
			[SpineAmount] [decimal](22, 2) NULL
			)
			
			insert into @staff_cost
			select 
			id ,
			[Spell] ,
			ROW_NUMBER() OVER (PARTITION BY id ORDER BY StartDate ) as VaribleValue,
			--	SH SPELL COMBINE
			--RANK() OVER (PARTITION BY id ORDER BY DATEPART(year,StartDate) ) as VaribleValue,
			[StartDate] ,
			[EndDate] ,
			[Reason] ,
			[Spine] ,
			[Basic] ,
			[NI] ,
			[NIOUT] ,
			[Super] ,
			[Total] ,
			[CoinID] ,
			[StaffType] ,
			[Other_Allowances] ,
			[PayScale] ,
			[PayScaleDesc] ,
			[SpineAmount] 
			from @staff_cost_a
			--select * from @staff_cost_a
			
			
---------------------------------------------------


		
	--- profile range need taking into account
	--- staff can be added at any point in year
			declare @profile_range table
			(
			variblevalue int,
			prof_sdate datetime,
			prof_edate datetime
			)
			insert into @profile_range
			select 
			variblevalue,
			MIN(StartDate),
			max(endDate)
			 from @staff_cost
			group by 
			variblevalue
			
			--select * from @profile_range
			
			declare @max_profile_range int
			set @max_profile_range = ( select MAX(variblevalue) from @profile_range )
			
			
			declare @profile_id table
			(
			id int identity(1,1),
			recid int
			)
			
			insert into @profile_id
			select id from @staff_cost
					group by id
					having COUNT(*) < @max_profile_range
					
			
			declare @iii int
			set @iii = 1
			declare @max_iii int
			set @max_iii = ( select MAX(id) from @profile_id )
			
			declare @iii_id int
			
			declare @iii_missing int
			
			
			while @iii <= @max_iii
			begin
			
				set @iii_id = ( select recid from @profile_id where id = @iii )
			
			-- reorder from 1 onwards depending on dates against profile
				update @staff_cost
				set VaribleValue = z.new_id
				from @staff_cost a
					inner join (
							select 
							distinct
							a.id,
							a.VaribleValue,
							--a.StartDate,
							--b.prof_sdate,
							MAX(
							case
							when a.startdate = b.prof_sdate then b.variblevalue
							when a.StartDate between b.prof_sdate and b.prof_edate
										then b.variblevalue
							else null end
								) as new_id
							from @staff_cost a
								, @profile_range b
										--on a.StartDate = b.prof_sdate
							where id = @iii_id
							group by
							a.id,
							a.VaribleValue
							--a.StartDate,
							--b.prof_sdate
							) z
								on a.id = z.id
								and a.VaribleValue = z.VaribleValue
			
					set @iii_missing = 1
					
					-- is missing flesh out to include all profile ranges
					while @iii_missing < = @max_profile_range
					begin
						
						
						if ( select COUNT(*) from @staff_cost 
								where id = 	@iii_id
								and VaribleValue = 	@iii_missing ) = ''
								begin
								
								insert into @staff_cost
								(
								id, spell, VaribleValue, StartDate, EndDate ,
								Reason, Spine, Basic, NI, NIOUT, Super, Total, CoinID, StaffType,
								Other_Allowances, PayScale, PayScaleDesc, SpineAmount
								)
								
								select @iii_id, 0, @iii_missing, 
								( Select prof_sdate from @profile_range
										where variblevalue = @iii_missing ) ,
								( Select prof_edate from @profile_range
										where variblevalue = @iii_missing ) ,
								'',
								0,
								0,
								0,
								0,
								0,
								0,
								@iii_id,
								0,
								0,
								0,
								'',
								0
								
								
								end			
						
						set @iii_missing = @iii_missing + 1
					end
					
					
			
			set @iii = @iii + 1
			end
			
			
----------------------------------------------------------------------						
			
			
			
			
			
			
			
			
			
			/*
				select 
				distinct
				VaribleValue,
				convert(varchar(10),StartDate,103) + ' - ' +
				convert(varchar(10),EndDate,103)
				from @staff_cost
			*/	
				
				--select A.PRJ_ID,
				--Department,
				--ChargeType,
				----[Description],
				----A.ID,
				--Category + ' - ' + NAME as descr,
				--StaffTypeDesc,
				----a.StartDate,
				----a.EndDate,
				--	b.[1],
				--	b.[2],
				--	b.[3],
				--	b.[4],
				--	b.[5],
				--	b.[6],
				--	b.[7],
				--	b.[8],
				--	b.[9],
				--	b.[10],
				--	b.[11],
				--	b.[12],
				--	b.[13],
				--	b.[14],
				--	b.[15],
				--A.TOTAL
				
				--from @ProjectStaffCostDetailed A
				--left outer join 
				--		(							
				--			select id,
				--			MAX([1]) as [1],
				--			MAX([2]) as [2],
				--			MAX([3]) as [3],
				--			MAX([4]) as [4],
				--			MAX([5]) as [5],
				--			MAX([6]) as [6],
				--			MAX([7]) as [7],
				--			MAX([8]) as [8],
				--			MAX([9]) as [9],
				--			MAX([10]) as [10],
				--			MAX([11]) as [11],
				--			MAX([12]) as [12],
				--			MAX([13]) as [13],
				--			MAX([14]) as [14],
				--			MAX([15]) as [15]


				--			from (				
				--					select id,[1],[2],[3],[4],[5],[6],[7],[8],[9],[10],[11],[12],[13],[14],[15]  from  @staff_cost
				--					PIVOT
				--					 (
				--					 MAX(Total)
				--					 FOR VaribleValue
				--					 IN ([1],[2],[3],[4],[5],[6],[7],[8],[9],[10],[11],[12],[13],[14],[15] )
				--					 )
				--					 AS p
				--				) a 
				--			group by id	
						
				--		) b
				--			on a.ID = b.id
				
				--order by a.id desc	


	--------------------------------

	
 declare @equip TABLE 
  (                        
  ID INT NULL,                        
  Rate FLOAT NULL,                        
  Units FLOAT NULL,                        
  CatID INT NULL,                        
  Description VARCHAR(900) NULL,                        
  Name VARCHAR(100) NULL,                        
  Code VARCHAR (30) NULL,                        
  UnitType VARCHAR (50) NULL,                        
  UnitTypeID INT NULL,                        
  Sequence_No INT NULL,                        
  CatChargeType VARCHAR (70) NULL,                        
  StartDate DateTime NULL,                        
  EndDate DateTime NULL,                        
  ItemType INT NULL,                        
  Sub_Element INT NULL,                        
  VAT_Percent FLOAT NULL,                        
  COST FLOAT NULL,                        
  VAT_Type INT NULL,                        
  VAT_Cost FLOAT NULL,                          
  Cost_Before_VAT FLOAT NULL,                        
  IsSwitchToConsumables BIT NULL,                        
  OrgConumableLimit FLOAT NULL,                        
  ConsumableCategory INT NULL,                        
  Inflation_Factor_ID INT NULL,                        
  Element INT NULL,                        
--  RecategoriseToConsumable BIT NULL,                        
  Effective_Date DATETIME NULL,                        
  ProjectCode VARCHAR(20),                        
  ProjectName VARCHAR(100),                        
  DefaultChargeType VARCHAR(70),  
  ExceptionalRule int,
  RateOnDate Datetime,                        
  VATDesc VARCHAR(250),
  ProjectStart DateTime,                        
  ActualElement INT,                      
  ResearchStudent INT NULL                        
 )      
 
insert into @equip
EXEC ProjectEquipmentCostDetailed @PRJ_ID,3,1,0,1
	
 
insert into @equip 
EXEC ProjectEquipmentCostDetailed @prj_id,2,1,0,1

--EXEC ProjectEquipmentCostDetailed 2623,2,1,0,1
/*
update @equip
set 
Description = SUBSTRING([Description],1,CHARINDEX('</br> ',[Description],1)-1)
*/
--select 
--@prj_id,
--'' as Department,
--CatChargeType,
--[Description] + ' - ' + Name,
--VATDesc,
--cost as [1],
--null as [2],
--null as [3],
--null as [4],
--null as [5],
--null as [6],
--null as [7],
--null as [8],
--null as [9],
--null as [10],
--null as [11],
--null as [12],
--null as [13],
--null as [14],
--null as [15],
--cost as TOTAL
--from @equip

-- select * from @equip

	------------------------------
	
declare @FTE_related table
(
	[Coin] [int] NULL,
	[StaffType] INT NULL,
	[Name] [varchar](250) NULL,
	[GroupID] [int] NULL,
	[ExpenditureGroup] [varchar](250) NULL,
	[ExpType] [varchar](30) NULL,
	[ExpenditureType] [varchar](30) NULL,
	[FTE] [float] NULL,
	[Amount] [float] NULL,
	[ChargeType] [varchar](70) NULL,
	[a] [int] NULL,
	[Inflation] [int] NULL,
	[BaseYear] [varchar](50) NULL,
	[ExpenditureFTEID] [int] NULL,
	[ProjectCode] [varchar](20) NULL,
	[ProjectName] [varchar](100) NULL,
	[ProjectStart] [datetime] NULL,
	[ProjectEnd] [datetime] NULL
)

insert into @FTE_related
EXEC ProjectIndirectCostDetails @prj_id,1,0,1

--SELECT 
--@prj_id,
--'' as Department,
--ChargeType,
--ExpenditureType,
--ExpenditureGroup + ' - ' + Name,
--Amount as [1],
--null as [2],
--null as [3],
--null as [4],
--null as [5],
--null as [6],
--null as [7],
--null as [8],
--null as [9],
--null as [10],
--null as [11],
--null as [12],
--null as [13],
--null as [14],
--null as [15],
--AMOUNT as TOTAL
-- FROM @FTE_related
-- WHERE AMOUNT <> 0
 


--select * from @FTE_related

--SELECT * FROM @equip

------------------
-- results

--SELECT * FROM @ProjectStaffCostDetailed

insert into [dbo].[UOE_P_PFACT_Prj_Dept_Split]
	(
	[PRJ_ID]
		  ,[Department]
		  ,[ChargeType]
		  ,[descr]
		  ,[StaffNumber]
		  ,[acc_code]
		  ,[StaffTypeDesc]
		  ,[1]
		  ,[2]
		  ,[3]
		  ,[4]
		  ,[5]
		  ,[6]
		  ,[7]
		  ,[8]
		  ,[9]
		  ,[10]
		  ,[11]
		  ,[12]
		  ,[13]
		  ,[14]
		  ,[15]
		  ,[TOTAL]
		  ,[stdate]
		  ,[eddate]
		  ,ROWID
		  ,hoursworked
		  ,PFACT_PAC_Mapping
		  ,CANFLAT

		  ,workpackage

				,
				basic1 ,
				basic2 ,
				basic3 ,
				basic4 ,
				basic5 ,
				basic6 ,
				basic7 ,
				basic8 ,
				basic9 ,
				basic10 ,
				basic11 ,
				basic12 ,
				basic13 ,
				basic14 ,
				basic15 ,
				basicTotal ,


				ni1 ,
				ni2 ,
				ni3 ,
				ni4 ,
				ni5 ,
				ni6 ,
				ni7 ,
				ni8 ,
				ni9 ,
				ni10 ,
				ni11 ,
				ni12 ,
				ni13 ,
				ni14 ,
				ni15 ,
				niTotal ,


				super1 ,
				super2 ,
				super3 ,
				super4 ,
				super5 ,
				super6 ,
				super7 ,
				super8 ,
				super9 ,
				super10 ,
				super11 ,
				super12 ,
				super13 ,
				super14 ,
				super15 ,
				superTotal 

				,recType 

				,CoIn 
				,FTE
				,finaladjust
	  )


select A.PRJ_ID,
				Department,
				ChargeType,
				--[Description],
				--A.ID,
				--Category + ' - ' + NAME as descr,
				--Category as descr,
			/*
				case
				when StaffType = 'PI' then 'PI '
				when StaffType = 'cI' then 'Co-I '
				else ''
				end 
				+
				*/
				case
				when CHARINDEX('-',isnull(name,''),1) > 0 then
						name
							else
				name + 
					case
					when ISNULL(s.StaffNumber,'') <> '' then
					' - ' + s.StaffNumber
					else '' 
					end
				
				end,
				case
				when CHARINDEX('-',isnull(name,''),1) > 0 then
						SUBSTRING(isnull(name,''),
							CHARINDEX('-',isnull(name,''),1)+1,
							10 )
				else
				ISNULL(s.StaffNumber,'')
				end,
				'' as acc_code,
				StaffTypeDesc,
				--a.StartDate,
				--a.EndDate,
					b.[1],
					b.[2],
					b.[3],
					b.[4],
					b.[5],
					b.[6],
					b.[7],
					b.[8],
					b.[9],
					b.[10],
					b.[11],
					b.[12],
					b.[13],
					b.[14],
					b.[15],
				A.TOTAL,
				a.StartDate,
				a.EndDate,
				
				--@st stdate,
				--@ed eddate,
				A.ID,
				a.FTE * 1650,
				a.Category,
				1,

				'00',


				c.[1],
					c.[2],
					c.[3],
					c.[4],
					c.[5],
					c.[6],
					c.[7],
					c.[8],
					c.[9],
					c.[10],
					c.[11],
					c.[12],
					c.[13],
					c.[14],
					c.[15],
					c.tot,

					d.[1],
					d.[2],
					d.[3],
					d.[4],
					d.[5],
					d.[6],
					d.[7],
					d.[8],
					d.[9],
					d.[10],
					d.[11],
					d.[12],
					d.[13],
					d.[14],
					d.[15],
					d.tot, 

					e.[1],
					e.[2],
					e.[3],
					e.[4],
					e.[5],
					e.[6],
					e.[7],
					e.[8],
					e.[9],
					e.[10],
					e.[11],
					e.[12],
					e.[13],
					e.[14],
					e.[15],
					e.tot
				
				,'Staff'

				,A.ID
				,A.FTE
				,null
				
				from @ProjectStaffCostDetailed A
				left outer join 
						(							
							select id,
							--	SH SPELL COMBINE
							--	MAX CHANGED TO SUM
							SUM([1]) as [1],
							SUM([2]) as [2],
							SUM([3]) as [3],
							SUM([4]) as [4],
							SUM([5]) as [5],
							SUM([6]) as [6],
							SUM([7]) as [7],
							SUM([8]) as [8],
							SUM([9]) as [9],
							SUM([10]) as [10],
							SUM([11]) as [11],
							SUM([12]) as [12],
							SUM([13]) as [13],
							SUM([14]) as [14],
							SUM([15]) as [15]


							from (				
									select id,[1],[2],[3],[4],[5],[6],[7],[8],[9],[10],[11],[12],[13],[14],[15]  from  @staff_cost
									PIVOT
									 (
									 MAX(Total)
									 FOR VaribleValue
									 IN ([1],[2],[3],[4],[5],[6],[7],[8],[9],[10],[11],[12],[13],[14],[15] )
									 )
									 AS p
								) a 
							group by id	
						
						) b
							on a.ID = b.id
							

					------------
							-- new basic
							left outer join 
						(							
							select id,
							--	SH SPELL COMBINE
							--	MAX CHANGED TO SUM
							SUM([1]) as [1],
							SUM([2]) as [2],
							SUM([3]) as [3],
							SUM([4]) as [4],
							SUM([5]) as [5],
							SUM([6]) as [6],
							SUM([7]) as [7],
							SUM([8]) as [8],
							SUM([9]) as [9],
							SUM([10]) as [10],
							SUM([11]) as [11],
							SUM([12]) as [12],
							SUM([13]) as [13],
							SUM([14]) as [14],
							SUM([15]) as [15],

							SUM(isnull([1],0)) +
							SUM(isnull([2],0)) +
							SUM(isnull([3],0)) +
							SUM(isnull([4],0)) +
							SUM(isnull([5],0)) +
							SUM(isnull([6],0)) +
							SUM(isnull([7],0)) +
							SUM(isnull([8],0)) +
							SUM(isnull([9],0)) +
							SUM(isnull([10],0)) +
							SUM(isnull([11],0)) +
							SUM(isnull([12],0)) +
							SUM(isnull([13],0)) +
							SUM(isnull([14],0)) +
							SUM(isnull([15],0)) as [tot]


							from (				
									select id,[1],[2],[3],[4],[5],[6],[7],[8],[9],[10],[11],[12],[13],[14],[15]
									from  @staff_cost
									PIVOT
									 (
									 MAX([basic])
									 FOR VaribleValue
									 IN ([1],[2],[3],[4],[5],[6],[7],[8],[9],[10],[11],[12],[13],[14],[15] )
									 )
									 AS p
								) a 
							group by id	
						
						) c
							on a.ID = c.id


							------------
							-- new ni
							left outer join 
						(							
							select id,
							--	SH SPELL COMBINE
							--	MAX CHANGED TO SUM
							SUM([1]) as [1],
							SUM([2]) as [2],
							SUM([3]) as [3],
							SUM([4]) as [4],
							SUM([5]) as [5],
							SUM([6]) as [6],
							SUM([7]) as [7],
							SUM([8]) as [8],
							SUM([9]) as [9],
							SUM([10]) as [10],
							SUM([11]) as [11],
							SUM([12]) as [12],
							SUM([13]) as [13],
							SUM([14]) as [14],
							SUM([15]) as [15],
							
							SUM(isnull([1],0)) +
							SUM(isnull([2],0)) +
							SUM(isnull([3],0)) +
							SUM(isnull([4],0)) +
							SUM(isnull([5],0)) +
							SUM(isnull([6],0)) +
							SUM(isnull([7],0)) +
							SUM(isnull([8],0)) +
							SUM(isnull([9],0)) +
							SUM(isnull([10],0)) +
							SUM(isnull([11],0)) +
							SUM(isnull([12],0)) +
							SUM(isnull([13],0)) +
							SUM(isnull([14],0)) +
							SUM(isnull([15],0)) as [tot]


							from (				
									select id,[1],[2],[3],[4],[5],[6],[7],[8],[9],[10],[11],[12],[13],[14],[15] 
									  from  @staff_cost
									PIVOT
									 (
									 MAX([NIOUT] )
									 FOR VaribleValue
									 IN ([1],[2],[3],[4],[5],[6],[7],[8],[9],[10],[11],[12],[13],[14],[15] )
									 )
									 AS p
								) a 
							group by id	
						
						) d
							on a.ID = d.id

							------------
							-- new super
							left outer join 
						(							
							select id,
							--	SH SPELL COMBINE
							--	MAX CHANGED TO SUM
							SUM([1]) as [1],
							SUM([2]) as [2],
							SUM([3]) as [3],
							SUM([4]) as [4],
							SUM([5]) as [5],
							SUM([6]) as [6],
							SUM([7]) as [7],
							SUM([8]) as [8],
							SUM([9]) as [9],
							SUM([10]) as [10],
							SUM([11]) as [11],
							SUM([12]) as [12],
							SUM([13]) as [13],
							SUM([14]) as [14],
							SUM([15]) as [15],

							SUM(isnull([1],0)) +
							SUM(isnull([2],0)) +
							SUM(isnull([3],0)) +
							SUM(isnull([4],0)) +
							SUM(isnull([5],0)) +
							SUM(isnull([6],0)) +
							SUM(isnull([7],0)) +
							SUM(isnull([8],0)) +
							SUM(isnull([9],0)) +
							SUM(isnull([10],0)) +
							SUM(isnull([11],0)) +
							SUM(isnull([12],0)) +
							SUM(isnull([13],0)) +
							SUM(isnull([14],0)) +
							SUM(isnull([15],0)) as [tot]


							from (				
									select id,[1],[2],[3],[4],[5],[6],[7],[8],[9],[10],[11],[12],[13],[14],[15]
									   from  @staff_cost
									PIVOT
									 (
									 MAX([super] )
									 FOR VaribleValue
									 IN ([1],[2],[3],[4],[5],[6],[7],[8],[9],[10],[11],[12],[13],[14],[15] )
									 )
									 AS p
								) a 
							group by id	
						
						) e
							on a.ID = e.id



					left outer join 
					(	
					select id, StaffNumber, SurName
					from
					dbo.Staff_Details
					where
						ISNULL(is_disabled,'') = ''
					) s on s.ID = a.Co_Investigator
					and SOUNDEX (a.NAME) = SOUNDEX (s.SurName)
					
					order by a.id desc
					
				--union all
				

insert into [dbo].[UOE_P_PFACT_Prj_Dept_Split]
	(
	[PRJ_ID]
		  ,[Department]
		  ,[ChargeType]
		  ,[descr]
		  ,[StaffNumber]
		  ,[acc_code]
		  ,[StaffTypeDesc]
		  ,[1]
		  ,[2]
		  ,[3]
		  ,[4]
		  ,[5]
		  ,[6]
		  ,[7]
		  ,[8]
		  ,[9]
		  ,[10]
		  ,[11]
		  ,[12]
		  ,[13]
		  ,[14]
		  ,[15]
		  ,[TOTAL]
		  ,[stdate]
		  ,[eddate]
		  ,ROWID
		  ,hoursworked
		  ,PFACT_PAC_Mapping
		  ,CANFLAT

		   ,workpackage

		  ,recType 
		  ,coIN
		  ,finaladjust
	  )				
				
				
select 
@prj_id,
'' as Department,
isnull(CatChargeType,a.DefaultChargeType) as CatChargeType,
--CatChargeType,
--a.[Description] + ' - ' + Name ,
--a.[name] ,
isnull(c.person,name) ,
case
when CHARINDEX('-',isnull(c.person,''),1) > 0 then
		SUBSTRING(isnull(c.person,''),
			CHARINDEX('-',isnull(c.person,''),1)+1,
			10 )
			else
	'' end as StaffNumber,
isnull(b.code,'') as acc_code,

VATDesc,
cost as [1],
null as [2],
null as [3],
null as [4],
null as [5],
null as [6],
null as [7],
null as [8],
null as [9],
null as [10],
null as [11],
null as [12],
null as [13],
null as [14],
null as [15],
cost as TOTAL,
a.StartDate,
a.EndDate,
--				@st stdate,
--				@ed eddate,
A.ID,

			 case 
				 when Name like 'Pool Technician%' then
							 units 
			  else 0
			  end,


case 
when CHARINDEX('<script>', a.[Description],1) > 0 then
RTRIM(
SUBSTRING(
a.[Description], 1, CHARINDEX('<script>', a.[Description],1)-1) 
)
else a.[Description] end,
0 ,

	'00'
				, b.Description

				,a.ID
				,null
from @equip a
	left outer join dbo.LUP_Element_Categories b
		on a.CatID = b.Id
	left outer join [dbo].[UOE_pool_tech] c
		on c.rec = a.id

order by a.id desc

--	union all

-- SPON %

declare @fundperc float
set @fundperc = (
					 SELECT distinct 
					 MAX(ISNULL(FteProjectIncomeAdjustment.IncomeTemplatePercentage,0)) AS IncomeTemplatePercentage           
					 FROM FteProjectIncomeAdjustment   
					 Where Project=@prj_id
				)






-----------------------------------------





-----------------------------------------



-----------------------------------------

												insert into [dbo].[UOE_P_PFACT_Prj_Dept_Split]
												(
												[PRJ_ID]
													  ,[Department]
													  ,[ChargeType]
													  ,[descr]
													  ,[StaffNumber]
													  ,[acc_code]
													  ,[StaffTypeDesc]
													  ,[1]
													  ,[2]
													  ,[3]
													  ,[4]
													  ,[5]
													  ,[6]
													  ,[7]
													  ,[8]
													  ,[9]
													  ,[10]
													  ,[11]
													  ,[12]
													  ,[13]
													  ,[14]
													  ,[15]
													  ,[TOTAL]
													  ,[stdate]
													  ,[eddate]
													  ,ROWID
													  ,hoursworked
													  ,PFACT_PAC_Mapping
													  ,SponsorCONT
													  ,CANFLAT

													  ,workpackage
													,recType 
													,COIN
													,finaladjust
												  )				
	
												SELECT 
												@prj_id,
												--'' as Department,
												isnull(s.dept,'') as Department,
												ChargeType,
												--ExpenditureGroup + ' - ' + Name,
												expendituretype + ' - ' + Name,
												s.StaffNumber as StaffNumber,
												'' as acc_code,
												ExpenditureType,
												Amount as [1],
												null as [2],
												null as [3],
												null as [4],
												null as [5],
												null as [6],
												null as [7],
												null as [8],
												null as [9],
												null as [10],
												null as [11],
												null as [12],
												null as [13],
												null as [14],
												null as [15],
												AMOUNT as TOTAL,
																@st stdate,
																@ed eddate,
												'' AS ID		,

												0,
												/*
															case 
																 when unittype = 'FTE/Annum' then
																			 units * 
																			 (( DATEDIFF(D,StartDate, EndDate)*1.00
																			 / 365 ))-- * AnnualContractHours
							 
																when unittype = 'Hours' then			 
																			 (units / 1650)*1.00
							 
																when unittype = 'Days' then			 
																			 ( ( (units * 7.5) *1.00 )	/ 1650 )
							 
																when unittype = 'Hours/Week' then	
																			( units * ( ( DATEDIFF(d,StartDate,EndDate)/ 365 ) * 44 ) ) / 1650
							 
																when unittype = 'Months' then				
																			( ( units * 137.5 ) * 1.00 ) / 1650
							
															  else 0
															  end,
												*/
												ExpenditureType,

												--Amount * ( @fundperc / 100 ),
												NEWAMOUNT * ( @fundperc / 100 ),
												0
												,'00'
												,'FTE Related'
												,a.Coin
												,null
												 FROM @FTE_related a
													 left outer join (
																	select 
																	b.Description as dept,
																	case
																	when @prj_id = 10945 and min(a.StaffNumber) = '427735_P55886' then ''
																	else
																	a.surname + ' ' + a.firstname 
																	end as who ,
																	--a.StaffNumber
																	min(a.StaffNumber) as StaffNumber
																	from 
																	dbo.Staff_Details a
																	left outer join dbo.LUP_Department b
																			on a.Department = b.ID
																	-- SH 2016/01/19
																	WHERE
																		(
																		  ISNULL(End_Date,'') = ''  
																		  OR 
																		  End_Date >= GETDATE()
																		  )
																	-- SH 2016/01/29	 
																	and ISNULL(a.is_disabled,'') = '' 
																	and b.Description  <> 'Non College'
																	group by 					
																			b.Description ,
																			a.surname + ' ' + a.firstname 	
																	) s on s.who = a.Name
													 LEFT OUTER JOIN (
											
																		SELECT CoIn, SUM(Amount) AS NEWAMOUNT, ExpType  FROM dbo.CostIndirectIncome 
																		WHERE PROJECT = @prj_id
																		AND UseInflation = 1
																		GROUP BY CoIn, ExpType
																		) NN
																		ON NN.CoIn = A.Coin
																		AND NN.ExpType = A.ExpType
												 WHERE AMOUNT <> 0
 
													order by id desc
										
	
/*

select 
s.dept,
case
when CHARINDEX('-',REVERSE(a.descr),1) > 0 then
case
	when ISNUMERIC(
				substring(REVERSE(a.descr),1,CHARINDEX('-',REVERSE(a.descr),1)-1)
				) = 1
				then 
				reverse(substring(REVERSE(a.descr),1,CHARINDEX('-',REVERSE(a.descr),1)-1))
				else
				''
				end
else
''
end
,

a.* from [dbo].[UOE_P_PFACT_Prj_Dept_Split] a

left outer join (
					select 
					b.Description as dept,
					a.surname + ' ' + a.firstname as who ,
					a.StaffNumber
					from 
					dbo.Staff_Details a
					left outer join dbo.LUP_Department b
							on a.Department = b.ID
					) s on s.StaffNumber = 
								case
									when CHARINDEX('-',REVERSE(a.descr),1) > 0 then
									case
										when ISNUMERIC(
													substring(REVERSE(a.descr),1,CHARINDEX('-',REVERSE(a.descr),1)-1)
													) = 1
													then 
													reverse(substring(REVERSE(a.descr),1,CHARINDEX('-',REVERSE(a.descr),1)-1))
													else
													''
													end
									else
									''
									end

where PRJ_ID = @prj_id
and isnull(a.Department,'') = ''
and isnull(s.dept,'') <> ''

*/	
	
	
update [dbo].[UOE_P_PFACT_Prj_Dept_Split]
set Department = s.dept
from [dbo].[UOE_P_PFACT_Prj_Dept_Split] a
left outer join (
					select 
					b.Description as dept,
					a.surname + ' ' + a.firstname as who ,
					--a.StaffNumber
					min(a.StaffNumber) as StaffNumber
					from 
					dbo.Staff_Details a
					left outer join dbo.LUP_Department b
							on a.Department = b.ID
					where ISNULL(a.is_disabled,'') = ''
					group by 					
							b.Description ,
							a.surname + ' ' + a.firstname 		
					) s on s.StaffNumber = 
								case
									when CHARINDEX('-',REVERSE(a.descr),1) > 0 then
									case
										when ISNUMERIC(
													substring(REVERSE(a.descr),1,CHARINDEX('-',REVERSE(a.descr),1)-1)
													) = 1
													then 
													reverse(substring(REVERSE(a.descr),1,CHARINDEX('-',REVERSE(a.descr),1)-1))
													else
													''
													end
									else
									''
									end

where PRJ_ID = @prj_id
and isnull(a.Department,'') = ''
and isnull(s.dept,'') <> ''
		

declare @deptabl table
(
dept varchar(10),
deptdesc nvarchar(300)
)

insert into @deptabl
exec dbo.UOE_P_PRJ_Staff_Dept_Get @prj_id

if (select COUNT(*) from @deptabl ) = 1
begin
update [UOE_P_PFACT_Prj_Dept_Split]
set dept = ( select dept from @deptabl )
where prj_id = @prj_id
end



update [dbo].[UOE_P_PFACT_Prj_Dept_Split]
set SponsorCONT = b.total
--	select * 
from [dbo].[UOE_P_PFACT_Prj_Dept_Split] a
inner join (

		SELECT 
		ItemIdInProject AS ID ,
		SUM(Amount) Total
		FROM CostIncomeTemplate      
		INNER JOIN LUP_Income_Template ON CostIncomeTemplate.IncomeTemplate = LUP_Income_Template.Id      
		INNER JOIN Funders ON CostIncomeTemplate.Funder = Funders.Id      
		WHERE Project=@prj_id And ((IsSpine=1 And ItemIdInTable=1) OR (ItemIdInTable<>1))      
		GROUP BY Funder,ItemIdInProject,ItemIDInTable,LUP_Income_Template.ID,LUP_Income_Template.TemplateCode,       
		LUP_Income_Template.Description,Funders.Code,Funders.Description,EU_Submission,percentagefunded,ExceptionRule, ExceptionRule2  
		) b
		on a.ROWID = b.ID
	where a.PRJ_ID = @prj_id
	
	

/*
update [dbo].[UOE_P_PFACT_Prj_Dept_Split]
set SponsorCONT = b.Amount
from [dbo].[UOE_P_PFACT_Prj_Dept_Split] a
inner join (

		 SELECT
		 LUP_FIX_ICT_Class.ICT_Class_Description AS ChargeType,
		 LUP_Expenditure.Description ExpenditureType,      
		 SUM(C.Amount) Amount
		 FROM dbo.CostFteIncomeTemplate C (NOLOCK)      
		 INNER JOIN dbo.LUP_Expenditure (NOLOCK) ON C.FteType = dbo.LUP_Expenditure.Id      
		 INNER JOIN dbo.Expenditure_Group (NOLOCK) ON C.FteGroup = dbo.Expenditure_Group.ID      
		 INNER JOIN Lup_Fix_ICT_Class (NOLOCK) ON LUP_Expenditure.ICT_Class_ID = LUP_FIX_ICT_Class.ICT_Class_ID      
		 INNER JOIN Project_Proposal (NOLOCK) ON C.Project=Project_Proposal.ID      
		 INNER JOIN Lup_Inflation_Factor (NOLOCK) ON LUP_Expenditure.Inflation=Lup_Inflation_Factor.ID      
		 INNER JOIN Funders (NOLOCK) ON C.Funder = Funders.Id       
		 INNER JOIN Lup_Income_Template (NOLOCK) ON    C.IncomeTemplate=Lup_Income_Template.ID      
		 WHERE C.Project =@prj_id       
		 GROUP BY C.Funder, C.FteGroup,C.FteType,LUP_Expenditure.Description,Expenditure_Group.Description, LUP_FIX_ICT_Class.ICT_Class_Description,      
		 LUP_Expenditure.Inflation,Project_Proposal.Code,Project_Proposal.Project_Name,Project_Proposal.Start_Date,Project_Proposal.End_Date,Lup_Inflation_Factor.Description,Funders.Code,Funders.Description,      
		 TemplateCode, Lup_Income_Template.Description,C.IncomeTemplate ,C.IncomeType  
		 
		  ) b
				on a.ChargeType = b.ChargeType
				and 
				-- FTE Change 2016/01/25
				--a.descr = b.ExpenditureType
				
				case
				when charindex(' - ',descr,1) > 0 then
				SUBSTRING(descr, 1, charindex(' - ',descr,1)-1)
				else
				''
				end = b.ExpenditureType
				
	where a.PRJ_ID = @prj_id
	-- FTE Change 2016/01/25
	--and b.Amount = 0 
*/	
	
	update [dbo].[UOE_P_PFACT_Prj_Dept_Split]
	set SponsorCONT = 	r.inflatfteval
	
	from  [dbo].[UOE_P_PFACT_Prj_Dept_Split] a
	inner join (
			SELECT 
				FTEIncomeDetails.*, b.Amount as fecamounttojoinon, g.PercentValue,
				FTEIncomeDetails.Amount * ( g.PercentValue / 100) as inflatfteval
					FROM        
					  (SELECT CII.GroupID, Expenditure_Group.Description ExpenditureGroup,CII.ExpType,LUP_Expenditure.Description  ExpenditureType,       
					  SUM(CII.FTE) FTE, CASE ISNULL(PIA.ITEMID_IN_PROJECT,0) WHEN 0 THEN 0 ELSE SUM(CII.Amount) END Amount,        
					  CII.Funder FunderID,ISNULL(CII.EUExpenditureFTEID,0) EUExpenditureFTEID  ,
					  PIA.id
					  
					  FROM dbo.CostIndirectIncome CII(NOLOCK)        
					  LEFT JOIN dbo.Project_Income_Adjustment PIA(NOLOCK)ON (PIA.ITEMID_IN_PROJECT=CII.COIN AND PIA.ITEMIN_TABLE =1 AND (CII.StaffType<>3) 
					  AND PIA.FUNDER =CII.Funder) OR (PIA.ITEMID_IN_PROJECT=CII.COIN AND PIA.ITEMIN_TABLE =2 AND CII.StaffType=3 AND PIA.FUNDER =CII.Funder)        
					  INNER JOIN dbo.LUP_Expenditure (NOLOCK)ON LUP_Expenditure.ID=CII.ExpType        
					  INNER JOIN  dbo.Funders (NOLOCK)ON CII.Funder = Funders.Id        
					  INNER JOIN dbo.Expenditure_Group (NOLOCK)ON Expenditure_Group.ID=CII.GroupID        
					  WHERE CII.Project=@prj_id AND CII.UseInflation=1 
					  	  --and [EXPtype] = 3
					  GROUP BY CII.Funder,CII.GroupID, CII.ExpType,LUP_Expenditure.Description, Expenditure_Group.Description,        
					  CII.Funder,Funders.Description,Funders.Code,PIA.ITEMID_IN_PROJECT,EUExpenditureFTEID
					  ,PIA.id     
					  ) FTEIncomeDetails    
					 inner join (    
					 SELECT 
				*
					FROM        
					  (SELECT CII.GroupID, Expenditure_Group.Description ExpenditureGroup,CII.ExpType,LUP_Expenditure.Description  ExpenditureType,       
					  SUM(CII.FTE) FTE, CASE ISNULL(PIA.ITEMID_IN_PROJECT,0) WHEN 0 THEN 0 ELSE SUM(CII.Amount) END Amount,        
					  CII.Funder FunderID,ISNULL(CII.EUExpenditureFTEID,0) EUExpenditureFTEID  ,
					  PIA.id     
					  FROM dbo.CostIndirectIncome CII(NOLOCK)        
					  LEFT JOIN dbo.Project_Income_Adjustment PIA(NOLOCK)ON (PIA.ITEMID_IN_PROJECT=CII.COIN AND PIA.ITEMIN_TABLE =1 AND (CII.StaffType<>3) 
					  AND PIA.FUNDER =CII.Funder) OR (PIA.ITEMID_IN_PROJECT=CII.COIN AND PIA.ITEMIN_TABLE =2 AND CII.StaffType=3 AND PIA.FUNDER =CII.Funder)        
					  INNER JOIN dbo.LUP_Expenditure (NOLOCK)ON LUP_Expenditure.ID=CII.ExpType        
					  INNER JOIN  dbo.Funders (NOLOCK)ON CII.Funder = Funders.Id        
					  INNER JOIN dbo.Expenditure_Group (NOLOCK)ON Expenditure_Group.ID=CII.GroupID        
					  WHERE CII.Project=@prj_id AND CII.UseInflation=1 
					  	 -- and [EXPtype] = 3
					  GROUP BY CII.Funder,CII.GroupID, CII.ExpType,LUP_Expenditure.Description, Expenditure_Group.Description,        
					  CII.Funder,Funders.Description,Funders.Code,PIA.ITEMID_IN_PROJECT,EUExpenditureFTEID
					  ,PIA.id     
					  ) FTEIncomeDetails 
			) b on FTEIncomeDetails.ID = b.ID
			
			inner join (
				SELECT
				 LUP_Expenditure.Description ExpenditureType,      
				 PercentValue
				 FROM dbo.CostFteIncomeTemplate C (NOLOCK)      
				 INNER JOIN dbo.LUP_Expenditure (NOLOCK) ON C.FteType = dbo.LUP_Expenditure.Id 
				 WHERE C.Project =@prj_id       
				 GROUP BY 
				 LUP_Expenditure.Description ,      
				 PercentValue	 
				) g
				 on g.ExpenditureType = FTEIncomeDetails.ExpenditureType
				
				) r
	
		on r.ExpenditureType = 
						case
						when charindex(' - ',descr,1) > 0 then
						SUBSTRING(descr, 1, charindex(' - ',descr,1)-1)
						else
						''
						end
						and
						a.TOTAL = r.fecamounttojoinon
						
		where a.PRJ_ID = @prj_id
		
/*	
update [dbo].[UOE_P_PFACT_Prj_Dept_Split]
set SponsorCONT = a.TOTAL  * (isnull(b.PercentValue ,100.00)/ 100.00)
from [dbo].[UOE_P_PFACT_Prj_Dept_Split] a
inner join (
		SELECT
		 LUP_FIX_ICT_Class.ICT_Class_Description AS ChargeType,
		 LUP_Expenditure.Description ExpenditureType,      
		 SUM(C.Amount) Amount,
		 PercentValue
		 FROM dbo.CostFteIncomeTemplate C (NOLOCK)      
		 INNER JOIN dbo.LUP_Expenditure (NOLOCK) ON C.FteType = dbo.LUP_Expenditure.Id      
		 INNER JOIN dbo.Expenditure_Group (NOLOCK) ON C.FteGroup = dbo.Expenditure_Group.ID      
		 INNER JOIN Lup_Fix_ICT_Class (NOLOCK) ON LUP_Expenditure.ICT_Class_ID = LUP_FIX_ICT_Class.ICT_Class_ID      
		 INNER JOIN Project_Proposal (NOLOCK) ON C.Project=Project_Proposal.ID      
		 INNER JOIN Lup_Inflation_Factor (NOLOCK) ON LUP_Expenditure.Inflation=Lup_Inflation_Factor.ID      
		 INNER JOIN Funders (NOLOCK) ON C.Funder = Funders.Id       
		 INNER JOIN Lup_Income_Template (NOLOCK) ON    C.IncomeTemplate=Lup_Income_Template.ID      
		 WHERE C.Project =@prj_id       
		 GROUP BY C.Funder, C.FteGroup,C.FteType,LUP_Expenditure.Description,Expenditure_Group.Description, LUP_FIX_ICT_Class.ICT_Class_Description,      
		 LUP_Expenditure.Inflation,Project_Proposal.Code,Project_Proposal.Project_Name,Project_Proposal.Start_Date,Project_Proposal.End_Date,Lup_Inflation_Factor.Description,Funders.Code,Funders.Description,      
		 TemplateCode, Lup_Income_Template.Description,C.IncomeTemplate ,C.IncomeType  
		 ,PercentValue
		
		  ) b
				on a.ChargeType = b.ChargeType
				and 
				-- FTE Change 2016/01/25
				--a.descr = b.ExpenditureType
				
				case
				when charindex(' - ',descr,1) > 0 then
				SUBSTRING(descr, 1, charindex(' - ',descr,1)-1)
				else
				''
				end = b.ExpenditureType
				
	where a.PRJ_ID = @prj_id
	-- FTE Change 2016/01/25
	--and b.Amount <> 0 	

*/



---	staff

	--	Inflation flag CHECK
	/*
		declare @p2 varchar(100)
		set @p2=NULL
		declare @p3 int
		set @p3=NULL
		exec ProjectFirstFunderDetails @prj_id , @p2 output,@p3 output
			--	select @p2, @p3


		declare @pinflationflag int
		set @pinflationflag=NULL
		exec ProjectRecategoriseCost @p3 ,@pinflationflag output
			--	select @pinflationflag
			*/

			update [dbo].[UOE_P_PFACT_Prj_Dept_Split]
			set 
			[Spon1] = s.[Spon1],
			[Spon2] = s.[Spon2],
			[Spon3] = s.[Spon3],
			[Spon4] = s.[Spon4],
			[Spon5] = s.[Spon5],
			[Spon6] = s.[Spon6],
			[Spon7] = s.[Spon7],
			[Spon8] = s.[Spon8],
			[Spon9] = s.[Spon9],
			 [Spon10] = s.[Spon10],
			 [Spon11] = s.[Spon11],
			 [Spon12] = s.[Spon12],
			 [Spon13] = s.[Spon13],
			 [Spon14] = s.[Spon14],
			 [Spon15] = s.[Spon15],
			 SponTotal = s.SponTot
			 --		select *
					 from 
					 [dbo].[UOE_P_PFACT_Prj_Dept_Split] a
					 inner join (
					 select 
								COinID,
								
								sum(case when yr = 1 then p.Total else 0 end) as [Spon1],
								sum(case when yr = 2 then p.Total else 0 end) as [Spon2],
								sum(case when yr = 3 then p.Total else 0 end) as [Spon3],
								sum(case when yr = 4 then p.Total else 0 end) as [Spon4],
								sum(case when yr = 5 then p.Total else 0 end) as [Spon5],
								sum(case when yr = 6 then p.Total else 0 end) as [Spon6],
								sum(case when yr = 7 then p.Total else 0 end) as [Spon7],
								sum(case when yr = 8 then p.Total else 0 end) as [Spon8],
								sum(case when yr = 9 then p.Total else 0 end) as [Spon9],
								sum(case when yr = 10 then p.Total else 0 end) as [Spon10],
								sum(case when yr = 11 then p.Total else 0 end) as [Spon11],
								sum(case when yr = 12 then p.Total else 0 end) as [Spon12],
								sum(case when yr = 13 then p.Total else 0 end) as [Spon13],
								sum(case when yr = 14 then p.Total else 0 end) as [Spon14],
								sum(case when yr = 15 then p.Total else 0 end) as [Spon15],
								SUM(p.Total) as SponTot
								/*
								sum(case when yr = 1 then NewTotal else 0 end) as [Spon1],
								sum(case when yr = 2 then NewTotal else 0 end) as [Spon2],
								sum(case when yr = 3 then NewTotal else 0 end) as [Spon3],
								sum(case when yr = 4 then NewTotal else 0 end) as [Spon4],
								sum(case when yr = 5 then NewTotal else 0 end) as [Spon5],
								sum(case when yr = 6 then NewTotal else 0 end) as [Spon6],
								sum(case when yr = 7 then NewTotal else 0 end) as [Spon7],
								sum(case when yr = 8 then NewTotal else 0 end) as [Spon8],
								sum(case when yr = 9 then NewTotal else 0 end) as [Spon9],
								sum(case when yr = 10 then NewTotal else 0 end) as [Spon10],
								sum(case when yr = 11 then NewTotal else 0 end) as [Spon11],
								sum(case when yr = 12 then NewTotal else 0 end) as [Spon12],
								sum(case when yr = 13 then NewTotal else 0 end) as [Spon13],
								sum(case when yr = 14 then NewTotal else 0 end) as [Spon14],
								sum(case when yr = 15 then Total else 0 end) as [Spon15],
								SUM(NewTotal) as SponTot
									*/
								 from (

								  SELECT 
									ROW_NUMBER() OVER (PARTITION BY CoinID ORDER BY StartDate ) as yr,
									*,
									
									total * 
									case when 
									fundper.PercentageFunded = 100 then 1
									else fundper.PercentageFunded / 100 end as NewTotal
									
									from (
										Select
										CoinID,
										  StaffType,
										  StartDate,EndDate,
										  ISNULL(Basic,0) Basic, 
										  ISNULL(Ni,0) Ni, 
										  ISNULL(NiOut,0) NiOut,                 
										  ISNULL(Super,0) Super, 
										  ISNULL(C.Other_Allowances,0) Other_Allowances, 
										  ISNULL(C.Total ,0) Total ,                  
  
										  ISNull(LPSD.Basic_pay,0)+ISNULL(LPSD.National_ins,0)+ISNULL(LPSD.National_Ins_out,0)+            
										  ISNULL(LPSD.superAnnuation,0)+ISNULL(LPSD.other_allowances,0) AS SpineAmount
										  FROM dbo.CostStaffIncome  C      
										  LEFT JOIN dbo.LUP_Pay_Scale ON LUP_Pay_Scale.Id = C.PayScale                 
										  LEFT JOIN dbo.LUP_Sub_Element ON LUP_Sub_Element.ID = LUP_Pay_Scale.Sub_Element           
										  LEFT JOIN  dbo.LUP_PAY_SCALE_DETAILS LPSD ON  LPSD.pay_scale= LUP_Pay_Scale.Id AND C.SPINE=LPSD.SPINE                  
										  WHERE C.Project=@prj_id  AND C.IsSpine=0  AND 
											
										  C.UseInflation=  
										  (
											SELECT top 1 
											-- A.Project,
											-- A.ItemID_In_Project AS ID,
											case
											when 
											B.Description <>'Zero Inflation'
											then 1 else 0 end as inflation
											FROM 
											Project_Income_Adjustment A 
												LEFT JOIN 
												--	select *from 
												Lup_Inflation_Factor B ON             
														A.Inflation=B.ID 
												inner join dbo.CostStaffIncome c
														on c.Project = A.Project
											WHERE A.Project=@prj_id 
											)
												AND stafftype<>3            

										  UNION              
										  SELECT CoinID,
										  StaffType,
										  StartDate,EndDate,
										  ISNULL(Basic,0) Basic, 
										  ISNULL(Ni,0) Ni, 
										  ISNULL(NiOut,0) NiOut,                 
										  ISNULL(Super,0) Super, 
										  ISNULL(C.Other_Allowances,0) Other_Allowances, 
										  ISNULL(Total ,0) Total ,                  
										  ISNull(LPSD.Basic_pay,0)+ISNULL(LPSD.National_ins,0)+ISNULL(LPSD.National_Ins_out,0)+            
										  ISNULL(LPSD.superAnnuation,0)+ISNULL(LPSD.other_allowances,0) AS SpineAmount
										  FROM dbo.CostStaffIncome  C      
										  LEFT JOIN dbo.LUP_Pay_Scale ON LUP_Pay_Scale.Id = C.PayScale                 
										  LEFT JOIN dbo.LUP_Sub_Element ON LUP_Sub_Element.ID = LUP_Pay_Scale.Sub_Element           
										  LEFT JOIN  dbo.LUP_PAY_SCALE_DETAILS LPSD ON  LPSD.pay_scale= LUP_Pay_Scale.Id AND C.SPINE=LPSD.SPINE                       
										  WHERE C.Project=@prj_id   AND 
										  C.UseInflation=
										  (
											SELECT top 1 
											-- A.Project,
											-- A.ItemID_In_Project AS ID,
											case
											when 
											B.Description <>'Zero Inflation'
											then 1 else 0 end as inflation
											FROM 
											Project_Income_Adjustment A 
												LEFT JOIN 
												--	select *from 
												Lup_Inflation_Factor B ON             
														A.Inflation=B.ID 
												inner join dbo.CostStaffIncome c
														on c.Project = A.Project
											WHERE A.Project=@prj_id 
											)
										       AND stafftype=3         
										) z


												inner join (
														SELECT 
														--Funder,
														ItemIdInProject AS ID ,
														ItemIDInTable AS ElementType,
														-- SUM(Amount) Total,      
														-- LUP_Income_Template.ID IncomeTemplateID, 
														-- LUP_Income_Template.TemplateCode,   
														-- LUP_Income_Template.Description TemplateDesc,
														-- Funders.Code FunderCode,
														-- Funders.Description FunderDesc,
														-- AVG(PercentValue) PercentValue,
													percentageFunded
														-- EU_Submission,
														-- ISNULL(ExceptionRule,0) as ExceptionRule,
														-- ISNULL(ExceptionRule2,0) AS ExceptionRule2        
													FROM CostIncomeTemplate      
													INNER JOIN LUP_Income_Template ON CostIncomeTemplate.IncomeTemplate = LUP_Income_Template.Id      
													INNER JOIN Funders ON CostIncomeTemplate.Funder = Funders.Id      
													WHERE Project= @prj_id
													And ((IsSpine=1 And ItemIdInTable=1) OR (ItemIdInTable<>1))      
													) fundper
														on fundper.id = z.COinID

										) p
									
										group by
										COinID

										) s
											on s.COinID = a.coin
										where prj_id  = @prj_id
										and rectype = 'staff'
									--	order by chargetype, descr
							



---		equip

		update [dbo].[UOE_P_PFACT_Prj_Dept_Split]
			set 
			[Spon1] = s.[Spon1],
			[Spon2] = s.[Spon2],
			[Spon3] = s.[Spon3],
			[Spon4] = s.[Spon4],
			[Spon5] = s.[Spon5],
			[Spon6] = s.[Spon6],
			[Spon7] = s.[Spon7],
			[Spon8] = s.[Spon8],
			[Spon9] = s.[Spon9],
			 [Spon10] = s.[Spon10],
			 [Spon11] = s.[Spon11],
			 [Spon12] = s.[Spon12],
			 [Spon13] = s.[Spon13],
			 [Spon14] = s.[Spon14],
			 [Spon15] = s.[Spon15],
			 SponTotal = s.SponTot
			 
			-- , SponsorCONT = s.SponTot
			 --	select *
					 from 
					 [dbo].[UOE_P_PFACT_Prj_Dept_Split] a
					 inner join (
					 select 
								COinID,
								sum(case when yr = 1 then Total else 0 end) as [Spon1],
								sum(case when yr = 2 then Total else 0 end) as [Spon2],
								sum(case when yr = 3 then Total else 0 end) as [Spon3],
								sum(case when yr = 4 then Total else 0 end) as [Spon4],
								sum(case when yr = 5 then Total else 0 end) as [Spon5],
								sum(case when yr = 6 then Total else 0 end) as [Spon6],
								sum(case when yr = 7 then Total else 0 end) as [Spon7],
								sum(case when yr = 8 then Total else 0 end) as [Spon8],
								sum(case when yr = 9 then Total else 0 end) as [Spon9],
								sum(case when yr = 10 then Total else 0 end) as [Spon10],
								sum(case when yr = 11 then Total else 0 end) as [Spon11],
								sum(case when yr = 12 then Total else 0 end) as [Spon12],
								sum(case when yr = 13 then Total else 0 end) as [Spon13],
								sum(case when yr = 14 then Total else 0 end) as [Spon14],
								sum(case when yr = 15 then Total else 0 end) as [Spon15],
								SUM(total) as SponTot

								 from (

								  SELECT 
									ROW_NUMBER() OVER (PARTITION BY CoinID ORDER BY StartDate ) as yr,
									*
									from (
										  SELECT ProjPropOtherDetails as COinID,Funder,StartDate,EndDate,
											ISNULL(Cost,0) total,
											--ISNULL(Cost * ( 1 - C.VAT_Percent / 100 ) ,0) total,
										Element  
										FROM dbo.CostEquNonStaffIncome  C      
										WHERE C.Project=@prj_id AND C.UseInflation=
										
										(
											SELECT top 1 
											-- A.Project,
											-- A.ItemID_In_Project AS ID,
											case
											when 
											B.Description <>'Zero Inflation'
											then 1 else 0 end as inflation
											FROM 
											Project_Income_Adjustment A 
												LEFT JOIN 
												--	select *from 
												Lup_Inflation_Factor B ON             
														A.Inflation=B.ID 
												inner join dbo.CostEquNonStaffIncome c
														on c.Project = A.Project
											WHERE A.Project=@prj_id 
											)
											         
										) z

										) p

										group by
										COinID

										) s
											on s.COinID = a.coin
										where prj_id  = @prj_id


---		fte

		update [dbo].[UOE_P_PFACT_Prj_Dept_Split]
			set 
			[Spon1] = s.[Spon1],
			[Spon2] = s.[Spon2],
			[Spon3] = s.[Spon3],
			[Spon4] = s.[Spon4],
			[Spon5] = s.[Spon5],
			[Spon6] = s.[Spon6],
			[Spon7] = s.[Spon7],
			[Spon8] = s.[Spon8],
			[Spon9] = s.[Spon9],
			 [Spon10] = s.[Spon10],
			 [Spon11] = s.[Spon11],
			 [Spon12] = s.[Spon12],
			 [Spon13] = s.[Spon13],
			 [Spon14] = s.[Spon14],
			 [Spon15] = s.[Spon15],
			 SponTotal = s.SponTot
			 --	select *
					 from 
					 [dbo].[UOE_P_PFACT_Prj_Dept_Split] a
					 inner join (
					 select 
								COinID,
								sum(case when yr = 1 then Total else 0 end) as [Spon1],
								sum(case when yr = 2 then Total else 0 end) as [Spon2],
								sum(case when yr = 3 then Total else 0 end) as [Spon3],
								sum(case when yr = 4 then Total else 0 end) as [Spon4],
								sum(case when yr = 5 then Total else 0 end) as [Spon5],
								sum(case when yr = 6 then Total else 0 end) as [Spon6],
								sum(case when yr = 7 then Total else 0 end) as [Spon7],
								sum(case when yr = 8 then Total else 0 end) as [Spon8],
								sum(case when yr = 9 then Total else 0 end) as [Spon9],
								sum(case when yr = 10 then Total else 0 end) as [Spon10],
								sum(case when yr = 11 then Total else 0 end) as [Spon11],
								sum(case when yr = 12 then Total else 0 end) as [Spon12],
								sum(case when yr = 13 then Total else 0 end) as [Spon13],
								sum(case when yr = 14 then Total else 0 end) as [Spon14],
								sum(case when yr = 15 then Total else 0 end) as [Spon15],
								SUM(total) as SponTot,
								stafftypedesc

								 from (

								  SELECT 
									ROW_NUMBER() OVER (PARTITION BY convert(varchar,CoinID)+stafftypedesc ORDER BY StartDate ) as yr,
									*
									from (
										SELECT C.Coin as coinid,
										C.StaffType,C.GroupId,C.ExpType,C.Funder,StartDate,EndDate,
								   ISNULL(Amount,0) as total,   
										ISNULL(Fte,0) Fte,
										
										d.Description  as stafftypedesc   
										  FROM dbo.CostIndirectIncome  C     
											left outer join [dbo].[LUP_Expenditure] d
												on C.ExpType = d.id       
								   WHERE C.Project=@prj_id  AND   C.UseInflation=1 

										) z

										) p

										group by
										COinID,
										stafftypedesc

										) s
											on s.COinID = a.coin
											and s.stafftypedesc = a.StaffTypeDesc
										where prj_id  = @prj_id
										and rectype <> 'staff'

	


	
update 	 [dbo].[UOE_P_PFACT_Prj_Dept_Split]
set PFACT_PAC_Mapping = LTRIM(RTRIM(REPLACE(REPLACE(replace(PFACT_PAC_Mapping,CHAR(9),''), CHAR(13),' '), CHAR(10), ' ')))
where PRJ_ID = @prj_id	
	
update [dbo].[UOE_P_PFACT_Prj_Dept_Split]
set new_PACcat = b.new_PACcat
from [dbo].[UOE_P_PFACT_Prj_Dept_Split] a
inner join dbo.UOE_PFACT_PAC_Cost_Category b
				on a.ChargeType = b.PAC_Category
				 and rtrim(a.PFACT_PAC_Mapping) = rtrim(b.pFact_Category)
	where a.PRJ_ID = @prj_id	
	


update [dbo].[UOE_P_PFACT_Prj_Dept_Split]	
set dept = b.Code
from [dbo].[UOE_P_PFACT_Prj_Dept_Split]	a
inner join [dbo].[LUP_Department] b
	on a.Department = b.Description
where a.PRJ_ID = @prj_id

DELETE FROM UOE_P_PFACT_Prj_Dept_Split_HOURS
WHERE PRJ_ID = @prj_id

INSERT INTO UOE_P_PFACT_Prj_Dept_Split_HOURS
SELECT
	[PRJ_ID]
	,[ROWID]
    --  ,[ChargeType]
      ,[descr]
      ,[StaffNumber]
      --,[acc_code]
      ,[StaffTypeDesc]
     -- ,[TOTAL]
      ,[stdate]
      ,[eddate]
      ,[hoursworked]
  FROM [dbo].[UOE_P_PFACT_Prj_Dept_Split]
  where ISNULL([hoursworked],'') <> ''
  and PRJ_ID = @prj_id
  
  
  
  --	fte coinid update
		update 
		[dbo].[UOE_P_PFACT_Prj_Dept_Split] 
		set ROWID = b.Coin
		--	Select a.ROWID, b.Coin,*
		from 
			[dbo].[UOE_P_PFACT_Prj_Dept_Split]  a
			inner join 
					(
						select expendituretype + ' - ' + Name as descr, * from @FTE_related
					) b on b.descr = a.descr
-- Change 31/07/2018 - Extra join needed to split times for the same member of staff
					 and b.amount = a.[1]
		where PRJ_ID = @prj_id
		and ISNULL(rowid,0) = 0


			----------------------------------
			/*
		update [dbo].[UOE_P_PFACT_Prj_Dept_Split]
		set 
			[1] = b.[1],
			[2] = b.[2],
			[3] = b.[3],
			[4] = b.[4],
			[5] = b.[5],
			[6] = b.[6],
			[7] = b.[7],
			[8] = b.[8],
			[9] = b.[9],
			[10] = b.[10],
			[11] = b.[11],
			[12] = b.[12],
			[13] = b.[13],
			[14] = b.[14],
			[15] = b.[15]
				--	select b.*, a.*
			from [dbo].[UOE_P_PFACT_Prj_Dept_Split] a
				inner join 
					( select 
						CoIn, descr, 
							SUM( case when yr = 1 then isnull(amount,0) end) as [1],
							SUM( case when yr = 2 then isnull(amount,0) end) as [2],
							SUM( case when yr = 3 then isnull(amount,0) end) as [3],
							SUM( case when yr = 4 then isnull(amount,0) end) as [4],
							SUM( case when yr = 5 then isnull(amount,0) end) as [5],
							SUM( case when yr = 6 then isnull(amount,0) end) as [6],
							SUM( case when yr = 7 then isnull(amount,0) end) as [7],
							SUM( case when yr = 8 then isnull(amount,0) end) as [8],
							SUM( case when yr = 9 then isnull(amount,0) end) as [9],
							SUM( case when yr = 10 then isnull(amount,0) end) as [10],
							SUM( case when yr = 11 then isnull(amount,0) end) as [11],
							SUM( case when yr = 12 then isnull(amount,0) end) as [12],
							SUM( case when yr = 13 then isnull(amount,0) end) as [13],
							SUM( case when yr = 14 then isnull(amount,0) end) as [14],
							SUM( case when yr = 15 then isnull(amount,0) end) as [15]

					 from 
						(
							select 
							CoIn,
							ExpenditureType + ' - ' + Name as descr, 
							ROW_NUMBER() OVER (PARTITION BY ExpenditureType + ' - ' + Name ORDER BY startdate ) as [yr],
							FTE, amount from (
									 SELECT CostIndirectCost.Coin, 
										case CostIndirectCost.StaffType         
										when 1 then  Staff_Details.SURNAME + ' ' + Staff_Details.FIRSTNAME        
										when 2 then Non_Standard_Staff_Using_PayScale.STAFF_NAME        
										when 3 then Project_Proposal_Other_Staff_Details.Name        
										when 4 then  StaffDetailsOff.SURNAME + ' ' + StaffDetailsOff.FIRSTNAME        
										when 5 then OffScaleGrade.StaffName         
										when 6 then Non_Standard_Staff_Using_PayScale.STAFF_NAME         
										end as Name,        
										Expenditure_Template_Expenditure.GroupID, 
										Expenditure_Group.Description ExpenditureGroup,
										ExpType,
										LUP_Expenditure.Description ExpenditureType,                
										(CostIndirectCost.FTE) FTE, 
										(CostIndirectCost.Amount) Amount,
										LUP_FIX_ICT_Class.ICT_Class_Description AS ChargeType ,               
										--Project_Proposal.Start_Date ProjectStart,Project_Proposal.End_Date ProjectEnd,
										CostIndirectCost.StartDate,
										CostIndirectCost.EndDate
	   
										FROM CostIndirectCost                
										INNER JOIN LUP_Expenditure ON LUP_Expenditure.ID=CostIndirectCost.ExpType                
										INNER JOIN Expenditure_Template_Expenditure ON Expenditure_Template_Expenditure.ID=ExpenditureFTEID                
										INNER JOIN Expenditure_Group ON Expenditure_Group.ID=CostIndirectCost.GroupID                
										INNER JOIN LUP_FIX_ICT_Class ON LUP_Expenditure.ICT_Class_ID = LUP_FIX_ICT_Class.ICT_Class_ID                
										INNER JOIN Project_Proposal ON CostIndirectCost.Project=Project_Proposal.ID                
										INNER JOIN Project_Proposal_Income_Template ON Project_Proposal_Income_Template.Project_Proposal = Project_Proposal.ID                
										INNER JOIN Income_Template_Funders ON Project_Proposal_Income_Template.Income_Template_Funder = Income_Template_Funders.ID                 
										INNER JOIN  Funders ON Income_Template_Funders.Funder = Funders.Id                
										--INNER JOIN LUP_Income_Template ON Income_Template_Funders.Income_Template = LUP_Income_Template.Id        
										LEFT JOIN Project_Proposal_Co_Investigators on Project_Proposal_Co_Investigators.ID= CostIndirectCost.Coin        
										LEFT JOIN Project_Proposal_Other_Staff_Details ON Project_Proposal_Other_Staff_Details.ID = CostIndirectCost.Coin AND CostIndirectCost.StaffType=3  
										LEFT JOIN Staff_Details ON Staff_Details.ID = Project_Proposal_Co_Investigators.co_investigator  AND (Non_Standard=1 AND CostIndirectCost.StaffType=1)      
										LEFT JOIN Non_Standard_Staff_Using_PayScale ON Non_Standard_Staff_Using_PayScale.ID = Project_Proposal_Co_Investigators.co_investigator AND (CostIndirectCost.StaffType=2 OR CostIndirectCost.StaffType=6)       
										LEFT JOIN OffScaleGrade On  OffScaleGrade.ID =PROJECT_PROPOSAL_CO_INVESTIGATORS.Co_Investigator AND (CostIndirectCost.StaffType=4 OR CostIndirectCost.StaffType=5)   
										LEFT JOIN Staff_Details AS StaffDetailsOff ON StaffDetailsOff.ID = OffScaleGrade.StaffID AND OffScaleGrade.Staffname IS NULL             
										WHERE Project=@prj_id  AND UseInflation=1  
										and CostIndirectCost.Amount <> 0
								) z
						) r
						group by
						CoIn, descr
					) b 
						on a.ROWID = b.CoIn
						and a.descr = b.descr
			
						where PRJ_ID = @prj_id
							and rectype = 'FTE Related'
						*/
					----------------------------------

		update [dbo].[UOE_P_PFACT_Prj_Dept_Split]
		set finaladjust = null
		where PRJ_ID = @prj_id



		----------------------------
		--- additional data

		declare @FUNDING_TEMP VARCHAR(400)

			SET @FUNDING_TEMP = (
			                             
							 SELECT top 1
							 LUP_Income_Template.TemplateCode
							 FROM Org_Details, Project_Proposal_Income_Template                                      
							 INNER JOIN Income_Template_Funders ON Project_Proposal_Income_Template.Income_Template_Funder=Income_Template_Funders.ID                                      
							 INNER JOIN LUP_Income_Template ON Income_Template_Funders.Income_Template=LUP_Income_Template.ID                                      
							 INNER JOIN Funders ON Income_Template_Funders.Funder=Funders.ID                                     
							 INNER JOIN LUP_Funding_Source ON Funders.FundingSource= LUP_Funding_source.ID                                     
							WHERE Project_Proposal_Income_Template.Project_Proposal=@prj_id       
							) 

			--	select @FUNDING_TEMP

		delete from T1_Staff_Data
		where prj_id = @prj_id

		delete from T1_Other_Data
		where prj_id = @prj_id

		
					declare @staffdata TABLE 
					 (                    
					  ID INT NULL,                    
					  Staff_Type INT NULL,                    
					  StaffType VARCHAR (50) NULL,                    
					  Co_Investigator INT NULL,                    
					  NAME VARCHAR(100),                    
					  Sub_Element INT NULL,                    
					  CODE VARCHAR (30) NULL,                    
					  Description VARCHAR(900),   --VARCHAR (100) NULL,                    
					  Tbl INT NULL,            
					  FTE FLOAT NULL,                    
					  Category VARCHAR(900),	--VARCHAR(100) NULL,                    
					  CATID INT NULL,                    
					  Sequence_NO INT NULL,                    
					  StartDate DATETIME NULL,                    
					  EndDate DATETIME NULL,                    
					  TOTAL FLOAT NULL,                    
					  EditedFTE FLOAT NULL,                    
					  RoleSequence INT NULL,                      
					  StaffTypeDesc VARCHAR(100) NULL,                    
					  SpinePoint INT NULL,                    
					  SpineOnDate DATETIME NULL,                    
					  IncrementDate DATETIME NULL,                    
					  DiscretionaryPoint INT NULL,                    
					  Payscale  INT NULL,              
					  Department VARCHAR(100) NULL,        
					  ChargeType VARCHAR (100), --modification 11        
					  ChargeTypeID INT NULL ,  --modification 11     
					  [AnnualContractHours] FLOAT NULL,    
					  [AnnualContractDays] FLOAT NULL,    
					  [WeeksInYear] FLOAT NULL                 
					 )   
					insert into @staffdata
					EXEC ProjectStaffCostDetailed @prj_id,1,1,5,''


							-- select * from @staffdata


						insert into 
						--	select * from
						T1_Staff_Data

						select @prj_id as prj_id, 
							a.id as rowid,
							StaffType,
							StaffTypeDesc,
							--Co_Investigator,
							--s.StaffNumber,
							CASE
							WHEN CHARINDEX('_',s.StaffNumber,1) > 0 THEN
							SUBSTRING(
								s.StaffNumber,CHARINDEX('_',s.StaffNumber,1)+1, 100) + '_' +
								SUBSTRING(
								s.StaffNumber,1, CHARINDEX('_',s.StaffNumber,1)-1)
							ELSE s.StaffNumber END
								,

							NAME,
							FTE,
							CATID,
							a.ChargeType,
							b.Description as catDesc,
							b.Code as catCode,
							a.Description,
							t.t1_bud_temp_heading,
							@FUNDING_TEMP,
							a.StartDate,
							a.EndDate,
							z.Units,
							z.Description
							from 
								@staffdata a
								left outer join ( select * from [dbo].[Staff_Details] 
								 where ISNULL(is_disabled,'') = '' ) s
										on s.ID = a.Co_Investigator
								left outer join [dbo].[LUP_Element_Categories] b
										on a.CATID = b.Id

										left outer join T1_CSV_Budget_Heading_Map t
										on b.Code = t.nominal
											and pfact_template = @FUNDING_TEMP

											left outer join (
														select CoInvestigator, units, Description from
														Project_Details_Staff_Cost  A(NOLOCK)     
																INNER JOIN Lup_Fix_Duration B(NOLOCK) ON A.unit_Type=B.ID 
																) z
																	on a.ID = z.CoInvestigator

	

					 declare @equipdata TABLE 
					 (                        
					  ID INT NULL,                        
					  Rate FLOAT NULL,                        
					  Units FLOAT NULL,                        
					  CatID INT NULL,                        
					  Description VARCHAR(900) NULL,                        
					  Name VARCHAR(100) NULL,                        
					  Code VARCHAR (30) NULL,                        
					  UnitType VARCHAR (50) NULL,                        
					  UnitTypeID INT NULL,                        
					  Sequence_No INT NULL,                        
					  ChargeType VARCHAR (70) NULL,                        
					  StartDate DateTime NULL,                        
					  EndDate DateTime NULL,                        
					  ItemType INT NULL,                        
					  Sub_Element INT NULL,                        
					  VAT_Percent FLOAT NULL,                        
					  COST FLOAT NULL,                        
					  VAT_Type INT NULL,                        
					  VAT_Cost FLOAT NULL,                          
					  Cost_Before_VAT FLOAT NULL,                        
					  IsSwitchToConsumables BIT NULL,                        
					  OrgConumableLimit FLOAT NULL,                        
					  ConsumableCategory INT NULL,                        
					  Inflation_Factor_ID INT NULL,                        
					  Element INT NULL,                        
					  --RecategoriseToConsumable BIT NULL,                        
					  Effective_Date DATETIME NULL,                        
					  ProjectCode VARCHAR(20),                        
					  ProjectName VARCHAR(100),                        
					  DefaultChargeType VARCHAR(70),    
					  ExceptionRule int,                    
					  RateOnDate Datetime,       
					  VatDesc varchar(100),                 
					  ProjectStart DateTime,                        
					  ActualElement INT,                      
					  ResearchStudent INT NULL                        
					 )        

					insert into @equipdata
					EXEC ProjectEquipmentCostDetailed @prj_id,0,1,0,1

						--	select * from @equipdata

					insert into 
					T1_Other_Data
					
					
					select 
					@prj_id as prj_id,
					a.ID as rowid,
					a.UnitType, 
					a.Code,
					a.Name,
					a.cost,
					a.VAT_Percent,
					a.VAT_Cost,
					a.Cost_Before_VAT,
					a.CatID,
					a.ChargeType,
					b.Description as catDesc,
					b.Code as catCode,
					a.Units,
					t.t1_bud_temp_heading,
					@FUNDING_TEMP,
					a.StartDate,
					a.EndDate
					from 
								@equipdata a
								left outer join [dbo].[LUP_Element_Categories] b
										on a.CATID = b.Id
										left outer join T1_CSV_Budget_Heading_Map t
										on b.Code = t.nominal
											and pfact_template = @FUNDING_TEMP


		-----------------------------------

		--	align FTE related costs to record start and end dates

		update [dbo].[UOE_P_PFACT_Prj_Dept_Split]
		set 
			stdate = b.startdate,
			eddate = b.enddate
		from [dbo].[UOE_P_PFACT_Prj_Dept_Split] a
			inner join @staffdata b
				on a.ROWID = b.id
		where PRJ_ID = @prj_id
		and recType = 'FTE Related'


		--------------------------------------


/*
DELETE FROM dbo.[UOE_P_PFACT_Prj_Dept_Split]

where prj_id = @prj_id 
AND ID IN 
	(	
	SELECT A.id
	--, B.* 
	FROM [UOE_P_PFACT_Prj_Dept_Split] A
	INNER JOIN dbo.UOE_costdetails_UOE_Other B
	ON A.ROWID = B.REC
	WHERE UOE <> 'ON'
	AND A.PRJ_ID = @prj_id
	)
*/

/*
		update [dbo].[UOE_P_PFACT_Prj_Dept_Split]
		set ChargeType = b.PAC_category
		from [dbo].[UOE_P_PFACT_Prj_Dept_Split] a
		left outer join UOE_PFACT_PAC_Cost_Category b
				on 
				case 
				when charindex('<script>',a.descr,1) > 0 then
				rtrim(SUBSTRING(a.descr,1,charindex('<script>',a.descr,1)-1))
				else a.descr end
					= b.pfact_category
		where PRJ_ID = @prj_id

*/

/*
DECLARE @MAXPROFYR INT
SET @MAXPROFYR  = (
			SELECT 
				CASE
				WHEN ISNULL(MAX([15]),0) <> 0 THEN 15
				WHEN ISNULL(MAX([14]),0) <> 0 THEN 14
				WHEN ISNULL(MAX([13]),0) <> 0 THEN 13
				WHEN ISNULL(MAX([12]),0) <> 0 THEN 12
				WHEN ISNULL(MAX([11]),0) <> 0 THEN 11
				WHEN ISNULL(MAX([10]),0) <> 0 THEN 10
				WHEN ISNULL(MAX([9]),0) <> 0 THEN 9
				WHEN ISNULL(MAX([8]),0) <> 0 THEN 8
				WHEN ISNULL(MAX([7]),0) <> 0 THEN 7
				WHEN ISNULL(MAX([6]),0) <> 0 THEN 6
				WHEN ISNULL(MAX([5]),0) <> 0 THEN 5
				WHEN ISNULL(MAX([4]),0) <> 0 THEN 4
				WHEN ISNULL(MAX([3]),0) <> 0 THEN 3
				WHEN ISNULL(MAX([2]),0) <> 0 THEN 2
				WHEN ISNULL(MAX([1]),0) <> 0 THEN 1
				END
				 FROM [dbo].[UOE_P_PFACT_Prj_Dept_Split]
				WHERE PRJ_ID = @prj_id
				)

	IF @MAXPROFYR > 1
	BEGIN
		UPDATE [dbo].[UOE_P_PFACT_Prj_Dept_Split]
		SET FLAT_LINE = 'on'
		from [dbo].[UOE_P_PFACT_Prj_Dept_Split]
		WHERE PRJ_ID = @prj_id
		and [2] is null 
	
	END
*/

end		



--truncate table [UOE_P_PFACT_Prj_Dept_Split]




