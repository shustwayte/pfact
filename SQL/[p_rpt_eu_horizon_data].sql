USE [pfact_t1]
GO
/****** Object:  StoredProcedure [dbo].[p_rpt_eu_horizon_data]    Script Date: 12/09/2018 15:25:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER PROC [dbo].[p_rpt_eu_horizon_data]
(
@prj_id int
)
		--		exec [dbo].[p_rpt_eu_horizon_data] 10489

as

begin

			select 
			ISNULL(c.name, a.descr) as name,
			case
			when ISNULL(c.name,'') = '' then 'NO' else 'YES' end as collpayrol,
			a.StaffTypeDesc,
			isnull(c.SpinePoint,0) as spine,
			[1],
			[2],
			[3],
			[4],
			[5],
			[6],
			[7],
			[8],
			[9],
			[10],
			[11],
			[12],
			[13],
			[14],
			[15],
			[total],
			a.fte,
			--DATEDIFF(month,stdate, eddate)+1 as mononprj,
			z.mthduration

			from [dbo].[UOE_P_PFACT_Prj_Dept_Split] a
			left outer join (
													select a.id, b.FirstName + ' ' + b.SurName as name,
													b.End_Date, B.SpinePoint
													 from 
													PROJECT_Proposal_Co_investigators  (nolock) a
														inner join 
														[dbo].[Staff_Details]  (nolock) b
															on a.Co_Investigator = b.ID
													where Project_Proposal = @prj_id
													) c
													on a.ROWID= c.id

				left outer join (
										select *,
										DATEDIFF(MONTH, startdate, enddate) + 1 as mthduration
										 from T1_Staff_Data (nolock) 
										where prj_id = @prj_id
									) z
									on z.prj_id = a.PRJ_ID
									and a.ROWID = z.rowid

				where a.prj_id  = @prj_id
				 and 
				 (
				 recType = 'Staff'
				 or 
				 recType  = 'DA COST - TECHNICIAN'
				 )

end


