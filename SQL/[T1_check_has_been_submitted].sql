USE [pfact_t1]
GO
/****** Object:  StoredProcedure [dbo].[T1_check_has_been_submitted]    Script Date: 17/10/2018 08:27:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


USE [pfact_t1]
GO
/****** Object:  StoredProcedure [dbo].[T1_check_has_been_submitted]    Script Date: 17/10/2018 09:46:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER proc [dbo].[T1_check_has_been_submitted]
(
@prjid  int
)

as

begin
				--  exec T1_check_has_been_submitted 10749




	 
	DECLARE @TEMPFUND TABLE(
		[Id] [int] NULL,
		[Funder] [varchar](100) NULL,
		[FunderID] [int] NULL,
		[Address] [varchar](203) NULL,
		[FunderAddress] [varchar](152) NULL,
		[PostCode] [varchar](50) NULL,
		[FunderScheme] [int] NULL,
		[FunderCode] [varchar](20) NULL,
		[FundingSource] [varchar](50) NULL,
		[ExceptionCost] [float] NULL,
		[Income_Template] [int] NULL,
		[TemplateCode] [varchar](40) NULL,
		[TemplateDesc] [varchar](50) NULL,
		[FundingSchemeDesc] [varchar](6000) NULL,
		[jesSchemeID] [varchar](50) NULL,
		[jesDocumentSubType] [varchar](100) NULL,
		[EU_Activity] [varchar](200) NULL,
		[blnAllowDiscretionaryPoint] [bit] NULL,
		[blnAllowMulptiplier] [bit] NULL,
		[Amount] [float] NULL,
		[IsPrimaryFunder] [bit] NULL,
		[Income_Type] [int] NULL,
		[Recategorisetoconsumable] [bit] NULL,
		[FUNDEDPERCENTAGE] [float] NULL,
		[INCOMETEMPLATEPERCENTAGE] [float] NULL
	)
	
	INSERT INTO @TEMPFUND
		EXEC ProjectIncomeTemplates @prjid,0

		-- check has template
		if ( select COUNT(*) from @TEMPFUND ) > 0
		begin



					SELECT 
								 --Project_Proposal_Status.ID,
								 --A.Description as Status_From, 
								 B.Description AS Status_TO,  
								-- Project_Proposal_Status.Created_On, 
								 convert(varchar,MAX(Project_Proposal_Status.Date_Of_Change),103) AS Date_Of_Change
								 --Project_Proposal_Status.Target_Date,   
								 --Project_Proposal_Status.Notes,  
								 --D.SurName + ' '+ D.FirstName AS SubmittedTo,  
								 --LUP_Users.User_Name AS Created_BY  
								 /*
								  CASE WHEN (LUP_Users.Name IS NULL OR LUP_USERS.NAME ='') AND (LUP_Users.Staff_ID IS NOT NULL)  THEN   
											 E.SurName + ' ' +E.FirstName  
									   WHEN (LUP_Users.Name IS NOT NULL OR LUP_USERS.NAME<>'') AND (LUP_Users.Staff_ID IS NULL ) THEN   
										 LUP_Users.Name  
									Else LUP_Users.User_Name 
									 END AS Created_By 
								   */                       
								 FROM Project_Proposal_Status   
								 INNER JOIN LUP_Status AS A ON  Project_Proposal_Status.Status_From = A.ID  
								 INNER JOIN LUP_Status AS B ON  Project_Proposal_Status.Status_TO = B.ID  
								 INNER JOIN LUP_Users ON LUP_Users.ID= Project_Proposal_Status.Created_By  
								 LEFT JOIN Staff_Details As E on E.Id = Lup_Users.staff_ID  
								 LEFT JOIN Staff_Details AS D ON Project_Proposal_Status.SubmittedTo=D.ID  

								  inner join (
									  select MAX(created_on) as cdate from Project_Proposal_Status
									  WHERE Project_Proposal= @prjid ) z on 
														Project_Proposal_Status.created_on = z.cdate


									--	INNER JOIN @TEMPFUND R
										--	ON R.Id = Project_Proposal

								 WHERE Project_Proposal= @prjid 
								 AND B.Description = 'Application Submitted'
								 group by B.Description
		end
			
end