USE [pfact_t1]
GO
/****** Object:  StoredProcedure [dbo].[p_rpt_FTE_details]    Script Date: 12/09/2018 15:26:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER PROC [dbo].[p_rpt_FTE_details]
(
@prj_id int
)

as

begin
		select distinct  StaffTypeDesc, prj_id
		from [dbo].[UOE_P_PFACT_Prj_Dept_Split] (nolock) 
			where prj_id  = @prj_id
			and recType = 'FTE Related'
end	

