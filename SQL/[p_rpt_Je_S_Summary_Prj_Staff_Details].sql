USE [pfact_t1]
GO
/****** Object:  StoredProcedure [dbo].[p_rpt_Je_S_Summary_Prj_Staff_Details]    Script Date: 12/09/2018 15:26:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



ALTER PROC [dbo].[p_rpt_Je_S_Summary_Prj_Staff_Details] 
(
@prj_id int
)

as

		---	exec [dbo].[p_rpt_Je_S_Summary_Prj_Staff_Details] 10440

begin


	

			select 
			a.ROWID,
			rectype, 
			a.PRJ_ID, a.ChargeType, a.descr, a.StaffTypeDesc,
			hoursworked, b.Sponval, 
			isnull(c.name,a.descr) as name , a.FTE, 
			z.units,
			z.unitsDescr,
			z.mthduration,

			(hoursworked / (z.mthduration / 12.00) ) / 44			
			as avgHrpW,
			a.SponTotal as costEst,
			( a.SponTotal / hoursworked ) * 1650 
			as salaryRate,

			
			case
			when c.End_Date <= (
								select end_date from dbo.Project_Proposal
								where ID = @prj_id
								)
								then 'N' else 'Y' end as postOutLastPrj


			 from [dbo].[UOE_P_PFACT_Prj_Dept_Split] (nolock) 
			 a
			 inner join (
 								Select
								rowid,
								prj_id,
								--rectype,
								sum(val) as val,
									sum(case
									when matchedfund = 'on' then 0
									WHEN ISNULL(finaladjust,'99999999') = '99999999' THEN 
									subincome
									else
									Sponval end) as Sponval
								from
								UOE_T1_CSV_DATA  (nolock) 
								where 
								prj_id =@prj_id
								and rectype = 'staff'  
								group by
								rowid,
								prj_id
							) b
										on b.ROWID = a.ROWID
							left outer join (
										select a.id, b.FirstName + ' ' + b.SurName as name,
										b.End_Date from 
										PROJECT_Proposal_Co_investigators  (nolock) a
											inner join [dbo].[Staff_Details]  (nolock) b
												on a.Co_Investigator = b.ID
										where Project_Proposal = @prj_id
										) c
										on a.ROWID= c.id

							left outer join (
										select *,
										DATEDIFF(MONTH, startdate, enddate) + 1 as mthduration
										 from T1_Staff_Data
										where prj_id = @prj_id
									) z
									on z.prj_id = a.PRJ_ID
									and a.ROWID = z.rowid

				where a.prj_id  = @prj_id
				and recType = 'staff'

				and a.StaffTypeDesc = 'Principal Investigator'


				/*

			select * from [dbo].[Staff_Details]
				where ID = 7061

				select * from [dbo].[LUP_Pay_Scale]
				where Sub_Element = 546

				select * from [dbo].[LUP_Pay_Scale_Details]
				where 
				--Spine = 63 	and 
				Pay_Scale = 284
				*/

end

