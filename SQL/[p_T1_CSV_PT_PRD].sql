USE [pfact_t1]
GO
/****** Object:  StoredProcedure [dbo].[p_T1_CSV_PT_PRD]    Script Date: 12/09/2018 15:28:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



ALTER PROC [dbo].[p_T1_CSV_PT_PRD]
(
@prjid int,
@username varchar(100)
)
AS

begin

	---		 exec [p_T1_CSV_PT_PRD] 10409, 'sh444'
	
	delete from T1_CSV_ROW_COUNT
	where prj_id = @prjid
	and which = 'PT_PRD'

	/*
	select distinct rectype, ChargeType, descr
			 from UOE_T1_CSV_DATA A

		select *
			 from UOE_T1_CSV_DATA A
			 where 
			recType like  '%FACILITIES%'

			 */
declare @restbl table (
	[BIDNUMBER] [varchar](9) NULL,
	[PFACT_ID] [int] NULL,
	[DESCR] [varchar](900) NULL,
	[BUDC] [varchar](10) NULL,
	[WP] [varchar](10) NULL,
	[NOMINAL] [varchar](10) NULL,
	[BUD_HEADING] [varchar](110) NULL,
	[DATE_START] [datetime] NULL,
	[DATE_END] [datetime] NULL,
	[FEC_TOT_AMT] [decimal](20, 2) NULL,
	[MATCH_FUND] [decimal](20, 2) NULL,
	[CC_TOT_AMT] [decimal](20, 2) NULL,
	[FIN_YEAR] [int] NULL,
	[FIN_PER] [int] NULL,
	[PERIOD_DAYS] [int] NULL,
	[GRADE] [int] NULL,
	[UNITS] [numeric](18, 2) NULL,
	[RATE] [numeric](18, 2) NULL,
	[sponval] [float] NULL,
	[rownum] [bigint] NULL,
	rowid int
) 			

			insert into @restbl

			select DISTINCT 
			'BID' +
			RIGHT('00000000' +
			replace(
				b.t1bid 
				, 'BID',''),6)
					as BIDNUMBER,
			A.prj_id as PFACT_ID,

			replace(a.descr,',','') as DESCR,
			a.dept as BUDC,
			A.workpackage as WP,
			q.catCode as NOMINAL,
			'BID' +
						RIGHT('00000000' +
						replace(
							b.t1bid 
							, 'BID',''),6) + 

							CASE
							WHEN isnull(q.t1_bud_temp_heading,'') = '' then '_00_99'
							else '_' +q.t1_bud_temp_heading end as BUD_HEADING,

			a.stdate as DATE_START,
			a.eddate as DATE_END,

			convert(decimal(20,2),val) AS FEC_TOT_AMT,

			convert(decimal(20,2),case
			when matchedfund = 'on' then ISNULL(val,0)
			else 0 end) as MATCH_FUND,

			convert(decimal(20,2),
			ABS(
			case
			when matchedfund = 'on' then 0
			else ( ISNULL(val,0) - ISNULL(
			
					sponval
					/*
						*
						case
						when isnull(finaladjust,0) = 0 then 
								--(i.PercentageFunded / 100)
								1

							else finaladjust /100
							end
							*/
						/** ( case
							when finaladjust = 0 then 1
							else finaladjust / 100.00
							end )*/
					,0) ) end ))  as CC_TOT_AMT,

				--year(dateadd(month, -3, a.point)) as FIN_YEAR,
				case
							   when fin.finvalue <= 5 then year(a.point) +1
								else year(a.point) end as FIN_YEAR,
				  --MONTH(DATEADD(M, 3, a.point)) as FIN_PER,
				  fin.finvalue as FIN_PER,
				  a.daycalc as PERIOD_DAYS,
			
			/*
			Grade A to D - Set to '1'
			Grade E - Set to '2'
			Grade F - Set to '3'
			Grade G - Set to '4'
			*/
			case 
				when Z.Code like 'TechAD%' THEN 1
				when Z.Code like 'TechE%' THEN 2
				when Z.Code like 'TechF%' THEN 3
				when Z.Code like 'TechG%' THEN 4
				END AS GRADE,
			z.Units AS UNITS,
			Z.Rate AS RATE,
			a.sponval,
			
						ROW_NUMBER() OVER (ORDER BY 
						a.descr
						, year(dateadd(month, -3, a.point))
						,MONTH(DATEADD(M, 3, a.point))
						
						)  as rownum
					,a.rowid
			--		select * 
			 from UOE_T1_CSV_DATA A
				inner join T1_Fisacl_Month fin
								on DATEPART(month,a.point) = fin.monthvalue

			left outer join 
						UOE_PFACT_T1_BID_TBL b
							on a.PRJ_ID = b.pfactid

			
			inner join 

						(	
							SELECT 
							--Funder,
							ItemIdInProject AS ID ,
							ItemIDInTable AS ElementType,
							-- SUM(Amount) Total,      
							-- LUP_Income_Template.ID IncomeTemplateID, 
							-- LUP_Income_Template.TemplateCode,   
							-- LUP_Income_Template.Description TemplateDesc,
							-- Funders.Code FunderCode,
							-- Funders.Description FunderDesc,
							 PercentValue as percentageFunded
								--percentageFunded
							-- EU_Submission,
							-- ISNULL(ExceptionRule,0) as ExceptionRule,
							-- ISNULL(ExceptionRule2,0) AS ExceptionRule2        
						FROM CostIncomeTemplate      
						INNER JOIN LUP_Income_Template ON CostIncomeTemplate.IncomeTemplate = LUP_Income_Template.Id      
						INNER JOIN Funders ON CostIncomeTemplate.Funder = Funders.Id      
						WHERE Project= @prjid
						And ((IsSpine=1 And ItemIdInTable=1) OR (ItemIdInTable<>1))      
						)
							i on i.ID = A.ROWID

				LEFT OUTER JOIN (
				
								SELECT DISTINCT 
								PPOD.Project_Proposal,
								PPOD.ID AS ROWID,
								Rate,Units,LUP_Element_Categories.ID
								,LUP_Element_Categories.Description,                        
								 LUP_Sub_Element.Description as SubElementDesc
								 ,LUP_Sub_Element.Code, LUP_Unit_Type_Of_Equ_And_NonStaff.Description as unitdesc ,LUP_Unit_Type_Of_Equ_And_NonStaff.ID as Equ_And_NonStaffID,                         
								 LUP_Element_Categories.Sequence_No,
								 LUP_Sub_Element.Inflation_Factor_ID,PPOD.Element,RecategoriseToConsumable,Lup_Inflation_Factor.Effective_Date,                      
								 Project_Proposal.Code as prjCode,Project_Proposal.Project_Name,
								 PPOD.RateOnDate,Project_Proposal.Start_Date ProjectStart,                 
								 PPOD.Element As ActualElement,PPOD.ResearchStudent FROM Project_Proposal_Sub_ELEMENT_ID (NOLOCK)                       
								 LEFT JOIN Project_Proposal_Others_Details PPOD (NOLOCK)  ON             
								 PPOD.ID=Project_Proposal_Sub_ELEMENT_ID.Project_Proposal_Others_Details                          
								 LEFT JOIN CostEquipNonStaff  (NOLOCK)  ON PPOD.ID=ProjPropOtherDetails                        
								 INNER  JOIN LUP_Sub_Element (NOLOCK)  ON Project_Proposal_Sub_ELEMENT_ID.Sub_Element_ID = LUP_Sub_Element.ID                         
								 INNER JOIN LUP_Element_Categories  (NOLOCK) ON LUP_Sub_Element.Category = LUP_Element_Categories.ID                         
								 LEFT JOIN LUP_FIX_ICT_CLASS (NOLOCK)  ON LUP_Element_Categories.ChargeType = LUP_FIX_ICT_CLASS.ICT_Class_ID                         
								 LEFT JOIN LUP_FIX_ICT_CLASS  subElementChargeType(NOLOCK) ON LUP_Sub_Element.Charge_Type = subElementChargeType.ICT_Class_ID
								 INNER JOIN SubElement_UnitType (NOLOCK)  ON SubElement_UnitTYpe.SubElement_ID=LUP_Sub_Element.ID                         
								 INNER JOIN LUP_Unit_Type_Of_Equ_And_NonStaff  (NOLOCK) ON SubElement_UnitType.Unit_Type= LUP_Unit_Type_Of_Equ_And_NonStaff.ID                        
								 INNER JOIN Lup_Inflation_Factor  (NOLOCK) ON LUP_Sub_Element.Inflation_Factor_ID=Lup_Inflation_Factor.ID                        
								 INNER JOIN Project_Proposal (NOLOCK)  ON PPOD.Project_Proposal=Project_Proposal.ID                        
								 WHERE PPOD.Project_Proposal= @prjid     
									--AND PPOD.ID = 50507  
							) Z
									ON Z.Project_Proposal = @prjid
									AND Z.ROWID = A.ROWID

							inner join 
									--	Select * from
										T1_Other_Data q
										on Q.rowid = A.ROWID
											and q.prj_id =@prjid

			where 
			--A.descr  like  '%Pool Technician%'
			q.catCode = 12110
			/*
			a.stafftypedesc not in (
									'Estate Lab' ,
									'Estate Desk',
									'Indirect',
									'Infrastructure Lab',
									'Infrastructure Desk'
									)
									*/
					and a.prj_id = @prjid

			order by 
			a.ROWID
			,replace(a.descr,',','')
			, case
							   when fin.finvalue <= 5 then year(a.point) +1
								else year(a.point) end 
			,fin.finvalue 



			select 
			[BIDNUMBER] ,
			[PFACT_ID] ,
			isnull(l.descrp,a.[DESCR]) as [DESCR] ,
			[BUDC] ,
			[WP] ,
			[NOMINAL] ,
			[BUD_HEADING] ,
			[DATE_START] ,
			[DATE_END] ,
			[FEC_TOT_AMT] ,
			[MATCH_FUND] ,
			case
			when [CC_TOT_AMT] between -0.50 and 0.50 then 0.00
			else [CC_TOT_AMT] end as [CC_TOT_AMT] ,
			[FIN_YEAR] ,
			[FIN_PER] ,
			[PERIOD_DAYS] ,
			[GRADE] ,
			[UNITS] ,
			[RATE] ,
			[sponval] ,
			[rownum] 
			 from @restbl a
				
				 --	exec [p_T1_CSV_PT_PRD] 10409, 'sh444'
			left outer join (	
				select 
				DESCR + ' #' + convert(varchar, ROW_NUMBER() OVER (ORDER BY DESCR)) as descrp, rowid
				
				from (
				select distinct 
				 DESCR, rowid
				 from @restbl
					where DESCR in (
									Select DESCR --, COUNT(*)
										from (
												select distinct 
												 DESCR, rowid
												 from @restbl
												 ) z
												group by DESCR
												having COUNT(*) > 1
												) 
						) t
						) l
							on l.rowid = a.rowid

			insert into
				---	select * from
			 T1_CSV_ROW_COUNT
			select @prjid,
				 'PT_PRD',
				 @@ROWCOUNT


	DELETE FROM
	T1_CSV_FILE_TOTALS
	WHERE
	pfact_id = @prjid
	AND src = 'PT_PRD'


			INSERT INTO T1_CSV_FILE_TOTALS
				 select 
				 pfact_id, 
				 'PT_PRD'  ,
				 SUM(MATCH_FUND) as MATCH_FUND,
				 SUM(CC_TOT_AMT) as CC_COST
				 	from 
						@restbl
						group by
						pfact_id
end


