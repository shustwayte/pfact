USE [pfact_t1]
GO
/****** Object:  StoredProcedure [dbo].[p_bid_author_rpt_prj_partner_details_total]    Script Date: 12/09/2018 15:25:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER PROC [dbo].[p_bid_author_rpt_prj_partner_details_total]
(
@prj_id int
)
as
			--		p_bid_author_rpt_prj_partner_details_total 10489
begin

		declare @res table
		(
		costtype varchar(300),
		cost float,
		income float
		)

		insert into @res
		select 
			--name, 
			costtype,
				
				SUM(ISNULL(case when ISNUMERIC(cost) = 1 then cost else 0 end,0)), 
				SUM(ISNULL(case when ISNUMERIC(income) = 1 then income else 0 end,0))
				FROM [pfact_t1].[dbo].[vw_UOE_P_PRJ_Budget_Table_costdetail_uoe_RPT] (nolock) 
				where ProjectID = 10489--@prj_id
				group by costtype
			


				select 
				costtype,
				ISNULL(cost,0) as cost,
				ISNULL(income,0) as income
				
				 from @res
				union all 

				select 'Total',
				SUM(ISNULL(cost,0)),
				SUM(ISNULL(income,0))
				 from @res

end

