USE [pfact_t1]
GO
/****** Object:  StoredProcedure [dbo].[p_rpt_Je_S_Summary_Prj_Staff_Details_NON_PI]    Script Date: 12/09/2018 15:26:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



ALTER PROC [dbo].[p_rpt_Je_S_Summary_Prj_Staff_Details_NON_PI] 
(
@prj_id int,
@rowid int = null

)

as

		---	exec [dbo].[p_rpt_Je_S_Summary_Prj_Staff_Details_NON_PI] 10487, 37739
		
begin



                               
				declare @resdata TABLE (                                  
				 [ID] [int] NULL ,                                  
				 [Staff_Type] [int] NULL ,                                  
				 [StaffType] [varchar] (50) NULL ,                                  
				 [Co_Investigator] [int] NULL ,                                  
				 [NAME] [varchar] (100) NULL ,                                  
				 [Sub_Element] [int] NULL ,                                  
				 [CODE] [varchar] (30) NULL ,                                  
				 [Description] [varchar] (100) NULL ,                                  
				 [Tbl] [int] NULL ,                                  
				 [Category] [varchar] (100) NULL ,                                  
				 [CATID] [int] NULL ,                                  
				 [Sequence_NO] [int] NULL ,                                  
				 [Basic] [float] NULL ,                                  
				 [NI] [float] NULL ,                                  
				 [NIOUT] [float] NULL ,                                  
				 [Super] [float] NULL ,                                  
				 [TOTAL] [float] NULL ,                                  
				 [Other_Allowances] [float] NULL ,                                  
				 [FirstName] [varchar] (100) NULL ,                                  
				 [LastName] [varchar] (100) NULL ,                                  
				 [jesPID] [varchar] (50) NULL ,                                  
				 [UseLondonWeighting] [bit] NULL ,                                  
				 [Title] [varchar] (25) NULL ,                                  
				 [Initials] [varchar] (25) NULL ,                                  
				 [StartDate] [datetime] NULL ,                                  
				 [EndDate] [datetime] NULL ,                                  
				 [SpinePoint] [int] NULL ,                                  
				 [SpineOnDate] [datetime] NULL ,                                  
				 [IncrementDate] [datetime] NULL ,                                  
				 [DiscretionaryPoint] [int] NULL ,                                  
				 [MonthsForIncrement] [int] NULL ,                                  
				 [ProjectCode] [varchar] (20) NULL ,                                  
				 [ProjectName] [varchar] (100) NULL ,                                  
				 [DefaultChargeType] [varchar] (70) NULL ,                                  
				 [ProjectStart] [datetime] NULL ,                                  
				 [ProjectEnd] [datetime] NULL ,                                  
				 [JesStaffTypeDesc] [varchar] (50) NULL ,     
				 [Base_Year] [varchar] (20) NULL ,                                  
				 [Inflation_Factor] [int] NULL  ,                                
				 [PayScale] [int] NULL ,                             
				 [StartSpine] [int] NULL ,                            
				 [StaffEndDate] [DATETIME] NULL ,              
				 [PayScaleEndDate] [DATETIME] NULL,                              
				 [NINumber] [varchar](15)NULL,                
				 [ISPI] INT NULL,       
				 [AnnualContractHours] FLOAT NULL,        
				 [AnnualContractDays] FLOAT NULL,        
				 [WeeksInYear] FLOAT NULL,       
				 [Department] VARCHAR(100)                    
				)              
  
				insert into @resdata 
				EXEC REPORTPROJECTSTAFFDETAILED @prj_id,1,1,0,0

			--	select * from @resdata 
	

			select 
			a.ROWID,
			rectype, 
			a.PRJ_ID, a.ChargeType, a.descr, a.StaffTypeDesc,
			hoursworked, b.Sponval, a.SponTotal, 
			
						isnull(c.name,a.descr) as name , 
			a.FTE * 100 as FTE, 
			z.units,
			z.unitsDescr,
			z.mthduration,

			(hoursworked / (z.mthduration / 12.00) ) / 44			
			as avgHrpW,
			a.SponTotal as costEst,
			( a.SponTotal / hoursworked ) * 1650 
			as salaryRate,

			
			case
			when c.End_Date <= (
								select end_date from dbo.Project_Proposal
								where ID = @prj_id
								)
								then 'N' else 'Y' end as postOutLastPrj

								,q.Id,
								q.End_Date,
								isnull(q.Start_Date, w.StartDate) as Start_Date,
								case when 
								isnull(q.DiscretionaryPoint,w.DiscretionaryPoint) = '1' then 'Y'
								else 'N' end as DiscretionaryPoint,
								case when 
								isnull(q.Use_London_Weighting,w.UseLondonWeighting) = '1' then 'Y'
								else 'N' end as Use_London_Weighting,
								isnull(q.Sub_Element,w.Sub_Element) as Sub_Element,
								isnull(q.SpinePoint, w.SpinePoint) as SpinePoint,
								--isnull(q.Code,w.CODE) as Code,
								--isnull(q.Category,w.Description) as Category,
								ISNULL(q.grade, 

								w.Description
													+ '      ' + convert(varchar(10),w.SpinePoint) ) as grade,
								isnull(q.starting_spine,w.SpinePoint) as starting_spine,
								isnull(q.Basic_Pay,w.Basic) as Basic_Pay,
								isnull(q.National_Ins_Out,w.NIOUT) as National_Ins_Out,
								isnull(q.Superannuation,w.Super) as Superannuation,
								isnull(q.Other_Allowances,w.Other_Allowances)  as Other_Allowances,
								isnull(q.Effective_Date,w.SpineOnDate) as Effective_Date,
								isnull(q.Increment_Date, w.IncrementDate) as Increment_Date

			 from [dbo].[UOE_P_PFACT_Prj_Dept_Split] (nolock) 
			 a
			 inner join (
 								Select
								rowid,
								prj_id,
								--rectype,
								sum(val) as val,
									sum(case
									when matchedfund = 'on' then 0
									WHEN ISNULL(finaladjust,'99999999') = '99999999' THEN 
									subincome
									else
									Sponval end) as Sponval
								from
								UOE_T1_CSV_DATA  (nolock) 
								where 
								prj_id =@prj_id
								and rectype = 'staff'  
								group by
								rowid,
								prj_id
							) b
										on b.ROWID = a.ROWID
							left outer join (
										select a.id, b.FirstName + ' ' + b.SurName as name,
										b.End_Date
										 from 
										PROJECT_Proposal_Co_investigators  (nolock) a
											inner join [dbo].[Staff_Details]  (nolock) b
												on a.Co_Investigator = b.ID
										where Project_Proposal = @prj_id
										) c
										on a.ROWID= c.id

							left outer join (
										select *,
										DATEDIFF(MONTH, startdate, enddate) + 1 as mthduration
										 from T1_Staff_Data (nolock) 
										where prj_id = @prj_id
									) z
									on z.prj_id = a.PRJ_ID
									and a.ROWID = z.rowid

							left outer join (
							
								---	pay scale data
									select a.id, 
									--b.FirstName + ' ' + b.SurName as name,
												b.End_Date, 
												b.Start_Date,
												case
													when ISNULL(b.DiscretionaryPoint,0) = 0 then 'N'
													else 'Y' end as DiscretionaryPoint,
												case
													when ISNULL(b.Use_London_Weighting,0) = 0 then 'N'
													else 'Y' end as Use_London_Weighting,
													b.Sub_Element,
													b.SpinePoint,
													c.code,
													c.Category,
													d.Description
													+ '      ' + convert(varchar(10),p.starting_spine)
													 as grade,
													p.starting_spine,
													e.Basic_Pay,
													e.National_Ins_Out,
													e.Superannuation,
													e.Other_Allowances,
													--e.DiscretionaryPoint,
													c.Effective_Date,
													c.Increment_Date


												 from 
												PROJECT_Proposal_Co_investigators  (nolock) a
												
													inner join [dbo].[Staff_Details]  (nolock) b
														on a.Co_Investigator = b.ID
														left outer join LUP_Pay_Scale  (nolock) c
															on c.sub_element = b.Sub_Element
														left outer join [dbo].[LUP_PayScaleGroup]  (nolock) d
															on c.GroupID = d.id
																left outer join [dbo].[LUP_Pay_Scale_Details]  (nolock) e
																	on e.Pay_Scale = c.id
																	and b.SpinePoint = e.Spine
																left outer join (
																Select min(spine) as starting_spine, 
																Pay_Scale
																from
																[dbo].[LUP_Pay_Scale_Details]
																group by Pay_Scale
																) p
																on p.Pay_Scale = c.id

												where Project_Proposal = @prj_id
												and case
													when ISNULL(@rowid,'') = '' then
													a.ID 
													else
													@rowid
													end = a.Id 
									

									-- Select * from @resdata
												) q
												on q.id = a.ROWID

												inner join @resdata w
													on w.ID = a.ROWID

				where a.prj_id  = @prj_id
				and recType = 'staff'

				and a.StaffTypeDesc <> 'Principal Investigator'

				and 
				case
				when ISNULL(@rowid,'') = '' then
				a.ROWID 
				else
				@rowid
				end = a.rowid


				/*

			select * from [dbo].[Staff_Details]
				where ID = 7061

				select * from [dbo].[LUP_Pay_Scale]
				where Sub_Element = 546

				select * from [dbo].[LUP_Pay_Scale_Details]
				where 
				--Spine = 63 	and 
				Pay_Scale = 284
				*/

end



