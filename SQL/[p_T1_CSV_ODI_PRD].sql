USE [pfact_t1]
GO
/****** Object:  StoredProcedure [dbo].[p_T1_CSV_ODI_PRD]    Script Date: 12/09/2018 15:28:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



ALTER PROC [dbo].[p_T1_CSV_ODI_PRD]
(
@prjid int,
@username varchar(100)
)
AS

begin

	---		 exec [p_T1_CSV_ODI_PRD] 10762, 'sh444'

	delete from T1_CSV_ROW_COUNT
	where prj_id = @prjid
	and which = 'ODI_PRD'



		declare @udata TABLE 
			 (                        
			  ID INT NULL,                        
			  Rate FLOAT NULL,                        
			  Units FLOAT NULL,                        
			  CatID INT NULL,                        
			  Description VARCHAR(900) NULL,                        
			  Name VARCHAR(100) NULL,                        
			  Code VARCHAR (30) NULL,                        
			  UnitType VARCHAR (50) NULL,                        
			  UnitTypeID INT NULL,                        
			  Sequence_No INT NULL,                        
			  CatChargeType VARCHAR (70) NULL,                        
			  StartDate DateTime NULL,                        
			  EndDate DateTime NULL,                        
			  ItemType INT NULL,                        
			  Sub_Element INT NULL,                        
			  VAT_Percent FLOAT NULL,                        
			  COST FLOAT NULL,                        
			  VAT_Type INT NULL,                        
			  VAT_Cost FLOAT NULL,                          
			  Cost_Before_VAT FLOAT NULL,                        
			  IsSwitchToConsumables BIT NULL,                        
			  OrgConumableLimit FLOAT NULL,                        
			  ConsumableCategory INT NULL,                        
			  Inflation_Factor_ID INT NULL,                        
			  Element INT NULL,                        
			 -- RecategoriseToConsumable BIT NULL,                        
			  Effective_Date DATETIME NULL,                        
			  ProjectCode VARCHAR(20),                        
			  ProjectName VARCHAR(100),                
       
			  DefaultChargeType VARCHAR(70),                        
			  ExceptionRule int,   
			  RateOnDate Datetime,                        
			  vatDesc varchar(300),
			  ProjectStart DateTime,                        
			  ActualElement INT,                      
			  ResearchStudent INT NULL                        
			 )      

			 insert into @udata

			EXEC ProjectEquipmentCostDetailed @prjid,2,1,0,0

			
			DECLARE @restbl table (
	[BIDNUMBER] [varchar](9) NULL,
	[PFACT_ID] [int] NULL,
	[DESCR] [varchar](900) NULL,
	[DEPT] [varchar](10) NULL,
	[WP] [varchar](10) NULL,
	[NOMINAL] [varchar](10) NULL,
	[BUD_HEADING] [varchar](110) NULL,
	[DATE_START] [datetime] NULL,
	[DATE_END] [datetime] NULL,
	[UNITS] [float] NULL,
	[UNIT_TYPE] [varchar](50) NULL,
	[FIN_YEAR] [int] NULL,
	[FIN_PER] [int] NULL,
	[PERIOD_DAYS_PJ] [int] NULL,
	[AMT] [decimal](20, 2) NULL,
	[MATCH_FUND] [decimal](20, 2) NULL,
	[CC] [decimal](20, 2) NULL,
	[AMT_VAT_EX] [decimal](20, 2) NULL,
	[AMT_VAT_RATE] [decimal](20, 2) NULL,
	[AMT_VAT] [decimal](20, 2) NULL,
	[AMT_TOT] [decimal](20, 2) NULL,
	[sponval] [float] NULL,
	[rowid] [int] NULL,
	[rownum] [bigint] NULL,
	finaladjust float 
) 
	/*
	select distinct rectype, ChargeType, descr
			 from UOE_T1_CSV_DATA A

		select *
			 from UOE_T1_CSV_DATA A
			 where 
			recType like  '%FACILITIES%'

			 */

			 insert into @restbl
			select distinct 
			'BID' +
			RIGHT('00000000' +
			replace(
				b.t1bid 
				, 'BID',''),6)
					as BIDNUMBER,
			A.prj_id as PFACT_ID,

			replace(a.descr,',','') as DESCR,
			a.dept as DEPT,
			A.workpackage as WP,
			q.catCode as NOMINAL,
			'BID' +
						RIGHT('00000000' +
						replace(
							b.t1bid 
							, 'BID',''),6) + 

							CASE
							WHEN isnull(q.t1_bud_temp_heading,'') = '' then '_00_99'
							else '_' +q.t1_bud_temp_heading end as BUD_HEADING,

			a.stdate as DATE_START,
			a.eddate as DATE_END,

			Z.Units as UNITS,
			z.unittype as UNIT_TYPE,

			   --year(dateadd(month, -3, a.point)) as FIN_YEAR,
			   case
							   when fin.finvalue <= 5 then year(a.point) +1
								else year(a.point) end as FIN_YEAR,
				  --MONTH(DATEADD(M, 3, a.point)) as FIN_PER,
				  fin.finvalue as FIN_PER,
				  a.daycalc as PERIOD_DAYS_PJ,


			convert(decimal(20,2),ISNULL(val,0)) as AMT,

			convert(decimal(20,2),case
			when matchedfund = 'on' then ISNULL(val,0)
			else 0 end ) as MATCH_FUND,

			convert(decimal(20,2),case
			when matchedfund = 'on' then 0
			else ( ISNULL(val,0) - ISNULL(
			
			case
				when finaladjust = 0 then 0
				when finaladjust = 0.00 then 0
				else
					sponval 
						 * ( case
							when finaladjust = 0 then 0
							when finaladjust = 0.00 then 0
							else finaladjust / 100.00
							end ) 
							end
					,0) ) end ) as CC,

			convert(decimal(20,2),Z.Cost_Before_VAT) as AMT_VAT_EX,
			convert(decimal(20,2),Z.VAT_Percent) as AMT_VAT_RATE,
			convert(decimal(20,2),Z.VAT_Cost) as AMT_VAT,
			convert(decimal(20,2),Z.cost) as AMT_TOT,
			sponval,
			a.rowid,
						ROW_NUMBER() OVER (ORDER BY 
						a.descr
						, year(dateadd(month, -3, a.point))
						,fin.finvalue 
						
						)  as rownum

					, finaladjust
					--	,A.*
			--		select * 
			 from UOE_T1_CSV_DATA A

			 inner join T1_Fisacl_Month fin
								on DATEPART(month,a.point) = fin.monthvalue
			left outer join 
						UOE_PFACT_T1_BID_TBL b
							on a.PRJ_ID = b.pfactid

					

				LEFT OUTER JOIN (
				
								Select * from @udata                     
								 
									--AND PPOD.ID = 50507  
							) Z
									ON Z.id = A.ROWID


									inner join 
									--	Select * from
										T1_Other_Data q
										on Q.rowid = A.ROWID
											and q.prj_id =@prjid

			where 
			/*
			A.recType not like  'DA COST%'
			and
			A.recType not like  'Staff'
			and
			A.recType not like  'FTE RELATED%' and 
			*/
			q.catCode in (
						25002,
						25004,
						21405,
						21410,
						21415,
						21420,
						21425,
						21430,
						25006,
						21435,
						25040,
						25042,
						25044,
						25046,
						25008,
						25076,
						25010,
						25060,
						25062,
						25070,
						25072,
						25074,
						25080,
						25090,
						23020,
						23025,
						23030,
						23105,
						23110,
						23115,
						23120,
						25105,
						25110,
						25115,
						25120,
						25125,
						25130,
						25205,
						25210,
						25305,
						25310,
						25315,
						25905,
						24005,
						24010,
						24105,
						25320,
						25325,
						25330,
						25335,
						25340,
						25345,
						25350,
						25355,
						25405,
						25505,
						25510,
						25530,
						25515,
						25535,
						25540,
						25545,
						25550,
						25605,
						25610,
						25560,
						25570,
						25705,
						25710,
						25715,
						25720,
						25725,
						25730,
						25735,
						25740,
						25742,
						25745,
						25747,
						25750,
						25755,
						25910,
						25920,
						25925,
						25930,
						25935,
						25940,
						25945

			)

			/*
			a.stafftypedesc not in (
									'Estate Lab' ,
									'Estate Desk',
									'Indirect',
									'Infrastructure Lab',
									'Infrastructure Desk'
									)
									*/
					and a.prj_id = @prjid

			order by 
			replace(a.descr,',','')
			, case
							   when fin.finvalue <= 5 then year(a.point) +1
								else year(a.point) end 
			,fin.finvalue 

if ( select top 1 COUNT(*) from @restbl group by rowid  ) > 1 
		begin
			select 
			[BIDNUMBER] ,
			[PFACT_ID] ,
			[DESCR] ,
			[DEPT] ,
			[WP] ,
			[NOMINAL] ,
			[BUD_HEADING] ,
			[DATE_START] ,
			[DATE_END] ,
			[UNITS] ,
			[UNIT_TYPE] ,
			[FIN_YEAR] ,
			[FIN_PER] ,
			[PERIOD_DAYS_PJ] ,
			[AMT] ,
			[MATCH_FUND] ,
			case
			when [CC] between -0.50 and 0.50 then 0.00
			else [CC] end as [CC] ,
			[AMT_VAT_EX] ,
			[AMT_VAT_RATE] ,
			[AMT_VAT] ,
			[AMT_TOT] ,
			[sponval] ,
			[rowid] ,
			[rownum]
			
			 from @restbl

	end

	if ( select top 1 COUNT(*) from @restbl group by rowid ) = 1 
		begin


		-- select * from @restbl 

			select 
			[BIDNUMBER] ,
			[PFACT_ID] ,
			[DESCR] ,
			[DEPT] ,
			[WP] ,
			[NOMINAL] ,
			[BUD_HEADING] ,
			[DATE_START] ,
			[DATE_END] ,
			[UNITS] ,
			[UNIT_TYPE] ,
			[FIN_YEAR] ,
			[FIN_PER] ,
			[PERIOD_DAYS_PJ] ,
			 ( select top 1 isnull(Total,0) from  [dbo].[UOE_P_PFACT_Prj_Dept_Split] q where PRJ_ID = @prjid and ROWID = a.rowid  )            as [AMT] ,

			 case
			 when 	( select top 1 matchedfund from  [dbo].[UOE_P_PFACT_Prj_Dept_Split] q where PRJ_ID = @prjid and ROWID = a.rowid  ) = 'on'    then
			  ( select top 1 isnull(Total,0) from  [dbo].[UOE_P_PFACT_Prj_Dept_Split] q where PRJ_ID = @prjid and ROWID = a.rowid  )   
			 else 0 end as [MATCH_FUND] ,

			 convert(decimal(20,2),
			
			 ( select top 1 isnull(Total,0) from  [dbo].[UOE_P_PFACT_Prj_Dept_Split] q where PRJ_ID = @prjid and ROWID = a.rowid  )  
			 -
			   (
				( select top 1 isnull(SponTotal,0) from  [dbo].[UOE_P_PFACT_Prj_Dept_Split] q where PRJ_ID = @prjid and ROWID = a.rowid  )   
		
				*
					(
						CASE
						WHEN ( select top 1 matchedfund from  [dbo].[UOE_P_PFACT_Prj_Dept_Split] q where PRJ_ID = @prjid and ROWID = a.rowid  ) = 'on'   THEN 0
						WHEN finaladjust = 0 THEN 0
						WHEN finaladjust = 0.00 THEN 0
						ELSE ISNULL(finaladjust,100) END / 100.00 
					)

						
				)
				-
				 case
			 when 	( select top 1 matchedfund from  [dbo].[UOE_P_PFACT_Prj_Dept_Split] q where PRJ_ID = @prjid and ROWID = a.rowid  ) = 'on'    then
			  ( select top 1 isnull(Total,0) from  [dbo].[UOE_P_PFACT_Prj_Dept_Split] q where PRJ_ID = @prjid and ROWID = a.rowid  )   
			 else 0 end
				)
			 
			 
			  as [CC] ,
			[AMT_VAT_EX] ,
			[AMT_VAT_RATE] ,
			[AMT_VAT] ,
			[AMT_TOT] ,
			[sponval] ,
			[rowid] ,
			[rownum]
			
			 from @restbl a



		end


		insert into T1_CSV_ROW_COUNT
		select @prjid,
				 'ODI_PRD',
				 @@ROWCOUNT


	DELETE FROM
	T1_CSV_FILE_TOTALS
	WHERE
	pfact_id = @prjid
	AND src = 'ODI_PRD'


			INSERT INTO T1_CSV_FILE_TOTALS
				 select 
				 pfact_id, 
				 'ODI_PRD'  ,
				 SUM(MATCH_FUND) as MATCH_FUND,
				 SUM(CC) as CC_COST
				 	from 
						@restbl
						group by
						pfact_id
		--	select * from @udata

-- 		select * from @restbl
end


