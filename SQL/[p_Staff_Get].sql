USE [pfact_t1]
GO
/****** Object:  StoredProcedure [dbo].[p_Staff_Get]    Script Date: 12/09/2018 15:27:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




ALTER PROC [dbo].[p_Staff_Get]

as

begin

/*
select [stafflist], staffemail
  from [dbo].[stafflist]
  */
SELECT 
'"' + SurName + ', ' + FirstName + '-' + StaffNumber + '",' as [stafflist],
'"' + a.Email + '",' as staffemail,
'"' + convert(varchar,a.Department) + '",' as dept
  FROM [dbo].[Staff_Details] a
  --LEFT OUTER JOIN 
	--[dbo].[LUP_Department] B
	--		on a.Department = b.ID
  order by surname
  
end  



