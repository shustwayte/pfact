USE [pfact_t1]
GO
/****** Object:  StoredProcedure [dbo].[p_rpt_FTE_details_Profile_Pay]    Script Date: 12/09/2018 15:26:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROC [dbo].[p_rpt_FTE_details_Profile_Pay]
(
@prj_id int,
@type varchar(100)

)					
			--		 [p_rpt_FTE_details_Profile_Pay]	10621, 'Estate Lab'

as

begin

		declare @mthstart_prj int

	declare @temp_prj table (
				id int identity(1,1),
				point datetime,
				acad_marker int,
				MONTHS INT,
				yr int,
				
				prjyr_marker int
				)
				
				declare @st_prj datetime
				/*
				set @st_prj = ( SELECT 
						  [Start_Date]
							  FROM [dbo].[Project_Proposal]
									where id = @prj_id
									)
									*/
				set @st_prj = ( SELECT  top 1
								stdate
								  FROM [UOE_P_PFACT_Prj_Dept_Split]
										where 
										PRJ_ID = @prj_id
										and recType = 'FTE Related'
										and StaffTypeDesc = @type
										ORDER BY stdate
									)
									
				set @mthstart_prj = 8--MONTH(@st)		
				
				declare @prjyr_marker_prj int
				
				if DATEPART(MONTH,@st_prj) = 8
				begin
					set @prjyr_marker_prj = 0
				end
				
				if DATEPART(MONTH,@st_prj) <> 8
				begin
					set @prjyr_marker_prj = 1
				end		
				
				
		
				declare @ed_prj datetime
				/*
				set @ed_prj = ( SELECT 
							  [End_Date]
						  FROM [dbo].[Project_Proposal]
						  where id = @prj_id
									)	
									*/

				set @ed_prj = ( SELECT  top 1
								eddate
								  FROM [UOE_P_PFACT_Prj_Dept_Split]
										where 
										PRJ_ID = @prj_id
										and recType = 'FTE Related'
										and StaffTypeDesc = @type
										ORDER BY eddate DESC
									)
									
	--	select @st_prj, @ed_prj									
				
				declare @i_prj int
				set @i_prj = 0
				
			

				declare @mth_start_prj int
				set @mth_start_prj = ( select MONTH(@st_prj)	)


				declare @month_between_prj int
				set @month_between_prj = ( 
										SELECT 
										DBO.UOE_MonthdIFF(@st_prj, @ed_prj)+1
										)
										
				--select @month_between
							
				declare @yr_prj int
				set @yr_prj = 1	

				declare @yr_marker_prj int
				set @yr_marker_prj = 0			

				declare @mths_prj int
				set @mths_prj = 12

				declare @mths_chk_prj int
				set @mths_chk_prj = 12
									
				while @i_prj <= @month_between_prj
				begin
					-- if august then year + 1
					set @yr_prj = ( 
						select 
						case 
						when month(dateadd(m,@i_prj, @st_prj)) = 8 then @yr_prj +1 
						else @yr_prj end
						)
						
					-- if year point reached, check how many months left	
					if @i_prj in (12, 24, 36, 48, 60, 72, 84, 96, 108, 120, 132, 144 )  
						begin
						
							if @month_between_prj - @i_prj < 12
							begin
							set @mths_prj = @month_between_prj - @i_prj
								if @mths_prj = 0
								begin
									set @mths_prj = 1 
								end
							end
						end
						
					if MONTH(dateadd(m,@i_prj, @st_prj)) = @mth_start_prj
					begin
						set @yr_marker_prj = @yr_marker_prj + 1
					end
						
				if MONTH( dateadd(m,@i_prj, @st_prj) ) = 8--@mthstart
				begin
				set @prjyr_marker_prj = @prjyr_marker_prj + 1
				end
						
				insert into @temp_prj ( point, acad_marker, MONTHS, yr, prjyr_marker)
					select 
						dateadd(m,@i_prj, @st_prj),
						@yr_prj,
						--@ACAD_YEAR_prj,
						@mths_prj,
						@yr_marker_prj,
						@prjyr_marker_prj
				
					set @i_prj = @i_prj + 1
					
				end						


				delete from @temp_prj
				where id > @month_between_prj
				
				-- ensure date profile doesn't exceed end date for project
				delete from @temp_prj
				where point > @ed_prj


				update @temp_prj
				set point = @st_prj
				where id = ( Select min(id) from @temp_prj )

				update @temp_prj
				set point = @ed_prj
				where id = ( Select max(id) from @temp_prj )
				
	-- profile map for project
	-- select * from @temp_prj

 
	 /*
	select 
	PRJ_ID, ROWID, 
	a.point, val, Basicval, NIval, Superval, OAval, b.yr
			FROM UOE_T1_CSV_DATA a
				inner join @temp_prj b
					on a.point = b.point
		WHERE PRJ_ID = @prj_id
		and ROWID = 37575
		and recType NOT in (
		'FTE Related' 
		)
		ORDER BY a.POINT
		*/


		select 
			c.yr, 

			convert(decimal(20,2),SUM(val)) as val, 
			
			d.styrrange,
			--d.edyrrange,
			case
			when c.yr = ( Select MAX(yr) from @temp_prj ) then @ed_prj
			else
			DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,d.edyrrange)+1,0)) 
			end as edyrrange
		from (
			select 
			PRJ_ID, ROWID, 
			z.point, val, b.yr
			from
				( Select *
					FROM UOE_T1_CSV_DATA  (nolock) a
						) Z
						inner join @temp_prj b
							on z.point = b.point
				WHERE PRJ_ID = @prj_id
				and recType = 'FTE Related'
				and StaffTypeDesc = @type
				
				--			ORDER BY a.POINT
			) c
			inner join (
				select 
				yr, MIN(point) as styrrange, MAX(point) as edyrrange
				 from @temp_prj
				 group by yr
				 ) d
				 on c.yr = d.yr
		group by 
		c.yr,
		d.styrrange,
			d.edyrrange
		order by c.yr


end	


