USE [pfact_t1]
GO
/****** Object:  StoredProcedure [dbo].[p_T1_CSV_COST_CALCS]    Script Date: 12/09/2018 15:27:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



ALTER PROC [dbo].[p_T1_CSV_COST_CALCS]
(
@prjid int,
@username varchar(100)
)
AS

begin

	---		 exec p_T1_CSV_COST_CALCS 10762 , 'sh444'

	delete from T1_CSV_ROW_COUNT
	where prj_id = @prjid
	and which = 'COST_CALCS'
	/*
	declare @prjlenDays float
	set @prjlenDays = (
						select DATEDIFF(d,Start_Date,dateadd(d,1,End_Date) )
						 from [dbo].[Project_Proposal]
						where ID = @prjid
					)
					*/

declare @restbl  table (
	[BIDNUMBER] [varchar](9) NULL,
	[PFACT_ID] [int] NULL,
	[STAFF] [varchar](20) NULL,
	[TBA_STAFF_NAME] [varchar](900) NULL,
	[WP] [varchar](10) NULL,
	[BUDC] [varchar](10) NULL,
	[BUD_HEADING] [varchar](110) NULL,
	[DATE_START] [datetime] NULL,
	[DATE_END] [datetime] NULL,
	[FIN_YEAR] [int] NULL,
	[FIN_PER] [int] NULL,
	[PERIOD_DAYS_PJ] [int] NULL,
	[FTE_CALC] [float] NULL,
	[FEC_SAL_ONLY] [decimal](20, 2) NULL,
	[FEC_NI] [decimal](20, 2) NULL,
	[FEC_PENSION] [decimal](20, 2) NULL,
	[FEC_APP_LEVY] [decimal](20, 2) NULL,
	[COST_TYPE] [varchar](100) NULL,
	[AMT_ESTATE] [decimal](20, 2) NULL,
	[AMT_INFRA] [decimal](20, 2) NULL,
	[AMT_INDIRECT] [decimal](20, 2) NULL,
	[ROLE] [varchar](250) NULL,
	[FEC_COST] [decimal](20, 2) NULL,
	[CC_COST] [decimal](20, 2) NULL,
	[MATCH_FUND] [decimal](20, 2) NULL,
	[CRUSER] [varchar](100) NULL,
	[PJ_DATE_START] [varchar](30) NULL,
	[PJ_DATE_END] [varchar](30) NULL,
	[rownum] [bigint] NULL,
	rowid int,

	estateVal float null,
	infraVal float null,
	indVal float null,

	sponsorship float null,

	estateSpon float null,
	infraSpon float null,
	indSpon float null,

	matchedFund float null,

	estateMatched float null,
	infraMatched float null,
	indMatched float null,

	subincome float null,
	estateSubincome float null,
	infraSubincome float null,
	indSubincome float null,

	Fmatched varchar(10),
	finaladjust varchar(10) ,


	-----		one month

	[1_FEC_SAL_ONLY] [decimal](20, 2) NULL,
	[1_FEC_NI] [decimal](20, 2) NULL,
	[1_FEC_PENSION] [decimal](20, 2) NULL,
	[1_FEC_APP_LEVY] [decimal](20, 2) NULL,

	[1_estateVal] float null,
	[1_infraVal] float null,
	[1_indVal] float null,

	[1_sponsorship] float null,

	[1_estateSpon] float null,
	[1_infraSpon] float null,
	[1_indSpon] float null,

	[1_matchedFund] float null,

	[1_estateMatched] float null,
	[1_infraMatched] float null,
	[1_indMatched] float null

)
		
		insert into @restbl
					select 
						--- a.stafftypedesc,
			
						'BID' +
						RIGHT('00000000' +
						replace(
							b.t1bid 
							, 'BID',''),6)
				
							as BIDNUMBER,
						A.prj_id as PFACT_ID,

						
							isnull(q.StaffNumber, 'TBA')
							 as STAFF,

						case 
						when isnull(q.StaffNumber, 'TBA') = 'TBA' then 
						replace(ISNULL(I.DUPLICATENAME, a.descr),',','')
						else '' end as TBA_STAFF_NAME,
						
						A.workpackage as WP,

						A.dept as BUDC,
						--q.catDesc as BUD_HEADING,

						'BID' +
						RIGHT('00000000' +
						replace(
							b.t1bid 
							, 'BID',''),6) + 

							CASE
							WHEN isnull(q.t1_bud_temp_heading,'') = '' then '_00_99'
							else '_' +q.t1_bud_temp_heading end as BUD_HEADING,

						a.stdate as DATE_START,
						a.eddate as DATE_END,

						--point,
							   --year(dateadd(month, -3, a.point))
							   case
							   when fin.finvalue <= 5 then year(a.point) +1
								else year(a.point) end
										as FIN_YEAR,
							  --MONTH(DATEADD(M, 3, a.point)) as FIN_PER,
							  fin.finvalue as FIN_PER,
							  a.daycalc as PERIOD_DAYS_PJ,

						/*
						convert(
						decimal(20,10),
						A.fte) as FTE_CALC,
						*/
						convert(decimal(20,10),
						case
							when w.Description = 'FTE/Annum' then w.Units
							-- length of project in years * 220
							-- 100 / 1
							when w.Description = 'Days' then 
										(w.Units * 7.5) / (prjlenDays * 7.5 )
							when w.Description = 'Hours' then 
										(w.Units) / (prjlenDays * 7.5 )
							when w.Description = 'Hours/Week' then 
										(w.Units) / 37.5
							when w.Description = 'Months' then 
										(w.Units * 137.5) / (prjlenDays * 7.5 )
							else
								Q.fte end) as FTE_CALC,
						convert(decimal(20,2),basicval) as FEC_SAL_ONLY,
						convert(decimal(20,2),nival) as FEC_NI,
						convert(decimal(20,2),superval) as FEC_PENSION,
						--convert(decimal(20,2),(basicval) * 0.005) as FEC_APP_LEVY,
						convert(decimal(20,2),OAval) as FEC_APP_LEVY,
						CASE
							when a.chargetype = 'Directly Allocated Costs' then 'DA'
							when a.chargetype = 'Directly Incurred Costs' then 'DI'
							when a.chargetype = 'Exceptions' then 'DI'
							when a.chargetype = 'Indirect Costs' then 'DI'
							ELSE
						a.chargetype END as COST_TYPE,

						convert(decimal(20,2),c.val) as AMT_ESTATE,
						convert(decimal(20,2),d.val) as AMT_INFRA,
						convert(decimal(20,2),e.val) as AMT_INDIRECT,

						case
						when a.stafftypedesc = 'Principal Investigator' then 'PRO'
						when a.stafftypedesc = 'Co Investigator' then 'LEC'
						when a.stafftypedesc = 'Co-Investigator' then 'LEC'
						when a.stafftypedesc = 'Researcher' then 'RES'
						when a.stafftypedesc = 'Research Assistant' then 'RES'
						when a.stafftypedesc = 'Technician' then 'TEC'
						when a.stafftypedesc = 'Administrative Staff' then 'ADM'
						when a.stafftypedesc = 'Administrator' then 'ADM'
						when a.stafftypedesc = 'Teach Replacment' then 'REP'
						when a.stafftypedesc = 'Principal Investigator (Non FEC)' then 'PI_NO'
						when a.stafftypedesc = 'Co Investigator (Non FEC)' then 'LEC_NO'
						when a.stafftypedesc = 'Researcher (Non FEC)' then 'RES_NO'
						when a.stafftypedesc = 'Teach Replacment (Non FEC)' then 'REP_NO'
						when a.stafftypedesc = 'KTP Associate' then 'KT'
						when a.stafftypedesc = 'Web Designer' then 'WD'
						when a.stafftypedesc = 'Mentor' then 'ME'


						when a.stafftypedesc = 'Principal Investigator' then 'PRO'
						when a.stafftypedesc = 'Principal Investigator 2nd Work Spell' then 'PRO'
						when a.stafftypedesc = 'Principal Investigator 3rd Work Spell' then 'PRO'
						when a.stafftypedesc = 'Co Investigator' then 'LEC'
						when a.stafftypedesc = 'Co Investigator 2nd Work Spell' then 'LEC'
						when a.stafftypedesc = 'Co Investtigator 3rd Work Spell' then 'LC'

						when a.stafftypedesc = 'Co-Investigator' then 'LEC'
						when a.stafftypedesc = 'Co-Investigator 2nd Work Spell' then 'LEC'
						when a.stafftypedesc = 'Co-Investtigator 3rd Work Spell' then 'LC'

						when a.stafftypedesc = 'Researcher' then 'RES'
						when a.stafftypedesc = 'Researcher 2nd Work Spell' then 'RES'
						when a.stafftypedesc = 'Research 3rd Work Spell' then 'RES'
						when a.stafftypedesc = 'Technician' then 'TEC'
						when a.stafftypedesc = 'Technician 2nd Work Spell' then 'TEC'
						when a.stafftypedesc = 'Technician 3rd Work Spell' then 'TEC'
						when a.stafftypedesc = 'Administrator' then 'ADM'
						when a.stafftypedesc = 'Administrator 2nd Work Spell' then 'ADM'
						when a.stafftypedesc = 'Administrator 3rd Work Spell' then 'ADM'
						when a.stafftypedesc = 'Teach Replacment' then 'REP'
						when a.stafftypedesc = 'Principal Investigator (Non FEC)' then 'PI_NO'
						when a.stafftypedesc = 'Co Investigator (Non FEC)' then 'LEC_NO'
						when a.stafftypedesc = 'Co-Investigator (Non FEC)' then 'LEC_NO'
						when a.stafftypedesc = 'Researcher (Non FEC)' then 'RES_NO'
						when a.stafftypedesc = 'Teach Replacement (Non FEC)' then 'REP_NO'


						when a.stafftypedesc = 'KTP Associate' then 'KT'
						when a.stafftypedesc = 'Web Designer' then 'WD'
						when a.stafftypedesc = 'Mentor' then 'ME'


						





						ELSE
						a.stafftypedesc END as ROLE,

						--a.val as FEC_COST,
						convert(decimal(20,2),
								ISNULL(basicval,0) +
								ISNULL(nival,0) +
								ISNULL(superval,0) +
								ISNULL( OAval  ,0) +
								ISNULL(c.val,0) +
								ISNULL(d.val,0) +
								ISNULL(e.val,0) 
									) as FEC_COST,

						--	matchedfund, 

						0 as CC_COST,

						/*
						convert(decimal(20,2),
						(
							
									ISNULL(basicval,0) +
									ISNULL(nival,0) +
									ISNULL(superval,0) +
									ISNULL( OAval ,0) 
								
								 

								+
								(
									ISNULL(c.val,0) +
									ISNULL(d.val,0) +
									ISNULL(e.val,0) 
								) 					
										-		--- MINUS

									(
											a.subincome + 
											ISNULL(c.Sponval,0) +
											ISNULL(d.Sponval,0) +
											ISNULL(e.Sponval,0) 
									)
								
								)) as CC_COST,
								*/
						
						convert(decimal(20,2),
							case
							when matchedfund = 'on' then 
								(
								ISNULL(basicval,0) +
								ISNULL(nival,0) +
								ISNULL(superval,0) +
								ISNULL( OAval ,0) 
								) +
								(
										ISNULL(c.matchedval,0) +
										ISNULL(d.matchedval,0) +
										ISNULL(e.matchedval,0) 
								)
							else 0 end 
								) as MATCH_FUND,


						@username as CRUSER,
						case
						when A.point = A.eddate then convert(varchar,DATEADD(mm, DATEDIFF(mm, 0, A.point), 0),103)
						else
							convert(varchar,a.point,103) 
							end
								as PJ_DATE_START,

						case
						when A.point = A.eddate then convert(varchar,A.point,103)
						else
							convert(varchar,DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,a.point)+1,0)) ,103)
							end
								as PJ_DATE_END,

						ROW_NUMBER() OVER (ORDER BY 
						a.descr
						,A.point

						)  as rownum ,

						a.rowid,

						/*
						,
						
						convert(decimal(20,2),
									sponval ) as sponval,
									a.point,
									A.COSTVAL,
									a.subincome
									*/
								--	select * 
							--	, A.point,DATEADD(mm, DATEDIFF(mm, 0, a.point), 0)
							
							c.val,
							d.val,
							e.val,
	
							a.sponval,

							c.sponval,
							d.sponval,
							e.sponval,
							
							case
							when A.matchedfund = 'on' then 
								ISNULL(basicval,0) +
								ISNULL(nival,0) +
								ISNULL(superval,0) +
								ISNULL( OAval ,0) 
							else 0 end ,

							c.matchedval,
							d.matchedval,
							e.matchedval,

							a.subincome,
							c.subincome,
							d.subincome,
							e.subincome,

							A.matchedfund,
							A.finaladjust,


					( select top 1 isnull(basicTotal,0) from  [dbo].[UOE_P_PFACT_Prj_Dept_Split] q where PRJ_ID = @prjid and ROWID = a.rowid and recType = 'staff' )	as [FEC_SAL_ONLY] ,
					( select top 1 isnull(niTotal,0) from  [dbo].[UOE_P_PFACT_Prj_Dept_Split] q where PRJ_ID = @prjid  and ROWID = a.rowid and recType = 'staff')	as [FEC_NI] ,
					( select top 1 isnull(superTotal,0) from  [dbo].[UOE_P_PFACT_Prj_Dept_Split] q where PRJ_ID = @prjid  and ROWID = a.rowid and recType = 'staff')	as [FEC_PENSION] ,
					-- ( select top 1 isnull(OAval,0) from  [dbo].UOE_T1_CSV_DATA q where PRJ_ID = @prjid  and ROWID = a.rowid )	as [FEC_APP_LEVY] ,

				(	SELECT top 1
							-- CoinID,
							cs.Other_Allowances      
								FROM dbo.CostStaff cs     
								INNER JOIN dbo.Project_Proposal_Co_Investigators ppci ON cs.CoinID = ppci.id     
								WHERE ppci.Project_Proposal  = @prjid AND  cs.IsSpine=1 AND cs.UseInflation=1   			 
								and CoinID = a.rowid
								) ,

					( select top 1 isnull(Total,0) from  [dbo].[UOE_P_PFACT_Prj_Dept_Split] q where PRJ_ID = @prjid and ROWID = a.rowid and StaffTypeDesc = 'Estate Lab' )	as [AMT_ESTATE] ,
					( select top 1 isnull(Total,0) from  [dbo].[UOE_P_PFACT_Prj_Dept_Split] q where PRJ_ID = @prjid and ROWID = a.rowid and StaffTypeDesc = 'Infrastructure Lab' )	as [AMT_INFRA] ,
					( select top 1 isnull(Total,0) from  [dbo].[UOE_P_PFACT_Prj_Dept_Split] q where PRJ_ID = @prjid and ROWID = a.rowid and StaffTypeDesc = 'Indirect' )	as [AMT_INDIRECT] ,

					( select top 1 isnull(SponTotal,0) 
						*
						CASE
						WHEN  ( select top 1 matchedfund from  [dbo].[UOE_P_PFACT_Prj_Dept_Split] q where PRJ_ID = @prjid and ROWID = a.rowid ) = 'on'  THEN 0
						WHEN finaladjust = 0 THEN 0
						WHEN finaladjust = 0.00 THEN 0
						ELSE ISNULL(finaladjust,100) END / 100.00 
					
					from  [dbo].[UOE_P_PFACT_Prj_Dept_Split] q where PRJ_ID = @prjid and ROWID = a.rowid  )	as [spon] ,

					( select top 1 isnull(SponTotal,0)
						*
						CASE
						WHEN ( select top 1 matchedfund from  [dbo].[UOE_P_PFACT_Prj_Dept_Split] q where PRJ_ID = @prjid and ROWID = a.rowid and StaffTypeDesc = 'Estate Lab' ) = 'on'  THEN 0
						WHEN finaladjust = 0 THEN 0
						WHEN finaladjust = 0.00 THEN 0
						ELSE ISNULL(finaladjust,100) END / 100.00 
					
					 from  [dbo].[UOE_P_PFACT_Prj_Dept_Split] q where PRJ_ID = @prjid and ROWID = a.rowid and StaffTypeDesc = 'Estate Lab' )	as estateSpon_1 ,
					( select top 1 isnull(SponTotal,0) 
					
						*
						CASE
						WHEN ( select top 1 matchedfund from  [dbo].[UOE_P_PFACT_Prj_Dept_Split] q where PRJ_ID = @prjid and ROWID = a.rowid and StaffTypeDesc = 'Infrastructure Lab' ) = 'on'   THEN 0
						WHEN finaladjust = 0 THEN 0
						WHEN finaladjust = 0.00 THEN 0
						ELSE ISNULL(finaladjust,100) END / 100.00 

					from  [dbo].[UOE_P_PFACT_Prj_Dept_Split] q where PRJ_ID = @prjid and ROWID = a.rowid and StaffTypeDesc = 'Infrastructure Lab' )	as infraSpon_1 ,
					( select top 1 isnull(SponTotal,0) 
					
						*
						CASE
						WHEN ( select top 1 matchedfund from  [dbo].[UOE_P_PFACT_Prj_Dept_Split] q where PRJ_ID = @prjid and ROWID = a.rowid and StaffTypeDesc = 'Indirect' ) = 'on'    THEN 0
						WHEN finaladjust = 0 THEN 0
						WHEN finaladjust = 0.00 THEN 0
						ELSE ISNULL(finaladjust,100) END / 100.00 
					
					from  [dbo].[UOE_P_PFACT_Prj_Dept_Split] q where PRJ_ID = @prjid and ROWID = a.rowid and StaffTypeDesc = 'Indirect' )	as indSpon_1 ,

				case
				when ( select top 1 matchedfund from  [dbo].[UOE_P_PFACT_Prj_Dept_Split] q where PRJ_ID = @prjid and ROWID = a.rowid ) = 'on' then 
							
						( select top 1 isnull(basicTotal,0) from  [dbo].[UOE_P_PFACT_Prj_Dept_Split] q where PRJ_ID = @prjid and ROWID = a.rowid and recType = 'staff' )	+
						( select top 1 isnull(niTotal,0) from  [dbo].[UOE_P_PFACT_Prj_Dept_Split] q where PRJ_ID = @prjid  and ROWID = a.rowid and recType = 'staff')	+
						( select top 1 isnull(superTotal,0) from  [dbo].[UOE_P_PFACT_Prj_Dept_Split] q where PRJ_ID = @prjid  and ROWID = a.rowid and recType = 'staff')	+
						( select top 1 isnull(OAval,0) from  [dbo].UOE_T1_CSV_DATA q where PRJ_ID = @prjid  and ROWID = a.rowid )	

				else 0 end,

				case
				when ( select top 1 matchedfund from  [dbo].[UOE_P_PFACT_Prj_Dept_Split] q where PRJ_ID = @prjid and ROWID = a.rowid and StaffTypeDesc = 'Estate Lab' )	 = 'on' then
						( select top 1 isnull(Total,0) from  [dbo].[UOE_P_PFACT_Prj_Dept_Split] q where PRJ_ID = @prjid and ROWID = a.rowid and StaffTypeDesc = 'Estate Lab' )	
				else 0 end,


				case
				when ( select top 1 matchedfund from  [dbo].[UOE_P_PFACT_Prj_Dept_Split] q where PRJ_ID = @prjid and ROWID = a.rowid and StaffTypeDesc = 'Infrastructure Lab' )	 = 'on' then
						( select top 1 isnull(Total,0) from  [dbo].[UOE_P_PFACT_Prj_Dept_Split] q where PRJ_ID = @prjid and ROWID = a.rowid and StaffTypeDesc = 'Infrastructure Lab' )	
				else 0 end,

				case
				when ( select top 1 matchedfund from  [dbo].[UOE_P_PFACT_Prj_Dept_Split] q where PRJ_ID = @prjid and ROWID = a.rowid and StaffTypeDesc = 'Indirect' )	 = 'on' then
						( select top 1 isnull(Total,0) from  [dbo].[UOE_P_PFACT_Prj_Dept_Split] q where PRJ_ID = @prjid and ROWID = a.rowid and StaffTypeDesc = 'Indirect' )	
				else 0 end





						
						 from UOE_T1_CSV_DATA A
						 inner join T1_Fisacl_Month fin
								on DATEPART(month,a.point) = fin.monthvalue
						left outer join 
									UOE_PFACT_T1_BID_TBL b
										on a.PRJ_ID = b.pfactid

									left outer join (
													select distinct prj_id, rowid, stafftypedesc, point, 
													 val, 
													 case
														when matchedfund = 'on' then val
														else 0 end as matchedval,
													 case
														when matchedfund = 'on' then 0
														else Sponval end as Sponval, 
														
													case
														when matchedfund = 'on' then 0
														else subincome  end as subincome 
													from UOE_T1_CSV_DATA
													where stafftypedesc IN (
														 'Estate Lab' ,
														 'Estate Desk'
														 )
														and prj_id =   @prjid
														) c
															on A.prj_id = c.prj_id
															and a.rowid = c.rowid
															--and a.point = c.point
															and DATEADD(mm, DATEDIFF(mm, 0, a.point), 0)
																	=  DATEADD(mm, DATEDIFF(mm, 0, c.point), 0)


				

									left outer join (
													select distinct prj_id, rowid, stafftypedesc, point, val, 
													case
														when matchedfund = 'on' then val
														else 0 end as matchedval,
													case
														when matchedfund = 'on' then 0
														else Sponval end as Sponval, 
														
													case
														when matchedfund = 'on' then 0
														else subincome  end as subincome 
														 from UOE_T1_CSV_DATA
													where stafftypedesc IN (
														 'Infrastructure Lab' ,
														 'Infrastructure Desk'
														 )
														and prj_id = @prjid
														) d
															on A.prj_id = d.prj_id
															and a.rowid = d.rowid
															--and a.point = d.point
															and DATEADD(mm, DATEDIFF(mm, 0, a.point), 0)
																	=  DATEADD(mm, DATEDIFF(mm, 0, d.point), 0)

									left outer join (
													select distinct prj_id, rowid, stafftypedesc, point, val,
													case
														when matchedfund = 'on' then val
														else 0 end as matchedval,
													case
														when matchedfund = 'on' then 0
														else Sponval end as Sponval, 
														
													case
														when matchedfund = 'on' then 0
														else subincome  end as subincome 
													 from UOE_T1_CSV_DATA
													where stafftypedesc = 'Indirect' 
														and prj_id = @prjid
														) e
															on A.prj_id = e.prj_id
															and a.rowid = e.rowid
															--and a.point = e.point
															and DATEADD(mm, DATEDIFF(mm, 0, a.point), 0)
																	=  DATEADD(mm, DATEDIFF(mm, 0, e.point), 0)

									 left outer join (
													select distinct A.id , B.StaffNumber
													  FROM PROJECT_Proposal_Co_investigators A 
														inner join [dbo].[Staff_Details] B
																	ON A.Co_Investigator = B.ID
													   WHERE A.Project_proposal= @prjid 
													   and ISNULL(b.is_disabled,'') = ''
													   ) s on s.Id = A.ROWID


								inner join 
									(	Select distinct  rowid, StaffNumber, fte, t1_bud_temp_heading, prj_id, catCode, DATEDIFF(d,StartDate,dateadd(d,1,EndDate) ) as prjlenDays from
										T1_staff_Data 
										)
										q
										on Q.rowid = A.ROWID
											and q.prj_id =@prjid
								left outer join
											( 
											select A.*,B.Description from
											Project_Details_Staff_Cost A(NOLOCK)     
												INNER JOIN Lup_Fix_Duration B(NOLOCK) ON A.unit_Type=B.ID  
												)	w
												on w.CoInvestigator = q.rowid

								--- UNIQUE TBA
								LEFT OUTER JOIN (
								
		
								SELECT distinct prj_id, rowid, NAME + ' #' +
								CONVERT(VARCHAR, 
								ROW_NUMBER() OVER (ORDER BY 
													ROWID
													,NAME
													))  as DUPLICATENAME
								 FROM
								T1_staff_Data U
								WHERE
									U.prj_id =@prjid
									AND U.NAME IN (	 
											Select distinct  NAME from
												T1_staff_Data q
												where q.prj_id =@prjid
												group by name
												having COUNT(*) > 1
												)
				) I
						ON A.ROWID = I.ROWID

						where 
						q.catCode in (
							21005,
							21105,
							21205,
							21305,
							21340,
							12105,
							12115,
							12120,
							12125,
							12135,
							12140,
							12145
							)

							and A.rectype = 'staff'  
						/*
						a.stafftypedesc not in (
												'Estate Lab' ,
												'Estate Desk',
												'Indirect',
												'Infrastructure Lab',
												'Infrastructure Desk'
												)
												*/
								 
							and	a.prj_id = @prjid

								-- and q.catDesc <> 'STUDENT FUNDING MAINTENANCE'

						order by 
						a.descr
						,A.point
						/*
						, year(dateadd(month, -3, a.point))
						,MONTH(DATEADD(M, 3, a.point))
						*/
	
		
		----------------
		if ( select top 1 COUNT(*) from @restbl group by rowid  ) > 1 
		begin
					select 
					[BIDNUMBER] ,
					[PFACT_ID] ,
					[STAFF] ,
					[TBA_STAFF_NAME] ,
					[WP] ,
					[BUDC] ,
					[BUD_HEADING] ,
					[DATE_START] ,
					[DATE_END] ,
					[FIN_YEAR] ,
					[FIN_PER] ,
					[PERIOD_DAYS_PJ] ,
					[FTE_CALC] ,
					[FEC_SAL_ONLY] ,
					[FEC_NI] ,
					[FEC_PENSION] ,
					[FEC_APP_LEVY] ,
					[COST_TYPE] ,
					[AMT_ESTATE] ,
					[AMT_INFRA] ,
					[AMT_INDIRECT] ,
					[ROLE] ,
					[FEC_COST] ,

					( FEC_SAL_ONLY +FEC_NI + FEC_PENSION + FEC_APP_LEVY + AMT_ESTATE + AMT_INFRA + AMT_INDIRECT )
							-
							( sponsorship + estateSpon + infraSpon + indSpon )
							-
							( matchedFund + estateMatched + infraMatched + indMatched ) as [CC_COST] ,

					[MATCH_FUND] ,
					[CRUSER] ,
					[PJ_DATE_START] ,
					[PJ_DATE_END] ,
					[rownum]
		
						 from @restbl
		end

	

		if ( select top 1 COUNT(*) from @restbl group by rowid ) = 1 
		begin
		
					select 
					[BIDNUMBER] ,
					[PFACT_ID] ,
					[STAFF] ,
					[TBA_STAFF_NAME] ,
					[WP] ,
					[BUDC] ,
					[BUD_HEADING] ,
					[DATE_START] ,
					[DATE_END] ,
					[FIN_YEAR] ,
					[FIN_PER] ,
					[PERIOD_DAYS_PJ] ,
					[FTE_CALC] ,
					[1_FEC_SAL_ONLY] AS [FEC_SAL_ONLY] ,
					[1_FEC_NI] AS [FEC_NI] ,
					[1_FEC_PENSION] AS [FEC_PENSION] ,
					[1_FEC_APP_LEVY] AS [FEC_APP_LEVY] ,
					[COST_TYPE] ,
					[1_estateVal] AS [AMT_ESTATE] ,
					[1_infraVal] AS [AMT_INFRA] ,
					[1_indVal] AS [AMT_INDIRECT] ,
					[ROLE] ,
					
					( [1_FEC_SAL_ONLY] + [1_FEC_NI] + [1_FEC_PENSION] + [1_FEC_APP_LEVY] + [1_estateVal] + [1_infraVal] + [1_indVal] ) AS [FEC_COST] ,

					convert(decimal(20,2),
					( [1_FEC_SAL_ONLY] + [1_FEC_NI] + [1_FEC_PENSION] + [1_FEC_APP_LEVY] + [1_estateVal] + [1_infraVal] + [1_indVal] )
							-
							( [1_sponsorship] + [1_estateSpon] + [1_infraSpon] + [1_indSpon] )
							-
							( [1_matchedFund] + [1_estateMatched] + [1_infraMatched] + [1_indMatched] ) 
							)	
							as [CC_COST] ,


				convert(decimal(20,2),
							case
							when ( select top 1 matchedfund from  [dbo].[UOE_P_PFACT_Prj_Dept_Split] q where PRJ_ID = @prjid and ROWID = a.rowid ) = 'on'  then 
								(
								ISNULL([1_FEC_SAL_ONLY],0) +
								ISNULL([1_FEC_NI],0) +
								ISNULL([1_FEC_PENSION],0) +
								ISNULL( [1_FEC_APP_LEVY] ,0) 
								) +
								(
										ISNULL([1_estatematched],0) +
										ISNULL([1_infraMatched],0) +
										ISNULL([1_indMatched],0) 
								)
							else 0 end 
								) as MATCH_FUND,

					[CRUSER] ,
					[PJ_DATE_START] ,
					[PJ_DATE_END] ,
					[rownum]
		
						 from @restbl A
		end

		-----------------

		insert into T1_CSV_ROW_COUNT
		select @prjid,
				 'COST_CALCS',
				 @@ROWCOUNT


	DELETE FROM
	T1_CSV_FILE_TOTALS
	WHERE
	pfact_id = @prjid
	AND src = 'COST_CALCS'


			INSERT INTO T1_CSV_FILE_TOTALS
				 select 
				 pfact_id, 
				 'COST_CALCS'  ,
				 SUM(MATCH_FUND) as MATCH_FUND,
				 SUM(CC_COST) as CC_COST
				 	from 
						@restbl
						group by
						pfact_id

					/*
						--	FOR TESTING

						select *, 
						FEC_SAL_ONLY +FEC_NI + FEC_PENSION + FEC_APP_LEVY + AMT_ESTATE + AMT_INFRA + AMT_INDIRECT,
						sponsorship + estateSpon + infraSpon + indSpon,
						matchedFund + estateMatched + infraMatched + indMatched,

							( FEC_SAL_ONLY +FEC_NI + FEC_PENSION + FEC_APP_LEVY + AMT_ESTATE + AMT_INFRA + AMT_INDIRECT )
							-
							( sponsorship + estateSpon + infraSpon + indSpon )
							-
							( matchedFund + estateMatched + infraMatched + indMatched ) as cc_cost

						
						 from @restbl
						where TBA_STAFF_NAME = 'PDRA 2 E/28 36 months TBA'
						*/


				--		select * from @restbl

end



