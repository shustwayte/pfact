USE [pfact_t1]
GO
/****** Object:  StoredProcedure [dbo].[p_rpt_Je_S_Summary_Facil_Equip]    Script Date: 12/09/2018 15:26:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROC [dbo].[p_rpt_Je_S_Summary_Facil_Equip]
(
@prj_id int,
@type varchar(100)
)
as
		--		exec p_rpt_Je_S_Summary_Facil_Equip 10621, 'Directly Allocated Costs'

begin
			 select 
			 ChargeType,
			 Name, Cost_Before_VAT, VAT_Cost,VAT_Percent--, *
			  from T1_Other_Data (nolock) 
			 where prj_id = @prj_id
			 and ChargeType = @type
			 and catDesc  in (
			 'RESEARCH CAPITAL EQUIPMENT', 'DA COSTS - RESEARCH FACILITIES'
			 )

			

end

