USE [pfact_t1]
GO
/****** Object:  StoredProcedure [dbo].[p_rpt_Je_S_Summary_Prj_Partners]    Script Date: 12/09/2018 15:26:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER PROC [dbo].[p_rpt_Je_S_Summary_Prj_Partners]
(
@prj_id int
)
		--		exec [dbo].[p_rpt_Je_S_Summary_Prj_Partners] 10440

as

begin

	declare @res table
	(
	partner varchar(300),
	costtype varchar(300),
	fec decimal(20,2),
	rcContribution decimal(20,2)
	)

	insert into @res
	select 
	name, 
	costtype,
		Cost, 
		income
	
		FROM [pfact_t1].[dbo].[vw_UOE_P_PRJ_Budget_Table_costdetail_uoe_RPT] (nolock) 
		where ProjectID = @prj_id
		order by 1,2

		if ( select COUNT(*) from @res ) > 1
		begin
			select *, 1 as ord from @res
			union all
			select 'Partner Totals', '', SUM(fec), SUM(rcContribution), 2 as ord from @res
		end



end



