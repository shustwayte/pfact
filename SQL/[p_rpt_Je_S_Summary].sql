USE [pfact_t1]
GO
/****** Object:  StoredProcedure [dbo].[p_rpt_Je_S_Summary]    Script Date: 12/09/2018 15:26:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROC  [dbo].[p_rpt_Je_S_Summary]
(
@prj_id int
)

as
					--	exec [dbo].[p_rpt_Je_S_Summary] 10759

begin

	declare @res table
	(
	ChargeType varchar(200),
	descr varchar(200),
	val decimal(20,2),
	sponval decimal(20,2),
	ord int,
	cat varchar(300)
	)

		insert into @res

		SELECT DISTINCT 
		a.ChargeType, 
		--recType, 
		
		case
	--	when a.catDesc like '%FACILITIES%' then 'Research Facilites'
		when a.recType = 'Staff' and a.ChargeType='Directly Incurred Costs'   then 'Staff'
		when a.recType = 'Staff' and a.ChargeType='Directly Allocated Costs'   then 'Staff'
		when a.recType <> 'Staff' and a.ChargeType='Directly Allocated Costs'   then a.StaffTypeDesc
		when a.ChargeType='Indirect Costs'   then 'Indirect'
		when a.ChargeType='Not VATable' then 'Indirect'
		else 'Non Staff' end as descr,

		--StaffTypeDesc, 

		--convert(decimal(20,2),sum(val)) as val, 
		0 as val,
		convert(decimal(20,2),sum(sponval)) as sponval,
		case
		when a.ChargeType = 'Directly Incurred Costs' then 1
		when a.ChargeType = 'Directly Allocated Costs' then 2
		when a.ChargeType = 'Indirect Costs' then 3
		when a.ChargeType = 'Exceptions' then 4
		else 5 end as ord,
		a.catDesc

		 from 
		 (
			Select
			A.rowid,
			A.prj_id,
			rectype,
			val,
				case
				when matchedfund = 'on' then 0
				WHEN ISNULL(finaladjust,'99999999') = '99999999' THEN 
				subincome
				else
				Sponval end as Sponval,
				A.ChargeType,
				StaffTypeDesc,
				B.catDesc
			from
			UOE_T1_CSV_DATA A (nolock) 
				left outer JOIN [dbo].[T1_Other_Data] (nolock)  B
						ON A.PRJ_ID = B.prj_id
						AND A.ROWID = B.rowid
			where 
			A.prj_id =  @prj_id
		)
		 a
			
			where a.prj_id  = @prj_id 
			GROUP BY a.ChargeType, 
			catDesc,
			--	recType, StaffTypeDesc
			case
		when a.recType = 'Staff' and a.ChargeType='Directly Incurred Costs'   then 'Staff'
		when a.recType = 'Staff' and a.ChargeType='Directly Allocated Costs'   then 'Staff'
		when a.recType <> 'Staff' and a.ChargeType='Directly Allocated Costs'   then a.StaffTypeDesc
		when a.ChargeType='Indirect Costs'   then 'Indirect'
		--when a.catDesc like '%FACILITIES%' then 'Research Facilites'
		when a.ChargeType='Not VATable' then 'Indirect'
		else 'Non Staff' end
			order by
			case
			when a.ChargeType = 'Directly Incurred Costs' then 1
			when a.ChargeType = 'Directly Allocated Costs' then 2
			when a.ChargeType = 'Indirect Costs' then 3
			when a.ChargeType = 'Exceptions' then 4
			else 5 end

			
			update @res
			set 
			val = c.val,
			sponval = c.spon
			from
				@res a
				inner join (
						Select
							ChargeType,
							SUM(spontotal) as val,

			case
				when matchedfund = 'on' then 0
				else
							case when ISNULL(finaladjust,'9999999999999') = '9999999999999'
							then
							SUM(sponsorcont) else

							sum(
							isnull(
										SponTotal *
											( case
											when finaladjust = 0   then 0
												when isnull(finaladjust,'9999999999999') = '9999999999999'  then 100 
												--when finaladjust = ''  then 100 
												else 
												finaladjust end / 100
												 ),0)
												 )

							end
					end		
							as spon,
							case
							when recType = 'Staff' and ChargeType='Directly Incurred Costs'   then 'Staff'
							when recType = 'Staff' and ChargeType='Directly Allocated Costs'   then 'Staff'
							when recType <> 'Staff' and ChargeType='Directly Allocated Costs'   then StaffTypeDesc
							when ChargeType='Indirect Costs'   then 'Indirect'
							else 'Non Staff' end as jj
							from 
						[dbo].[UOE_P_PFACT_Prj_Dept_Split] (nolock) 
							where prj_id = @prj_id
							group by
							matchedfund,
							ChargeType,
							ISNULL(finaladjust,'9999999999999'),
							case
							when recType = 'Staff' and ChargeType='Directly Incurred Costs'   then 'Staff'
							when recType = 'Staff' and ChargeType='Directly Allocated Costs'   then 'Staff'
							when recType <> 'Staff' and ChargeType='Directly Allocated Costs'   then StaffTypeDesc
							when ChargeType='Indirect Costs'   then 'Indirect'
							else 'Non Staff' end
							) c 
								on a.ChargeType = c.ChargeType
								and a.descr = c.jj


		select distinct
		ChargeType, 
		case
		when cat like '%RESEARCH FACILITIES%'
		then 'Research Facilites' else
		descr end as descr
		
		, val, sponval, ord	--	, cat
		
		 from @res
		union all
		select 
		'UOE Totals', '', SUM (val), SUM(sponval), 10 --	, ''
		 from @res





end

