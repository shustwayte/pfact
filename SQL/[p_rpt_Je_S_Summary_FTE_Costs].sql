USE [pfact_t1]
GO
/****** Object:  StoredProcedure [dbo].[p_rpt_Je_S_Summary_FTE_Costs]    Script Date: 12/09/2018 15:26:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER PROC [dbo].[p_rpt_Je_S_Summary_FTE_Costs]
(
@prj_id int
)

as
		--		exec [p_rpt_Je_S_Summary_FTE_Costs] 10487
begin

		select 
		ChargeType,
		StaffTypeDesc,
		sum(SponTotal) as tot
		 from [dbo].[UOE_P_PFACT_Prj_Dept_Split] (nolock) 
					 a
					 where a.PRJ_ID = @prj_id
					 and recType = 'FTE Related'
			 
		group by 
		ChargeType,
		StaffTypeDesc
		
end

