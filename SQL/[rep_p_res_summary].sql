USE [pfact_t1]
GO
/****** Object:  StoredProcedure [dbo].[rep_p_res_summary]    Script Date: 12/09/2018 15:29:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROC [dbo].[rep_p_res_summary] 
(
@prj_id int
)

as
		--		exec rep_p_res_summary	10759

begin


	declare @res table
	(
	id int,
	descr varchar(100),
	totFec float,
	funderContrib float

	)

	insert into @res
		select 
		1,
		'Staff Costs' as descr,
		isnull(SUM(val),0) as totFec,
		isnull(SUM(Sponval),0) as FunderControbu
		
		from 
		--[dbo].[UOE_P_PFACT_Prj_Dept_Split]
		(
			Select
			rowid,
			prj_id,
			rectype,
			val,
				case
				when matchedfund = 'on' then 0
				WHEN ISNULL(finaladjust,'99999999') = '99999999' THEN 
				Sponval
				else
				subincome end as Sponval
			from
			UOE_T1_CSV_DATA 
			where 
			prj_id =@prj_id
			and rectype = 'staff'  
		)
		 a
		inner join 
											(	Select *, DATEDIFF(d,StartDate,dateadd(d,1,EndDate) ) as prjlenDays from
												T1_staff_Data 
												)
												q
												on Q.rowid = A.ROWID
													and q.prj_id =@prj_id
													and A.rectype = 'staff'  
			where a.prj_id  = @prj_id
						and 
								q.catCode in (
									21005,
									21105,
									21205,
									21305,
									21340,
									12105,
									12115,
									12120,
									12125,
									12135,
									12140,
									12145
									)
								
			union all


			select 
			2,
		'Equipment Costs' as descr,
		isnull(SUM(val),0) as totFec,
		isnull(SUM(Sponval),0) as FunderControbu
		from 
		--[dbo].[UOE_P_PFACT_Prj_Dept_Split] 
		(
			Select
			rowid,
			prj_id,
			rectype,
			val,
				case
				when matchedfund = 'on' then 0
				WHEN ISNULL(finaladjust,'99999999') = '99999999' THEN 
				Sponval
				else
				subincome end as Sponval
			from
			UOE_T1_CSV_DATA 
			where 
			prj_id =@prj_id
		)
		a

			inner join 
									--	Select * from
										T1_Other_Data q
										on Q.rowid = A.ROWID
											and q.prj_id =@prj_id

			where a.prj_id  = @prj_id
						and 
						(
								q.catcode IN ( --12130 , 
								19005 )
							or
							--	q.catDesc in ( 'CAPITAL FUNDING', 'Research Capital Equipment' )
								q.catDesc in (  'Research Capital Equipment' )

							)

			union all


			select 
			3,
		'Other Costs' as descr,
		isnull(SUM(val),0) as totFec,
		isnull(SUM(Sponval),0) as FunderControbu
		from 
		--[dbo].[UOE_P_PFACT_Prj_Dept_Split] 
		(
			Select
			rowid,
			prj_id,
			rectype,
			val,
				case
				when matchedfund = 'on' then 0
				WHEN ISNULL(finaladjust,'99999999') = '99999999' THEN 
				Sponval
				else
				subincome end as Sponval
			from
			UOE_T1_CSV_DATA 
			where 
			prj_id =@prj_id
		)
		a

			inner join 
									--	Select * from
										T1_Other_Data q
										on Q.rowid = A.ROWID
											and q.prj_id =@prj_id

								left outer join 
									--	Select * from
										T1_staff_Data h
										on h.rowid = A.ROWID
											and h.prj_id =@prj_id

								left outer join 
									--	Select * from
										T1_other_Data f
										on f.rowid = A.ROWID
											and f.prj_id =@prj_id

			where a.prj_id  = @prj_id
						and 


			(
			h.catCode in ( 22005, 22075, 22080 )
			or
			f.catCode in ( 22005, 22075, 22080 )
			or


								q.catCode in (
						25002,
						25004,
						21405,
						21410,
						21415,
						21420,
						21425,
						21430,
						25006,
						21435,
						25040,
						25042,
						25044,
						25046,
						25008,
						25076,
						25010,
						25060,
						25062,
						25070,
						25072,
						25074,
						25080,
						25090,
						23020,
						23025,
						23030,
						23105,
						23110,
						23115,
						23120,
						25105,
						25110,
						25115,
						25120,
						25125,
						25130,
						25205,
						25210,
						25305,
						25310,
						25315,
						25905,
						24005,
						24010,
						24105,
						25320,
						25325,
						25330,
						25335,
						25340,
						25345,
						25350,
						25355,
						25405,
						25505,
						25510,
						25530,
						25515,
						25535,
						25540,
						25545,
						25550,
						25605,
						25610,
						25560,
						25570,
						25705,
						25710,
						25715,
						25720,
						25725,
						25730,
						25735,
						25740,
						25742,
						25745,
						25747,
						25750,
						25755,
						25910,
						25920,
						25925,
						25930,
						25935,
						25940,
						25945
				)
			)


		insert into @res
		( id , descr , totFec, funderContrib )
		select 4 , 'Total Direct Costs', SUM(totFec), SUM(fundercontrib) from 
		@res


		

		insert into @res
		( id , descr , totFec, funderContrib )
		select 5 , 'Other Directly Allocated Costs',
		ISNULL(SUM(val),0), ISNULL(SUM(subincome),0) from 
		(
			Select
			a.rowid,
			a.prj_id,
			rectype,
			val,
				case
				when matchedfund = 'on' then 0
				WHEN ISNULL(finaladjust,'99999999') = '99999999' THEN 
				Sponval
				else
				subincome end as subincome
				
				
			from
			UOE_T1_CSV_DATA a
				inner join 
				--	SELECT * FROM
				T1_Other_Data b
					on a.PRJ_ID = b.prj_id
					and a.ROWID = b.rowid
			where 
			a.prj_id = @prj_id and
			a.ChargeType = 'Directly Allocated Costs'
			AND (b.catDesc= 'DA COSTS - RESEARCH FACILITIES' OR b.catDesc = 'DA COST - TECHNICIAN')
		) a

		insert into @res
		( id , descr , totFec, funderContrib )
		select 6 , 'Estate Lab', 
		isnull(SUM(val),0), isnull(SUM(subincome),0)
		 from 
		(
			Select
			rowid,
			prj_id,
			rectype,
			val,
				case
				when matchedfund = 'on' then 0
				WHEN ISNULL(finaladjust,'99999999') = '99999999' THEN 
				Sponval
				else
				subincome end as subincome
			from
			UOE_T1_CSV_DATA 
			where 
			prj_id =@prj_id and
			stafftypedesc IN (
														 'Estate Lab' ,
														 'Estate Desk'
														 ) 
		) a


		insert into @res
		( id , descr , totFec, funderContrib )
		select 7 , 'Indirect',
		isnull(SUM(val),0), isnull(SUM(subincome),0) from 
		(
			Select
			rowid,
			prj_id,
			rectype,
			val,
				case
				when matchedfund = 'on' then 0
				WHEN ISNULL(finaladjust,'99999999') = '99999999' THEN 
				Sponval
				else
				subincome end as subincome
			from
			UOE_T1_CSV_DATA 
			where 
			prj_id =@prj_id and
			stafftypedesc IN (
														 'Indirect'
														 ) 
		) a



		insert into @res
		( id , descr , totFec, funderContrib )
		select 8 , 'Infrastructure Lab', 
		isnull(SUM(val),0), isnull(SUM(subincome),0) from 
		(
			Select
			rowid,
			prj_id,
			rectype,
			val,
				case
				when matchedfund = 'on' then 0
				WHEN ISNULL(finaladjust,'99999999') = '99999999' THEN 
				Sponval
				else
				subincome end as subincome
			from
			UOE_T1_CSV_DATA 
			where 
			prj_id =@prj_id and
			stafftypedesc IN (
														 'Infrastructure Lab' ,
														 'Infrastructure Desk'
														 ) 
		) a


														 
		insert into @res
		( id , descr , totFec, funderContrib )
		select 9, 'Totals', 
		isnull(SUM(totfec),0), isnull(SUM(FUNDERCONTRIB),0)
		FROM @res
			WHERE id <> 4


														 

		select * from @res




end	



