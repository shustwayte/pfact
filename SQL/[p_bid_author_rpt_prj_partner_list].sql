USE [pfact_t1]
GO
/****** Object:  StoredProcedure [dbo].[p_bid_author_rpt_prj_partner_list]    Script Date: 12/09/2018 15:25:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



ALTER PROC [dbo].[p_bid_author_rpt_prj_partner_list]
(
@prj_id int
)
		--		 [dbo].[p_bid_author_rpt_prj_partner_list] 10489
as

begin 

	select distinct 
		name
		FROM [pfact_t1].[dbo].[vw_UOE_P_PRJ_Budget_Table_costdetail_uoe_RPT]
			where ProjectID = @prj_id

end

