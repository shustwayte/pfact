USE [pfact_t1]
GO
/****** Object:  StoredProcedure [dbo].[p_rpt_staff_details]    Script Date: 12/09/2018 15:27:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER PROC [dbo].[p_rpt_staff_details]
(
@prj_id int
)

as
			--		exec p_rpt_staff_details 10440
begin
      
		declare @prjdet table (                                  
		 [ID] [int] NULL ,                                  
		 [Staff_Type] [int] NULL ,                                  
		 [StaffType] [varchar] (50) NULL ,                                  
		 [Co_Investigator] [int] NULL ,                                  
		 [NAME] [varchar] (100) NULL ,                                  
		 [Sub_Element] [int] NULL ,                                  
		 [CODE] [varchar] (30) NULL ,                                  
		 [Description] [varchar] (100) NULL ,                                  
		 [Tbl] [int] NULL ,                                  
		 [Category] [varchar] (100) NULL ,                                  
		 [CATID] [int] NULL ,                                  
		 [Sequence_NO] [int] NULL ,                                  
		 [Basic] [float] NULL ,                                  
		 [NI] [float] NULL ,                                  
		 [NIOUT] [float] NULL ,                                  
		 [Super] [float] NULL ,                                  
		 [TOTAL] [float] NULL ,                                  
		 [Other_Allowances] [float] NULL ,                                  
		 [FirstName] [varchar] (100) NULL ,                                  
		 [LastName] [varchar] (100) NULL ,                                  
		 [jesPID] [varchar] (50) NULL ,                                  
		 [UseLondonWeighting] [bit] NULL ,                                  
		 [Title] [varchar] (25) NULL ,                                  
		 [Initials] [varchar] (25) NULL ,                                  
		 [StartDate] [datetime] NULL ,                                  
		 [EndDate] [datetime] NULL ,                                  
		 [SpinePoint] [int] NULL ,                                  
		 [SpineOnDate] [datetime] NULL ,                                  
		 [IncrementDate] [datetime] NULL ,                                  
		 [DiscretionaryPoint] [int] NULL ,                                  
		 [MonthsForIncrement] [int] NULL ,                                  
		 [ProjectCode] [varchar] (20) NULL ,                                  
		 [ProjectName] [varchar] (100) NULL ,                                  
		 [DefaultChargeType] [varchar] (70) NULL ,                                  
		 [ProjectStart] [datetime] NULL ,                                  
		 [ProjectEnd] [datetime] NULL ,                                  
		 [JesStaffTypeDesc] [varchar] (50) NULL ,     
		 [Base_Year] [varchar] (20) NULL ,                                  
		 [Inflation_Factor] [int] NULL  ,                                
		 [PayScale] [int] NULL ,                             
		 [StartSpine] [int] NULL ,                            
		 [StaffEndDate] [DATETIME] NULL ,              
		 [PayScaleEndDate] [DATETIME] NULL,                              
		 [NINumber] [varchar](15)NULL,                
		 [ISPI] INT NULL,       
		 [AnnualContractHours] FLOAT NULL,        
		 [AnnualContractDays] FLOAT NULL,        
		 [WeeksInYear] FLOAT NULL,       
		 [Department] VARCHAR(100)                    
		)     
		insert into @prjdet
		EXEC REPORTPROJECTSTAFFDETAILED @prj_id,1,1,0,0


		select 
		a.id, 
		isnull(c.firstName + ' '+ c.SurName,a.NAME) as Name, 
		StaffType,
		Description,
		a.SpinePoint,
		DATEDIFF(M,StartDate, EndDate) +1 AS MONTHONPRJ,
		b.FTE,
		@prj_id as prj_id,
		case
								when StaffType = 'Principal Investigator' then 1
								when StaffType = 'Co-Investigator' then 2
								else 3 end as ord
			 from @prjdet a
				inner join (
							select 
							ROWID,
							convert(decimal(20,2),FTE * 100) AS FTE
							 from [dbo].[UOE_P_PFACT_Prj_Dept_Split]
								where prj_id  = @prj_id
								and recType = 'staff'
								) b
								on a.ID = b.ROWID

				
				left outer join [dbo].[Staff_Details]
								c on c.ID = a.Co_Investigator

								


end

