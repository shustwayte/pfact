USE [pfact_t1]
GO
/****** Object:  StoredProcedure [dbo].[p_BID_rpt_Staff_details]    Script Date: 12/09/2018 15:25:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



ALTER PROC [dbo].[p_BID_rpt_Staff_details]
(
@prj_id int
)

as
			--		exec p_BID_rpt_Staff_details 10489

begin

			declare @resdata TABLE (    
				 [ID] [int] NULL ,                                  
				 [Staff_Type] [int] NULL ,                                  
				 [StaffType] [varchar] (50) NULL ,                                  
				 [Co_Investigator] [int] NULL ,                                  
				 [NAME] [varchar] (100) NULL ,                                  
				 [Sub_Element] [int] NULL ,                                  
				 [CODE] [varchar] (30) NULL ,                                  
				 [Description] [varchar] (100) NULL ,                                  
				 [Tbl] [int] NULL ,                                  
				 [Category] [varchar] (100) NULL ,                                  
				 [CATID] [int] NULL ,                                  
				 [Sequence_NO] [int] NULL ,                                  
				 [Basic] [float] NULL ,                                  
				 [NI] [float] NULL ,                                  
				 [NIOUT] [float] NULL ,                                  
				 [Super] [float] NULL ,                                  
				 [TOTAL] [float] NULL ,                                  
				 [Other_Allowances] [float] NULL ,                                  
				 [FirstName] [varchar] (100) NULL ,                                  
				 [LastName] [varchar] (100) NULL ,                                  
				 [jesPID] [varchar] (50) NULL ,                                  
				 [UseLondonWeighting] [bit] NULL ,                                  
				 [Title] [varchar] (25) NULL ,                                  
				 [Initials] [varchar] (25) NULL ,                                  
				 [StartDate] [datetime] NULL ,                                  
				 [EndDate] [datetime] NULL ,                                  
				 [SpinePoint] [int] NULL ,                                  
				 [SpineOnDate] [datetime] NULL ,                                  
				 [IncrementDate] [datetime] NULL ,                                  
				 [DiscretionaryPoint] [int] NULL ,                                  
				 [MonthsForIncrement] [int] NULL ,                                  
				 [ProjectCode] [varchar] (20) NULL ,                                  
				 [ProjectName] [varchar] (100) NULL ,                                  
				 [DefaultChargeType] [varchar] (70) NULL ,                                  
				 [ProjectStart] [datetime] NULL ,                                  
				 [ProjectEnd] [datetime] NULL ,                                  
				 [JesStaffTypeDesc] [varchar] (50) NULL ,     
				 [Base_Year] [varchar] (20) NULL ,                                  
				 [Inflation_Factor] [int] NULL  ,                                
				 [PayScale] [int] NULL ,                             
				 [StartSpine] [int] NULL ,                            
				 [StaffEndDate] [DATETIME] NULL ,              
				 [PayScaleEndDate] [DATETIME] NULL,                              
				 [NINumber] [varchar](15)NULL,                
				 [ISPI] INT NULL,       
				 [AnnualContractHours] FLOAT NULL,        
				 [AnnualContractDays] FLOAT NULL,        
				 [WeeksInYear] FLOAT NULL,       
				 [Department] VARCHAR(100)                    
				)              
  
				insert into @resdata 
				EXEC REPORTPROJECTSTAFFDETAILED @prj_id,1,1,0,0

			--	select * from @resdata
			declare @repdata table
				(
				id int identity(1,1),
				prjName varchar(300),
				roleCode varchar(300),
				staff varchar(300),
				dept varchar(300),
				 [ProjectStart] [datetime] NULL ,                                  
				 [ProjectEnd] [datetime] NULL ,
				 [deadline] [datetime] NULL ,
				 matchfund varchar(3)
				)


				declare @deadline date
				set @deadline = (
									select DeadLine from Project_Proposal
									where ID = @prj_id
								)


				declare @matchfund int
				set @matchfund = (	select SUM(
									case
									when isnull(matchedfund ,'') = 'on' then 1
									else 0 end) --, matchedfund
									from  [dbo].[UOE_P_PFACT_Prj_Dept_Split] (nolock) 
									where prj_id  = @prj_id
								)

				insert into @repdata
				select 
					ProjectName,
					--b.code,
					B.description as code,
					a.NAME,
					a.Department, 
					[ProjectStart],                                  
					[ProjectEnd] ,
					@deadline ,
					case
					when @matchfund = 1 then 'Yes'
					else 'No' end 
					from @resdata a
						left outer join
						--		select * from 
						 LUP_FIX_Staff_type  (nolock) b
								on a.StaffType = b.Description

								where 
								(
								B.description like '%Principal Investigator%'
								or
								B.description like '%Co-Investigator%'
								)

								order by 
								case
								when StaffType = 'Principal Investigator' then 1
								when StaffType = 'Co-Investigator' then 2
								else 3 end, name




				select * from @repdata

end

