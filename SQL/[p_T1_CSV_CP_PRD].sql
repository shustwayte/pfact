USE [pfact_t1]
GO
/****** Object:  StoredProcedure [dbo].[p_T1_CSV_CP_PRD]    Script Date: 12/09/2018 15:27:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



ALTER PROC [dbo].[p_T1_CSV_CP_PRD]
(
@prjid int,
@username varchar(100)
)
AS

begin

	---		 exec [p_T1_CSV_CP_PRD] 10625, 'sh444'

	
delete from T1_CSV_ROW_COUNT
	where prj_id = @prjid
	and which = 'CP_PRD'

		declare @capdata table
			(
			id int identity(1,1),
			prj_id int,
			rowid int,
			cost float,
			stdate datetime,
			eddate datetime,
			totdays int,
			dayrate float,
			sponsorval float,
			dayratesponsor float,
			monthsbetween int
			)
			insert into @capdata

						Select a.prj_id, a.rowid,
							cost, 
							stdate,
							ISNULL(dependdate, eddate) as eddate,
									datediff(d,
										stdate,
										ISNULL(dependdate, eddate) ) + 1,
							cost / ( datediff(d,
										stdate,
										ISNULL(dependdate, eddate) ) + 1 ),


							case
								when ISNULL(finaladjust,'99999999') = '99999999' then 
									sponsorcont 
								else
									SponTotal *
													(
														p.finaladjust / 100.00
													)
								end
							,
							case
								when ISNULL(finaladjust,'99999999') = '99999999' then 
									sponsorcont 
								else
									SponTotal *
													(
														p.finaladjust / 100.00
													)
								end
								 / ( datediff(d,
										stdate,
										ISNULL(dependdate, eddate) ) + 1 ) ,

										 ( 
										SELECT 
										DBO.UOE_MonthdIFF(stdate, 
																ISNULL(dependdate, eddate)
																		) 
										)
							from
										T1_Other_Data a
										inner join (
												select prj_id, 
														ROWID,
														CONVERT(datetime,
															substring(dependdate,7, 4) + '-' +
																substring(dependdate,4, 2) + '-' +
																	substring(dependdate,1, 2)
															) as dependdate,
															stdate,
															eddate,
															sponsorcont , SponTotal
															  from [dbo].[UOE_P_PFACT_Prj_Dept_Split]
													where prj_id  = @prjid
							
													) u
													on A.ROWID = u.ROWID
													and A.PRJ_ID = u.PRJ_ID


											inner join (
											select distinct 
													PRJ_ID,
													rowid, 
													finaladjust
													 from 
																[dbo].[UOE_P_PFACT_Prj_Dept_Split]	
																where PRJ_ID = @prjid
																) p
																on p.rowid = a.rowid
										where
											 a.prj_id =@prjid
											 and a.catDesc in ( 'CAPITAL FUNDING', 'Research Capital Equipment' )

							

			-- select * from @capdata
----------------------------------------

		declare @CP_PRD_Data table (
						id int identity(1,1),
						rowid int,
						point datetime,
						val float,
						daycount int,
						startdate datetime,
						enddate datetime,
						sponsorval float
						)
	 	
----------------------- project profile start
	declare @mthstart_prj int
	set @mthstart_prj = 8--MONTH(@st)		

	declare @prjyr_marker_prj int

				declare @st_prj datetime
				declare @ed_prj datetime
				declare @i_prj int
				declare @mth_start_prj int
				declare @month_between_prj int
				declare @yr_prj int
				declare @yr_marker_prj int
				declare @mths_prj int
				declare @mths_chk_prj int
											
			declare @dayi int
			declare @dayimax int

			declare @dayrate float

			declare @dayratesponsor float
											

				declare @i int
				declare @maxi int
				
				declare @rowid int

				set @i = 1
				set @maxi = ( select MAX(id) from @capdata )


				while @i <= @maxi
				begin
				
					set @rowid = ( select rowid from @capdata where id = @i )

					declare @temp_prj table (
						id int identity(1,1),
						rowid int,
						point datetime,
						acad_marker int,
						MONTHS INT,
						yr int,
						val float,
						prjyr_marker int,
						daycount int,
						startdate datetime,
						enddate datetime,
						sponsorval float
						)

					set @dayrate  = ( select dayrate from @capdata where id = @i )

					set @dayratesponsor  = ( select dayratesponsor from @capdata where id = @i )
					

						set @st_prj = ( SELECT 
								  stdate
									  FROM @capdata
											where id = @i
											)
				
											if DATEPART(MONTH,@st_prj) = 8
											begin
												set @prjyr_marker_prj = 0
											end
				
											if DATEPART(MONTH,@st_prj) <> 8
											begin
												set @prjyr_marker_prj = 1
											end		
							
							set @ed_prj = ( SELECT 
											eddate
											  FROM @capdata
													where id = @i
												)	
									
								--	select @st_prj, @ed_prj									
									
											update @temp_prj
											set val = 0,
											sponsorval = 0
				
											set @i_prj = 0
				
											set @mth_start_prj = ( select MONTH(@st_prj)	)

											set @month_between_prj = ( 
																	SELECT 
																	DBO.UOE_MonthdIFF(@st_prj, @ed_prj)+1
																	)
										
											--select @month_between
							
											set @yr_prj = 1	

											set @yr_marker_prj = 0			

											set @mths_prj = 12

											set @mths_chk_prj = 12
									
											while @i_prj <= @month_between_prj
											begin
												-- if august then year + 1
												set @yr_prj = ( 
													select 
													case 
													when month(dateadd(m,@i_prj, @st_prj)) = 8 then @yr_prj +1 
													else @yr_prj end
													)
						
												-- if year point reached, check how many months left	
												if @i_prj in (12, 24, 36, 48, 60, 72, 84, 96, 108, 120, 132, 144 )  
													begin
						
														if @month_between_prj - @i_prj < 12
														begin
														set @mths_prj = @month_between_prj - @i_prj
															if @mths_prj = 0
															begin
																set @mths_prj = 1 
															end
														end
													end
						
												if MONTH(dateadd(m,@i_prj, @st_prj)) = @mth_start_prj
												begin
													set @yr_marker_prj = @yr_marker_prj + 1
												end
						
											if MONTH( dateadd(m,@i_prj, @st_prj) ) = 8--@mthstart
											begin
											set @prjyr_marker_prj = @prjyr_marker_prj + 1
											end
						
											insert into @temp_prj ( rowid, point, acad_marker, MONTHS, yr, prjyr_marker,
															startdate, enddate)
												select 
													@rowid,
													dateadd(m,@i_prj, @st_prj),
													@yr_prj,
													--@ACAD_YEAR_prj,
													@mths_prj,
													@yr_marker_prj,
													@prjyr_marker_prj,
													case
													when @i_prj = 1 then @st_prj
													else 
															DATEADD(mm, DATEDIFF(mm, 0, 
																				dateadd(m,@i_prj, @st_prj) ), 0)
													end, 

													DATEADD (dd, -1, DATEADD(mm, DATEDIFF(mm, 0, 
																				dateadd(m,@i_prj, @st_prj)
																					) + 1, 0))
													

												set @i_prj = @i_prj + 1
					
											end						


											delete from @temp_prj
											where id > @month_between_prj
				
											-- ensure date profile doesn't exceed end date for project
											delete from @temp_prj
											where point > @ed_prj
				

										update @temp_prj
													set point = @st_prj
													where id = ( Select min(id) from @temp_prj )

													update @temp_prj
													set point = @ed_prj
													where id = ( Select MAX(id) from @temp_prj )
							------------------ project profile end		
							-- profile map for project

-------------------------------------------


				
			set @dayi = 1 
			set @dayimax = ( select MAX(id) from @temp_prj )
			
			while @dayi <= @dayimax
			begin
			
				update @temp_prj
				set daycount = 
					case
					when @dayi <> @dayimax then
					DATEDIFF( d,point, 
								DATEADD (dd, -1, DATEADD(mm, DATEDIFF(mm, 0, point) + 1, 0))
								) + 1
					else

					DATEDIFF( d,DATEADD(mm, DATEDIFF(mm, 0,point), 0), 
								point
								) + 1
						end 

				where 
				ID = @dayi
				
			
			set @dayi = @dayi + 1
			
			end

			update @temp_prj
			set val = daycount * @dayrate,
			sponsorval = daycount * @dayratesponsor
				--------------------------------------------

				update @temp_prj
				set startdate = @st_prj
				where id = ( Select min(id) from @temp_prj )

				update @temp_prj
				set enddate = @ed_prj
				where id = ( Select max(id) from @temp_prj )

				--	select * from @temp_prj

				

				insert into @CP_PRD_Data
						select 
							rowid,
							point,
							val,
							daycount,
							startdate,
							enddate,
							sponsorval
							from @temp_prj
				
					set @i = @i + 1

				end

------------------------------------------------

	--		select * from @CP_PRD_Data



DECLARE @RESTBL TABLE  (
	[BIDNUMBER] [varchar](9) NULL,
	[PFACT_ID] [int] NULL,
	[DESCR] [varchar](900) NULL,
	[DEPT] [varchar](10) NULL,
	[WP] [varchar](10) NULL,
	[NOMINAL] [varchar](10) NULL,
	[BUD_HEADING] [varchar](110) NULL,
	[DATE_START] [datetime] NULL,
	[DATE_END] [datetime] NULL,
	[UNIT] [float] NULL,
	[UNIT_TYPE] [varchar](50) NULL,
	[FIN_YEAR] [int] NULL,
	[FIN_PER] [int] NULL,
	[PERIOD_DAYS_PJ] [int] NULL,
	[AMT] [decimal](20, 2) NULL,
	[MATCH_FUND] [decimal](20, 2) NULL,
	[CC] [decimal](20, 2) NULL,
	[DEPN_DATE_END] [varchar](30) NULL,
	[DEPN] decimal(20,2) NULL,
	[AMT_VAT_EX] [float] NULL,
	[AMT_VAT_RATE] [float] NULL,
	[AMT_VAT] [float] NULL,
	[AMT_TOT] [float] NULL,
	rowid int
	--[rownum] [bigint] NULL
) 

			INSERT INTO @RESTBL
			select DISTINCT 
			
			'BID' +
			RIGHT('00000000' +
			replace(
				b.t1bid 
				, 'BID',''),6)
					as BIDNUMBER,
			A.prj_id as PFACT_ID,

			replace(a.descr,',','') as DESCR,
			a.dept as DEPT,
			A.workpackage as WP,
			q.catCode as NOMINAL,
			'BID' +
						RIGHT('00000000' +
						replace(
							b.t1bid 
							, 'BID',''),6) + 

							CASE
							WHEN isnull(q.t1_bud_temp_heading,'') = '' then '_00_99'
							else '_' +q.t1_bud_temp_heading end as BUD_HEADING,

			a.stdate as DATE_START,
			a.eddate as DATE_END,

			q.units AS UNIT,
			q.UnitType AS UNIT_TYPE,
				--year(dateadd(month, -3, a.point)) as FIN_YEAR,
				case
							   when fin.finvalue <= 5 then year(l.point) +1
								else year(l.point) end as FIN_YEAR,
				  --MONTH(DATEADD(M, 3, a.point)) as FIN_PER,
				  fin.finvalue as FIN_PER,
				  l.daycount as PERIOD_DAYS_PJ,

				  /*
			convert(decimal(20,2),
					l.val 
					) AS AMT,
					*/

			convert(decimal(20,2),
					q.cost
					) AS AMT,

			case
			when matchedfund = 'on' then 
			convert(decimal(20,2),
					l.val 
					)
					else
					0 
					end AS MATCH_FUND,
			convert(decimal(20,2),
					
					case
			when matchedfund = 'on' then 0
			else
					l.val
						- ( l.sponsorval
							/*	
								*	 ( case
														when finaladjust = 0 then 1
														else finaladjust / 100.00
														end ) 
														*/
								)
			end
					) AS CC,
			convert(varchar,
					u.dependdate					
					,103) AS DEPN_DATE_END,
			 convert(decimal(20,2),
					q.cost ) / k.monthsbetween  AS DEPN,

			q.Cost_Before_VAT AS AMT_VAT_EX,
			q.VAT_Percent AS AMT_VAT_RATE,
			q.VAT_Cost AS AMT_VAT,
			q.cost AS AMT_TOT,


			/*
						, ROW_NUMBER() OVER (ORDER BY 
						a.descr
						, l.point
						)  as rownum
						*/
				
						--, l.point
						
			--		select * 

			a.rowid
			 from UOE_T1_CSV_DATA A

			
			inner join (
						select prj_id, 
								ROWID,
								CONVERT(datetime,
									substring(dependdate,7, 4) + '-' +
										substring(dependdate,4, 2) + '-' +
											substring(dependdate,1, 2)
									) as dependdate
									  from [dbo].[UOE_P_PFACT_Prj_Dept_Split]
							where prj_id  = @prjid
							
							) u
							on A.ROWID = u.ROWID
							and A.PRJ_ID = u.PRJ_ID

			inner join @capdata k
						on k.rowid = A.ROWID

			left outer join 
						UOE_PFACT_T1_BID_TBL b
							on a.PRJ_ID = b.pfactid

					

				LEFT OUTER JOIN (
				
								SELECT DISTINCT 
								PPOD.Project_Proposal,
								PPOD.ID AS ROWID,
								Rate,Units,LUP_Element_Categories.ID
								,LUP_Element_Categories.Description,                        
								 LUP_Sub_Element.Description as SubElementDesc
								 ,LUP_Sub_Element.Code, LUP_Unit_Type_Of_Equ_And_NonStaff.Description as unitdesc ,LUP_Unit_Type_Of_Equ_And_NonStaff.ID as Equ_And_NonStaffID,                         
								 LUP_Element_Categories.Sequence_No,
								 LUP_Sub_Element.Inflation_Factor_ID,PPOD.Element,RecategoriseToConsumable,Lup_Inflation_Factor.Effective_Date,                      
								 Project_Proposal.Code as prjCode,Project_Proposal.Project_Name,
								 PPOD.RateOnDate,Project_Proposal.Start_Date ProjectStart,                 
								 PPOD.Element As ActualElement,PPOD.ResearchStudent FROM Project_Proposal_Sub_ELEMENT_ID (NOLOCK)                       
								 LEFT JOIN Project_Proposal_Others_Details PPOD (NOLOCK)  ON             
								 PPOD.ID=Project_Proposal_Sub_ELEMENT_ID.Project_Proposal_Others_Details                          
								 LEFT JOIN CostEquipNonStaff  (NOLOCK)  ON PPOD.ID=ProjPropOtherDetails                        
								 INNER  JOIN LUP_Sub_Element (NOLOCK)  ON Project_Proposal_Sub_ELEMENT_ID.Sub_Element_ID = LUP_Sub_Element.ID                         
								 INNER JOIN LUP_Element_Categories  (NOLOCK) ON LUP_Sub_Element.Category = LUP_Element_Categories.ID                         
								 LEFT JOIN LUP_FIX_ICT_CLASS (NOLOCK)  ON LUP_Element_Categories.ChargeType = LUP_FIX_ICT_CLASS.ICT_Class_ID                         
								 LEFT JOIN LUP_FIX_ICT_CLASS  subElementChargeType(NOLOCK) ON LUP_Sub_Element.Charge_Type = subElementChargeType.ICT_Class_ID
								 INNER JOIN SubElement_UnitType (NOLOCK)  ON SubElement_UnitTYpe.SubElement_ID=LUP_Sub_Element.ID                         
								 INNER JOIN LUP_Unit_Type_Of_Equ_And_NonStaff  (NOLOCK) ON SubElement_UnitType.Unit_Type= LUP_Unit_Type_Of_Equ_And_NonStaff.ID                        
								 INNER JOIN Lup_Inflation_Factor  (NOLOCK) ON LUP_Sub_Element.Inflation_Factor_ID=Lup_Inflation_Factor.ID                        
								 INNER JOIN Project_Proposal (NOLOCK)  ON PPOD.Project_Proposal=Project_Proposal.ID                        
								 WHERE PPOD.Project_Proposal= @prjid     
									--AND PPOD.ID = 50507  
							) Z
									ON Z.Project_Proposal = @prjid
									AND Z.ROWID = A.ROWID


										inner join 
									--	Select * from
										T1_Other_Data q
										on Q.rowid = A.ROWID
											and q.prj_id =@prjid

										inner join @CP_PRD_Data	l
												on A.ROWID = l.rowid

										inner join T1_Fisacl_Month fin
												on DATEPART(month,l.point) = fin.monthvalue


			where 
			-- q.catDesc = 'CAPITAL FUNDING'
			q.catcode = 28005
			/*
			A.recType not like  'DA COST%'
			and
			A.recType not like  'Staff'
			and
			A.recType not like  'FTE RELATED%'
			*/
			/*
			a.stafftypedesc not in (
									'Estate Lab' ,
									'Estate Desk',
									'Indirect',
									'Infrastructure Lab',
									'Infrastructure Desk'
									)
									*/
					and a.prj_id = @prjid

			order by 
			--a.descr
			replace(a.descr,',','')
			, case
							   when fin.finvalue <= 5 then year(l.point) +1
								else year(l.point) end ,
				  --MONTH(DATEADD(M, 3, a.point)) as FIN_PER,
				  fin.finvalue
			

SELECT [BIDNUMBER] ,
			[PFACT_ID] ,
			[DESCR] ,
			[DEPT] ,
			[WP] ,
			[NOMINAL] ,
			[BUD_HEADING] ,
			[DATE_START] ,
			[DATE_END] ,
			[UNIT] ,
			[UNIT_TYPE],
			[FIN_YEAR] ,
			[FIN_PER] ,
			[PERIOD_DAYS_PJ] ,
			CASE WHEN grpnum = 1 THEN 
			[AMT] ELSE 0 END AS [AMT],
			[MATCH_FUND] ,
			[cc],
			[DEPN_DATE_END] ,
			[DEPN] ,
			[AMT_VAT_EX] ,
			[AMT_VAT_RATE] ,
			[AMT_VAT],
			[AMT_TOT],
			rownum
						FROM (

								SELECT 
								[BIDNUMBER] ,
								[PFACT_ID] ,
								[DESCR] ,
								[DEPT] ,
								[WP] ,
								[NOMINAL] ,
								[BUD_HEADING] ,
								[DATE_START] ,
								[DATE_END] ,
								[UNIT] ,
								[UNIT_TYPE],
								[FIN_YEAR] ,
								[FIN_PER] ,
								[PERIOD_DAYS_PJ] ,
								[AMT] ,
								[MATCH_FUND] ,
								case
									when [CC] between -0.50 and 0.50 then 0.00
									else [CC] end as [cc],
								[DEPN_DATE_END] ,
								[DEPN] ,
								[AMT_VAT_EX] ,
								[AMT_VAT_RATE] ,
								[AMT_VAT],
								[AMT_TOT],
			
								 ROW_NUMBER() OVER (ORDER BY 
											descr
											--, l.point
											)  as rownum,

						

											ROW_NUMBER() OVER (PARTITION BY descr ORDER BY rowid ) as grpnum

								 FROM @RESTBL

							)
							Z
			
		insert into T1_CSV_ROW_COUNT
		select @prjid,
				 'CP_PRD',
				 @@ROWCOUNT


	DELETE FROM
	T1_CSV_FILE_TOTALS
	WHERE
	pfact_id = @prjid
	AND src = 'CP_PRD'


			INSERT INTO T1_CSV_FILE_TOTALS
				 select 
				 pfact_id, 
				 'CP_PRD'  ,
				 SUM(MATCH_FUND) as MATCH_FUND,
				 SUM(CC) as CC_COST
				 	from 
						@restbl
						group by
						pfact_id
end


