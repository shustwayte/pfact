USE [pfact_t1]
GO
/****** Object:  StoredProcedure [dbo].[p_T1_CSV_STU_PRD]    Script Date: 12/09/2018 15:28:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



ALTER PROC [dbo].[p_T1_CSV_STU_PRD]
(
@prjid int,
@username varchar(100)
)
AS

begin

	---		 exec [p_T1_CSV_STU_PRD] 10398, 'sh444'

	delete from T1_CSV_ROW_COUNT
	where prj_id = @prjid
	and which = 'STU_PRD'

	
	declare @restbl table (
	[BIDNUMBER] [varchar](9) NULL,
	[PFACT_ID] [int] NULL,
	[STUDENT] [varchar](900) NULL,
	[BUDC] [varchar](10) NULL,
	[WP] [varchar](10) NULL,
	[NOMINAL] [varchar](10) NULL,
	[BUD_HEADING] [varchar](110) NULL,
	[DATE_START] [datetime] NULL,
	[DATE_END] [datetime] NULL,
	[FEC_TOT_AMT] [float] NULL,
	[MATCH_FUND] [decimal](20, 2) NULL,
	[CC_TOT_AMT] [decimal](20, 2) NULL,
	[FIN_YEAR] [int] NULL,
	[FIN_PER] [int] NULL,
	[PERIOD_DAYS_PJ] [int] NULL,
	[STUDENT_RATE] [int] NOT NULL,
	[FTE] [int] NOT NULL,
	[STUDENT_TYPE] [varchar](3) NOT NULL,
	[rownum] [bigint] NULL
) 

		insert into @restbl
			select DISTINCT 
			'BID' +
			RIGHT('00000000' +
			replace(
				b.t1bid 
				, 'BID',''),6)
					as BIDNUMBER,
			A.prj_id as PFACT_ID,

			replace(a.descr,',','') as STUDENT,
			a.dept as BUDC,
			A.workpackage as WP,
			isnull(f.catCode,q.catCode) as NOMINAL,
			'BID' +
						RIGHT('00000000' +
						replace(
							b.t1bid 
							, 'BID',''),6) + 

							CASE
							WHEN isnull(
									ISNULL(f.t1_bud_temp_heading, q.t1_bud_temp_heading)
									,'') = '' then '_00_99'
							else '_' + ISNULL(f.t1_bud_temp_heading, q.t1_bud_temp_heading) end as BUD_HEADING,

			a.stdate as DATE_START,
			a.eddate as DATE_END,
			convert(decimal(20,2),
			ISNULL(
			case
			when f.catCode = '22005' then a.val
			when q.catCode = '22075' then A.VAL
			end ,0) )
				AS FEC_TOT_AMT,
			--'' AS FEC_TOT_AMT,
			
			convert(decimal(20,2),
					case
						when matchedfund = 'on' then 
							case
							when f.catCode = '22005' then a.val
							when q.catCode = '22075' then A.VAL
							end
						else 0 end 
						) AS MATCH_FUND,
			--'' AS MATCH_FUND,
			ISNULL(
			convert(decimal(20,2),
					case
						when matchedfund = 'on' then 0
						else (
								case
								when f.catCode = '22005' then a.val
								when q.catCode = '22075' then A.VAL
								end
							)
								- ( --	sponval 

									
								case
								when f.catCode = '22005' then a.Sponval
								when q.catCode = '22075' then A.Sponval
								end
									* 
									CASE 
										WHEN ISNULL(finaladjust,'99999999') = '99999999' THEN 
													1
										ELSE
										finaladjust / 100
										END
								/* ( case
													when finaladjust = 0 then 1
													else finaladjust / 100.00
													end ) 
													*/
													)
						end ),0)
						 as CC_TOT_AMT,

				--'' AS CC_TOT_AMT,
				--year(dateadd(month, -3, a.point)) as FIN_YEAR,
				case
							   when fin.finvalue <= 5 then year(a.point) +1
								else year(a.point) end as FIN_YEAR,
				  --MONTH(DATEADD(M, 3, a.point)) as FIN_PER,
				  fin.finvalue as FIN_PER,
				  a.daycalc as PERIOD_DAYS_PJ,
			
			case
			when q.descr like 'RC%' then 1 else 2 end AS STUDENT_RATE,
			--A.FTE AS FTE,
			1 AS FTE,
			'PGR' AS STUDENT_TYPE,
			
			
						ROW_NUMBER() OVER (ORDER BY 
						a.descr
						, year(dateadd(month, -3, a.point))
						,MONTH(DATEADD(M, 3, a.point))
						
						)  as rownum
						
					
			--		select * 
			
			 from UOE_T1_CSV_DATA A
			 inner join T1_Fisacl_Month fin
								on DATEPART(month,a.point) = fin.monthvalue
			left outer join 
						UOE_PFACT_T1_BID_TBL b
							on a.PRJ_ID = b.pfactid

					



						left outer join (
													select distinct prj_id, rowid, stafftypedesc, point, val
													 from UOE_T1_CSV_DATA
													where stafftypedesc IN (
														 'Estate Lab' ,
														 'Estate Desk'
														 )
														and prj_id = @prjid
														) c
															on A.prj_id = c.prj_id
															and a.rowid = c.rowid
															and a.point = c.point

									left outer join (
													select distinct prj_id, rowid, stafftypedesc, point, val from UOE_T1_CSV_DATA
													where stafftypedesc IN (
														 'Infrastructure Lab' ,
														 'Infrastructure Desk'
														 )
														and prj_id = @prjid
														) d
															on A.prj_id = d.prj_id
															and a.rowid = d.rowid
															and a.point = d.point

									left outer join (
													select distinct prj_id, rowid, stafftypedesc, point, val from UOE_T1_CSV_DATA
													where stafftypedesc = 'Indirect' 
														and prj_id = @prjid
														) e
															on A.prj_id = e.prj_id
															and a.rowid = e.rowid
															and a.point = e.point

								left outer join 
									--	Select * from
										T1_staff_Data q
										on Q.rowid = A.ROWID
											and q.prj_id =@prjid

								left outer join 
									--	Select * from
										T1_other_Data f
										on f.rowid = A.ROWID
											and f.prj_id =@prjid
			where 
			(
			q.catCode in ( 22005, 22075, 22080 )
			or
			f.catCode in ( 22005, 22075, 22080 )
			)
			
					and a.prj_id = @prjid

			order by 
			replace(a.descr,',','')
			, case
							   when fin.finvalue <= 5 then year(a.point) +1
								else year(a.point) end 
			, fin.finvalue 

			select 
			[BIDNUMBER] ,
			[PFACT_ID] ,
			[STUDENT] ,
			[BUDC] ,
			[WP] ,
			[NOMINAL] ,
			[BUD_HEADING] ,
			[DATE_START] ,
			[DATE_END] ,
			[FEC_TOT_AMT] ,
			[MATCH_FUND] ,
			case
			when [CC_TOT_AMT] between -0.50 and 0.50 then 0.00
			else [CC_TOT_AMT] end as [CC_TOT_AMT],
			[FIN_YEAR] ,
			[FIN_PER] ,
			[PERIOD_DAYS_PJ] ,
			[STUDENT_RATE] ,
			[FTE] ,
			[STUDENT_TYPE] ,
			[rownum]
			 from @restbl
			
		insert into T1_CSV_ROW_COUNT
		select @prjid,
				 'STU_PRD',
				 @@ROWCOUNT

	DELETE FROM
	T1_CSV_FILE_TOTALS
	WHERE
	pfact_id = @prjid
	AND src = 'STU_PRD'


			INSERT INTO T1_CSV_FILE_TOTALS
				 select 
				 pfact_id, 
				 'STU_PRD'  ,
				 SUM(MATCH_FUND) as MATCH_FUND,
				 SUM(CC_TOT_AMT) as CC_COST
				 	from 
						@restbl
						group by
						pfact_id

end


