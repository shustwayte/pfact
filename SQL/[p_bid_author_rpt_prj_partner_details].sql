USE [pfact_t1]
GO
/****** Object:  StoredProcedure [dbo].[p_bid_author_rpt_prj_partner_details]    Script Date: 12/09/2018 15:25:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



ALTER PROC [dbo].[p_bid_author_rpt_prj_partner_details]
(
@prj_id int,
@name varchar(300)
)
as
			--		p_bid_author_rpt_prj_partner_details 10489, 'Bath'
begin

		declare @res table
		(
		costtype varchar(300),
		cost float,
		income float
		)

		insert into @res
		select 
			--name, 
			costtype,
				Cost, 
				income
				FROM [pfact_t1].[dbo].[vw_UOE_P_PRJ_Budget_Table_costdetail_uoe_RPT] (nolock) 
				where ProjectID = @prj_id
				and name = @name


				select 
				costtype,
				ISNULL(cost,0) as cost,
				ISNULL(income,0) as income
				
				 from @res
				union all 

				select 'Total',
				SUM(ISNULL(cost,0)),
				SUM(ISNULL(income,0))
				 from @res

end

