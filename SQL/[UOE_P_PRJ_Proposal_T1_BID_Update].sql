USE [pfact_t1]
GO
/****** Object:  StoredProcedure [dbo].[UOE_P_PRJ_Proposal_T1_BID_Update]    Script Date: 12/09/2018 15:29:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

 ALTER PROC [dbo].[UOE_P_PRJ_Proposal_T1_BID_Update]
 (
 @pfactid int,
 @t1bidnum varchar(100)
 )

 as

 begin


	if ( select count(*) from UOE_PFACT_T1_BID_TBL where pfactid = @pfactid) = 0
	begin

		insert into UOE_PFACT_T1_BID_TBL
		select @pfactid, @t1bidnum

	end

	if ( select count(*) from UOE_PFACT_T1_BID_TBL where pfactid = @pfactid) <> 0
	begin

		update UOE_PFACT_T1_BID_TBL
		set t1bid = @t1bidnum
		where pfactid = @pfactid

	end

 end

