USE [pfact_t1]
GO
/****** Object:  StoredProcedure [dbo].[p_T1_CSV_FAC_PRD]    Script Date: 12/09/2018 15:28:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



ALTER PROC [dbo].[p_T1_CSV_FAC_PRD]
(
@prjid int,
@username varchar(100)
)
AS

begin

	---		 exec [p_T1_CSV_FAC_PRD] 10388, 'sh444'

	delete from T1_CSV_ROW_COUNT
	where prj_id = @prjid
	and which = 'FAC_PRD'


DECLARE @RESTBL TABLE (
	[BIDNUMBER] [varchar](9) NULL,
	[PFACT_ID] [int] NULL,
	[FACULTY] [varchar](30) NULL,
	[BUDC] [varchar](10) NULL,
	[COST_TYPE] [varchar](100) NULL,
	[WP] [varchar](10) NULL,
	[NOMINAL] [varchar](10) NULL,
	[BUD_HEADING] [varchar](110) NULL,
	[DATE_START] [datetime] NULL,
	[DATE_END] [datetime] NULL,
	[UNITS] [numeric](18, 2) NULL,
	[TOT_AMT] [decimal](20, 2) NULL,
	[MATCH_FUND] [decimal](20, 2) NULL,
	[TOT_CC] [decimal](20, 2) NULL,
	[FIN_YEAR] [int] NULL,
	[FIN_PER] [int] NULL,
	[PERIOD_DAYS_PJ] [int] NULL,
	[UOM] [varchar](50) NOT NULL,
	[FAC_RATE_AMT] [numeric](18, 2) NULL,
	[sponval] [float] NULL,
	[rownum] [bigint] NULL
) 
	/*
	select distinct rectype, ChargeType, descr
			 from UOE_T1_CSV_DATA A

		select *
			 from UOE_T1_CSV_DATA A
			 where 
			recType like  '%FACILITIES%'

			 */
			 INSERT INTO @RESTBL

			select DISTINCT 
			--- a.stafftypedesc,
			
			'BID' +

			RIGHT('00000000' +
			replace(
				b.t1bid 
				, 'BID',''),6)
				
				as BIDNUMBER,
			A.prj_id as PFACT_ID,

			q.Code as FACULTY,
			--a.descr as FACULTY,

			a.dept as BUDC,
			--A.recType as COST_TYPE,
			CASE
							when a.chargetype = 'Directly Allocated Costs' then 'DA'
							when a.chargetype = 'Directly Incurred Costs' then 'DI'
							when a.chargetype = 'Exceptions' then 'DI'
							when a.chargetype = 'Indirect Costs' then 'DI'
							ELSE
						a.chargetype END as COST_TYPE,

			A.workpackage as WP,

			q.catCode as NOMINAL,
			--q.catDesc as BUD_HEADING,
			'BID' +
						RIGHT('00000000' +
						replace(
							b.t1bid 
							, 'BID',''),6) + 

							CASE
							WHEN isnull(q.t1_bud_temp_heading,'') = '' then '_00_99'
							else '_' +q.t1_bud_temp_heading end as BUD_HEADING,

			a.stdate as DATE_START,
			a.eddate as DATE_END,

			Z.Units as UNITS,

			convert(decimal(20,2),ISNULL(val,0)) as TOT_AMT,

			convert(decimal(20,2),case
			when matchedfund = 'on' then ISNULL(val,0)
			else 0 end) as MATCH_FUND,

			convert(decimal(20,2),case
			when matchedfund = 'on' then 0
			else ( ISNULL(VAL,0) - ISNULL(
				SPONVAL
			
					/** ( case
						when finaladjust = 0 then 1
						else finaladjust / 100.00
						end )*/
			,0) ) end ) as TOT_CC,


			--point,
				   --year(dateadd(month, -3, a.point)) as FIN_YEAR,
				   case
							   when fin.finvalue <= 5 then year(a.point) +1
								else year(a.point) end as FIN_YEAR,
				  --MONTH(DATEADD(M, 3, a.point)) as FIN_PER,
				  fin.finvalue as FIN_PER,
				  a.daycalc as PERIOD_DAYS_PJ,

			Z.unitdesc as UOM,
			Z.Rate as FAC_RATE_AMT,
			sponval, 

						ROW_NUMBER() OVER (ORDER BY 
						a.descr
						, year(dateadd(month, -3, a.point))
						,MONTH(DATEADD(M, 3, a.point))
						
						)  as rownum

		
					--,A.*
			--		select * 
			 from UOE_T1_CSV_DATA A
			 inner join T1_Fisacl_Month fin
								on DATEPART(month,a.point) = fin.monthvalue

			left outer join 
						UOE_PFACT_T1_BID_TBL b
							on a.PRJ_ID = b.pfactid

					

				INNER JOIN (
				
								SELECT DISTINCT 
								PPOD.Project_Proposal,
								PPOD.ID AS ROWID,
								Rate,Units,LUP_Element_Categories.ID
								,LUP_Element_Categories.Description,                        
								 LUP_Sub_Element.Description as SubElementDesc
								 ,LUP_Sub_Element.Code, LUP_Unit_Type_Of_Equ_And_NonStaff.Description as unitdesc ,LUP_Unit_Type_Of_Equ_And_NonStaff.ID as Equ_And_NonStaffID,                         
								 LUP_Element_Categories.Sequence_No,
								 LUP_Sub_Element.Inflation_Factor_ID,PPOD.Element,RecategoriseToConsumable,Lup_Inflation_Factor.Effective_Date,                      
								 Project_Proposal.Code as prjCode,Project_Proposal.Project_Name,
								 PPOD.RateOnDate,Project_Proposal.Start_Date ProjectStart,                 
								 PPOD.Element As ActualElement,PPOD.ResearchStudent FROM Project_Proposal_Sub_ELEMENT_ID (NOLOCK)                       
								 LEFT JOIN Project_Proposal_Others_Details PPOD (NOLOCK)  ON             
								 PPOD.ID=Project_Proposal_Sub_ELEMENT_ID.Project_Proposal_Others_Details                          
								 LEFT JOIN CostEquipNonStaff  (NOLOCK)  ON PPOD.ID=ProjPropOtherDetails                        
								 INNER  JOIN LUP_Sub_Element (NOLOCK)  ON Project_Proposal_Sub_ELEMENT_ID.Sub_Element_ID = LUP_Sub_Element.ID                         
								 INNER JOIN LUP_Element_Categories  (NOLOCK) ON LUP_Sub_Element.Category = LUP_Element_Categories.ID                         
								 LEFT JOIN LUP_FIX_ICT_CLASS (NOLOCK)  ON LUP_Element_Categories.ChargeType = LUP_FIX_ICT_CLASS.ICT_Class_ID                         
								 LEFT JOIN LUP_FIX_ICT_CLASS  subElementChargeType(NOLOCK) ON LUP_Sub_Element.Charge_Type = subElementChargeType.ICT_Class_ID
								 INNER JOIN SubElement_UnitType (NOLOCK)  ON SubElement_UnitTYpe.SubElement_ID=LUP_Sub_Element.ID                         
								 INNER JOIN LUP_Unit_Type_Of_Equ_And_NonStaff  (NOLOCK) ON SubElement_UnitType.Unit_Type= LUP_Unit_Type_Of_Equ_And_NonStaff.ID                        
								 INNER JOIN Lup_Inflation_Factor  (NOLOCK) ON LUP_Sub_Element.Inflation_Factor_ID=Lup_Inflation_Factor.ID                        
								 INNER JOIN Project_Proposal (NOLOCK)  ON PPOD.Project_Proposal=Project_Proposal.ID                        
								 WHERE PPOD.Project_Proposal= @prjid     
									--AND PPOD.ID = 50507  
							) Z
									ON Z.Project_Proposal = @prjid
									AND Z.ROWID = A.ROWID


								inner join 
									--	Select * from
										T1_Other_Data q
										on Q.rowid = A.ROWID
											and q.prj_id =@prjid

			where 
			--A.recType like  '%FACILITIES%'
			q.catcode IN ( 12130, 19005 )
			/*
			a.stafftypedesc not in (
									'Estate Lab' ,
									'Estate Desk',
									'Indirect',
									'Infrastructure Lab',
									'Infrastructure Desk'
									)
									*/
					and a.prj_id = @prjid

			order by 
			--a.descr
			q.Code
			, case
							   when fin.finvalue <= 5 then year(a.point) +1
								else year(a.point) end 
			,fin.finvalue


			SELECT 
			[BIDNUMBER] ,
			[PFACT_ID] ,
			[FACULTY] ,
			[BUDC] ,
			[COST_TYPE] ,
			[WP] ,
			[NOMINAL] ,
			[BUD_HEADING] ,
			[DATE_START] ,
			[DATE_END] ,
			[UNITS] ,
			[TOT_AMT] ,
			[MATCH_FUND] ,
			case
			when [TOT_CC] between -.050 and 0.50 then 0.00
			else [TOT_CC] end as [TOT_CC],
			[FIN_YEAR] ,
			[FIN_PER] ,
			[PERIOD_DAYS_PJ] ,
			[UOM] ,
			[FAC_RATE_AMT] ,
			[sponval] ,
			[rownum]
			
			 FROM @RESTBL
			
		insert into T1_CSV_ROW_COUNT
		select @prjid,
				 'FAC_PRD',
				 @@ROWCOUNT


	DELETE FROM
	T1_CSV_FILE_TOTALS
	WHERE
	pfact_id = @prjid
	AND src = 'FAC_PRD'


			INSERT INTO T1_CSV_FILE_TOTALS
				 select 
				 pfact_id, 
				 'FAC_PRD'  ,
				 SUM(MATCH_FUND) as MATCH_FUND,
				 SUM(TOT_CC) as CC_COST
				 	from 
						@restbl
						group by
						pfact_id
end


