USE [pfact_t1]
GO
/****** Object:  StoredProcedure [dbo].[p_T1_BIDNUMBER_GET]    Script Date: 12/09/2018 15:27:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



ALTER PROC [dbo].[p_T1_BIDNUMBER_GET]
(
@prjid int
)
AS
		--		EXEC [dbo].[p_T1_BIDNUMBER_GET] 10388
begin

	SELECT *, 
	'BID' +

						RIGHT('00000000' +
						replace(
							t1bid 
							, 'BID',''),6)
				
							as BIDNUMBER
	 FROM 
	UOE_PFACT_T1_BID_TBL 
		WHERE pfactid = @prjid
end


