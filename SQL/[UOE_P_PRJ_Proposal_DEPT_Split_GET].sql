USE [pfact_t1]
GO
/****** Object:  StoredProcedure [dbo].[UOE_P_PRJ_Proposal_DEPT_Split_GET]    Script Date: 12/09/2018 15:29:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



ALTER PROCedure [dbo].[UOE_P_PRJ_Proposal_DEPT_Split_GET]
(
@prj_id int,
@type varchar(10) 
)

as
/*

	[dbo].[UOE_P_PRJ_Proposal_DEPT_Split_GET] 10555 , 'prjyr'

		[dbo].[UOE_P_PRJ_Proposal_DEPT_Split_GET] 10388 , 'finyr'

	NIGHTMARE!!!
	
	PROJECT YEAR SPLIT
	
	If the budget line is for a staff memeber then pfact calculates by days in year.
	i.e. days / 365
	but less than 365 if less than that amount of days left on budget line.
	
	Project Year 1
	Basic Salary = £2,823 + (£35,089/365*334) = £34,931.84
	NI                   = £    296 + (£  3,717/365*334) =  £ 3,697.31
	Super            = £    508 + (£  6,316/365*334) = £  6,287.57

	Total = £44,916.85 = £44,917 rounded
	
	It then performs the same for Project Year 2.  
	
	Leap years are NOT taken into consideration so month = 2 is set to 28 days
	
	view id from results
	then check @temp calc for that id.
	runningtot determines if days left if >=365 if so use 365 else use amount of days left * by budget cost
	sum up those values from @temp table 
	
	
	21/09/2015	Directly Incurred Costs
	New process to account for inflation years.


*/

	
--	exec [dbo].[UOE_P_PRJ_Proposal_DEPT_Split_GET] 2676, 'prjyr'

begin

/*
declare @prj_id int, @type varchar(10)
set @prj_id = @prj_id
set @type = 'prjyr'
*/





	DELETE FROM UOE_T1_CSV_DATA
		WHERE PRJ_ID = @prj_id


		
----------------------- project profile start
	declare @mthstart_prj int

	declare @temp_prj table (
				id int identity(1,1),
				point datetime,
				acad_marker int,
				MONTHS INT,
				yr int,
				val float,
				calc float,
				prjyr_marker int
				)
				
				declare @st_prj datetime
				set @st_prj = ( SELECT 
						  [Start_Date]
							  FROM [dbo].[Project_Proposal]
									where id = @prj_id
									)
									
				set @mthstart_prj = 8--MONTH(@st)		
				
				declare @prjyr_marker_prj int
				
				if DATEPART(MONTH,@st_prj) = 8
				begin
					set @prjyr_marker_prj = 0
				end
				
				if DATEPART(MONTH,@st_prj) <> 8
				begin
					set @prjyr_marker_prj = 1
				end		
				
				
		
				declare @ed_prj datetime
				set @ed_prj = ( SELECT 
							  [End_Date]
						  FROM [dbo].[Project_Proposal]
						  where id = @prj_id
									)	
									
	--	select @st_prj, @ed_prj									
									
				update @temp_prj
				set val = 0,
				calc = 0
				
				declare @i_prj int
				set @i_prj = 0
				
			

				declare @mth_start_prj int
				set @mth_start_prj = ( select MONTH(@st_prj)	)


				declare @month_between_prj int
				set @month_between_prj = ( 
										SELECT 
										DBO.UOE_MonthdIFF(@st_prj, @ed_prj)+1
										)
										
				--select @month_between
							
				declare @yr_prj int
				set @yr_prj = 1	

				declare @yr_marker_prj int
				set @yr_marker_prj = 0			

				declare @mths_prj int
				set @mths_prj = 12

				declare @mths_chk_prj int
				set @mths_chk_prj = 12
									
				while @i_prj <= @month_between_prj
				begin
					-- if august then year + 1
					set @yr_prj = ( 
						select 
						case 
						when month(dateadd(m,@i_prj, @st_prj)) = 8 then @yr_prj +1 
						else @yr_prj end
						)
						
					-- if year point reached, check how many months left	
					if @i_prj in (12, 24, 36, 48, 60, 72, 84, 96, 108, 120, 132, 144 )  
						begin
						
							if @month_between_prj - @i_prj < 12
							begin
							set @mths_prj = @month_between_prj - @i_prj
								if @mths_prj = 0
								begin
									set @mths_prj = 1 
								end
							end
						end
						
					if MONTH(dateadd(m,@i_prj, @st_prj)) = @mth_start_prj
					begin
						set @yr_marker_prj = @yr_marker_prj + 1
					end
						
				if MONTH( dateadd(m,@i_prj, @st_prj) ) = 8--@mthstart
				begin
				set @prjyr_marker_prj = @prjyr_marker_prj + 1
				end
						
				insert into @temp_prj ( point, acad_marker, MONTHS, yr, prjyr_marker)
					select 
						dateadd(m,@i_prj, @st_prj),
						@yr_prj,
						--@ACAD_YEAR_prj,
						@mths_prj,
						@yr_marker_prj,
						@prjyr_marker_prj
				
					set @i_prj = @i_prj + 1
					
				end						


				delete from @temp_prj
				where id > @month_between_prj
				
				-- ensure date profile doesn't exceed end date for project
				delete from @temp_prj
				where point > @ed_prj
				
------------------ project profile end		
-- profile map for project
--	 select * from @temp_prj
	

declare @resprof table
(
	recid int identity(1,1),
	[id] [int] ,
	[PRJ_ID] [int] NULL,
	[dept] [varchar](10) NULL,
	[Department] [varchar](100) NULL,
	[ChargeType] [varchar](100) NULL,
	[descr] [varchar](900) NULL,
	[StaffNumber] [varchar](20) NULL,
	[acc_code] [varchar](10)  NULL,
	[StaffTypeDesc] [varchar](250) NULL,
	[1] [float] NULL,
	[2] [float] NULL,
	[3] [float] NULL,
	[4] [float] NULL,
	[5] [float] NULL,
	[6] [float] NULL,
	[7] [float] NULL,
	[8] [float] NULL,
	[9] [float] NULL,
	[10] [float] NULL,
	[11] [float] NULL,
	[12] [float] NULL,
	[13] [float] NULL,
	[14] [float] NULL,
	[15] [float] NULL,
	[total] [float]  NULL,
	[stdate] [datetime] NULL,
	[eddate] [datetime] NULL,
	[ROWID] [int] NULL,
	[SponsorCONT] [float]  NULL,
	[hoursworked] [float]  NULL,
	[PFACT_PAC_Mapping] [nvarchar](300) NULL,
	[new_PACcat] [nvarchar](300) NULL,
	[flat_line] [varchar](10) NULL,
	CANFLAT INT,
	workpackage varchar(10),
	dependdate varchar(20),

				basic1 float,
				basic2 float,
				basic3 float,
				basic4 float,
				basic5 float,
				basic6 float,
				basic7 float,
				basic8 float,
				basic9 float,
				basic10 float,
				basic11 float,
				basic12 float,
				basic13 float,
				basic14 float,
				basic15 float,
				basicTotal float,


				ni1 float,
				ni2 float,
				ni3 float,
				ni4 float,
				ni5 float,
				ni6 float,
				ni7 float,
				ni8 float,
				ni9 float,
				ni10 float,
				ni11 float,
				ni12 float,
				ni13 float,
				ni14 float,
				ni15 float,
				niTotal float,

				super1 float,
				super2 float,
				super3 float,
				super4 float,
				super5 float,
				super6 float,
				super7 float,
				super8 float,
				super9 float,
				super10 float,
				super11 float,
				super12 float,
				super13 float,
				super14 float,
				super15 float,
				superTotal float,


				Spon1 float,
				Spon2 float,
				Spon3 float,
				Spon4 float,
				Spon5 float,
				Spon6 float,
				Spon7 float,
				Spon8 float,
				Spon9 float,
				Spon10 float,
				Spon11 float,
				Spon12 float,
				Spon13 float,
				Spon14 float,
				Spon15 float,
				SponTotal float,

				recType varchar(50),

				matchedfund varchar(10),
				finaladjust float,
				FTE float,
				Other_Allowances decimal(20,2),

				calcFundPerc float

	)

	insert into @resprof
		
select 
		[id]
		  ,[PRJ_ID]
		  ,[dept]
		  ,[Department]
		  ,[ChargeType]
		  ,a.[descr]
		  ,[StaffNumber]
		  ,[acc_code]
		  ,a.[StaffTypeDesc]
		  ,[1]
		  ,[2]
		  ,[3]
		  ,[4]
		  ,[5]
		  ,[6]
		  ,[7]
		  ,[8]
		  ,[9]
		  ,[10]
		  ,[11]
		  ,[12]
		  ,[13]
		  ,[14]
		  ,[15]
		  ,isnull([TOTAL],0) as [total]
		  ,[stdate]
		  ,[eddate]
		  ,a.ROWID
		  ,isnull(SponsorCONT,0) as SponsorCONT
			,isnull(hoursworked,0) as hoursworked
			,PFACT_PAC_Mapping
			,new_PACcat
			,flat_line
			,CANFLAT
			,workpackage
			,dependdate

				,
						basic1 ,
						basic2 ,
						basic3 ,
						basic4 ,
						basic5 ,
						basic6 ,
						basic7 ,
						basic8 ,
						basic9 ,
						basic10 ,
						basic11 ,
						basic12 ,
						basic13 ,
						basic14 ,
						basic15 ,
						basicTotal ,


						ni1 ,
						ni2 ,
						ni3 ,
						ni4 ,
						ni5 ,
						ni6 ,
						ni7 ,
						ni8 ,
						ni9 ,
						ni10 ,
						ni11 ,
						ni12 ,
						ni13 ,
						ni14 ,
						ni15 ,
						niTotal ,

						super1 ,
						super2 ,
						super3 ,
						super4 ,
						super5 ,
						super6 ,
						super7 ,
						super8 ,
						super9 ,
						super10 ,
						super11 ,
						super12 ,
						super13 ,
						super14 ,
						super15 ,
						superTotal ,

						case when matchedfund = 'on' then 0 else Spon1 end,
						case when matchedfund = 'on' then 0 else Spon2 end,
						case when matchedfund = 'on' then 0 else Spon3 end,
						case when matchedfund = 'on' then 0 else Spon4 end,
						case when matchedfund = 'on' then 0 else Spon5 end,
						case when matchedfund = 'on' then 0 else Spon6 end,
						case when matchedfund = 'on' then 0 else Spon7 end,
						case when matchedfund = 'on' then 0 else Spon8 end,
						case when matchedfund = 'on' then 0 else Spon9 end,
						case when matchedfund = 'on' then 0 else Spon10 end,
						case when matchedfund = 'on' then 0 else Spon11 end,
						case when matchedfund = 'on' then 0 else Spon12 end,
						case when matchedfund = 'on' then 0 else Spon13 end,
						case when matchedfund = 'on' then 0 else Spon14 end,
						case when matchedfund = 'on' then 0 else Spon15 end,
						case when matchedfund = 'on' then 0 else SponTotal end,
						 
						recType ,
						matchedfund,
						finaladjust,
						FTE, b.Other_Allowances, 

						0.00
--		select *
		 from 
		 [dbo].[UOE_P_PFACT_Prj_Dept_Split] a
			left outer join 
			(
					select 
					StaffTypeDesc, descr, ROWID,
					convert(decimal(20,2),b.Other_Allowances) as Other_Allowances
					 from [dbo].[UOE_P_PFACT_Prj_Dept_Split] a
							left outer join (
										SELECT 
										CoinID, sum(cs.Other_Allowances) as Other_Allowances
											FROM dbo.CostStaff cs     
											INNER JOIN dbo.Project_Proposal_Co_Investigators ppci ON cs.CoinID = ppci.id     
											WHERE ppci.Project_Proposal  = @prj_id AND  cs.IsSpine=1 AND cs.UseInflation=1
											group by CoinID
											) b
											on a.ROWID = b.COinID and  StaffTypeDesc not in (
																		'Estate Lab',
																		'Indirect',
																		'Infrastructure Lab' )
						where prj_id  = @prj_id
						) b on a.ROWID = b.ROWID
							and a.StaffTypeDesc = b.StaffTypeDesc

		where prj_id  = @prj_id
	--	order by chargetype, descr
		order by id


		update @resprof
		set calcFundPerc = 
			case
			when matchedfund = 'on' then 0.00
			when isnull(finaladjust,'99999999999999') <> '99999999999999' then finaladjust
			when finaladjust = '0' then 0.00
			else 
				(SponsorCONT / SponTotal ) * 100 
				end
		
	/*
	delete FROM @resprof
	where descr <> 'PDRA 2 Desk'
	*/
	--	SELECT 'From pfact', * FROM @resprof



	
		------		RE PROFILE SPONSOR FOR STAFF - START
		declare @reprofilespontab table
		(
		id int identity(1,1),
		rowid int,
		recid int
		)

		insert into @reprofilespontab
		select rowid, recid from @resprof
			where recType = 'staff'
			and ChargeType <> 'Exceptions'
			and isnull([1],0) = 0
			
			--	select * from @reprofilespontab

			declare @sponreprofile int
			set @sponreprofile = 1

			declare @sponreprofileMAX int
			set @sponreprofileMAX = ( select MAX(id) from @reprofilespontab )
			
			declare @rowidtosponreprofile int 

			while @sponreprofile <= @sponreprofileMAX
			begin


				set @rowidtosponreprofile = ( select recid from @reprofilespontab where id = @sponreprofile )

				IF ( select [1] from @resprof where recid = @rowidtosponreprofile ) = 0
				begin

					if ( select [2] from @resprof where recid = @rowidtosponreprofile ) <> 0
					begin
						--	only move spon value up one year place
						update @resprof
						set spon1 =0,
							Spon2 = Spon1,
							Spon3 = Spon2,
							Spon4 = Spon3,
							Spon5 = Spon4,
							Spon6 = Spon5,
							Spon7 = Spon6,
							Spon8 = Spon7,
							Spon9 = Spon8,
							Spon10 = Spon9,
							Spon11 = Spon10,
							Spon12 = Spon11,
							Spon13 = Spon12,
							Spon14 = Spon13,
							Spon15 = Spon14
							where  recid = @rowidtosponreprofile

						--select * from @resprof where recid = @rowidtosponreprofile	

					end

							if 
								( select [2] from @resprof where recid = @rowidtosponreprofile ) = 0 AND
									( select [3] from @resprof where recid = @rowidtosponreprofile ) <> 0
							begin
								--	only move spon value up one year place
								update @resprof
								set spon1 =0,
									Spon2 = 0,
									Spon3 = Spon1,
									Spon4 = Spon2,
									Spon5 = Spon3,
									Spon6 = Spon4,
									Spon7 = Spon5,
									Spon8 = Spon6,
									Spon9 = Spon7,
									Spon10 = Spon8,
									Spon11 = Spon9,
									Spon12 = Spon10,
									Spon13 = Spon11,
									Spon14 = Spon12,
									Spon15 = Spon13
									where  recid = @rowidtosponreprofile

								--select * from @resprof where recid = @rowidtosponreprofile	

							end


									if 
										( select [2] from @resprof where recid = @rowidtosponreprofile ) = 0 AND
											( select [3] from @resprof where recid = @rowidtosponreprofile ) = 0 AND 
												( select [4] from @resprof where recid = @rowidtosponreprofile ) <> 0 
									begin
										--	only move spon value up one year place
										update @resprof
										set spon1 =0,
											Spon2 = 0,
											Spon3 = 0,
											Spon4 = Spon1,
											Spon5 = Spon2,
											Spon6 = Spon3,
											Spon7 = Spon4,
											Spon8 = Spon5,
											Spon9 = Spon6,
											Spon10 = Spon7,
											Spon11 = Spon8,
											Spon12 = Spon9,
											Spon13 = Spon10,
											Spon14 = Spon11,
											Spon15 = Spon12
											where  recid = @rowidtosponreprofile

										--select * from @resprof where recid = @rowidtosponreprofile	

									end

				end


				set @sponreprofile = @sponreprofile + 1
			end
			
					
	
		------		RE PROFILE SPONSOR FOR STAFF - END		

	
	



	

	
/*
	delete from @resprof
	where id <> 1562
*/
	-- select * from @resprof
	
		DECLARE @START_I INT
		SET @START_I = 1

		DECLARE @END_I INT
		SET @END_I = ( SELECT MAX(recid) FROM @resprof ) 


		
		declare @mthstart int		
		
		-- should profiling be done
		declare @chk int
		set @chk = 0
		
		WHILE @START_I <= @END_I

		BEGIN
		

		
----------------		Exceptions START

		--	select 'ChargeType', ChargeType from @resprof

		if ( select ChargeType from @resprof WHERE recid = @START_I) = 'Exceptions'
		begin



		--	select 'here check22222222', * from @resprof

		declare @rowidEXCEPTION int
		set @rowidEXCEPTION = ( SELECT ROWID FROM @resprof WHERE recid = @START_I)

		declare @staff_cost_EXCEPTIONS table 
			(
			RECID INT IDENTITY(1,1),
			id int,
			[Spell] [int] NOT NULL,
			[StartDate] [datetime] NOT NULL,
			[EndDate] [datetime] NOT NULL,
			[Reason] [varchar](100) NULL,
			[Spine] [int] NULL,
			[Basic] [float] NOT NULL,
			[NI] [float] NOT NULL,
			[NIOUT] [float] NOT NULL,
			[Super] [float] NOT NULL,
			[Total] [float] NOT NULL,
			[CoinID] [int] NOT NULL,
			[StaffType] [int] NOT NULL,
			[Other_Allowances] [float] NULL,
			[PayScale] [int] NULL,
			[PayScaleDesc] [varchar](80) NULL,
			[SpineAmount] [decimal](22, 2) NULL
			)

			DELETE FROM @staff_cost_EXCEPTIONS

			insert into @staff_cost_EXCEPTIONS
			(
			[Spell] ,
			[StartDate] ,
			[EndDate] ,
			[Reason] ,
			[Spine] ,
			[Basic] ,
			[NI] ,
			[NIOUT] ,
			[Super] ,
			[Total] ,
			[CoinID] ,
			[StaffType] ,
			[Other_Allowances] ,
			[PayScale] ,
			[PayScaleDesc] ,
			[SpineAmount]
			)
			
			EXEC StaffCostFromTable 0,1,1, @rowidEXCEPTION,  6


			DELETE FROM @staff_cost_EXCEPTIONS
			WHERE RECID > ( Select MIN(RECID) FROM @staff_cost_EXCEPTIONS WHERE Reason = 'End of Spell' )
		
			--	SELECT '@staff_cost_EXCEPTIONS', * FROM @staff_cost_EXCEPTIONS

			-- check if same
			if( select SUM(SpineAmount) from @staff_cost_EXCEPTIONS ) = 
					(
					 select max(SponTotal)
					 * max(( case
									--when finaladjust = 0   then 0
									when finaladjust IS null  then 100 
									when finaladjust = ''  then 100 
									else 
									finaladjust end / 100
									 ))
					 
					  from @resprof A	
						INNER JOIN @staff_cost_EXCEPTIONS
						 B ON A.ROWID = B.CoinID
						 )
			begin

							UPDATE  @resprof
							SET Spon1 = NULL,
							Spon2 = NULL,
							Spon3 = NULL,
							Spon4 = NULL,
							Spon5 = NULL,
							Spon6 = NULL,
							Spon7 = NULL,
							Spon8 = NULL,
							Spon9 = NULL,
							Spon10 = NULL,
							Spon11 = NULL,
							Spon12 = NULL,
							Spon13 = NULL,
							Spon14 = NULL,
							Spon15 = NULL
								 WHERE recid = @START_I

						UPDATE @resprof
						SET Spon1 = CASE WHEN B.RECID = 1 THEN B.SpineAmount ELSE 0 END
						FROM 
						@resprof A	
							INNER JOIN @staff_cost_EXCEPTIONS
							 B ON A.ROWID = B.CoinID
							 WHERE B.RECID = 1

						UPDATE @resprof
						SET Spon2 = CASE WHEN B.RECID = 2 THEN B.SpineAmount ELSE 0 END
						FROM 
						@resprof A	
							INNER JOIN @staff_cost_EXCEPTIONS
							 B ON A.ROWID = B.CoinID
							 WHERE B.RECID = 2


						UPDATE @resprof
						SET Spon3 = CASE WHEN B.RECID = 3 THEN B.SpineAmount ELSE 0 END
						FROM 
						@resprof A	
							INNER JOIN @staff_cost_EXCEPTIONS
							 B ON A.ROWID = B.CoinID
							 WHERE B.RECID = 3

						UPDATE @resprof
						SET Spon4 = CASE WHEN B.RECID = 4 THEN B.SpineAmount ELSE 0 END
						FROM 
						@resprof A	
							INNER JOIN @staff_cost_EXCEPTIONS
							 B ON A.ROWID = B.CoinID
							 WHERE B.RECID = 4

						UPDATE @resprof
						SET Spon5 = CASE WHEN B.RECID = 5 THEN B.SpineAmount ELSE 0 END
						FROM 
						@resprof A	
							INNER JOIN @staff_cost_EXCEPTIONS
							 B ON A.ROWID = B.CoinID
							 WHERE B.RECID = 5

						UPDATE @resprof
						SET Spon6 = CASE WHEN B.RECID = 6 THEN B.SpineAmount ELSE 0 END
						FROM 
						@resprof A	
							INNER JOIN @staff_cost_EXCEPTIONS
							 B ON A.ROWID = B.CoinID
							 WHERE B.RECID = 6

						UPDATE @resprof
						SET Spon7 = CASE WHEN B.RECID = 7 THEN B.SpineAmount ELSE 0 END
						FROM 
						@resprof A	
							INNER JOIN @staff_cost_EXCEPTIONS
							 B ON A.ROWID = B.CoinID
							 WHERE B.RECID = 7

						UPDATE @resprof
						SET Spon8 = CASE WHEN B.RECID = 8 THEN B.SpineAmount ELSE 0 END
						FROM 
						@resprof A	
							INNER JOIN @staff_cost_EXCEPTIONS
							 B ON A.ROWID = B.CoinID
							 WHERE B.RECID = 8

						UPDATE @resprof
						SET Spon9 = CASE WHEN B.RECID = 9 THEN B.SpineAmount ELSE 0 END
						FROM 
						@resprof A	
							INNER JOIN @staff_cost_EXCEPTIONS
							 B ON A.ROWID = B.CoinID
							 WHERE B.RECID = 9

						UPDATE @resprof
						SET Spon10 = CASE WHEN B.RECID = 10 THEN B.SpineAmount ELSE 0 END
						FROM 
						@resprof A	
							INNER JOIN @staff_cost_EXCEPTIONS
							 B ON A.ROWID = B.CoinID
							 WHERE B.RECID = 10

						UPDATE @resprof
						SET Spon11 = CASE WHEN B.RECID = 11 THEN B.SpineAmount ELSE 0 END
						FROM 
						@resprof A	
							INNER JOIN @staff_cost_EXCEPTIONS
							 B ON A.ROWID = B.CoinID
							 WHERE B.RECID = 11

						UPDATE @resprof
						SET Spon12 = CASE WHEN B.RECID = 12 THEN B.SpineAmount ELSE 0 END
						FROM 
						@resprof A	
							INNER JOIN @staff_cost_EXCEPTIONS
							 B ON A.ROWID = B.CoinID
							 WHERE B.RECID = 12

						UPDATE @resprof
						SET Spon13 = CASE WHEN B.RECID = 13 THEN B.SpineAmount ELSE 0 END
						FROM 
						@resprof A	
							INNER JOIN @staff_cost_EXCEPTIONS
							 B ON A.ROWID = B.CoinID
							 WHERE B.RECID = 13

						UPDATE @resprof
						SET Spon14 = CASE WHEN B.RECID = 14 THEN B.SpineAmount ELSE 0 END
						FROM 
						@resprof A	
							INNER JOIN @staff_cost_EXCEPTIONS
							 B ON A.ROWID = B.CoinID
							 WHERE B.RECID = 14

						UPDATE @resprof
						SET Spon15 = CASE WHEN B.RECID = 15 THEN B.SpineAmount ELSE 0 END
						FROM 
						@resprof A	
							INNER JOIN @staff_cost_EXCEPTIONS
							 B ON A.ROWID = B.CoinID
							 WHERE B.RECID = 15
			end

						/*
						SELECT 'EXCEPT', *
								FROM @resprof 
								*/
				
		
		
		
		
		end

----------------		Exceptions END	


			if @type = 'finyr'
				and 
					( SELECT [2] FROM @resprof
								WHERE recid = @START_I ) is null
			begin
				set @chk = 1
			end
			
			
			if @type = 'prjyr'
			begin
				set @chk = 1
			end
			
			--select 'run profile', @start_i , @chk
		
				IF @chk = 1
						--and @type = 'PRJYR'
				BEGIN
					
			
					--set @prjyr_marker = 1
							
			-- RESULTS TABLE
							
							declare @st datetime
							set @st = ( select stdate
												FROM  
												@resprof 
												WHERE recid = @START_I
												and prj_id = @prj_id
												)
											
							set @mthstart = 8--MONTH(@st)	
							
							declare @prjyr_marker int
							
							if DATEPART(MONTH,@st) = 8
							begin
								set @prjyr_marker = 0
							end
							
							if DATEPART(MONTH,@st) <> 8
							begin
								set @prjyr_marker = 1
							end					
							
							declare @ed datetime
							set @ed = ( select eddate
												FROM  
												@resprof 
												WHERE recid = @START_I
												and prj_id = @prj_id
												)	
							
							declare @mth_start int
							set @mth_start = ( select MONTH(@st)	)


							declare @month_between int
							set @month_between = ( 
													SELECT 
													DBO.UOE_MonthdIFF(@st, @ed)+1
													)
													
							--select @month_between

							declare @yr int
							set @yr = 1	

							declare @yr_marker int
							set @yr_marker = 0			

							declare @mths int
							set @mths = 12


							declare @mths_chk int
							set @mths_chk = 12
							
							declare @temp table (
							id int identity(1,1),
							point datetime,
							acad_marker int,
							MONTHS INT,
							yr int,
							val float,
							calc float,
							prjyr_marker int,
							RECID INT,
							ROWID INT,
							daycount int,
							totdays int,
							runningtot int,
							dividedays int,
							value float,
							[Basicval] float,
							[NIval] float,
							[Superval] float,
							[BasicvalCalc] float,
							[NIvalCalc] float,
							[SupervalCalc] float,
							[Sponval] float,
							[SponvalCalc] float,
							FTE float,
							[FTEval] float,
							[FTEvalCalc] float,
							finaladjust float
							)
							
							update @temp
							set val = 0,
							calc = 0,
							[BasicvalCalc] = 0,
							[NIvalCalc] = 0,
							[SupervalCalc] = 0,
							[SponvalCalc] = 0,
							[FTEvalCalc] = 0,
							finaladjust = 0

								
							declare @i int
							set @i = 0
												
							while @i <= @month_between
							begin
							
								-- if august then year + 1
								set @yr = ( 
									select 
									case 
									when month(dateadd(m,@i, @st)) = 8 then @yr +1 
									else @yr end
									)
									
								-- if year point reached, check how many months left	
								if @i in (12, 24, 36, 48, 60, 72, 84, 96, 108, 120, 132, 144 )  
									begin
									
										if @month_between - @i < 12
										begin
										set @mths = @month_between - @i
											if @mths = 0
											begin
												set @mths = 1 
											end
										end
									end
									
								if MONTH(dateadd(m,@i, @st)) = @mth_start
								begin
									set @yr_marker = @yr_marker + 1
								end
									
							if MONTH( dateadd(m,@i, @st) ) = 8--@mthstart
							begin
							set @prjyr_marker = @prjyr_marker + 1
							end
									
							insert into @temp ( point, acad_marker, MONTHS, yr, prjyr_marker, RECID, ROWID)
								select
								 
									case 
									when @i = 0 then @st
									else
									--	dateadd(m,@i, @st) 
									DATEADD(mm, DATEDIFF(mm, 0, dateadd(m,@i, @st) ), 0)
									end,
								
								--	dateadd(m,@i, @st),
									@yr,
									@mths,
									@yr_marker,
									@prjyr_marker,
									@START_I,
									@i
							
								set @i = @i + 1
								
							end						
							
							DELETE FROM @temp
							WHERE RECID <> @START_I
							
							/*
							IF @START_I = 10
							BEGIN

							SELECT * FROM @temp
							END
							*/
							
							delete from @temp
							where ROWID > @month_between-1
							
							-- ensure date profile doesn't exceed end date for project
							delete from @temp
							where point > @ed_prj
					
					
							-- ensure date profile doesn't exceed end date for RECORD
							delete from @temp
							where point > @ed
							
							update @temp
							set point = @st
							where id = ( Select min(id) from @temp )

							update @temp
							set point = @ed
							where id = ( Select MAX(id) from @temp )

						/*
							if @START_I = 6
							begin
								SELECT 'date', * FROM @temp
							end
							*/
				
				-- update values according to pfact feed			
				update @temp
				set 
				finaladjust = ( select finaladjust from @resprof 
								where recid = @START_I )
				
					,
					val = ( select total from @resprof 
								where recid = @START_I )
							/
							( select COUNT(*) from @temp )
					,
					Basicval = ( select Basictotal from @resprof 
								where recid = @START_I )
							/
							( select COUNT(*) from @temp )

					,
					NIval = ( select NItotal from @resprof 
								where recid = @START_I )
							/
							( select COUNT(*) from @temp )

					,
					Superval = ( select Supertotal from @resprof 
								where recid = @START_I )
							/
							( select COUNT(*) from @temp )
					,
					FTEval = ( select FTE from @resprof 
								where recid = @START_I )
							/
							( select COUNT(*) from @temp )
					,
					FTE = ( select FTE from @resprof 
								where recid = @START_I )
					,
					Sponval = ( select SponTotal *
										( case
										--when finaladjust = 0   then 0
									when finaladjust IS null  then 100
									when finaladjust = ''  then 100
									 else 
									finaladjust end / 100
									 )
										 from @resprof 
								where recid = @START_I )
							/
							( select COUNT(*) from @temp )
					
						--realign profile of record against project profile
						update @temp
						set yr = a.yr,
						prjyr_marker = a.prjyr_marker
						--	select * 
						from @temp_prj a				
						left outer join @temp b
						on convert(varchar(6),a.point,112) = convert(varchar(6),b.point,112)

			
				------------	START
				declare @chkcalcDO int
				set @chkcalcDO = 0
				
				if @type = 'prjyr'
				begin
						if ( select canflat from @resprof
								where recid = @START_I ) = 1
							begin
								set @chkcalcDO = 1
							end
							
							
						if ( select ChargeType from @resprof
								where recid = @START_I ) = 'Directly Incurred Costs'
							begin
								set @chkcalcDO = 1
							end	

					/*
						if ( select StaffTypeDesc from @resprof
								where recid = @START_I ) in (
									  'Estate Lab',
										'Estate Desk',
										'Infrastructure Lab',
										'Infrastructure Desk',
										'Indirect' 
									 )

							begin
								set @chkcalcDO = 1
							end	
							*/
						
				end									
				/*
				
				if @type = 'finyr'
				begin	
						
						if ( select StaffTypeDesc from @resprof
								where recid = @START_I ) in (
									  'Estate Lab',
										'Estate Desk',
										'Infrastructure Lab',
										'Infrastructure Desk',
										'Indirect' 
									 )

							begin
								set @chkcalcDO = 1
							end		
						
				end	
				*/
				
				--	select @START_I, @chkcalcDO	
				
				
						if @type = 'prjyr'
						begin
							update @temp 
							set MONTHS = b.num
							from @temp a
								inner join 
								(
									select
									yr,
									COUNT(*) as num
									from 
									@temp
									group by yr
									) b 
										on a.yr = b.yr
							if ( select canflat from @resprof
									where recid = @START_I ) = 1
									
							begin	
								update @temp
								set MONTHS = b.mths
								from 
								@temp a
								inner join (			
										select COUNT(*) as mths, prjyr_marker
										from @temp
										group by prjyr_marker
										) b on a.prjyr_marker = b.prjyr_marker		
							end
										
						
						
			
										
							if @chkcalcDO = 1
									
							begin
				
				-- reprofile according to fy split			
				update @temp
				set val = b.[1],
				Basicval = b.[basic1],
				NIval = b.ni1,
				Superval = b.super1,
				Sponval = b.Spon1
				from @temp a
					inner join @resprof b
							on a.RECID = b.recid
				where a.prjyr_marker = 1
				
				update @temp
				set val = b.[2],
				Basicval = b.[basic2],
				NIval = b.ni2,
				Superval = b.super2,
				Sponval = b.Spon2
				from @temp a
					inner join @resprof b
							on a.RECID = b.recid
				where a.prjyr_marker = 2
				
				update @temp
				set val = b.[3],
				Basicval = b.[basic3],
				NIval = b.ni3,
				Superval = b.super3,
				Sponval = b.Spon3
				from @temp a
					inner join @resprof b
							on a.RECID = b.recid
				where a.prjyr_marker = 3
				
				update @temp
				set val = b.[4],
				Basicval = b.[basic4],
				NIval = b.ni4,
				Superval = b.super4,
				Sponval = b.Spon4
				from @temp a
					inner join @resprof b
							on a.RECID = b.recid
				where a.prjyr_marker = 4
				
				update @temp
				set val = b.[5],
				Basicval = b.[basic5],
				NIval = b.ni4,
				Superval = b.super5,
				Sponval = b.Spon5
				from @temp a
					inner join @resprof b
							on a.RECID = b.recid
				where a.prjyr_marker = 5
				
				update @temp
				set val = b.[6],
				Basicval = b.[basic6],
				NIval = b.ni6,
				Superval = b.super6,
				Sponval = b.Spon6
				from @temp a
					inner join @resprof b
							on a.RECID = b.recid
				where a.prjyr_marker = 6
				
				update @temp
				set val = b.[7],
				Basicval = b.[basic7],
				NIval = b.ni7,
				Superval = b.super7,
				Sponval = b.Spon7
				from @temp a
					inner join @resprof b
							on a.RECID = b.recid
				where a.prjyr_marker = 7
				
				update @temp
				set val = b.[8],
				Basicval = b.[basic8],
				NIval = b.ni8,
				Superval = b.super8,
				Sponval = b.Spon8
				from @temp a
					inner join @resprof b
							on a.RECID = b.recid
				where a.prjyr_marker = 8
				
				update @temp
				set val = b.[9],
				Basicval = b.[basic9],
				NIval = b.ni9,
				Superval = b.super9,
				Sponval = b.Spon9
				from @temp a
					inner join @resprof b
							on a.RECID = b.recid
				where a.prjyr_marker = 9
				
				update @temp
				set val = b.[10],
				Basicval = b.[basic10],
				NIval = b.ni10,
				Superval = b.super10,
				Sponval = b.Spon10
				from @temp a
					inner join @resprof b
							on a.RECID = b.recid
				where a.prjyr_marker = 10
				
				update @temp
				set val = b.[11],
				Basicval = b.[basic11],
				NIval = b.ni11,
				Superval = b.super11,
				Sponval = b.Spon11
				from @temp a
					inner join @resprof b
							on a.RECID = b.recid
				where a.prjyr_marker = 11
				
				update @temp
				set val = b.[12],
				Basicval = b.[basic12],
				NIval = b.ni12,
				Superval = b.super12,
				Sponval = b.Spon12
				from @temp a
					inner join @resprof b
							on a.RECID = b.recid
				where a.prjyr_marker = 12
				
				update @temp
				set val = b.[13],
				Basicval = b.[basic13],
				NIval = b.ni13,
				Superval = b.super13,
				Sponval = b.Spon13
				from @temp a
					inner join @resprof b
							on a.RECID = b.recid
				where a.prjyr_marker = 13
				
				update @temp
				set val = b.[14],
				Basicval = b.[basic14],
				NIval = b.ni14,
				Superval = b.super14,
				Sponval = b.Spon14
				from @temp a
					inner join @resprof b
							on a.RECID = b.recid
				where a.prjyr_marker = 14
				
				update @temp
				set val = b.[15],
				Basicval = b.[basic15],
				NIval = b.ni15,
				Superval = b.super15,
				Sponval = b.Spon15

				from @temp a
					inner join @resprof b
							on a.RECID = b.recid
				where a.prjyr_marker = 15
							
							
							end			
										
										
										
										
						end
						
						if @type = 'finyr'
						begin
							update @temp 
							set MONTHS = b.num
							from @temp a
								inner join 
								(
									select
									prjyr_marker,
									COUNT(*) as num
									from 
									@temp
									group by prjyr_marker
									) b 
										on a.prjyr_marker = b.prjyr_marker
						end
							 
					--select 'view profiled months', * from @temp
					
						
							 
			declare @tot float
			set @tot = ( select total from @resprof where recid = @START_I )

			IF ISNULL(@TOT,0.00) > 0.00
			BEGIN
												
					update @temp
					set 
						calc = MONTHS * val,
						BasicvalCalc = MONTHS * Basicval,
						NIvalCalc = MONTHS * NIval,
						SupervalCalc = MONTHS * Superval,
						SponvalCalc = MONTHS * Sponval
						/*,
						FTEvalCalc = MONTHS * FTEval
						*/
							
					if @type = 'finyr'
					begin		
							
							update @resprof
							set 
							[1] = tot.[1],
							[2] = tot.[2],
							[3] = tot.[3],
							[4] = tot.[4],
							[5] = tot.[5],
							[6] = tot.[6],
							[7] = tot.[7],
							[8] = tot.[8],
							[9] = tot.[9],
							[10] = tot.[10],
							[11] = tot.[11],
							[12] = tot.[12],
							[13] = tot.[13],
							[14] = tot.[14],
							[15] = tot.[15]
							from
							@resprof b
							inner join (
														 
									select
									id, 
									MAX([1]) as [1],
									MAX([2]) as [2],
									MAX([3]) as [3],
									MAX([4]) as [4],
									MAX([5]) as [5],
									MAX([6]) as [6],
									MAX([7]) as [7],
									MAX([8]) as [8],
									MAX([9]) as [9],
									MAX([10]) as [10],
									MAX([11]) as [11],
									MAX([12]) as [12],
									MAX([13]) as [13],
									MAX([14]) as [14],
									MAX([15]) as [15]
									from ( 
									 select 
									@START_I as id,
									case when prjyr_marker = 1 then MAX(calc) else 0 end as [1],
									case when prjyr_marker = 2 then MAX(calc) else 0 end as [2],
									case when prjyr_marker = 3 then MAX(calc) else 0 end as [3],
									case when prjyr_marker = 4 then MAX(calc) else 0 end as [4],
									case when prjyr_marker = 5 then MAX(calc) else 0 end as [5],
									case when prjyr_marker = 6 then MAX(calc) else 0 end as [6],
									case when prjyr_marker = 7 then MAX(calc) else 0 end as [7],
									case when prjyr_marker = 8 then MAX(calc) else 0 end as [8],
									case when prjyr_marker = 9 then MAX(calc) else 0 end as [9],
									case when prjyr_marker = 10 then MAX(calc) else 0 end as [10],
									case when prjyr_marker = 11 then MAX(calc) else 0 end as [11],
									case when prjyr_marker = 12 then MAX(calc) else 0 end as [12],
									case when prjyr_marker = 13 then MAX(calc) else 0 end as [13],
									case when prjyr_marker = 14 then MAX(calc) else 0 end as [14],
									case when prjyr_marker = 15 then MAX(calc) else 0 end as [15],
									prjyr_marker -- use yr for project year	
									 from @temp					
									 group by prjyr_marker -- use yr for project year	
									 ) z
									 group by id
											 ) tot
											 on b.recid = tot.id


							---	BASIC

							update @resprof
							set 
							
							[Basic1] = tot.[1],
							[Basic2] = tot.[2],
							[Basic3] = tot.[3],
							[Basic4] = tot.[4],
							[Basic5] = tot.[5],
							[Basic6] = tot.[6],
							[Basic7] = tot.[7],
							[Basic8] = tot.[8],
							[Basic9] = tot.[9],
							[Basic10] = tot.[10],
							[Basic11] = tot.[11],
							[Basic12] = tot.[12],
							[Basic13] = tot.[13],
							[Basic14] = tot.[14],
							[Basic15] = tot.[15]
							from
							@resprof b
							inner join (
														 
									select
									id, 
									MAX([1]) as [1],
									MAX([2]) as [2],
									MAX([3]) as [3],
									MAX([4]) as [4],
									MAX([5]) as [5],
									MAX([6]) as [6],
									MAX([7]) as [7],
									MAX([8]) as [8],
									MAX([9]) as [9],
									MAX([10]) as [10],
									MAX([11]) as [11],
									MAX([12]) as [12],
									MAX([13]) as [13],
									MAX([14]) as [14],
									MAX([15]) as [15]
									from ( 
									 select 
									@START_I as id,
									case when prjyr_marker = 1 then MAX(BasicvalCalc) else 0 end as [1],
									case when prjyr_marker = 2 then MAX(BasicvalCalc) else 0 end as [2],
									case when prjyr_marker = 3 then MAX(BasicvalCalc) else 0 end as [3],
									case when prjyr_marker = 4 then MAX(BasicvalCalc) else 0 end as [4],
									case when prjyr_marker = 5 then MAX(BasicvalCalc) else 0 end as [5],
									case when prjyr_marker = 6 then MAX(BasicvalCalc) else 0 end as [6],
									case when prjyr_marker = 7 then MAX(BasicvalCalc) else 0 end as [7],
									case when prjyr_marker = 8 then MAX(BasicvalCalc) else 0 end as [8],
									case when prjyr_marker = 9 then MAX(BasicvalCalc) else 0 end as [9],
									case when prjyr_marker = 10 then MAX(BasicvalCalc) else 0 end as [10],
									case when prjyr_marker = 11 then MAX(BasicvalCalc) else 0 end as [11],
									case when prjyr_marker = 12 then MAX(BasicvalCalc) else 0 end as [12],
									case when prjyr_marker = 13 then MAX(BasicvalCalc) else 0 end as [13],
									case when prjyr_marker = 14 then MAX(BasicvalCalc) else 0 end as [14],
									case when prjyr_marker = 15 then MAX(BasicvalCalc) else 0 end as [15],
									prjyr_marker -- use yr for project year	
									
									 from @temp					
									 group by prjyr_marker -- use yr for project year	
									 ) z
									 group by id
											 ) tot
											 on b.recid = tot.id



						---	NI

							update @resprof
							set 
							
							[NI1] = tot.[1],
							[NI2] = tot.[2],
							[NI3] = tot.[3],
							[NI4] = tot.[4],
							[NI5] = tot.[5],
							[NI6] = tot.[6],
							[NI7] = tot.[7],
							[NI8] = tot.[8],
							[NI9] = tot.[9],
							[NI10] = tot.[10],
							[NI11] = tot.[11],
							[NI12] = tot.[12],
							[NI13] = tot.[13],
							[NI14] = tot.[14],
							[NI15] = tot.[15]
							from
							@resprof b
							inner join (
														 
									select
									id, 
									MAX([1]) as [1],
									MAX([2]) as [2],
									MAX([3]) as [3],
									MAX([4]) as [4],
									MAX([5]) as [5],
									MAX([6]) as [6],
									MAX([7]) as [7],
									MAX([8]) as [8],
									MAX([9]) as [9],
									MAX([10]) as [10],
									MAX([11]) as [11],
									MAX([12]) as [12],
									MAX([13]) as [13],
									MAX([14]) as [14],
									MAX([15]) as [15]
									from ( 
									 select 
									@START_I as id,
									case when prjyr_marker = 1 then MAX(NIvalCalc) else 0 end as [1],
									case when prjyr_marker = 2 then MAX(NIvalCalc) else 0 end as [2],
									case when prjyr_marker = 3 then MAX(NIvalCalc) else 0 end as [3],
									case when prjyr_marker = 4 then MAX(NIvalCalc) else 0 end as [4],
									case when prjyr_marker = 5 then MAX(NIvalCalc) else 0 end as [5],
									case when prjyr_marker = 6 then MAX(NIvalCalc) else 0 end as [6],
									case when prjyr_marker = 7 then MAX(NIvalCalc) else 0 end as [7],
									case when prjyr_marker = 8 then MAX(NIvalCalc) else 0 end as [8],
									case when prjyr_marker = 9 then MAX(NIvalCalc) else 0 end as [9],
									case when prjyr_marker = 10 then MAX(NIvalCalc) else 0 end as [10],
									case when prjyr_marker = 11 then MAX(NIvalCalc) else 0 end as [11],
									case when prjyr_marker = 12 then MAX(NIvalCalc) else 0 end as [12],
									case when prjyr_marker = 13 then MAX(NIvalCalc) else 0 end as [13],
									case when prjyr_marker = 14 then MAX(NIvalCalc) else 0 end as [14],
									case when prjyr_marker = 15 then MAX(NIvalCalc) else 0 end as [15],
									prjyr_marker -- use yr for project year	
									
									 from @temp					
									 group by prjyr_marker -- use yr for project year	
									 ) z
									 group by id
											 ) tot
											 on b.recid = tot.id


						---	Super

							update @resprof
							set 
							
							[Super1] = tot.[1],
							[Super2] = tot.[2],
							[Super3] = tot.[3],
							[Super4] = tot.[4],
							[Super5] = tot.[5],
							[Super6] = tot.[6],
							[Super7] = tot.[7],
							[Super8] = tot.[8],
							[Super9] = tot.[9],
							[Super10] = tot.[10],
							[Super11] = tot.[11],
							[Super12] = tot.[12],
							[Super13] = tot.[13],
							[Super14] = tot.[14],
							[Super15] = tot.[15]
							from
							@resprof b
							inner join (
														 
									select
									id, 
									MAX([1]) as [1],
									MAX([2]) as [2],
									MAX([3]) as [3],
									MAX([4]) as [4],
									MAX([5]) as [5],
									MAX([6]) as [6],
									MAX([7]) as [7],
									MAX([8]) as [8],
									MAX([9]) as [9],
									MAX([10]) as [10],
									MAX([11]) as [11],
									MAX([12]) as [12],
									MAX([13]) as [13],
									MAX([14]) as [14],
									MAX([15]) as [15]
									from ( 
									 select 
									@START_I as id,
									case when prjyr_marker = 1 then MAX(SupervalCalc) else 0 end as [1],
									case when prjyr_marker = 2 then MAX(SupervalCalc) else 0 end as [2],
									case when prjyr_marker = 3 then MAX(SupervalCalc) else 0 end as [3],
									case when prjyr_marker = 4 then MAX(SupervalCalc) else 0 end as [4],
									case when prjyr_marker = 5 then MAX(SupervalCalc) else 0 end as [5],
									case when prjyr_marker = 6 then MAX(SupervalCalc) else 0 end as [6],
									case when prjyr_marker = 7 then MAX(SupervalCalc) else 0 end as [7],
									case when prjyr_marker = 8 then MAX(SupervalCalc) else 0 end as [8],
									case when prjyr_marker = 9 then MAX(SupervalCalc) else 0 end as [9],
									case when prjyr_marker = 10 then MAX(SupervalCalc) else 0 end as [10],
									case when prjyr_marker = 11 then MAX(SupervalCalc) else 0 end as [11],
									case when prjyr_marker = 12 then MAX(SupervalCalc) else 0 end as [12],
									case when prjyr_marker = 13 then MAX(SupervalCalc) else 0 end as [13],
									case when prjyr_marker = 14 then MAX(SupervalCalc) else 0 end as [14],
									case when prjyr_marker = 15 then MAX(SupervalCalc) else 0 end as [15],
									prjyr_marker -- use yr for project year	
									
									 from @temp					
									 group by prjyr_marker -- use yr for project year	
									 ) z
									 group by id
											 ) tot
											 on b.recid = tot.id




							---	Spon

							update @resprof
							set 
							
							[Spon1] = tot.[1],
							[Spon2] = tot.[2],
							[Spon3] = tot.[3],
							[Spon4] = tot.[4],
							[Spon5] = tot.[5],
							[Spon6] = tot.[6],
							[Spon7] = tot.[7],
							[Spon8] = tot.[8],
							[Spon9] = tot.[9],
							[Spon10] = tot.[10],
							[Spon11] = tot.[11],
							[Spon12] = tot.[12],
							[Spon13] = tot.[13],
							[Spon14] = tot.[14],
							[Spon15] = tot.[15]
							from
							@resprof b
							inner join (
														 
									select
									id, 
									MAX([1]) as [1],
									MAX([2]) as [2],
									MAX([3]) as [3],
									MAX([4]) as [4],
									MAX([5]) as [5],
									MAX([6]) as [6],
									MAX([7]) as [7],
									MAX([8]) as [8],
									MAX([9]) as [9],
									MAX([10]) as [10],
									MAX([11]) as [11],
									MAX([12]) as [12],
									MAX([13]) as [13],
									MAX([14]) as [14],
									MAX([15]) as [15]
									from ( 
									 select 
									@START_I as id,
									case when prjyr_marker = 1 then MAX(SponvalCalc) else 0 end as [1],
									case when prjyr_marker = 2 then MAX(SponvalCalc) else 0 end as [2],
									case when prjyr_marker = 3 then MAX(SponvalCalc) else 0 end as [3],
									case when prjyr_marker = 4 then MAX(SponvalCalc) else 0 end as [4],
									case when prjyr_marker = 5 then MAX(SponvalCalc) else 0 end as [5],
									case when prjyr_marker = 6 then MAX(SponvalCalc) else 0 end as [6],
									case when prjyr_marker = 7 then MAX(SponvalCalc) else 0 end as [7],
									case when prjyr_marker = 8 then MAX(SponvalCalc) else 0 end as [8],
									case when prjyr_marker = 9 then MAX(SponvalCalc) else 0 end as [9],
									case when prjyr_marker = 10 then MAX(SponvalCalc) else 0 end as [10],
									case when prjyr_marker = 11 then MAX(SponvalCalc) else 0 end as [11],
									case when prjyr_marker = 12 then MAX(SponvalCalc) else 0 end as [12],
									case when prjyr_marker = 13 then MAX(SponvalCalc) else 0 end as [13],
									case when prjyr_marker = 14 then MAX(SponvalCalc) else 0 end as [14],
									case when prjyr_marker = 15 then MAX(SponvalCalc) else 0 end as [15],
									prjyr_marker -- use yr for project year	
									
									 from @temp					
									 group by prjyr_marker -- use yr for project year	
									 ) z
									 group by id
											 ) tot
											 on b.recid = tot.id


						end	
						
					
					
						
	if @type = 'prjyr'
					begin		
							
							if @chkcalcDO = 1
									
							begin
							
									update @temp
									set 
										calc = val / MONTHS,
										BasicvalCalc = Basicval / MONTHS,
										NIvalCalc = NIval / MONTHS,
										SupervalCalc = Superval / MONTHS,
										SponvalCalc = Sponval / MONTHS
										/*,
										FTEvalCalc = FTEval / MONTHS
										*/
									
			-------------------------------- nightmare!						
			update @temp
			set runningtot = DATEDIFF(d,@st, @ed)
			
			declare @dayi int
			set @dayi = 0
			
			declare @dayimax int
			set @dayimax = ( select MAX(ROWID) from @temp )
			
			while @dayi <= @dayimax
			begin
			
				update @temp
				set daycount = 
					case
					when MONTH(point) = 2 then 28
					when @dayi <> @dayimax then
					DATEDIFF( d,point, 
								DATEADD (dd, -1, DATEADD(mm, DATEDIFF(mm, 0, point) + 1, 0))
								) + 1
					else

					DATEDIFF( d,DATEADD(mm, DATEDIFF(mm, 0,point), 0), 
								point
								) + 1
						end 

					/*
					case
					when MONTH(point) = 2 then 28
					else
					DATEDIFF(d,point, isnull(( select point from @temp	where ROWID = @dayi + 1 ),@ed+1) )
					end
					*/
				where 
				ROWID = @dayi
				
				update @temp
				set runningtot = ( select runningtot from @temp	where ROWID = @dayi - 1 ) -
					case
					when MONTH(point) = 2 then 28
					else
					DATEDIFF(d,point, isnull(( select point from @temp	where ROWID = @dayi + 1 ),@ed+1) )
					end
					
				where 
				ROWID = @dayi
				and ROWID <> 0
			
			
			set @dayi = @dayi + 1
			
			end
			
			
				
			update @temp
			set totdays = b.totdays
			from @temp a
				inner join (
								select
								prjyr_marker,
								yr,
								SUM(daycount) as totdays
								from
								@temp	
								group by prjyr_marker, yr
								
							) b on a.prjyr_marker = b.prjyr_marker	
							AND a.yr = b.yr
			
			
							
			update @temp
			set dividedays = b.totdays			
				from @temp a
						inner join (
									select
								prjyr_marker,
								--yr,
								SUM(daycount) as totdays
								from
								@temp	
								group by prjyr_marker
									) b 
									on a.prjyr_marker = b.prjyr_marker
									
			update @temp
			set 
			value = b.value
				from @temp a
						inner join (
									select 
									id,
																		
									daycount*1.00 /
									case
									when MONTHS = 1 then daycount
									else 
										case	
										when MONTHS = 12 then 365
										else
										dividedays
										--totdays
										end
									end
									* val as value
									from @temp	
									) b 
									on a.id = b.id



			---	basic
			update @temp
			set 
			Basicval = b.value
				from @temp a
						inner join (
									select 
									id,
																		
									daycount*1.00 /
									case
									when MONTHS = 1 then daycount
									else 
										case	
										when MONTHS = 12 then 365
										else
										dividedays
										--totdays
										end
									end
									* Basicval as value
									from @temp	
									) b 
									on a.id = b.id


			---	super
			update @temp
			set 
			superval = b.value
				from @temp a
						inner join (
									select 
									id,
																		
									daycount*1.00 /
									case
									when MONTHS = 1 then daycount
									else 
										case	
										when MONTHS = 12 then 365
										else
										dividedays
										--totdays
										end
									end
									* superval as value
									from @temp	
									) b 
									on a.id = b.id


			---	ni
			update @temp
			set 
			nival = b.value
				from @temp a
						inner join (
									select 
									id,
																		
									daycount*1.00 /
									case
									when MONTHS = 1 then daycount
									else 
										case	
										when MONTHS = 12 then 365
										else
										dividedays
										--totdays
										end
									end
									* nival as value
									from @temp	
									) b 
									on a.id = b.id

			---	Spon
			update @temp
			set 
			Sponval = b.value
				from @temp a
						inner join (
									select 
									id,
																		
									(daycount*1.00 /
									case
									when MONTHS = 1 then daycount
									else 
										case	
										when MONTHS = 12 then 365
										else
										dividedays
										--totdays
										end
									end
									--- fred
									* sponval
									)
									/*
									* ( case
									when finaladjust IS null  then 100 else 
									finaladjust end / 100
									 )
									*/
									
									
									
									 as value
									from @temp	
									) b 
									on a.id = b.id



			---	FTE
			declare @totdays int
			set @totdays = ( select sum( daycount ) from @temp 
								where RECID = @START_I )
				
			
			declare @ftedayperc float 
			set @ftedayperc  = (
								SELECT 
								 MAX(FTE)  / @totdays * 1.00 AS FTE
									from @temp	
									where RECID = @START_I
									)

									

			update @temp
			set 
			FTEval = b.value
				from @temp a
						inner join (
									select 
									id,
									(daycount*1.00 )
									*
									@ftedayperc
									as value
									from @temp	
									) b 
									on a.id = b.id

			
			
			if ( select ChargeType from @resprof
				where RECID = @START_I ) = 'Directly Incurred Costs'
				begin
		
				if ( select canflat from @resprof
				where RECID = @START_I ) = 0
				begin
					update @temp
					--set dividedays = DATEDIFF(d,@st, @ed)
					set dividedays = ( select sum(daycount) from @temp ) -- DATEDIFF(d,@st, @ed)
					-- SQL Server doesn't include a count for the last day!!!
				
					update @temp
					set val = ( select [1] from
									@resprof
									where RECID = @START_I ),
						Basicval = ( select [Basic1] from
									@resprof
									where RECID = @START_I ),
						NIval = ( select [NI1] from
									@resprof
									where RECID = @START_I ),
						Superval = ( select [super1] from
									@resprof
									where RECID = @START_I ),
						--	Sponval = ( select [Spon1] from
						Sponval = ( select [SponTotal] *
									( case
									--when finaladjust = 0   then 0
									when finaladjust IS null  then 100 
									when finaladjust = ''  then 100 
									
									else 
									finaladjust end / 100
									 )
						
									 from
									@resprof
									where RECID = @START_I ),
						FTEval = ( select [FTEval] from
									@resprof
									where RECID = @START_I )
				
					update @temp
						set 
						value = b.value
							from @temp a
									inner join (
												select 
												id,
																					
												daycount*1.00 /
												
													dividedays
													--totdays
													
												* val as value
												from @temp	
												) b 
												on a.id = b.id

					--	Basic
					update @temp
						set 
						Basicval = b.value
							from @temp a
									inner join (
												select 
												id,
																					
												daycount*1.00 /
												
													dividedays
													--totdays
													
												* Basicval as value
												from @temp	
												) b 
												on a.id = b.id

					--	Super
					update @temp
						set 
						Superval = b.value
							from @temp a
									inner join (
												select 
												id,
																					
												daycount*1.00 /
												
													dividedays
													--totdays
													
												* Superval as value
												from @temp	
												) b 
												on a.id = b.id

					--	NI
					update @temp
						set 
						NIval = b.value
							from @temp a
									inner join (
												select 
												id,
																					
												daycount*1.00 /
												
													dividedays
													--totdays
													
												* NIval as value
												from @temp	
												) b 
												on a.id = b.id

					--	Spon
					update @temp
						set 
						Sponval = b.value
							from @temp a
									inner join (
												select 
												id,
												(									
												daycount*1.00 /
												
													dividedays
													--totdays
													
												* Sponval )
												* ( case
												--when finaladjust = 0   then 0
												when finaladjust IS null  then 100 
												when finaladjust = ''  then 100 
												else 
												finaladjust end / 100
												 )
												as value
												from @temp	
												) b 
												on a.id = b.id


					--	FTE
					update @temp
						set 
						FTEval = b.value
							from @temp a
									inner join (
												select 
												id,
																					
												daycount*1.00 /
												
													dividedays
													--totdays
													
												* FTEval as value
												from @temp	
												) b 
												on a.id = b.id

					end
			
				end				
														
			/*
			update @temp
			set dividedays = b.dividedays			,
			value = b.value
				from @temp a
						inner join (
									select 
									id,
									case
									when MONTHS = 1 then daycount
									else 
										case	
										when runningtot >= 365 then 365
										else
										totdays
										end
									end as dividedays,
									
									daycount*1.00 /
									case
									when MONTHS = 1 then daycount
									else 
										case	
										when runningtot >= 365 then 365
										else
										totdays
										end
									end
									* val as value
									from @temp	
									) b 
									on a.id = b.id
				*/					
									-------------------------------- nightmare!
							
							end
							
							if @chkcalcDO <> 1
									
							begin
							
							update @resprof
							set 
							[1] = tot.[1],
							[2] = tot.[2],
							[3] = tot.[3],
							[4] = tot.[4],
							[5] = tot.[5],
							[6] = tot.[6],
							[7] = tot.[7],
							[8] = tot.[8],
							[9] = tot.[9],
							[10] = tot.[10],
							[11] = tot.[11],
							[12] = tot.[12],
							[13] = tot.[13],
							[14] = tot.[14],
							[15] = tot.[15]
							from
							@resprof b
							inner join (
														 
									select
									id, 
									MAX([1]) as [1],
									MAX([2]) as [2],
									MAX([3]) as [3],
									MAX([4]) as [4],
									MAX([5]) as [5],
									MAX([6]) as [6],
									MAX([7]) as [7],
									MAX([8]) as [8],
									MAX([9]) as [9],
									MAX([10]) as [10],
									MAX([11]) as [11],
									MAX([12]) as [12],
									MAX([13]) as [13],
									MAX([14]) as [14],
									MAX([15]) as [15]
									from ( 
									 select 
									@START_I as id,
									case when yr = 1 then MAX(calc) else 0 end as [1],
									case when yr = 2 then MAX(calc) else 0 end as [2],
									case when yr = 3 then MAX(calc) else 0 end as [3],
									case when yr = 4 then MAX(calc) else 0 end as [4],
									case when yr = 5 then MAX(calc) else 0 end as [5],
									case when yr = 6 then MAX(calc) else 0 end as [6],
									case when yr = 7 then MAX(calc) else 0 end as [7],
									case when yr = 8 then MAX(calc) else 0 end as [8],
									case when yr = 9 then MAX(calc) else 0 end as [9],
									case when yr = 10 then MAX(calc) else 0 end as [10],
									case when yr = 11 then MAX(calc) else 0 end as [11],
									case when yr = 12 then MAX(calc) else 0 end as [12],
									case when yr = 13 then MAX(calc) else 0 end as [13],
									case when yr = 14 then MAX(calc) else 0 end as [14],
									case when yr = 15 then MAX(calc) else 0 end as [15],
									yr -- use yr for project year	
									 from @temp					
									 group by yr -- use yr for project year	
									 ) z
									 group by id
											 ) tot
											 on b.recid = tot.id

							-- BASIC
							update @resprof
							set 
							[Basic1] = tot.[1],
							[Basic2] = tot.[2],
							[Basic3] = tot.[3],
							[Basic4] = tot.[4],
							[Basic5] = tot.[5],
							[Basic6] = tot.[6],
							[Basic7] = tot.[7],
							[Basic8] = tot.[8],
							[Basic9] = tot.[9],
							[Basic10] = tot.[10],
							[Basic11] = tot.[11],
							[Basic12] = tot.[12],
							[Basic13] = tot.[13],
							[Basic14] = tot.[14],
							[Basic15] = tot.[15]
							from
							@resprof b
							inner join (
														 
									select
									id, 
									MAX([1]) as [1],
									MAX([2]) as [2],
									MAX([3]) as [3],
									MAX([4]) as [4],
									MAX([5]) as [5],
									MAX([6]) as [6],
									MAX([7]) as [7],
									MAX([8]) as [8],
									MAX([9]) as [9],
									MAX([10]) as [10],
									MAX([11]) as [11],
									MAX([12]) as [12],
									MAX([13]) as [13],
									MAX([14]) as [14],
									MAX([15]) as [15]
									from ( 
									 select 
									@START_I as id,
									case when yr = 1 then MAX(BasicvalCalc) else 0 end as [1],
									case when yr = 2 then MAX(BasicvalCalc) else 0 end as [2],
									case when yr = 3 then MAX(BasicvalCalc) else 0 end as [3],
									case when yr = 4 then MAX(BasicvalCalc) else 0 end as [4],
									case when yr = 5 then MAX(BasicvalCalc) else 0 end as [5],
									case when yr = 6 then MAX(BasicvalCalc) else 0 end as [6],
									case when yr = 7 then MAX(BasicvalCalc) else 0 end as [7],
									case when yr = 8 then MAX(BasicvalCalc) else 0 end as [8],
									case when yr = 9 then MAX(BasicvalCalc) else 0 end as [9],
									case when yr = 10 then MAX(BasicvalCalc) else 0 end as [10],
									case when yr = 11 then MAX(BasicvalCalc) else 0 end as [11],
									case when yr = 12 then MAX(BasicvalCalc) else 0 end as [12],
									case when yr = 13 then MAX(BasicvalCalc) else 0 end as [13],
									case when yr = 14 then MAX(BasicvalCalc) else 0 end as [14],
									case when yr = 15 then MAX(BasicvalCalc) else 0 end as [15],
									yr -- use yr for project year	
									 from @temp					
									 group by yr -- use yr for project year	
									 ) z
									 group by id
											 ) tot
											 on b.recid = tot.id


							-- Super
							update @resprof
							set 
							[Super1] = tot.[1],
							[Super2] = tot.[2],
							[Super3] = tot.[3],
							[Super4] = tot.[4],
							[Super5] = tot.[5],
							[Super6] = tot.[6],
							[Super7] = tot.[7],
							[Super8] = tot.[8],
							[Super9] = tot.[9],
							[Super10] = tot.[10],
							[Super11] = tot.[11],
							[Super12] = tot.[12],
							[Super13] = tot.[13],
							[Super14] = tot.[14],
							[Super15] = tot.[15]
							from
							@resprof b
							inner join (
														 
									select
									id, 
									MAX([1]) as [1],
									MAX([2]) as [2],
									MAX([3]) as [3],
									MAX([4]) as [4],
									MAX([5]) as [5],
									MAX([6]) as [6],
									MAX([7]) as [7],
									MAX([8]) as [8],
									MAX([9]) as [9],
									MAX([10]) as [10],
									MAX([11]) as [11],
									MAX([12]) as [12],
									MAX([13]) as [13],
									MAX([14]) as [14],
									MAX([15]) as [15]
									from ( 
									 select 
									@START_I as id,
									case when yr = 1 then MAX(SupervalCalc) else 0 end as [1],
									case when yr = 2 then MAX(SupervalCalc) else 0 end as [2],
									case when yr = 3 then MAX(SupervalCalc) else 0 end as [3],
									case when yr = 4 then MAX(SupervalCalc) else 0 end as [4],
									case when yr = 5 then MAX(SupervalCalc) else 0 end as [5],
									case when yr = 6 then MAX(SupervalCalc) else 0 end as [6],
									case when yr = 7 then MAX(SupervalCalc) else 0 end as [7],
									case when yr = 8 then MAX(SupervalCalc) else 0 end as [8],
									case when yr = 9 then MAX(SupervalCalc) else 0 end as [9],
									case when yr = 10 then MAX(SupervalCalc) else 0 end as [10],
									case when yr = 11 then MAX(SupervalCalc) else 0 end as [11],
									case when yr = 12 then MAX(SupervalCalc) else 0 end as [12],
									case when yr = 13 then MAX(SupervalCalc) else 0 end as [13],
									case when yr = 14 then MAX(SupervalCalc) else 0 end as [14],
									case when yr = 15 then MAX(SupervalCalc) else 0 end as [15],
									yr -- use yr for project year	
									 from @temp					
									 group by yr -- use yr for project year	
									 ) z
									 group by id
											 ) tot
											 on b.recid = tot.id


							-- NI
							update @resprof
							set 
							[NI1] = tot.[1],
							[NI2] = tot.[2],
							[NI3] = tot.[3],
							[NI4] = tot.[4],
							[NI5] = tot.[5],
							[NI6] = tot.[6],
							[NI7] = tot.[7],
							[NI8] = tot.[8],
							[NI9] = tot.[9],
							[NI10] = tot.[10],
							[NI11] = tot.[11],
							[NI12] = tot.[12],
							[NI13] = tot.[13],
							[NI14] = tot.[14],
							[NI15] = tot.[15]
							from
							@resprof b
							inner join (
														 
									select
									id, 
									MAX([1]) as [1],
									MAX([2]) as [2],
									MAX([3]) as [3],
									MAX([4]) as [4],
									MAX([5]) as [5],
									MAX([6]) as [6],
									MAX([7]) as [7],
									MAX([8]) as [8],
									MAX([9]) as [9],
									MAX([10]) as [10],
									MAX([11]) as [11],
									MAX([12]) as [12],
									MAX([13]) as [13],
									MAX([14]) as [14],
									MAX([15]) as [15]
									from ( 
									 select 
									@START_I as id,
									case when yr = 1 then MAX(NIvalCalc) else 0 end as [1],
									case when yr = 2 then MAX(NIvalCalc) else 0 end as [2],
									case when yr = 3 then MAX(NIvalCalc) else 0 end as [3],
									case when yr = 4 then MAX(NIvalCalc) else 0 end as [4],
									case when yr = 5 then MAX(NIvalCalc) else 0 end as [5],
									case when yr = 6 then MAX(NIvalCalc) else 0 end as [6],
									case when yr = 7 then MAX(NIvalCalc) else 0 end as [7],
									case when yr = 8 then MAX(NIvalCalc) else 0 end as [8],
									case when yr = 9 then MAX(NIvalCalc) else 0 end as [9],
									case when yr = 10 then MAX(NIvalCalc) else 0 end as [10],
									case when yr = 11 then MAX(NIvalCalc) else 0 end as [11],
									case when yr = 12 then MAX(NIvalCalc) else 0 end as [12],
									case when yr = 13 then MAX(NIvalCalc) else 0 end as [13],
									case when yr = 14 then MAX(NIvalCalc) else 0 end as [14],
									case when yr = 15 then MAX(NIvalCalc) else 0 end as [15],
									yr -- use yr for project year	
									 from @temp					
									 group by yr -- use yr for project year	
									 ) z
									 group by id
											 ) tot
											 on b.recid = tot.id


						-- Spon
							update @resprof
							set 
							[Spon1] = tot.[1],
							[Spon2] = tot.[2],
							[Spon3] = tot.[3],
							[Spon4] = tot.[4],
							[Spon5] = tot.[5],
							[Spon6] = tot.[6],
							[Spon7] = tot.[7],
							[Spon8] = tot.[8],
							[Spon9] = tot.[9],
							[Spon10] = tot.[10],
							[Spon11] = tot.[11],
							[Spon12] = tot.[12],
							[Spon13] = tot.[13],
							[Spon14] = tot.[14],
							[Spon15] = tot.[15]
							from
							@resprof b
							inner join (
														 
									select
									id, 
									MAX([1]) as [1],
									MAX([2]) as [2],
									MAX([3]) as [3],
									MAX([4]) as [4],
									MAX([5]) as [5],
									MAX([6]) as [6],
									MAX([7]) as [7],
									MAX([8]) as [8],
									MAX([9]) as [9],
									MAX([10]) as [10],
									MAX([11]) as [11],
									MAX([12]) as [12],
									MAX([13]) as [13],
									MAX([14]) as [14],
									MAX([15]) as [15]
									from ( 
									 select 
									@START_I as id,
									case when yr = 1 then MAX(SponvalCalc) else 0 end as [1],
									case when yr = 2 then MAX(SponvalCalc) else 0 end as [2],
									case when yr = 3 then MAX(SponvalCalc) else 0 end as [3],
									case when yr = 4 then MAX(SponvalCalc) else 0 end as [4],
									case when yr = 5 then MAX(SponvalCalc) else 0 end as [5],
									case when yr = 6 then MAX(SponvalCalc) else 0 end as [6],
									case when yr = 7 then MAX(SponvalCalc) else 0 end as [7],
									case when yr = 8 then MAX(SponvalCalc) else 0 end as [8],
									case when yr = 9 then MAX(SponvalCalc) else 0 end as [9],
									case when yr = 10 then MAX(SponvalCalc) else 0 end as [10],
									case when yr = 11 then MAX(SponvalCalc) else 0 end as [11],
									case when yr = 12 then MAX(SponvalCalc) else 0 end as [12],
									case when yr = 13 then MAX(SponvalCalc) else 0 end as [13],
									case when yr = 14 then MAX(SponvalCalc) else 0 end as [14],
									case when yr = 15 then MAX(SponvalCalc) else 0 end as [15],
									yr -- use yr for project year	
									 from @temp					
									 group by yr -- use yr for project year	
									 ) z
									 group by id
											 ) tot
											 on b.recid = tot.id


						end
						
						
						if @chkcalcDO = 1
									
							begin
							
							update @resprof
							set 
							[1] = tot.[1],
							[2] = tot.[2],
							[3] = tot.[3],
							[4] = tot.[4],
							[5] = tot.[5],
							[6] = tot.[6],
							[7] = tot.[7],
							[8] = tot.[8],
							[9] = tot.[9],
							[10] = tot.[10],
							[11] = tot.[11],
							[12] = tot.[12],
							[13] = tot.[13],
							[14] = tot.[14],
							[15] = tot.[15]
							from
							@resprof b
							inner join (
														 
									select
									id, 
									MAX([1]) as [1],
									MAX([2]) as [2],
									MAX([3]) as [3],
									MAX([4]) as [4],
									MAX([5]) as [5],
									MAX([6]) as [6],
									MAX([7]) as [7],
									MAX([8]) as [8],
									MAX([9]) as [9],
									MAX([10]) as [10],
									MAX([11]) as [11],
									MAX([12]) as [12],
									MAX([13]) as [13],
									MAX([14]) as [14],
									MAX([15]) as [15]
									from ( 
									 select 
									@START_I as id,
									case when yr = 1 then sum(value) else 0 end as [1],
									case when yr = 2 then sum(value) else 0 end as [2],
									case when yr = 3 then sum(value) else 0 end as [3],
									case when yr = 4 then sum(value) else 0 end as [4],
									case when yr = 5 then sum(value) else 0 end as [5],
									case when yr = 6 then sum(value) else 0 end as [6],
									case when yr = 7 then sum(value) else 0 end as [7],
									case when yr = 8 then sum(value) else 0 end as [8],
									case when yr = 9 then sum(value) else 0 end as [9],
									case when yr = 10 then sum(value) else 0 end as [10],
									case when yr = 11 then sum(value) else 0 end as [11],
									case when yr = 12 then sum(value) else 0 end as [12],
									case when yr = 13 then sum(value) else 0 end as [13],
									case when yr = 14 then sum(value) else 0 end as [14],
									case when yr = 15 then sum(value) else 0 end as [15],
									yr -- use yr for project year	
									 from @temp					
									 group by yr -- use yr for project year	
									 ) z
									 group by id
											 ) tot
											 on b.recid = tot.id
							
							-- BASIC
							update @resprof
							set 
							[Basic1] = tot.[1],
							[Basic2] = tot.[2],
							[Basic3] = tot.[3],
							[Basic4] = tot.[4],
							[Basic5] = tot.[5],
							[Basic6] = tot.[6],
							[Basic7] = tot.[7],
							[Basic8] = tot.[8],
							[Basic9] = tot.[9],
							[Basic10] = tot.[10],
							[Basic11] = tot.[11],
							[Basic12] = tot.[12],
							[Basic13] = tot.[13],
							[Basic14] = tot.[14],
							[Basic15] = tot.[15]
							from
							@resprof b
							inner join (
														 
									select
									id, 
									MAX([1]) as [1],
									MAX([2]) as [2],
									MAX([3]) as [3],
									MAX([4]) as [4],
									MAX([5]) as [5],
									MAX([6]) as [6],
									MAX([7]) as [7],
									MAX([8]) as [8],
									MAX([9]) as [9],
									MAX([10]) as [10],
									MAX([11]) as [11],
									MAX([12]) as [12],
									MAX([13]) as [13],
									MAX([14]) as [14],
									MAX([15]) as [15]
									from ( 
									 select 
									@START_I as id,
									case when yr = 1 then MAX(BasicvalCalc) else 0 end as [1],
									case when yr = 2 then MAX(BasicvalCalc) else 0 end as [2],
									case when yr = 3 then MAX(BasicvalCalc) else 0 end as [3],
									case when yr = 4 then MAX(BasicvalCalc) else 0 end as [4],
									case when yr = 5 then MAX(BasicvalCalc) else 0 end as [5],
									case when yr = 6 then MAX(BasicvalCalc) else 0 end as [6],
									case when yr = 7 then MAX(BasicvalCalc) else 0 end as [7],
									case when yr = 8 then MAX(BasicvalCalc) else 0 end as [8],
									case when yr = 9 then MAX(BasicvalCalc) else 0 end as [9],
									case when yr = 10 then MAX(BasicvalCalc) else 0 end as [10],
									case when yr = 11 then MAX(BasicvalCalc) else 0 end as [11],
									case when yr = 12 then MAX(BasicvalCalc) else 0 end as [12],
									case when yr = 13 then MAX(BasicvalCalc) else 0 end as [13],
									case when yr = 14 then MAX(BasicvalCalc) else 0 end as [14],
									case when yr = 15 then MAX(BasicvalCalc) else 0 end as [15],
									yr -- use yr for project year	
									 from @temp					
									 group by yr -- use yr for project year	
									 ) z
									 group by id
											 ) tot
											 on b.recid = tot.id


							-- Super
							update @resprof
							set 
							[Super1] = tot.[1],
							[Super2] = tot.[2],
							[Super3] = tot.[3],
							[Super4] = tot.[4],
							[Super5] = tot.[5],
							[Super6] = tot.[6],
							[Super7] = tot.[7],
							[Super8] = tot.[8],
							[Super9] = tot.[9],
							[Super10] = tot.[10],
							[Super11] = tot.[11],
							[Super12] = tot.[12],
							[Super13] = tot.[13],
							[Super14] = tot.[14],
							[Super15] = tot.[15]
							from
							@resprof b
							inner join (
														 
									select
									id, 
									MAX([1]) as [1],
									MAX([2]) as [2],
									MAX([3]) as [3],
									MAX([4]) as [4],
									MAX([5]) as [5],
									MAX([6]) as [6],
									MAX([7]) as [7],
									MAX([8]) as [8],
									MAX([9]) as [9],
									MAX([10]) as [10],
									MAX([11]) as [11],
									MAX([12]) as [12],
									MAX([13]) as [13],
									MAX([14]) as [14],
									MAX([15]) as [15]
									from ( 
									 select 
									@START_I as id,
									case when yr = 1 then MAX(SupervalCalc) else 0 end as [1],
									case when yr = 2 then MAX(SupervalCalc) else 0 end as [2],
									case when yr = 3 then MAX(SupervalCalc) else 0 end as [3],
									case when yr = 4 then MAX(SupervalCalc) else 0 end as [4],
									case when yr = 5 then MAX(SupervalCalc) else 0 end as [5],
									case when yr = 6 then MAX(SupervalCalc) else 0 end as [6],
									case when yr = 7 then MAX(SupervalCalc) else 0 end as [7],
									case when yr = 8 then MAX(SupervalCalc) else 0 end as [8],
									case when yr = 9 then MAX(SupervalCalc) else 0 end as [9],
									case when yr = 10 then MAX(SupervalCalc) else 0 end as [10],
									case when yr = 11 then MAX(SupervalCalc) else 0 end as [11],
									case when yr = 12 then MAX(SupervalCalc) else 0 end as [12],
									case when yr = 13 then MAX(SupervalCalc) else 0 end as [13],
									case when yr = 14 then MAX(SupervalCalc) else 0 end as [14],
									case when yr = 15 then MAX(SupervalCalc) else 0 end as [15],
									yr -- use yr for project year	
									 from @temp					
									 group by yr -- use yr for project year	
									 ) z
									 group by id
											 ) tot
											 on b.recid = tot.id


							-- NI
							update @resprof
							set 
							[NI1] = tot.[1],
							[NI2] = tot.[2],
							[NI3] = tot.[3],
							[NI4] = tot.[4],
							[NI5] = tot.[5],
							[NI6] = tot.[6],
							[NI7] = tot.[7],
							[NI8] = tot.[8],
							[NI9] = tot.[9],
							[NI10] = tot.[10],
							[NI11] = tot.[11],
							[NI12] = tot.[12],
							[NI13] = tot.[13],
							[NI14] = tot.[14],
							[NI15] = tot.[15]
							from
							@resprof b
							inner join (
														 
									select
									id, 
									MAX([1]) as [1],
									MAX([2]) as [2],
									MAX([3]) as [3],
									MAX([4]) as [4],
									MAX([5]) as [5],
									MAX([6]) as [6],
									MAX([7]) as [7],
									MAX([8]) as [8],
									MAX([9]) as [9],
									MAX([10]) as [10],
									MAX([11]) as [11],
									MAX([12]) as [12],
									MAX([13]) as [13],
									MAX([14]) as [14],
									MAX([15]) as [15]
									from ( 
									 select 
									@START_I as id,
									case when yr = 1 then MAX(NIvalCalc) else 0 end as [1],
									case when yr = 2 then MAX(NIvalCalc) else 0 end as [2],
									case when yr = 3 then MAX(NIvalCalc) else 0 end as [3],
									case when yr = 4 then MAX(NIvalCalc) else 0 end as [4],
									case when yr = 5 then MAX(NIvalCalc) else 0 end as [5],
									case when yr = 6 then MAX(NIvalCalc) else 0 end as [6],
									case when yr = 7 then MAX(NIvalCalc) else 0 end as [7],
									case when yr = 8 then MAX(NIvalCalc) else 0 end as [8],
									case when yr = 9 then MAX(NIvalCalc) else 0 end as [9],
									case when yr = 10 then MAX(NIvalCalc) else 0 end as [10],
									case when yr = 11 then MAX(NIvalCalc) else 0 end as [11],
									case when yr = 12 then MAX(NIvalCalc) else 0 end as [12],
									case when yr = 13 then MAX(NIvalCalc) else 0 end as [13],
									case when yr = 14 then MAX(NIvalCalc) else 0 end as [14],
									case when yr = 15 then MAX(NIvalCalc) else 0 end as [15],
									yr -- use yr for project year	
									 from @temp					
									 group by yr -- use yr for project year	
									 ) z
									 group by id
											 ) tot
											 on b.recid = tot.id


							-- Spon
							update @resprof
							set 
							[Spon1] = tot.[1],
							[Spon2] = tot.[2],
							[Spon3] = tot.[3],
							[Spon4] = tot.[4],
							[Spon5] = tot.[5],
							[Spon6] = tot.[6],
							[Spon7] = tot.[7],
							[Spon8] = tot.[8],
							[Spon9] = tot.[9],
							[Spon10] = tot.[10],
							[Spon11] = tot.[11],
							[Spon12] = tot.[12],
							[Spon13] = tot.[13],
							[Spon14] = tot.[14],
							[Spon15] = tot.[15]
							from
							@resprof b
							inner join (
														 
									select
									id, 
									MAX([1]) as [1],
									MAX([2]) as [2],
									MAX([3]) as [3],
									MAX([4]) as [4],
									MAX([5]) as [5],
									MAX([6]) as [6],
									MAX([7]) as [7],
									MAX([8]) as [8],
									MAX([9]) as [9],
									MAX([10]) as [10],
									MAX([11]) as [11],
									MAX([12]) as [12],
									MAX([13]) as [13],
									MAX([14]) as [14],
									MAX([15]) as [15]
									from ( 
									 select 
									@START_I as id,
									case when yr = 1 then MAX(SponvalCalc) else 0 end as [1],
									case when yr = 2 then MAX(SponvalCalc) else 0 end as [2],
									case when yr = 3 then MAX(SponvalCalc) else 0 end as [3],
									case when yr = 4 then MAX(SponvalCalc) else 0 end as [4],
									case when yr = 5 then MAX(SponvalCalc) else 0 end as [5],
									case when yr = 6 then MAX(SponvalCalc) else 0 end as [6],
									case when yr = 7 then MAX(SponvalCalc) else 0 end as [7],
									case when yr = 8 then MAX(SponvalCalc) else 0 end as [8],
									case when yr = 9 then MAX(SponvalCalc) else 0 end as [9],
									case when yr = 10 then MAX(SponvalCalc) else 0 end as [10],
									case when yr = 11 then MAX(SponvalCalc) else 0 end as [11],
									case when yr = 12 then MAX(SponvalCalc) else 0 end as [12],
									case when yr = 13 then MAX(SponvalCalc) else 0 end as [13],
									case when yr = 14 then MAX(SponvalCalc) else 0 end as [14],
									case when yr = 15 then MAX(SponvalCalc) else 0 end as [15],
									yr -- use yr for project year	
									 from @temp					
									 group by yr -- use yr for project year	
									 ) z
									 group by id
											 ) tot
											 on b.recid = tot.id
							
							end
						end		
					--	exec [dbo].[UOE_P_PRJ_Proposal_DEPT_Split_GET] 2676, 'prjyr'
					--select * from @temp
					-- flat line results
					if ( select flat_line from @resprof
								where recid = @START_I ) = 'on'
					begiN
					
					declare @flattot float
					set @flattot = ( 
									( Select total from @resprof where recid = @START_I ) 
										/
										-- divide by total moths across project
									( Select COUNT(*) from @temp_prj ) 
									)
									
					if @type = 'prjyr'
					begin				
					
						update @resprof
								set 
								[1] = tot.[1],
								[2] = tot.[2],
								[3] = tot.[3],
								[4] = tot.[4],
								[5] = tot.[5],
								[6] = tot.[6],
								[7] = tot.[7],
								[8] = tot.[8],
								[9] = tot.[9],
								[10] = tot.[10],
								[11] = tot.[11],
								[12] = tot.[12],
								[13] = tot.[13],
								[14] = tot.[14],
								[15] = tot.[15]
								from
								@resprof b
								inner join (
															 
										select
										id, 
										MAX([1]) as [1],
										MAX([2]) as [2],
										MAX([3]) as [3],
										MAX([4]) as [4],
										MAX([5]) as [5],
										MAX([6]) as [6],
										MAX([7]) as [7],
										MAX([8]) as [8],
										MAX([9]) as [9],
										MAX([10]) as [10],
										MAX([11]) as [11],
										MAX([12]) as [12],
										MAX([13]) as [13],
										MAX([14]) as [14],
										MAX([15]) as [15]
										from ( 
										 select 
										@START_I as id,
										sum(case when yr = 1 then @flattot else 0 end) as [1],
										sum(case when yr = 2 then @flattot else 0 end) as [2],
										sum(case when yr = 3 then @flattot else 0 end) as [3],
										sum(case when yr = 4 then @flattot else 0 end) as [4],
										sum(case when yr = 5 then @flattot else 0 end) as [5],
										sum(case when yr = 6 then @flattot else 0 end) as [6],
										sum(case when yr = 7 then @flattot else 0 end) as [7],
										sum(case when yr = 8 then @flattot else 0 end) as [8],
										sum(case when yr = 9 then @flattot else 0 end) as [9],
										sum(case when yr = 10 then @flattot else 0 end) as [10],
										sum(case when yr = 11 then @flattot else 0 end) as [11],
										sum(case when yr = 12 then @flattot else 0 end) as [12],
										sum(case when yr = 13 then @flattot else 0 end) as [13],
										sum(case when yr = 14 then @flattot else 0 end) as [14],
										sum(case when yr = 15 then @flattot else 0 end) as [15],
										yr -- use yr for project year	
										 from @temp_prj					
										 group by yr -- use yr for project year	
										 ) z
										 group by id
												 ) tot
												 on b.recid = tot.id
						end
						
						
					if @type = 'finyr'
					begin				
					
						update @resprof
								set 
								[1] = tot.[1],
								[2] = tot.[2],
								[3] = tot.[3],
								[4] = tot.[4],
								[5] = tot.[5],
								[6] = tot.[6],
								[7] = tot.[7],
								[8] = tot.[8],
								[9] = tot.[9],
								[10] = tot.[10],
								[11] = tot.[11],
								[12] = tot.[12],
								[13] = tot.[13],
								[14] = tot.[14],
								[15] = tot.[15]
								from
								@resprof b
								inner join (
															 
										select
										id, 
										MAX([1]) as [1],
										MAX([2]) as [2],
										MAX([3]) as [3],
										MAX([4]) as [4],
										MAX([5]) as [5],
										MAX([6]) as [6],
										MAX([7]) as [7],
										MAX([8]) as [8],
										MAX([9]) as [9],
										MAX([10]) as [10],
										MAX([11]) as [11],
										MAX([12]) as [12],
										MAX([13]) as [13],
										MAX([14]) as [14],
										MAX([15]) as [15]
										from ( 
										 select 
										@START_I as id,
										sum(case when prjyr_marker = 1 then @flattot else 0 end) as [1],
										sum(case when prjyr_marker = 2 then @flattot else 0 end) as [2],
										sum(case when prjyr_marker = 3 then @flattot else 0 end) as [3],
										sum(case when prjyr_marker = 4 then @flattot else 0 end) as [4],
										sum(case when prjyr_marker = 5 then @flattot else 0 end) as [5],
										sum(case when prjyr_marker = 6 then @flattot else 0 end) as [6],
										sum(case when prjyr_marker = 7 then @flattot else 0 end) as [7],
										sum(case when prjyr_marker = 8 then @flattot else 0 end) as [8],
										sum(case when prjyr_marker = 9 then @flattot else 0 end) as [9],
										sum(case when prjyr_marker = 10 then @flattot else 0 end) as [10],
										sum(case when prjyr_marker = 11 then @flattot else 0 end) as [11],
										sum(case when prjyr_marker = 12 then @flattot else 0 end) as [12],
										sum(case when prjyr_marker = 13 then @flattot else 0 end) as [13],
										sum(case when prjyr_marker = 14 then @flattot else 0 end) as [14],
										sum(case when prjyr_marker = 15 then @flattot else 0 end) as [15],
										prjyr_marker -- use yr for project year	
										 from @temp_prj					
										 group by prjyr_marker -- use yr for project year	
										 ) z
										 group by id
												 ) tot
												 on b.recid = tot.id
						end	
									
					
					end
					
					
					end
		
		END


	--	select 'here check', * from @resprof

		---	T1 CSV DATA

	--select 'resprof', * from @resprof 

		--	select 'temp', * from @temp 




		/*

		select 'here',
							a.PRJ_ID,
							a.dept,
							a.Department,
							a.ChargeType,
							a.descr,
							a.StaffNumber,
							a.acc_code,
							a.StaffTypeDesc,
							a.total,
							a.stdate,
							a.eddate,
							a.ROWID,
							a.hoursworked,
							a.workpackage,
							b.point,

								datediff(day, 
									--dateadd(day, 1-day(point), point)
									CASE
									WHEN @st > point THEN dateadd(day, 1-day(@st), @st)
									ELSE
									dateadd(day, 1-day(point), point) END
												,
																  dateadd(month, 1, 
																	  CASE
																		WHEN @ed < point THEN dateadd(day, 1-day(@ed), @ed)
																		ELSE
																			  dateadd(day, 1-day(point), point)
																			  END
																			  
																			  ))		as daycalc,
							

							case
							when isnull(a.basicTotal,'') = '' then val
							else
							b.CALC
							end ,
							a.basicTotal,
							val,
							b.CALC,

							b.Basicval,
							b.NIval,
							Superval
	
								
							 from
								@resprof a
									inner join @temp b
										on a.recid = b.RECID
*/

			--------	FLAT VALUE

			/*
			update @temp
								set daycount = datediff(day, 
									--dateadd(day, 1-day(point), point)
									CASE
									WHEN @st > point THEN dateadd(day, 1-day(@st), @st)
									ELSE
									dateadd(day, 1-day(point), point) END
												,
																  dateadd(month, 1, 
																	  CASE
																		WHEN @ed < point THEN dateadd(day, 1-day(@ed), @ed)
																		ELSE
																			  dateadd(day, 1-day(point), point)
																			  END
																			  
																			  ))
																			  */
			set @dayi = 0
			
		--	declare @dayimax int
			set @dayimax = ( select MAX(ROWID) from @temp )

			declare @val_DIFF float = 0
			declare @basicval_DIFF float = 0
			declare @nival_DIFF float = 0
			declare @superval_DIFF float = 0
			declare @sponval_DIFF float = 0
			
			while @dayi <= @dayimax
			begin
			
				if @dayi = @dayimax
				begin

					
					declare @valtotROUND DECIMAL(20,2)
					set @valtotROUND = ( SELECT CONVERT(DECIMAL(20,2),Total) FROM @resprof WHERE recid = @START_I )
					set @val_DIFF = ( ( Select SUM(convert(decimal(20,2),value)) from @temp ) - @valtotROUND )
					
					declare @basictotROUND DECIMAL(20,2)
					set @basictotROUND = ( SELECT CONVERT(DECIMAL(20,2),basicTotal) FROM @resprof WHERE recid = @START_I )
					set @basicval_DIFF = ( ( Select SUM(convert(decimal(20,2),basicval)) from @temp ) - @basictotROUND )

					declare @nitotROUND DECIMAL(20,2)
					set @nitotROUND = ( SELECT CONVERT(DECIMAL(20,2),niTotal) FROM @resprof WHERE recid = @START_I )
					set @nival_DIFF = ( ( Select SUM(convert(decimal(20,2),nival)) from @temp ) - @nitotROUND )

					declare @supertotROUND DECIMAL(20,2)
					set @supertotROUND = ( SELECT CONVERT(DECIMAL(20,2),superTotal) FROM @resprof WHERE recid = @START_I )
					set @superval_DIFF = ( ( Select SUM(convert(decimal(20,2),superval)) from @temp ) - @supertotROUND )

					declare @spontotROUND DECIMAL(20,2)
					set @spontotROUND = ( SELECT CONVERT(DECIMAL(20,2),SponTotal) *
													( case
													when isnull(finaladjust,'99999999999999') <> '99999999999999' then finaladjust
													--when finaladjust = 0   then 0
													when finaladjust IS null  then 100 
													when finaladjust = ''  then 100 
													else 
													finaladjust end / 100
													 )
													 FROM @resprof WHERE recid = @START_I )
					set @sponval_DIFF = ( ( Select SUM(convert(decimal(20,2),Sponval)) from @temp ) - @spontotROUND )

				end
			
				update @temp
				set daycount = 
					case
					when MONTH(point) = 2 then 28
					when @dayi <> @dayimax then
					DATEDIFF( d,point, 
								DATEADD (dd, -1, DATEADD(mm, DATEDIFF(mm, 0, point) + 1, 0))
								) + 1
					else

					DATEDIFF( d,DATEADD(mm, DATEDIFF(mm, 0,point), 0), 
								point
								) + 1
						end 

					/*
					case
					when MONTH(point) = 2 then 28
					else
					DATEDIFF(d,point, isnull(( select point from @temp	where ROWID = @dayi + 1 ),@ed+1) )
					end
					*/,
					value = CONVERT(decimal(20,2),value) - @val_DIFF,
					Basicval = CONVERT(decimal(20,2),Basicval) - @basicval_DIFF,
					NIval = CONVERT(decimal(20,2),NIval) - @nival_DIFF,
					Superval = CONVERT(decimal(20,2),Superval) - @superval_DIFF	--, 
					--Sponval = CONVERT(decimal(20,2),Sponval) - @sponval_DIFF
				where 
				ROWID = @dayi

				
				
			set @dayi = @dayi + 1
			
			end
			
					
/*
			declare @totdaysFLAT int
			set @totdaysFLAT = ( select sum( daycount ) from @temp 
								where RECID = @START_I )
				
			
			declare @FLATVAL float 
			set @FLATVAL  = (
								SELECT 
								 SUM(VAL)  / @totdaysFLAT * 1.00 AS FTE
									from @temp	
									where RECID = @START_I
									)

						
			update @temp
			set 
			VAL = b.value
				from @temp a
						inner join (
									select 
									id,
									(daycount*1.00 )
									*
									@FLATVAL
									as value
									from @temp	
									) b 
									on a.id = b.id
					*/


			-------
			
				INSERT INTO 		
				---		select * from
						UOE_T1_CSV_DATA
						
				
						select 
							a.PRJ_ID,
							a.dept,
							a.Department,
							a.ChargeType,
							a.descr,
							a.StaffNumber,
							a.acc_code,
							a.StaffTypeDesc,
							a.total,
							a.stdate,
							a.eddate,
							a.ROWID,
							a.hoursworked,
							a.workpackage,
							b.point,
							daycount,
							/*
								datediff(day, 
									--dateadd(day, 1-day(point), point)
									CASE
									WHEN @st > point THEN dateadd(day, 1-day(@st), @st)
									ELSE
									dateadd(day, 1-day(point), point) END
												,
																  dateadd(month, 1, 
																	  CASE
																		WHEN @ed < point THEN dateadd(day, 1-day(@ed), @ed)
																		ELSE
																			  dateadd(day, 1-day(point), point)
																			  END
																			  
																			  ))		as daycalc,
																			  */
							/*
							case
							when isnull(a.basicTotal,'') = '' then val
							else
							b.CALC
							end ,
							*/
							CONVERT(decimal(20,2),value),
							CONVERT(decimal(20,2),b.Basicval),
							CONVERT(decimal(20,2),b.NIval),
							CONVERT(decimal(20,2),Superval),

							recType,
							CONVERT(decimal(20,2),b.val),

							CONVERT(decimal(20,2),
							 
										case
										when 
											a.finaladjust is null then 
												CONVERT(decimal(20,2),Sponval)
												when a.finaladjust = ''  then CONVERT(decimal(20,2),Sponval) 
												else
								

								isnull(
									CONVERT(decimal(20,2),Sponval) *
										( case
											when a.finaladjust IS null  then 100 
											when a.finaladjust = ''  then 100 
											else 
											a.finaladjust end / 100
											 ),0)

											 /*
													isnull(
														Sponval *
															( case
																when isnull(finaladjust,0) = 0 then 100 else 
																finaladjust end / 100
																 ),0)
											*/
										END )
							AS
							Sponval,
							FTEval,
							matchedfund,
							case
							when 
							isnull(a.finaladjust,'99999999999999')  = '99999999999999' then null
							else a.finaladjust end 
								, 0 , 0, 0
							 from
								@resprof a
									inner join @temp b
										on a.recid = b.RECID
				
		SET @START_I = @START_I + 1
		
		--set @id = @id + 1
	END
	
	---------------------------------------------------------------------------------
	----------------Directly Incurred Costs START
	
	
	
	IF @type = 'prjyr'
	BEGIN
	
			
		DECLARE @RES_NON_STAFF TABLE
		(
		COSTNAME VARCHAR(900) ,
		VAL FLOAT,
		YR INT
		)

		declare @prjid int
		set @prjid = @prj_id

		declare @extra table
		 (                        
		 rowid int identity(1,1),
		  ID INT NULL,                        
		  Rate FLOAT NULL,                        
		  Units FLOAT NULL,                        
		  CatID INT NULL,                        
		  Description VARCHAR(900) NULL,                        
		  Name VARCHAR(100) NULL,                        
		  Code VARCHAR (30) NULL,                        
		  UnitType VARCHAR (50) NULL,                        
		  UnitTypeID INT NULL,                        
		  Sequence_No INT NULL,                        
		  CatChargeType VARCHAR (70) NULL,                        
		  StartDate DateTime NULL,                        
		  EndDate DateTime NULL,                        
		  ItemType INT NULL,                        
		  Sub_Element INT NULL,                        
		  VAT_Percent FLOAT NULL,                        
		  COST FLOAT NULL,                        
		  VAT_Type INT NULL,                        
		  VAT_Cost FLOAT NULL,                          
		  Cost_Before_VAT FLOAT NULL,                        
		  IsSwitchToConsumables BIT NULL,                        
		  OrgConumableLimit FLOAT NULL,                        
		  ConsumableCategory INT NULL,                        
		  Inflation_Factor_ID INT NULL,                        
		  Element INT NULL,                        
		  Effective_Date DATETIME NULL,                        
		  ProjectCode VARCHAR(20),                        
		  ProjectName VARCHAR(100),                        
		  DefaultChargeType VARCHAR(70),                        
		  ExceptionalRule VARCHAR(70),                        
		  RateOnDate Datetime,                        
		  VatDesc VARCHAR(200),                        
		  ProjectStart DateTime,                        
		  ActualElement INT,                      
		  ResearchStudent INT NULL                        
		 )  

		insert into @extra
		EXEC ProjectEquipmentCostDetailed @prjid,0,0,0,0

		delete from @extra
		where Element <> 2

		--	select * from @extra

				declare @ff int
				declare @ffmax int

				set @ff = ( select MIN(rowid) from @extra )
				set @ffmax = ( select MAX(rowid) from @extra )

				declare @cost table
				(
				id int,
				startdate datetime,
				enddate datetime,
				amount float
				)

				insert into @cost
				EXEC EquipmentCostFromTable @prjid,1

				delete from @cost 
				where id not in ( select id from @extra )

				--	SELECT * FROM @cost

				DECLARE @indirectcost table (
					id int identity(1,1),
					[Coin] [int] NOT NULL,
					[Description] [varchar](800) NOT NULL,
					[StartDate] [datetime] NULL,
					[EndDate] [datetime] NULL,
					[Amount] [float] NOT NULL
					)

				
								
				DECLARE @IDCHK INT

				DECLARE @DESCR VARCHAR(300)

				WHILE @ff <= @ffmax

				BEGIN

				DELETE FROM @indirectcost
				

				declare @tempDICOSTS table (
								id int identity(1,1),
								point datetime,
								acad_marker int,
								ACAD_YR NVARCHAR(50),
								MONTHS INT,
								yr int,
								mark int,
								daysinmth int,
								val float,
								--calc float,
								prjyr_marker int,
								totdays int,
								dayval float,
								mthval float
								)

					SET @IDCHK = ( SELECT ID FROM @extra WHERE rowid = @ff )
					
					SET @DESCR = ( SELECT [Name] FROM @extra WHERE rowid = @ff )

					--select * from @cost

				--DELETE FROM @cost
				--WHERE ID <> @IDCHK

					--select * from @cost

				insert into @indirectcost	
				select 
				A.id,
				B.[Name],
				A.startdate,
				A.enddate,
				A.amount
				 from @cost A
				INNER JOIN @extra B
					ON A.id = B.ID
				WHERE --B.[Name] = @DESCR
				A.id = @IDCHK
					


				------------------------------------------------------------------------

					--declare @prjyr_marker int
					set @prjyr_marker = 1
					
					
								--declare @st datetime
								set @st = ( select MIN(startdate)
													FROM  
													@indirectcost
													)
								
								--declare @mthstart int					
								set @mthstart = 8--MONTH(@st)				
								
								--declare @ed datetime
								set @ed = ( select MAX(enddate)
													FROM  
													@indirectcost
													)
													
								--declare @i int
								set @i = 0
								
								--declare @mth_start int
								set @mth_start = ( select MONTH(@st)	)

							--- length of the project
								--declare @month_between int
								set @month_between = ( 
														SELECT 
														DBO.UOE_MonthdIFF(@st, @ed)+1
														)
														
							
							--	details so far
							--select * from @excel_budg							
							--	select @month_between
								
							--select @month_between
											
								DECLARE @ACAD_YEAR NVARCHAR(50)
								set @ACAD_YEAR = ''
								SET @ACAD_YEAR = 
													CASE
													WHEN MONTH(@ST) >= 8 THEN 
													CONVERT(VARCHAR(4),YEAR(@ST)) + '/' + RIGHT(CONVERT(VARCHAR(4),YEAR(@ST)+1),2)
													ELSE 
													CONVERT(VARCHAR(4),YEAR(@ST)-1) + '/' + RIGHT(CONVERT(VARCHAR(4),YEAR(@ST)),2) 
													END
											

								--declare @yr int
								set @yr = 1	

								--declare @yr_marker int
								set @yr_marker = 0			

								--declare @mths int
								set @mths = 12


								--declare @mths_chk int
								set @mths_chk = 12
													
								while @i <= @month_between
								begiN
									-- if august then year + 1
									set @yr = ( 
										select 
										case 
										when month(dateadd(m,@i, @st)) = 8 then @yr +1 
										else @yr end
										)
										
										
									set @ACAD_YEAR = ( 
										select 
										case 
										when month(dateadd(m,@i, @st)) = 8 then 
										
										convert(varchar,
											convert(int,left(@ACAD_YEAR,4)) + 1 ) 
											+ '/' +
											RIGHT(
											'0'+
											convert(varchar,
											convert(int,right(@ACAD_YEAR,2)) + 1 ) 
											,2)
										

										else @ACAD_YEAR end
										)	
										
									-- if year point reached, check how many months left	
									if @i in (12, 24, 36, 48, 60, 72, 84, 96, 108, 120, 132, 144 )  
										begin
										
											if @month_between - @i < 12
											begin
											set @mths = @month_between - @i
												if @mths = 0
												begin
													set @mths = 1 
												end
											end
										end
										
									if MONTH(dateadd(m,@i, @st)) = @mth_start
									begin
										set @yr_marker = @yr_marker + 1
									end
										
								if MONTH( dateadd(m,@i, @st) ) = 8--@mthstart
								begin
								set @prjyr_marker = @prjyr_marker + 1
								end
										
								insert into @tempDICOSTS (point, acad_marker, ACAD_YR, MONTHS, yr, prjyr_marker)
									select 
										dateadd(m,@i, @st),
										@yr,
										@ACAD_YEAR,
										@mths,
										@yr_marker,
										@prjyr_marker
								
									set @i = @i + 1

								end						
					
								delete from @tempDICOSTS
								where id > @month_between

								-- ensure date profile doesn't exceed end date for project
								delete from @tempDICOSTS
								where point > @ed
								
					--	calculate days within the months
					update @tempDICOSTS
					set daysinmth = 
								datediff(day, dateadd(day, 1-day(point), point),
										  dateadd(month, 1, dateadd(day, 1-day(point), point)))			

					update @tempDICOSTS
					set daysinmth = 28
						where DATEPART(month,point) = 2
					----- layout of project profile by month
					
								
					update @tempDICOSTS
					set val = 
								( Select amount from @indirectcost
									where point between StartDate and EndDate),
						mark = 	( Select id from @indirectcost
									where point between StartDate and EndDate)

								 
					update @tempDICOSTS
					set 
					totdays = b.tot
					from @tempDICOSTS a
						inner join 
							(
							select SUM(daysinmth) as tot, mark					
								 from @tempDICOSTS	a
								 group by mark
								) b on
										a.mark = b.mark

					--select * from @temp

					
					update @tempDICOSTS
					set dayval = val / totdays
					
					update @tempDICOSTS
					set mthval = daysinmth * dayval
									
					--	select * from @temp	a
			--	SELECT 'indirects', * FROM @indirectcost
			--	select * from @temp	 
					
					INSERT INTO @RES_NON_STAFF	 
					select @DESCR, SUM(mthval),
						yr
						 from @tempDICOSTS	a
						 group by yr
						 	 
					
					SET @ff = @ff + 1
					
					END
					
			/*		
					SELECT 
					--'Directly Incurred Costs' AS CHARGETYPE,
					COSTNAME,
					SUM( CASE WHEN YR = 1 THEN ISNULL(VAL,0) ELSE 0 END ) AS Y1,
					SUM( CASE WHEN YR = 2 THEN ISNULL(VAL,0) ELSE 0 END ) AS Y2,
					SUM( CASE WHEN YR = 3 THEN ISNULL(VAL,0) ELSE 0 END ) AS Y3,
					SUM( CASE WHEN YR = 4 THEN ISNULL(VAL,0) ELSE 0 END ) AS Y4,
					SUM( CASE WHEN YR = 5 THEN ISNULL(VAL,0) ELSE 0 END ) AS Y5,
					SUM( CASE WHEN YR = 6 THEN ISNULL(VAL,0) ELSE 0 END ) AS Y6,
					SUM( CASE WHEN YR = 7 THEN ISNULL(VAL,0) ELSE 0 END ) AS Y7,
					SUM( CASE WHEN YR = 8 THEN ISNULL(VAL,0) ELSE 0 END ) AS Y8,
					SUM( CASE WHEN YR = 9 THEN ISNULL(VAL,0) ELSE 0 END ) AS Y9,
					SUM( CASE WHEN YR = 10 THEN ISNULL(VAL,0) ELSE 0 END ) AS Y10,
					SUM( CASE WHEN YR = 11 THEN ISNULL(VAL,0) ELSE 0 END ) AS Y11,
					SUM( CASE WHEN YR = 12 THEN ISNULL(VAL,0) ELSE 0 END ) AS Y12,
					SUM( CASE WHEN YR = 13 THEN ISNULL(VAL,0) ELSE 0 END ) AS Y13,
					SUM( CASE WHEN YR = 14 THEN ISNULL(VAL,0) ELSE 0 END ) AS Y14,
					SUM( CASE WHEN YR = 15 THEN ISNULL(VAL,0) ELSE 0 END ) AS Y15
					FROM @RES_NON_STAFF
					GROUP BY COSTNAME
			*/
	
	/*
	uat 2015 11 15
	
	UPDATE @resprof
	SET 
	[1] = B.Y1,
	[2] = B.Y2,
	[3] = B.Y3,
	[4] = B.Y4,
	[5] = B.Y5,
	[6] = B.Y6,
	[7] = B.Y7,
	[8] = B.Y8,
	[9] = B.Y9,
	[10] = B.Y10,
	[11] = B.Y11,
	[12] = B.Y12,
	[13] = B.Y13,
	[14] = B.Y14,
	[15] = B.Y15
	
	FROM @resprof A
		INNER JOIN (
					SELECT 
					--'Directly Incurred Costs' AS CHARGETYPE,
					COSTNAME,
					SUM( CASE WHEN YR = 1 THEN ISNULL(VAL,0) ELSE 0 END ) AS Y1,
					SUM( CASE WHEN YR = 2 THEN ISNULL(VAL,0) ELSE 0 END ) AS Y2,
					SUM( CASE WHEN YR = 3 THEN ISNULL(VAL,0) ELSE 0 END ) AS Y3,
					SUM( CASE WHEN YR = 4 THEN ISNULL(VAL,0) ELSE 0 END ) AS Y4,
					SUM( CASE WHEN YR = 5 THEN ISNULL(VAL,0) ELSE 0 END ) AS Y5,
					SUM( CASE WHEN YR = 6 THEN ISNULL(VAL,0) ELSE 0 END ) AS Y6,
					SUM( CASE WHEN YR = 7 THEN ISNULL(VAL,0) ELSE 0 END ) AS Y7,
					SUM( CASE WHEN YR = 8 THEN ISNULL(VAL,0) ELSE 0 END ) AS Y8,
					SUM( CASE WHEN YR = 9 THEN ISNULL(VAL,0) ELSE 0 END ) AS Y9,
					SUM( CASE WHEN YR = 10 THEN ISNULL(VAL,0) ELSE 0 END ) AS Y10,
					SUM( CASE WHEN YR = 11 THEN ISNULL(VAL,0) ELSE 0 END ) AS Y11,
					SUM( CASE WHEN YR = 12 THEN ISNULL(VAL,0) ELSE 0 END ) AS Y12,
					SUM( CASE WHEN YR = 13 THEN ISNULL(VAL,0) ELSE 0 END ) AS Y13,
					SUM( CASE WHEN YR = 14 THEN ISNULL(VAL,0) ELSE 0 END ) AS Y14,
					SUM( CASE WHEN YR = 15 THEN ISNULL(VAL,0) ELSE 0 END ) AS Y15
					FROM @RES_NON_STAFF
					GROUP BY COSTNAME
					) B ON
					B.COSTNAME = A.descr
	WHERE ChargeType = 'Directly Incurred Costs'
	
	*/
	END
	
	----------------Directly Incurred Costs END
	---------------------------------------------------------------------------------
	
	-------------------------------- staff spell adjust for prjyr
	
	


	-----------------------------------------------------------
	-- returned results
	select 
	[id]
      ,a.[PRJ_ID]
      ,[dept]
      ,[Department]
      ,[ChargeType]
      ,[descr]
      ,[StaffNumber]
      ,[acc_code]
      ,[StaffTypeDesc]
      ,[1]
      
	  ,[2]
      ,[3]
      ,[4]
      ,[5]
	  ,[6]
      ,[7]
      ,[8]
      ,[9]
      ,[10]
      ,[11]
      ,[12]
      ,[13]
      ,[14]
      ,[15]
      ,isnull([TOTAL],0) as [total]
      ,[stdate]
      ,[eddate]
      ,a.ROWID
      ,
	  case
	  when matchedfund = 'on' then 0
	  else
	  
	--  case when A.PRJ_ID = 10569 then
			case
					--	when 
						--	finaladjust is null  then 
							--	SponsorCONT
						when 
							isnull(finaladjust,'999999999999999') = '999999999999999'   then 
								SponsorCONT
								else
			
									isnull(
										SponTotal *
											( case
											when finaladjust = 0   then 0
												when finaladjust IS null  then 100 
												when finaladjust = ''  then 100 
												else 
												finaladjust end / 100
												 ),0)
						ENd
				  
/*
	  else
						case
						when 
							finaladjust is null  then 
								SponsorCONT
						when 
							isnull(finaladjust,0) <> 0  then 
								SponsorCONT
								else
			
									isnull(
										SponTotal *
											( case
											when finaladjust = 0   then 0
												when finaladjust IS null  then 100 
												when finaladjust = ''  then 100 
												else 
												finaladjust end / 100
												 ),0)
						END
				  END	*/
		end
		
	  as SponsorCONT
	  /*
	  case
	  when matchedfund = 'on' then 0
	  else
	  isnull(
			SponsorCONT
				*	( case
						when isnull(finaladjust,0) = 0 then 100 else 
						finaladjust end / 100 )
			
			,0) 
	  end 
	  as SponsorCONT
	  */
		,isnull(hoursworked,0) as hoursworked
		,PFACT_PAC_Mapping
		,CASE
		when recType = 'fte related' then ''
		else
			case
			WHEN ISNULL(B.t1_bud_temp_heading, '') = '' THEN
			'T1 Cost mappning needed for:</br><font size="2" color="red">' + B.catCode + ' : ' + PFACT_PAC_Mapping + '</font></br>Please contact admin'
			ELSE			
			b.income_template + ': ' + B.t1_bud_temp_heading
			END 
			
		end AS new_PACcat
		,flat_line
	,CANFLAT
	,workpackage
	,dependdate
	,matchedfund
	,
	case
	  when matchedfund = 'on' then null
	  else
			finaladjust
		end as finaladjust
		, isnull(A.SponTotal,0) as SponTotal
		--	, matchedfund, finaladjust, A.*
	 from @resprof A
	 /*
	 LEFT OUTER JOIN 
	 ( SELECT DISTINCT [project_pcf_cost_type_descr] FROM [dbo].[PAC_PFACT_cost_type_LKUP]
	 ) B 
	 ON A.new_PACcat = B.[project_pcf_cost_type_descr]
	 */

		left outer join (

		Select distinct prj_id, rowid, catCode, t1_bud_temp_heading , catDesc , income_template from T1_Staff_Data
			where prj_id = @prj_id
		union all 
		Select distinct prj_id, rowid, catCode, t1_bud_temp_heading, catDesc , income_template from T1_Other_Data
			where prj_id = @prj_id
		)
			b on b.rowid = A.ROWID

	 -------------------
	 		--		[dbo].[UOE_P_PRJ_Proposal_DEPT_Split_GET] 10388 , 'finyr'
	 
	 -------------------	FTE UPDATE
		
				declare @fte_data table
		(
			id int identity(1,1),
			descr varchar(500),
			rowid int,
			stafftypedesc varchar(500),
			st_date datetime,
			ed_date datetime
		)

		insert into @fte_data
			SELECT distinct descr, ROWID, StaffTypeDesc, stdate, eddate
			FROM UOE_T1_CSV_DATA
					WHERE PRJ_ID = @prj_id
					and StaffTypeDesc in (
						'Estate Lab' ,
						'Estate Desk',
						'Infrastructure Lab' ,
						'Infrastructure Desk',
						'Indirect' 
						)
					--order by ROWID 
		
		
		--	select 'fte date', * from @fte_data


		----------------
		/*
		declare @mthstart_prj int
		declare @descr varchar(500)
		declare @st_prj datetime
		declare @prjyr_marker_prj int
		declare @ed_prj datetime
		declare @i_prj int
		declare @mth_start_prj int
		declare @month_between_prj int
		declare @yr_prj int
		declare @yr_marker_prj int
		declare @mths_prj int
		declare @mths_chk_prj int
		declare @i int

		*/
		declare @ftei int
		set @ftei = 1

		declare @maxftei int
		set @maxftei = ( select MAX(id) from @fte_data )

		declare @rowid int, @stafftypedesc varchar(500)

		
		declare @st_date_FTE DATETIME
		declare @ED_date_FTE DATETIME

		while @ftei <= @maxftei

		begin

		SET @st_date_FTE = ( select st_date from @fte_data where id = @ftei )
				SET @ED_date_FTE = ( select ED_date from @fte_data where id = @ftei )

				declare @temp_prjFTE table (
				id int identity(1,1),
				point datetime,
				amount float,
				totaldays int,
				dayrate float
				)

			set @descr = ( select descr from @fte_data where id = @ftei )
			set @rowid = ( select rowid from @fte_data where id = @ftei )
			set @stafftypedesc = ( select stafftypedesc from @fte_data where id = @ftei )

				
				
				set @st_prj = ( SELECT 
						  [Start_Date]
							  FROM [dbo].[Project_Proposal]
									where id = @prj_id
									)
									
				set @mthstart_prj = 8--MONTH(@st)		
				
				
				
				if DATEPART(MONTH,@st_prj) = 8
				begin
					set @prjyr_marker_prj = 0
				end
				
				if DATEPART(MONTH,@st_prj) <> 8
				begin
					set @prjyr_marker_prj = 1
				end		
				
				
		
				
				set @ed_prj = ( SELECT 
							  [End_Date]
						  FROM [dbo].[Project_Proposal]
						  where id = @prj_id
									)	
									
	--	select @st_prj, @ed_prj									
									
				
				
				
				set @i_prj = 0
				
			

				
				set @mth_start_prj = ( select MONTH(@st_prj)	)


				
				set @month_between_prj = ( 
										SELECT 
										DBO.UOE_MonthdIFF(@st_prj, @ed_prj)+1
										)
										
				--select @month_between
							
				
				set @yr_prj = 1	

				
				set @yr_marker_prj = 0			

				
				set @mths_prj = 12

				
				set @mths_chk_prj = 12
									
				while @i_prj <= @month_between_prj
				begin
					-- if august then year + 1
					set @yr_prj = ( 
						select 
						case 
						when month(dateadd(m,@i_prj, @st_prj)) = 8 then @yr_prj +1 
						else @yr_prj end
						)
						
					-- if year point reached, check how many months left	
					if @i_prj in (12, 24, 36, 48, 60, 72, 84, 96, 108, 120, 132, 144 )  
						begin
						
							if @month_between_prj - @i_prj < 12
							begin
							set @mths_prj = @month_between_prj - @i_prj
								if @mths_prj = 0
								begin
									set @mths_prj = 1 
								end
							end
						end
						
					if MONTH(dateadd(m,@i_prj, @st_prj)) = @mth_start_prj
					begin
						set @yr_marker_prj = @yr_marker_prj + 1
					end
						
				if MONTH( dateadd(m,@i_prj, @st_prj) ) = 8--@mthstart
				begin
				set @prjyr_marker_prj = @prjyr_marker_prj + 1
				end
						
				insert into @temp_prjFTE ( point)
					select 
						--dateadd(m,@i_prj, @st_prj)
						case 
									when @i_prj = 0 then @st_date_FTE
									else
									--	dateadd(m,@i, @st) 
									DATEADD(mm, DATEDIFF(mm, 0, dateadd(m,@i_prj, @st_PRJ) ), 0)
									end
						
				
					set @i_prj = @i_prj + 1
					
				end						


				delete from @temp_prjFTE
				where id > @month_between_prj
				
				-- ensure date profile doesn't exceed end date for project
				delete from @temp_prjFTE
				where point > @ed_prj


				
				update @temp_prjFTE
							set point = @ST_date_FTE
							where id = ( Select MIN(id) from @temp_prjFTE )

				update @temp_prjFTE
							set point = @ED_date_FTE
							where id = ( Select MAX(id) from @temp_prjFTE )

				--SELECT @ftei, * FROM @temp_prjFTE

				--	RESET TABLE CONTENT
				update @temp_prjFTE
					set 
					amount = 0,
					totaldays = 0,
					dayrate = 0

										update @temp_prjFTE
										set 
										amount = B.amount,
										totaldays = b.totdays,
										dayrate = B.dayrate
										--SELECT * 
										FROM
										 @temp_prjFTE A
												INNER JOIN (

														 SELECT CostIndirectCost.Coin, 
															case CostIndirectCost.StaffType         
															when 1 then  Staff_Details.SURNAME + ' ' + Staff_Details.FIRSTNAME        
															when 2 then Non_Standard_Staff_Using_PayScale.STAFF_NAME        
															when 3 then Project_Proposal_Other_Staff_Details.Name        
															when 4 then  StaffDetailsOff.SURNAME + ' ' + StaffDetailsOff.FIRSTNAME        
															when 5 then OffScaleGrade.StaffName         
															when 6 then Non_Standard_Staff_Using_PayScale.STAFF_NAME         
															end as Name,        
															Expenditure_Template_Expenditure.GroupID, 
															Expenditure_Group.Description ExpenditureGroup,
															ExpType,
															LUP_Expenditure.Description ExpenditureType,                
															(CostIndirectCost.FTE) FTE, 
															(CostIndirectCost.Amount) *	( Select top 1 
															case
															when finaladjust IS null then 100
															when finaladjust = '' then 100
															else finaladjust end
															
															 / 100 FROM @resprof WHERE ROWID = @rowid ) Amount,
															LUP_FIX_ICT_Class.ICT_Class_Description AS ChargeType ,               
															--Project_Proposal.Start_Date ProjectStart,Project_Proposal.End_Date ProjectEnd,
															CostIndirectCost.StartDate,
															CostIndirectCost.EndDate,
															DATEDIFF(d, CostIndirectCost.StartDate, CostIndirectCost.EndDate)+
																( 
																						case
																											when 
																												(
																													(
																													 dbo.IsLeapYearLkup(DATEPART(YEAR,StartDate)) = 1 
																													)
																													Or
																													(
																													 dbo.IsLeapYearLkup(DATEPART(YEAR,EndDate)) = 1 
																													) 
																												)
																												and dbo.februarybetweendates(StartDate, EndDate) = 1
																												then 0
																												else 1 end )
																
															
															
															 as totdays,
															CostIndirectCost.Amount / 
																			(DATEDIFF(d, CostIndirectCost.StartDate, CostIndirectCost.EndDate)+
																				( 
																						case
																											when 
																												(
																													(
																													 dbo.IsLeapYearLkup(DATEPART(YEAR,StartDate)) = 1 
																													)
																													Or
																													(
																													 dbo.IsLeapYearLkup(DATEPART(YEAR,EndDate)) = 1 
																													) 
																												)
																												and dbo.februarybetweendates(StartDate, EndDate) = 1
																												then 0
																												else 1 end )
																			) as dayrate
	   
															FROM CostIndirectCost                
															INNER JOIN LUP_Expenditure ON LUP_Expenditure.ID=CostIndirectCost.ExpType                
															INNER JOIN Expenditure_Template_Expenditure ON Expenditure_Template_Expenditure.ID=ExpenditureFTEID                
															INNER JOIN Expenditure_Group ON Expenditure_Group.ID=CostIndirectCost.GroupID                
															INNER JOIN LUP_FIX_ICT_Class ON LUP_Expenditure.ICT_Class_ID = LUP_FIX_ICT_Class.ICT_Class_ID                
															INNER JOIN Project_Proposal ON CostIndirectCost.Project=Project_Proposal.ID                
															INNER JOIN Project_Proposal_Income_Template ON Project_Proposal_Income_Template.Project_Proposal = Project_Proposal.ID                
															INNER JOIN Income_Template_Funders ON Project_Proposal_Income_Template.Income_Template_Funder = Income_Template_Funders.ID                 
															INNER JOIN  Funders ON Income_Template_Funders.Funder = Funders.Id                
															--INNER JOIN LUP_Income_Template ON Income_Template_Funders.Income_Template = LUP_Income_Template.Id        
															LEFT JOIN Project_Proposal_Co_Investigators on Project_Proposal_Co_Investigators.ID= CostIndirectCost.Coin        
															LEFT JOIN Project_Proposal_Other_Staff_Details ON Project_Proposal_Other_Staff_Details.ID = CostIndirectCost.Coin AND CostIndirectCost.StaffType=3  
															LEFT JOIN Staff_Details ON Staff_Details.ID = Project_Proposal_Co_Investigators.co_investigator  AND (Non_Standard=1 AND CostIndirectCost.StaffType=1)      
															LEFT JOIN Non_Standard_Staff_Using_PayScale ON Non_Standard_Staff_Using_PayScale.ID = Project_Proposal_Co_Investigators.co_investigator AND (CostIndirectCost.StaffType=2 OR CostIndirectCost.StaffType=6)       
															LEFT JOIN OffScaleGrade On  OffScaleGrade.ID =PROJECT_PROPOSAL_CO_INVESTIGATORS.Co_Investigator AND (CostIndirectCost.StaffType=4 OR CostIndirectCost.StaffType=5)   
															LEFT JOIN Staff_Details AS StaffDetailsOff ON StaffDetailsOff.ID = OffScaleGrade.StaffID AND OffScaleGrade.Staffname IS NULL             
															WHERE Project=@prj_id  AND UseInflation=1  
															and CostIndirectCost.Amount <> 0
															AND CoIn = @rowid
															AND LUP_Expenditure.Description = @stafftypedesc
															) B ON A.point = B.StartDate

			



				--	select * from @temp_prjFTE


				
				set @i = 1

				declare @maxi int
				set @maxi = ( select MAX(id) from @temp_prjFTE )

					declare @amout float
					declare @totaldays int
					declare @dayrate float

					declare @amoutCARRYOVER float
					declare @totaldaysCARRYOVER int
					declare @dayrateCARRYOVER float

				
				while @i <= @maxi
				begin

						if ( select isnull(amount,0) from @temp_prjFTE where id = @i ) = 0
						begin
							update @temp_prjFTE
								set 
									amount = @amoutCARRYOVER,
										totaldays = @totaldaysCARRYOVER,
										dayrate = @dayrateCARRYOVER
									where id = @i
						end


						--	select 'sdfsdfsdf', * from @temp_prjFTE

						if ( select isnull(amount,0) from @temp_prjFTE where id = @i ) <> 0
						begin

							set @amout = ( select amount from @temp_prjFTE where id = @i )
							set @totaldays = ( select totaldays from @temp_prjFTE where id = @i )
							set @dayrate = ( select dayrate from @temp_prjFTE where id = @i )

							update @temp_prjFTE
								set 
									amount = @amout,
									totaldays = @totaldays,
									dayrate = @dayrate
									where id = @i

								set @amoutCARRYOVER = @amout
								set @totaldaysCARRYOVER = @totaldays
								set @dayrateCARRYOVER = @dayrate

						end
						

						set @i = @i + 1

				end


				--	select 'sdfsdfsdf', * from @temp_prjFTE

					--	select * from @temp_prjFTE

					/*
					select 'results', b.daycalc * a.dayrate as newrate, a.*, b.daycalc * a.dayrate, b.* 
					from 
						UOE_T1_CSV_DATA b
						left outer join
								@temp_prjFTE a
									on a.point = b.point
										WHERE PRJ_ID = @prj_id
										AND ROWID = @rowid
										AND StaffTypeDesc = @stafftypedesc
						*/
				
				
				
				update UOE_T1_CSV_DATA
				set val = (b.daycalc * a.dayrate)
				from 
					UOE_T1_CSV_DATA b
					left outer join
							@temp_prjFTE a
								on a.point = b.point
									WHERE PRJ_ID = @prj_id
									AND ROWID = @rowid
									AND StaffTypeDesc = @stafftypedesc

			--	ORDER BY a.POINT


				set @ftei = @ftei + 1

			end


	 -------------------	FTE UPDATE


	 -------------------	STAFF PROFILE UPDATE
		
				declare @fte_data_staff table
		(
			id int identity(1,1),
			descr varchar(500),
			rowid int,
			stafftypedesc varchar(500),
			st_date datetime,
			ed_date datetime
		)

		insert into @fte_data_staff
			SELECT distinct descr, ROWID, StaffTypeDesc, stdate, eddate
			FROM UOE_T1_CSV_DATA
					WHERE PRJ_ID = @prj_id
					and StaffTypeDesc not in (
						'Estate Lab' ,
						'Estate Desk',
						'Infrastructure Lab' ,
						'Infrastructure Desk',
						'Indirect' 
						)
						--	and ROWID = 37365
					--order by ROWID 
		
		
			--	select 'fte date', * from @fte_data_staff


		----------------
		/*
			declare @mthstart_prj int
			declare @descr varchar(500)
			declare @st_prj datetime
			declare @prjyr_marker_prj int
			declare @ed_prj datetime
			declare @i_prj int
			declare @mth_start_prj int
			declare @month_between_prj int
			declare @yr_prj int
			declare @yr_marker_prj int
			declare @mths_prj int
			declare @mths_chk_prj int
			declare @i int

			declare @rowid int, @stafftypedesc varchar(500)

			declare @maxi int
		*/
		
		declare @ftei_staff int
		set @ftei_staff = 1

		declare @maxftei_staff int
		set @maxftei_staff = ( select MAX(id) from @fte_data_staff )

		declare @st_date_STAFF DATETIME
		declare @ED_date_STAFF DATETIME

		while @ftei_staff <= @maxftei_staff

		begin

		SET @st_date_STAFF = ( select st_date from @fte_data_staff where id = @ftei_staff )
				SET @ED_date_STAFF = ( select ED_date from @fte_data_staff where id = @ftei_staff )

				declare @temp_prSTAFF table (
				id int identity(1,1),
				point datetime,
				BASICamount float,
				BASICtotaldays int,
				BASICdayrate float,
				NIamount float,
				NItotaldays int,
				NIdayrate float,
				Superamount float,
				Supertotaldays int,
				Superdayrate float,
				OAamount float,
				OAtotaldays int,
				OAdayrate float
				)

			set @descr = ( select descr from @fte_data_staff where id = @ftei_staff )
			set @rowid = ( select rowid from @fte_data_staff where id = @ftei_staff )
			set @stafftypedesc = ( select stafftypedesc from @fte_data_staff where id = @ftei_staff )

				
				
				set @st_prj = ( SELECT 
						  [Start_Date]
							  FROM [dbo].[Project_Proposal]
									where id = @prj_id
									)
									
				set @mthstart_prj = 8--MONTH(@st)		
				
				
				
				if DATEPART(MONTH,@st_prj) = 8
				begin
					set @prjyr_marker_prj = 0
				end
				
				if DATEPART(MONTH,@st_prj) <> 8
				begin
					set @prjyr_marker_prj = 1
				end		
				
				
		
				
				set @ed_prj = ( SELECT 
							  [End_Date]
						  FROM [dbo].[Project_Proposal]
						  where id = @prj_id
									)	
									
	--	select @st_prj, @ed_prj									
									
				
				
				
				set @i_prj = 0
				
			

				
				set @mth_start_prj = ( select MONTH(@st_prj)	)


				
				set @month_between_prj = ( 
										SELECT 
										DBO.UOE_MonthdIFF(@st_prj, @ed_prj)+1
										)
										
				--select @month_between
							
				
				set @yr_prj = 1	

				
				set @yr_marker_prj = 0			

				
				set @mths_prj = 12

				
				set @mths_chk_prj = 12
									
				while @i_prj <= @month_between_prj
				begin
					-- if august then year + 1
					set @yr_prj = ( 
						select 
						case 
						when month(dateadd(m,@i_prj, @st_prj)) = 8 then @yr_prj +1 
						else @yr_prj end
						)
						
					-- if year point reached, check how many months left	
					if @i_prj in (12, 24, 36, 48, 60, 72, 84, 96, 108, 120, 132, 144 )  
						begin
						
							if @month_between_prj - @i_prj < 12
							begin
							set @mths_prj = @month_between_prj - @i_prj
								if @mths_prj = 0
								begin
									set @mths_prj = 1 
								end
							end
						end
						
					if MONTH(dateadd(m,@i_prj, @st_prj)) = @mth_start_prj
					begin
						set @yr_marker_prj = @yr_marker_prj + 1
					end
						
				if MONTH( dateadd(m,@i_prj, @st_prj) ) = 8--@mthstart
				begin
				set @prjyr_marker_prj = @prjyr_marker_prj + 1
				end
						
				insert into @temp_prSTAFF ( point)
					select 
						--dateadd(m,@i_prj, @st_prj)
						case 
									when @i_prj = 0 then @st_date_STAFF
									else
									--	dateadd(m,@i, @st) 
									DATEADD(mm, DATEDIFF(mm, 0, dateadd(m,@i_prj, @st_PRJ) ), 0)
									end
					set @i_prj = @i_prj + 1
					
				end						


				delete from @temp_prSTAFF
				where id > @month_between_prj
				
				-- ensure date profile doesn't exceed end date for project
				delete from @temp_prSTAFF
				where point > @ed_prj


				update @temp_prSTAFF
							set point = @ST_date_STAFF
							where id = ( Select MIN(id) from @temp_prSTAFF )

				update @temp_prSTAFF
							set point = @ED_date_STAFF
							where id = ( Select MAX(id) from @temp_prSTAFF )

				--SELECT @ftei_staff, * FROM @temp_prSTAFF

				--	RESET TABLE CONTENT
				update @temp_prSTAFF
					set 
					BASICamount = 0,
					BASICtotaldays = 0,
					BASICdayrate = 0,
					NIamount = 0,
					NItotaldays = 0,
					NIdayrate = 0,
					Superamount = 0,
					Supertotaldays = 0,
					Superdayrate = 0,
					OAamount = 0,
					OAtotaldays = 0,
					OAdayrate = 0

										update @temp_prSTAFF
										set 
										BASICamount = B.BASIC,
										BASICtotaldays = b.totdays,
										BASICdayrate = B.basicdayrate,

										NIamount = B.NiOut,
										NItotaldays = b.totdays,
										NIdayrate = B.nidayrate,

										Superamount = B.Super,
										Supertotaldays = b.totdays,
										Superdayrate = B.superdayrate,

										OAamount = B.Other_Allowances,
										OAtotaldays = b.totdays,
										OAdayrate = B.OAdayrate

										--SELECT * 
										FROM
										 @temp_prSTAFF A
												INNER JOIN (


												SELECT
												CoinID,
												StartDate,

														Basic 
													--end as Basic
														,NI,NIOUT,Super,Total,cs.StaffType,cs.Other_Allowances
														,DATEDIFF(d, StartDate, EndDate)+
														( 
																						case
																											when 
																												(
																													(
																													 dbo.IsLeapYearLkup(DATEPART(YEAR,StartDate)) = 1 
																													)
																													Or
																													(
																													 dbo.IsLeapYearLkup(DATEPART(YEAR,EndDate)) = 1 
																													) 
																												)
																												and dbo.februarybetweendates(StartDate, EndDate) = 1
																												then 0
																												else 1 end )
														
														
														 as totdays,
																									basic / (DATEDIFF(d, StartDate, EndDate)+
																										( case
																											when 
																												(
																													(
																													 dbo.IsLeapYearLkup(DATEPART(YEAR,StartDate)) = 1 
																													)
																													Or
																													(
																													 dbo.IsLeapYearLkup(DATEPART(YEAR,EndDate)) = 1 
																													) 
																												)
																												and dbo.februarybetweendates(StartDate, EndDate) = 1
																												then 0
																												else 1 end )
																									) as basicdayrate,
																									niout / (DATEDIFF(d, StartDate, EndDate)+
																										( case
																											when 
																												(
																													(
																													 dbo.IsLeapYearLkup(DATEPART(YEAR,StartDate)) = 1 
																													)
																													Or
																													(
																													 dbo.IsLeapYearLkup(DATEPART(YEAR,EndDate)) = 1 
																													) 
																												)
																												and dbo.februarybetweendates(StartDate, EndDate) = 1
																												then 0
																												else 1 end )
																									) as nidayrate,
																									super / (DATEDIFF(d, StartDate, EndDate)+
																										( case
																											when 
																												(
																													(
																													 dbo.IsLeapYearLkup(DATEPART(YEAR,StartDate)) = 1 
																													)
																													Or
																													(
																													 dbo.IsLeapYearLkup(DATEPART(YEAR,EndDate)) = 1 
																													) 
																												)
																												and dbo.februarybetweendates(StartDate, EndDate) = 1
																												then 0
																												else 1 end )
																									) as superdayrate,
																									cs.Other_Allowances / (DATEDIFF(d, StartDate, EndDate)+
																										( case
																											when 
																												(
																													(
																													 dbo.IsLeapYearLkup(DATEPART(YEAR,StartDate)) = 1 
																													)
																													Or
																													(
																													 dbo.IsLeapYearLkup(DATEPART(YEAR,EndDate)) = 1 
																													) 
																												)
																												and dbo.februarybetweendates(StartDate, EndDate) = 1
																												then 0
																												else 1 end )
																									) as OAdayrate
														/*,              
													cs.PayScale, 
													LUP_Sub_Element.Description AS PayScaleDesc,
	
													ISNull(LPSD.Basic_pay,0)+ISNULL(LPSD.National_ins,0)+ISNULL(LPSD.National_Ins_out,0)+          
													ISNULL(LPSD.superAnnuation,0)+ISNULL(LPSD.other_allowances,0) AS SpineAmount    */         
													FROM dbo.CostStaff cs     
													INNER JOIN dbo.Project_Proposal_Co_Investigators ppci ON cs.CoinID = ppci.id     
													LEFT JOIN dbo.LUP_Pay_Scale ON LUP_Pay_Scale.Id = cs.PayScale               
													LEFT JOIN dbo.LUP_Sub_Element ON LUP_Sub_Element.ID = LUP_Pay_Scale.Sub_Element         
													LEFT JOIN  dbo.LUP_PAY_SCALE_DETAILS LPSD ON  LPSD.pay_scale= LUP_Pay_Scale.Id AND cs.SPINE=LPSD.SPINE    
													WHERE ppci.Project_Proposal  = @prj_id AND  cs.IsSpine=1 AND cs.UseInflation=1   			 
																										 and CoinID = @rowid

														 /*
												SELECT CoinID,StaffType,Funder,
												StartDate,EndDate,
												ISNULL(Basic,0) Basic, 
												ISNULL(Ni,0) Ni, 
												ISNULL(NiOut,0) NiOut,                 
												ISNULL(Super,0) Super, 
												ISNULL(C.Other_Allowances,0) Other_Allowances,
												 ISNULL(C.Total ,0) Total ,                  
												PayScale,ISNULL(Spell,0) Spell,Reason,c.Spine,LUP_Sub_Element.Description AS PayScaleDesc,      
												ISNull(LPSD.Basic_pay,0)+ISNULL(LPSD.National_ins,0)+ISNULL(LPSD.National_Ins_out,0)+            
												ISNULL(LPSD.superAnnuation,0)+ISNULL(LPSD.other_allowances,0) AS SpineAmount,ISNULL(LUP_Pay_Scale.End_Date,'01-JAN-1900') AS PayScaleEnd, Increment_Date AS IncrementDate 

												,DATEDIFF(d, StartDate, EndDate)+1 as totdays,
													basic / (DATEDIFF(d, StartDate, EndDate)+1) as basicdayrate,
													niout / (DATEDIFF(d, StartDate, EndDate)+1) as nidayrate,
													super / (DATEDIFF(d, StartDate, EndDate)+1) as superdayrate,
													C.Other_Allowances / (DATEDIFF(d, StartDate, EndDate)+1) as OAdayrate
																			
												FROM dbo.CostStaffIncome  C      
												LEFT JOIN dbo.LUP_Pay_Scale ON LUP_Pay_Scale.Id = C.PayScale                 
												LEFT JOIN dbo.LUP_Sub_Element ON LUP_Sub_Element.ID = LUP_Pay_Scale.Sub_Element           
												LEFT JOIN  dbo.LUP_PAY_SCALE_DETAILS LPSD ON  LPSD.pay_scale= LUP_Pay_Scale.Id AND C.SPINE=LPSD.SPINE                  
												WHERE C.Project=@prj_id  AND C.IsSpine=1  AND C.UseInflation=1  AND stafftype<>3      				 
														 and CoinID = @rowid
														 */
															) B ON A.point = B.StartDate

			



					--	select * from @temp_prSTAFF


				
				set @i = 1

				
				set @maxi = ( select MAX(id) from @temp_prSTAFF )

					declare @BASICamout float
					declare @BASICtotaldays int
					declare @BASICdayrate float

					declare @BASICamoutCARRYOVER float
					declare @BASICtotaldaysCARRYOVER int
					declare @BASICdayrateCARRYOVER float

					----

					declare @NIamout float
					declare @NItotaldays int
					declare @NIdayrate float

					declare @NIamoutCARRYOVER float
					declare @NItotaldaysCARRYOVER int
					declare @NIdayrateCARRYOVER float

					----

					declare @Superamout float
					declare @Supertotaldays int
					declare @Superdayrate float

					declare @SuperamoutCARRYOVER float
					declare @SupertotaldaysCARRYOVER int
					declare @SuperdayrateCARRYOVER float

					----

					declare @OAamout float
					declare @OAtotaldays int
					declare @OAdayrate float

					declare @OAamoutCARRYOVER float
					declare @OAtotaldaysCARRYOVER int
					declare @OAdayrateCARRYOVER float

					----

				
				while @i <= @maxi
				begin

						if ( select isnull(BASICamount,0) from @temp_prSTAFF where id = @i ) = 0
						begin
							update @temp_prSTAFF
								set 
										BASICamount = @BASICamoutCARRYOVER,
										BASICtotaldays = @BASICtotaldaysCARRYOVER,
										BASICdayrate = @BASICdayrateCARRYOVER,

										NIamount = @NIamoutCARRYOVER,
										NItotaldays = @NItotaldaysCARRYOVER,
										NIdayrate = @NIdayrateCARRYOVER,

										Superamount = @SuperamoutCARRYOVER,
										Supertotaldays = @SupertotaldaysCARRYOVER,
										Superdayrate = @SuperdayrateCARRYOVER,

										OAamount = @OAamoutCARRYOVER,
										OAtotaldays = @OAtotaldaysCARRYOVER,
										OAdayrate = @OAdayrateCARRYOVER

									where id = @i
						end


						if ( select isnull(BASICamount,0) from @temp_prSTAFF where id = @i ) <> 0
						begin

							set @BASICamout = ( select BASICamount from @temp_prSTAFF where id = @i )
							set @BASICtotaldays = ( select BASICtotaldays from @temp_prSTAFF where id = @i )
							set @BASICdayrate = ( select BASICdayrate from @temp_prSTAFF where id = @i )

							set @NIamout = ( select NIamount from @temp_prSTAFF where id = @i )
							set @NItotaldays = ( select NItotaldays from @temp_prSTAFF where id = @i )
							set @NIdayrate = ( select NIdayrate from @temp_prSTAFF where id = @i )

							set @Superamout = ( select Superamount from @temp_prSTAFF where id = @i )
							set @Supertotaldays = ( select Supertotaldays from @temp_prSTAFF where id = @i )
							set @Superdayrate = ( select Superdayrate from @temp_prSTAFF where id = @i )

							set @OAamout = ( select OAamount from @temp_prSTAFF where id = @i )
							set @OAtotaldays = ( select OAtotaldays from @temp_prSTAFF where id = @i )
							set @OAdayrate = ( select OAdayrate from @temp_prSTAFF where id = @i )

							update @temp_prSTAFF
								set 
									BASICamount = @BASICamout,
										BASICtotaldays = @BASICtotaldays,
										BASICdayrate = @BASICdayrate,

										NIamount = @NIamout,
										NItotaldays = @NItotaldays,
										NIdayrate = @NIdayrate,

										Superamount = @Superamout,
										Supertotaldays = @Supertotaldays,
										Superdayrate = @Superdayrate,

										OAamount = @OAamout,
										OAtotaldays = @OAtotaldays,
										OAdayrate = @OAdayrate

									where id = @i

								set @BASICamoutCARRYOVER = @BASICamout
								set @BASICtotaldaysCARRYOVER = @BASICtotaldays
								set @BASICdayrateCARRYOVER = @BASICdayrate

								set @NIamoutCARRYOVER = @NIamout
								set @NItotaldaysCARRYOVER = @NItotaldays
								set @NIdayrateCARRYOVER = @NIdayrate

								set @SuperamoutCARRYOVER = @Superamout
								set @SupertotaldaysCARRYOVER = @Supertotaldays
								set @SuperdayrateCARRYOVER = @Superdayrate

								set @OAamoutCARRYOVER = @OAamout
								set @OAtotaldaysCARRYOVER = @OAtotaldays
								set @OAdayrateCARRYOVER = @OAdayrate

						end
						
						--select 'first', @i, * from @temp_prSTAFF

						set @i = @i + 1

				end


					--select 'sdfsdfsdf', @i, * from @temp_prSTAFF

					--	select * from @temp_prSTAFF

				/*
					select 'results', *	--b.daycalc * a.dayrate as newrate, a.*, b.daycalc * a.dayrate, b.* 
					from 
						UOE_T1_CSV_DATA b
						left outer join
								@temp_prSTAFF a
									on a.point = b.point
										WHERE PRJ_ID = @prj_id
										AND ROWID = @rowid
										AND StaffTypeDesc = @stafftypedesc
					*/
				
				

				update UOE_T1_CSV_DATA
				set 
				basicval = (b.daycalc * a.BASICdayrate),
				NIval = (b.daycalc * a.NIdayrate),
				Superval = (b.daycalc * a.Superdayrate),
				OAval = (b.daycalc * a.OAdayrate)
				from 
				
					UOE_T1_CSV_DATA b
					left outer join
							@temp_prSTAFF a
								on a.point = b.point
									WHERE PRJ_ID = @prj_id
									AND ROWID = @rowid
									AND StaffTypeDesc = @stafftypedesc

			--	ORDER BY a.POINT


				set @ftei_staff = @ftei_staff + 1

			end


	 -------------------	STAFF PROFILE UPDATE
	

		-------------------	COST PROFILE UPDATE
		
				declare @fte_data_SPONSOR table
		(
			id int identity(1,1),
			descr varchar(500),
			rowid int,
			stafftypedesc varchar(500),
			st_date datetime,
			ed_date datetime
		)

		insert into @fte_data_SPONSOR
			SELECT distinct descr, ROWID, StaffTypeDesc, stdate, eddate
			FROM UOE_T1_CSV_DATA
					WHERE PRJ_ID = @prj_id
					and StaffTypeDesc not in (
						'Estate Lab' ,
						'Estate Desk',
						'Infrastructure Lab' ,
						'Infrastructure Desk',
						'Indirect' 
						)
						--	and ROWID = 37365
					--order by ROWID 
		
		
			--	select 'fte date', * from @fte_data_SPONSOR


		----------------
		/*
		declare @mthstart_prj int
		declare @descr varchar(500)
		declare @st_prj datetime
		declare @prjyr_marker_prj int
		declare @ed_prj datetime
		declare @i_prj int
		declare @mth_start_prj int
		declare @month_between_prj int
		declare @yr_prj int
		declare @yr_marker_prj int
		declare @mths_prj int
		declare @mths_chk_prj int
		declare @i int

		declare @rowid int, @stafftypedesc varchar(500)

		declare @maxi int

		*/
		
		declare @ftei_SPONSOR int
		set @ftei_SPONSOR = 1

		declare @maxftei_SPONSOR int
		set @maxftei_SPONSOR = ( select MAX(id) from @fte_data_SPONSOR )

		declare @st_date_COST DATETIME
		declare @ED_date_COST DATETIME

		while @ftei_SPONSOR <= @maxftei_SPONSOR

		begin

				SET @st_date_COST = ( select st_date from @fte_data_SPONSOR where id = @ftei_SPONSOR )
				SET @ED_date_COST = ( select ED_date from @fte_data_SPONSOR where id = @ftei_SPONSOR )

				declare @temp_pr_COST table (
				id int identity(1,1),
				point datetime,
				COSTamount float,
				COSTtotaldays int,
				COSTdayrate float
				)

			set @descr = ( select descr from @fte_data_SPONSOR where id = @ftei_SPONSOR )
			set @rowid = ( select rowid from @fte_data_SPONSOR where id = @ftei_SPONSOR )
			set @stafftypedesc = ( select stafftypedesc from @fte_data_SPONSOR where id = @ftei_SPONSOR )

				
				
				set @st_prj = ( SELECT 
						  [Start_Date]
							  FROM [dbo].[Project_Proposal]
									where id = @prj_id
									)
									
				set @mthstart_prj = 8--MONTH(@st)		
				
				
				
				if DATEPART(MONTH,@st_prj) = 8
				begin
					set @prjyr_marker_prj = 0
				end
				
				if DATEPART(MONTH,@st_prj) <> 8
				begin
					set @prjyr_marker_prj = 1
				end		
				
				
		
				
				set @ed_prj = ( SELECT 
							  [End_Date]
						  FROM [dbo].[Project_Proposal]
						  where id = @prj_id
									)	
									
	--	select @st_prj, @ed_prj									
									
				
				
				
				set @i_prj = 0
				
			

				
				set @mth_start_prj = ( select MONTH(@st_prj)	)


				
				set @month_between_prj = ( 
										SELECT 
										DBO.UOE_MonthdIFF(@st_prj, @ed_prj)+1
										)
										
				--select @month_between
							
				
				set @yr_prj = 1	

				
				set @yr_marker_prj = 0			

				
				set @mths_prj = 12

				
				set @mths_chk_prj = 12
									
				while @i_prj <= @month_between_prj
				begin
					-- if august then year + 1
					set @yr_prj = ( 
						select 
						case 
						when month(dateadd(m,@i_prj, @st_prj)) = 8 then @yr_prj +1 
						else @yr_prj end
						)
						
					-- if year point reached, check how many months left	
					if @i_prj in (12, 24, 36, 48, 60, 72, 84, 96, 108, 120, 132, 144 )  
						begin
						
							if @month_between_prj - @i_prj < 12
							begin
							set @mths_prj = @month_between_prj - @i_prj
								if @mths_prj = 0
								begin
									set @mths_prj = 1 
								end
							end
						end
						
					if MONTH(dateadd(m,@i_prj, @st_prj)) = @mth_start_prj
					begin
						set @yr_marker_prj = @yr_marker_prj + 1
					end
						
				if MONTH( dateadd(m,@i_prj, @st_prj) ) = 8--@mthstart
				begin
				set @prjyr_marker_prj = @prjyr_marker_prj + 1
				end
						
				insert into @temp_pr_COST ( point)
					select 
						--dateadd(m,@i_prj, @st_prj)
						case 
									when @i_prj = 0 then @st_date_COST
									else
									--	dateadd(m,@i, @st) 
									DATEADD(mm, DATEDIFF(mm, 0, dateadd(m,@i_prj, @st_PRJ) ), 0)
									end
					set @i_prj = @i_prj + 1
					
				end						


				delete from @temp_pr_COST
				where id > @month_between_prj
				
				-- ensure date profile doesn't exceed end date for project
				delete from @temp_pr_COST
				where point > @ed_prj


				--SELECT @ftei_SPONSOR, * FROM @temp_pr_COST
				
				update @temp_pr_COST
							set point = @ST_date_COST
							where id = ( Select MIN(id) from @temp_pr_COST )

				update @temp_pr_COST
							set point = @ED_date_COST
							where id = ( Select MAX(id) from @temp_pr_COST )


				--	RESET TABLE CONTENT
				update @temp_pr_COST
					set 
					COSTamount = 0,
					COSTtotaldays = 0,
					COSTdayrate = 0

										update @temp_pr_COST
										set 
										COSTamount = B.Total * ( Select top 1
										
										case
															when finaladjust IS null then 100
															when finaladjust = '' then 100
															else finaladjust end
														
										 / 100 FROM @resprof WHERE ROWID = @rowid ),
										COSTtotaldays = b.totdays,
										COSTdayrate = B.COSTdayrate
										--SELECT * 
										FROM
										 @temp_pr_COST A
												INNER JOIN (

												SELECT Spell,StartDate,EndDate,Reason,cs.Spine,
												--case when LUP_Sub_Element.Description like '%matched%' then 0 else
													Basic 
												--end as Basic
													,NI,NIOUT,Super,
													
													Total ,--*	( Select ISNULL(finaladjust,100) / 100 FROM @resprof WHERE ROWID = @rowid ) as Total,
													CoinID,cs.StaffType,cs.Other_Allowances,              
												cs.PayScale, 
												LUP_Sub_Element.Description AS PayScaleDesc,
	
												ISNull(LPSD.Basic_pay,0)+ISNULL(LPSD.National_ins,0)+ISNULL(LPSD.National_Ins_out,0)+          
												ISNULL(LPSD.superAnnuation,0)+ISNULL(LPSD.other_allowances,0) AS SpineAmount 
												
												,DATEDIFF(d, StartDate, EndDate)+												
												( case
																											when 
																												(
																													(
																													 dbo.IsLeapYearLkup(DATEPART(YEAR,StartDate)) = 1 
																													)
																													Or
																													(
																													 dbo.IsLeapYearLkup(DATEPART(YEAR,EndDate)) = 1 
																													) 
																												)
																												and dbo.februarybetweendates(StartDate, EndDate) = 1
																												then 0
																												else 1 end )
												
												
												 as totdays,
													Total / (DATEDIFF(d, StartDate, EndDate)+
													( case
																											when 
																												(
																													(
																													 dbo.IsLeapYearLkup(DATEPART(YEAR,StartDate)) = 1 
																													)
																													Or
																													(
																													 dbo.IsLeapYearLkup(DATEPART(YEAR,EndDate)) = 1 
																													) 
																												)
																												and dbo.februarybetweendates(StartDate, EndDate) = 1
																												then 0
																												else 1 end )
													) as COSTdayrate
													
													            
												FROM dbo.CostStaff cs     
												INNER JOIN dbo.Project_Proposal_Co_Investigators ppci ON cs.CoinID = ppci.id     
												LEFT JOIN dbo.LUP_Pay_Scale ON LUP_Pay_Scale.Id = cs.PayScale               
												LEFT JOIN dbo.LUP_Sub_Element ON LUP_Sub_Element.ID = LUP_Pay_Scale.Sub_Element         
												LEFT JOIN  dbo.LUP_PAY_SCALE_DETAILS LPSD ON  LPSD.pay_scale= LUP_Pay_Scale.Id AND cs.SPINE=LPSD.SPINE    
												WHERE ppci.Project_Proposal  = @prj_id AND  cs.IsSpine=1 AND cs.UseInflation=1  
												
														 and CoinID = @rowid
															) B ON A.point = B.StartDate

			



					--	select * from @temp_pr_COST


				
				set @i = 1

				
				set @maxi = ( select MAX(id) from @temp_pr_COST )

					declare @COSTamout float
					declare @COSTtotaldays int
					declare @COSTdayrate float

					declare @COSTamoutCARRYOVER float
					declare @COSTtotaldaysCARRYOVER int
					declare @COSTdayrateCARRYOVER float

					
				
				while @i <= @maxi
				begin

						if ( select isnull(COSTamount,0) from @temp_pr_COST where id = @i ) = 0
						begin
							update @temp_pr_COST
								set 
										COSTamount = @COSTamoutCARRYOVER,
										COSTtotaldays = @COSTtotaldaysCARRYOVER,
										COSTdayrate = @COSTdayrateCARRYOVER

									where id = @i
						end


						if ( select isnull(COSTamount,0) from @temp_pr_COST where id = @i ) <> 0
						begin

							set @COSTamout = ( select COSTamount from @temp_pr_COST where id = @i )
							set @COSTtotaldays = ( select COSTtotaldays from @temp_pr_COST where id = @i )
							set @COSTdayrate = ( select COSTdayrate from @temp_pr_COST where id = @i )


							update @temp_pr_COST
								set 
									COSTamount = @COSTamout,
										COSTtotaldays = @COSTtotaldays,
										COSTdayrate = @COSTdayrate

									where id = @i

								set @COSTamoutCARRYOVER = @COSTamout
								set @COSTtotaldaysCARRYOVER = @COSTtotaldays
								set @COSTdayrateCARRYOVER = @COSTdayrate


						end
						
						--select 'first', @i, * from @temp_pr_COST

						set @i = @i + 1

				end


					--select 'sdfsdfsdf', @i, * from @temp_pr_COST

					--	select * from @temp_pr_COST

				/*
					select 'results', *	--b.daycalc * a.dayrate as newrate, a.*, b.daycalc * a.dayrate, b.* 
					from 
						UOE_T1_CSV_DATA b
						left outer join
								@temp_pr_COST a
									on a.point = b.point
										WHERE PRJ_ID = @prj_id
										AND ROWID = @rowid
										AND StaffTypeDesc = @stafftypedesc
					*/
				
				

				update UOE_T1_CSV_DATA
				
				set 
				
				COSTval = (b.daycalc * a.COSTdayrate)
				from 
				
					UOE_T1_CSV_DATA 
				
					b
					left outer join
							@temp_pr_COST a
								on a.point = b.point
									WHERE PRJ_ID = @prj_id
									AND ROWID = @rowid
									AND StaffTypeDesc = @stafftypedesc

			--	ORDER BY a.POINT


				set @ftei_SPONSOR = @ftei_SPONSOR + 1

			end


	 -------------------	COST PROFILE UPDATE


	 		-------------------	EQUIPMENT PROFILE UPDATE
		
				declare @fte_data_EQUIP table
		(
			id int identity(1,1),
			descr varchar(500),
			rowid int,
			stafftypedesc varchar(500),
			st_date datetime,
			ed_date datetime
		)

		insert into @fte_data_EQUIP
			SELECT distinct descr, ROWID, StaffTypeDesc, stdate, eddate
			FROM UOE_T1_CSV_DATA
					WHERE PRJ_ID = @prj_id
					and StaffTypeDesc not in (
						'Estate Lab' ,
						'Estate Desk',
						'Infrastructure Lab' ,
						'Infrastructure Desk',
						'Indirect' 
						)
						AND ROWID IN (
								SELECT ProjPropOtherDetails
								 FROM 
								CostEquipNonStaff cens 
								INNER JOIN project_proposal_others_details ppod ON cens.ProjPropOtherDetails = ppod.id  
								WHERE ppod.project_proposal = @prj_id
							)


						--	and ROWID = 37365
					--order by ROWID 
		
		
			--	select 'fte date', * from @fte_data_EQUIP


		----------------
	/*
		declare @mthstart_prj int
		declare @descr varchar(500)
		declare @st_prj datetime
		declare @prjyr_marker_prj int
		declare @ed_prj datetime
		declare @i_prj int
		declare @mth_start_prj int
		declare @month_between_prj int
		declare @yr_prj int
		declare @yr_marker_prj int
		declare @mths_prj int
		declare @mths_chk_prj int
		declare @i int

		declare @rowid int, @stafftypedesc varchar(500)

		declare @maxi int
	*/
		
		
		declare @ftei_EQUIP int
		set @ftei_EQUIP = 1

		declare @maxftei_EQUIP int
		set @maxftei_EQUIP = ( select MAX(id) from @fte_data_EQUIP )

		
declare @st_date_EQUIP DATETIME
		declare @ED_date_EQUIP DATETIME

		while @ftei_EQUIP <= @maxftei_EQUIP

		begin

		SET @st_date_EQUIP = ( select st_date from @fte_data_EQUIP where id = @ftei_EQUIP )
				SET @ED_date_EQUIP = ( select ED_date from @fte_data_EQUIP where id = @ftei_EQUIP )

				declare @temp_pr_EQUIP table (
				id int identity(1,1),
				point datetime,
				EQUIPamount float,
				EQUIPtotaldays int,
				EQUIPdayrate float,

				EquipAmountSPON float,
				EquipdayrateSPON float
				)

			set @descr = ( select descr from @fte_data_EQUIP where id = @ftei_EQUIP )
			set @rowid = ( select rowid from @fte_data_EQUIP where id = @ftei_EQUIP )
			set @stafftypedesc = ( select stafftypedesc from @fte_data_EQUIP where id = @ftei_EQUIP )

				
				
				set @st_prj = ( SELECT 
						  [Start_Date]
							  FROM [dbo].[Project_Proposal]
									where id = @prj_id
									)
									
				set @mthstart_prj = 8--MONTH(@st)		
				
				
				
				if DATEPART(MONTH,@st_prj) = 8
				begin
					set @prjyr_marker_prj = 0
				end
				
				if DATEPART(MONTH,@st_prj) <> 8
				begin
					set @prjyr_marker_prj = 1
				end		
				
				
		
				
				set @ed_prj = ( SELECT 
							  [End_Date]
						  FROM [dbo].[Project_Proposal]
						  where id = @prj_id
									)	
									
	--	select @st_prj, @ed_prj									
									
				
				
				
				set @i_prj = 0
				
			

				
				set @mth_start_prj = ( select MONTH(@st_prj)	)


				
				set @month_between_prj = ( 
										SELECT 
										DBO.UOE_MonthdIFF(@st_prj, @ed_prj)+1
										)
										
				--select @month_between
							
				
				set @yr_prj = 1	

				
				set @yr_marker_prj = 0			

				
				set @mths_prj = 12

				
				set @mths_chk_prj = 12
									
				while @i_prj <= @month_between_prj
				begin
					-- if august then year + 1
					set @yr_prj = ( 
						select 
						case 
						when month(dateadd(m,@i_prj, @st_prj)) = 8 then @yr_prj +1 
						else @yr_prj end
						)
						
					-- if year point reached, check how many months left	
					if @i_prj in (12, 24, 36, 48, 60, 72, 84, 96, 108, 120, 132, 144 )  
						begin
						
							if @month_between_prj - @i_prj < 12
							begin
							set @mths_prj = @month_between_prj - @i_prj
								if @mths_prj = 0
								begin
									set @mths_prj = 1 
								end
							end
						end
						
					if MONTH(dateadd(m,@i_prj, @st_prj)) = @mth_start_prj
					begin
						set @yr_marker_prj = @yr_marker_prj + 1
					end
						
				if MONTH( dateadd(m,@i_prj, @st_prj) ) = 8--@mthstart
				begin
				set @prjyr_marker_prj = @prjyr_marker_prj + 1
				end
						
				insert into @temp_pr_EQUIP ( point)
					select 
						--dateadd(m,@i_prj, @st_prj)
						case 
									when @i_prj = 0 then @st_date_EQUIP
									else
									--	dateadd(m,@i, @st) 
									DATEADD(mm, DATEDIFF(mm, 0, dateadd(m,@i_prj, @st_PRJ) ), 0)
									end
					set @i_prj = @i_prj + 1
					
				end						


				delete from @temp_pr_EQUIP
				where id > @month_between_prj
				
				-- ensure date profile doesn't exceed end date for project
				delete from @temp_pr_EQUIP
				where point > @ed_prj


					update @temp_pr_EQUIP
							set point = @ST_date_EQUIP
							where id = ( Select MIN(id) from @temp_pr_EQUIP )

					update @temp_pr_EQUIP
								set point = @ED_date_EQUIP
								where id = ( Select MAX(id) from @temp_pr_EQUIP )
				--SELECT @ftei_EQUIP, * FROM @temp_pr_EQUIP

																			--	RESET TABLE CONTENT
																			update @temp_pr_EQUIP
																				set 
																				EQUIPamount = 0,
																				EQUIPtotaldays = 0,
																				EQUIPdayrate = 0

																				declare @adjustpercequip float
																				set @adjustpercequip = (
																												SELECT 
																												--Funder,
																												--ItemIdInProject AS ID ,
																												--ItemIDInTable AS ElementType,
																												-- SUM(Amount) Total,      
																												-- LUP_Income_Template.ID IncomeTemplateID, 
																												-- LUP_Income_Template.TemplateCode,   
																												-- LUP_Income_Template.Description TemplateDesc,
																												-- Funders.Code FunderCode,
																												-- Funders.Description FunderDesc,
																												 --PercentValue as percentageFunded

																												 percentageFunded 
																													--percentageFunded
																												-- EU_Submission,
																												-- ISNULL(ExceptionRule,0) as ExceptionRule,
																												-- ISNULL(ExceptionRule2,0) AS ExceptionRule2        
																											FROM CostIncomeTemplate      
																											INNER JOIN LUP_Income_Template ON CostIncomeTemplate.IncomeTemplate = LUP_Income_Template.Id      
																											INNER JOIN Funders ON CostIncomeTemplate.Funder = Funders.Id      
																											WHERE Project= @prjid
																											and ItemIdInProject  = @rowid
																											And ((IsSpine=1 And ItemIdInTable=1) OR (ItemIdInTable<>1))   
																											)


																									update @temp_pr_EQUIP
																									set 
																									EQUIPamount = B.Cost,
																									EQUIPtotaldays = b.totdays,
																									EQUIPdayrate = B.COSTdayrate

																									--SELECT * 
																									FROM
																									 @temp_pr_EQUIP A
																											INNER JOIN (

																											SELECT ProjPropOtherDetails,StartDate,EndDate,
																											ISNULL(Cost,0) 
																											--	*	( Select ISNULL(finaladjust,100) / 100 FROM @resprof WHERE ROWID = @rowid )
																													as Cost 
																											,DATEDIFF(d, StartDate, EndDate)+
																												( case
																																										when 
																																											(
																																												(
																																												 dbo.IsLeapYearLkup(DATEPART(YEAR,StartDate)) = 1 
																																												)
																																												Or
																																												(
																																												 dbo.IsLeapYearLkup(DATEPART(YEAR,EndDate)) = 1 
																																												) 
																																											)
																																											and dbo.februarybetweendates(StartDate, EndDate) = 1
																																											then 0
																																											else 1 end )
												
																											 as totdays,
																											ISNULL(Cost,0) 
																												--*	( Select ISNULL(finaladjust,100) / 100 FROM @resprof WHERE ROWID = @rowid )
																												 / (DATEDIFF(d, StartDate, EndDate)+
																											( case
																																										when 
																																											(
																																												(
																																												 dbo.IsLeapYearLkup(DATEPART(YEAR,StartDate)) = 1 
																																												)
																																												Or
																																												(
																																												 dbo.IsLeapYearLkup(DATEPART(YEAR,EndDate)) = 1 
																																												) 
																																											)
																																											and dbo.februarybetweendates(StartDate, EndDate) = 1
																																											then 0
																																											else 1 end )
												
																											) as COSTdayrate
																											 FROM 
																											CostEquipNonStaff cens INNER JOIN project_proposal_others_details ppod ON cens.ProjPropOtherDetails = ppod.id  
																											WHERE ppod.project_proposal = @prj_id AND cens.UseInflation = 1


																													 and ProjPropOtherDetails = @rowid
																														) B ON A.point = B.StartDate

																								



																				--	select * from @temp_pr_EQUIP


				
																			set @i = 1

				
																			set @maxi = ( select MAX(id) from @temp_pr_EQUIP )

																				declare @EQUIPamout float
																				declare @EQUIPtotaldays int
																				declare @EQUIPdayrate float
					
																				

																				declare @EQUIPamoutCARRYOVER float
																				declare @EQUIPtotaldaysCARRYOVER int
																				declare @EQUIPdayrateCARRYOVER float

																			
				
																			while @i <= @maxi
																			begin

																					if ( select isnull(EQUIPamount,0) from @temp_pr_EQUIP where id = @i ) = 0
																					begin
																						update @temp_pr_EQUIP
																							set 
																									EQUIPamount = @EQUIPamoutCARRYOVER,
																									EQUIPtotaldays = @EQUIPtotaldaysCARRYOVER,
																									EQUIPdayrate = @EQUIPdayrateCARRYOVER

																								where id = @i
																					end


																					if ( select isnull(EQUIPamount,0) from @temp_pr_EQUIP where id = @i ) <> 0
																					begin

																						set @EQUIPamout = ( select EQUIPamount from @temp_pr_EQUIP where id = @i )
																						set @EQUIPtotaldays = ( select EQUIPtotaldays from @temp_pr_EQUIP where id = @i )
																						set @EQUIPdayrate = ( select EQUIPdayrate from @temp_pr_EQUIP where id = @i )

																						

																						update @temp_pr_EQUIP
																							set 
																								EQUIPamount = @EQUIPamout,
																									EQUIPtotaldays = @EQUIPtotaldays,
																									EQUIPdayrate = @EQUIPdayrate

																								where id = @i

																							set @EQUIPamoutCARRYOVER = @EQUIPamout
																							set @EQUIPtotaldaysCARRYOVER = @EQUIPtotaldays
																							set @EQUIPdayrateCARRYOVER = @EQUIPdayrate


																					end
						
																					--select 'first', @i, * from @temp_pr_EQUIP

																					set @i = @i + 1

																			end

------------------------------------------------


																--	RESET TABLE CONTENT
																			update @temp_pr_EQUIP
																				set 
																				
					
																				EquipAmountSPON = 0,
																				EquipdayrateSPON = 0

																				

																									update @temp_pr_EQUIP
																									set 
																									

																									EquipAmountSPON = t.CostSpon,
																									EquipdayrateSPON = t.COSTdayrateSpon
																									--SELECT * 
																									FROM
																									 @temp_pr_EQUIP A
																											inner join (
							 
																											SELECT ProjPropOtherDetails,Funder,StartDate,EndDate,
																												ISNULL(Cost,0) Cost,
																											Element  ,
																											ISNULL(Cost,0)  * case
																															--	when @adjustpercequip = 0 then 1
																																when @adjustpercequip = 100 then 1
																																	else @adjustpercequip / 100 end
																													as CostSpon ,


																												ISNULL(Cost,0)  * case
																															--	when @adjustpercequip = 0 then 1
																																when @adjustpercequip = 100 then 1
																																	else @adjustpercequip / 100 end
																												--*	( Select ISNULL(finaladjust,100) / 100 FROM @resprof WHERE ROWID = @rowid )
																												 / (DATEDIFF(d, StartDate, EndDate)+
																											( case
																																										when 
																																											(
																																												(
																																												 dbo.IsLeapYearLkup(DATEPART(YEAR,StartDate)) = 1 
																																												)
																																												Or
																																												(
																																												 dbo.IsLeapYearLkup(DATEPART(YEAR,EndDate)) = 1 
																																												) 
																																											)
																																											and dbo.februarybetweendates(StartDate, EndDate) = 1
																																											then 0
																																											else 1 end )
												
																											) as COSTdayrateSpon
																											FROM dbo.CostEquNonStaffIncome  C      
																											WHERE C.Project=@prj_id AND C.UseInflation=1   
																											and ProjPropOtherDetails = @rowid
																											)
																											 t on A.point = t.StartDate



																				--	select * from @temp_pr_EQUIP


				
																			declare @wtfi int
																			set @wtfi = 1

				
																			set @maxi = ( select MAX(id) from @temp_pr_EQUIP )

																				declare @EQUIPamoutSpon float
																				declare @EQUIPdayrateSpon float


																				declare @EQUIPamoutSPONCARRYOVER float
																				declare @EQUIPdayrateSPONCARRYOVER float
				
																			while @wtfi <= @maxi
																			begin

																					if ( select isnull(EQUIPamountSpon,0) from @temp_pr_EQUIP where id = @wtfi ) = 0
																					begin
																						update @temp_pr_EQUIP
																							set 
																								
																									EQUIPamountSpon = @EQUIPamoutSponCARRYOVER,
																									EQUIPdayrateSpon = @EQUIPdayrateSponCARRYOVER

																								where id = @wtfi
																					end


																					if ( select isnull(EQUIPamountSpon,0) from @temp_pr_EQUIP where id = @wtfi ) <> 0
																					begin

																					
																						set @EQUIPamoutSpon = ( select EQUIPamountSpon from @temp_pr_EQUIP where id = @wtfi )
																						set @EQUIPdayrateSpon = ( select EQUIPdayrateSpon from @temp_pr_EQUIP where id = @wtfi )


																						update @temp_pr_EQUIP
																							set 
																							

																									EQUIPamountSpon = @EQUIPamoutSpon,
										
																									EQUIPdayrateSpon = @EQUIPdayrateSpon

																								where id = @wtfi

																						

																							set @EQUIPamoutSponCARRYOVER = @EQUIPamoutSpon
								
																							set @EQUIPdayrateSponCARRYOVER = @EQUIPdayrateSpon

																					end
						
																					--select 'first', @i, * from @temp_pr_EQUIP

																					set @wtfi = @wtfi + 1

																			end

------------------------------------------------














					--select 'sdfsdfsdf', @i, * from @temp_pr_EQUIP

					--	select * from @temp_pr_EQUIP

			/*
					select 'results', *	,b.daycalc * a.EQUIPdayrate as newrate, a.*, b.daycalc * a.EQUIPdayrate
					from 
						UOE_T1_CSV_DATA b
						left outer join
								@temp_pr_EQUIP a
									on a.point = b.point
										WHERE PRJ_ID = @prj_id
										AND ROWID = @rowid
										AND StaffTypeDesc = @stafftypedesc
				*/
			
				

				update UOE_T1_CSV_DATA
				
				set 
				
				val = (b.daycalc * a.EQUIPdayrate),
				Sponval = 
					case
					when isnull(finaladjust,0) = 0 then
							(	(b.daycalc * a.EquipdayrateSPON) 
											*	( Select case
															when finaladjust IS null then 100
															when finaladjust = '' then 100
															else finaladjust end / 100 FROM @resprof WHERE ROWID = @rowid ) )


															else

															(	(b.daycalc * a.Equipdayrate) 
															*	( Select case
																			when finaladjust IS null then 100
																			when finaladjust = '' then 100
																			else finaladjust end / 100 FROM @resprof WHERE ROWID = @rowid ) )


															end
															

				from 
				
					UOE_T1_CSV_DATA 
				
					b
					left outer join
							@temp_pr_EQUIP a
								on a.point = b.point

									WHERE PRJ_ID = @prj_id
									AND ROWID = @rowid
									AND StaffTypeDesc = @stafftypedesc

			--	ORDER BY a.POINT


				set @ftei_EQUIP = @ftei_EQUIP + 1

			end


	 -------------------	EQUIPMENT PROFILE UPDATE

	 
	 	-------------------	ESTATE INDIRECT INFRA PROFILE UPDATE
		
				declare @fte_data_ESTINDINF table
		(
			id int identity(1,1),
			descr varchar(500),
			rowid int,
			stafftypedesc varchar(500),
			st_date datetime,
			ed_date datetime
		)

		insert into @fte_data_ESTINDINF
			SELECT distinct descr, ROWID, StaffTypeDesc, stdate, eddate
			FROM UOE_T1_CSV_DATA
					WHERE PRJ_ID = @prj_id
					and StaffTypeDesc  in (
						'Estate Lab' ,
						'Estate Desk',
						'Infrastructure Lab' ,
						'Infrastructure Desk',
						'Indirect' 
						)
						

						--	and ROWID = 37365
					--order by ROWID 
		
		
			--	select 'fte date', * from @fte_data_ESTINDINF


		----------------
	/*
		declare @mthstart_prj int
		declare @descr varchar(500)
		declare @st_prj datetime
		declare @prjyr_marker_prj int
		declare @ed_prj datetime
		declare @i_prj int
		declare @mth_start_prj int
		declare @month_between_prj int
		declare @yr_prj int
		declare @yr_marker_prj int
		declare @mths_prj int
		declare @mths_chk_prj int
		declare @i int

		declare @rowid int, @stafftypedesc varchar(500)

		declare @maxi int
		*/
		
		
		declare @ftei_ESTINDINF int
		set @ftei_ESTINDINF = 1

		declare @maxftei_ESTINDINF int
		set @maxftei_ESTINDINF = ( select MAX(id) from @fte_data_ESTINDINF )

		
		declare @st_date_ESTINDINF DATETIME
		declare @ED_date_ESTINDINF DATETIME

		while @ftei_ESTINDINF <= @maxftei_ESTINDINF

		begin

		SET @st_date_ESTINDINF = ( select st_date from @fte_data_ESTINDINF where id = @ftei_ESTINDINF )
				SET @ED_date_ESTINDINF = ( select ED_date from @fte_data_ESTINDINF where id = @ftei_ESTINDINF )

				declare @temp_pr_ESTINDINF table (
				id int identity(1,1),
				point datetime,
				EQUIPamount float,
				EQUIPtotaldays int,
				EQUIPdayrate float,
				EQUIPdayrateNOTEMP float

				)

			set @descr = ( select descr from @fte_data_ESTINDINF where id = @ftei_ESTINDINF )
			set @rowid = ( select rowid from @fte_data_ESTINDINF where id = @ftei_ESTINDINF )
			set @stafftypedesc = ( select stafftypedesc from @fte_data_ESTINDINF where id = @ftei_ESTINDINF )

				
				
				set @st_prj = ( SELECT 
						  [Start_Date]
							  FROM [dbo].[Project_Proposal]
									where id = @prj_id
									)
									
				set @mthstart_prj = 8--MONTH(@st)		
				
				
				
				if DATEPART(MONTH,@st_prj) = 8
				begin
					set @prjyr_marker_prj = 0
				end
				
				if DATEPART(MONTH,@st_prj) <> 8
				begin
					set @prjyr_marker_prj = 1
				end		
				
				
		
				
				set @ed_prj = ( SELECT 
							  [End_Date]
						  FROM [dbo].[Project_Proposal]
						  where id = @prj_id
									)	
									
	--	select @st_prj, @ed_prj									
									
				
				
				
				set @i_prj = 0
				
			

				
				set @mth_start_prj = ( select MONTH(@st_prj)	)


				
				set @month_between_prj = ( 
										SELECT 
										DBO.UOE_MonthdIFF(@st_prj, @ed_prj)+1
										)
										
				--select @month_between
							
				
				set @yr_prj = 1	

				
				set @yr_marker_prj = 0			

				
				set @mths_prj = 12

				
				set @mths_chk_prj = 12
									
				while @i_prj <= @month_between_prj
				begin
					-- if august then year + 1
					set @yr_prj = ( 
						select 
						case 
						when month(dateadd(m,@i_prj, @st_prj)) = 8 then @yr_prj +1 
						else @yr_prj end
						)
						
					-- if year point reached, check how many months left	
					if @i_prj in (12, 24, 36, 48, 60, 72, 84, 96, 108, 120, 132, 144 )  
						begin
						
							if @month_between_prj - @i_prj < 12
							begin
							set @mths_prj = @month_between_prj - @i_prj
								if @mths_prj = 0
								begin
									set @mths_prj = 1 
								end
							end
						end
						
					if MONTH(dateadd(m,@i_prj, @st_prj)) = @mth_start_prj
					begin
						set @yr_marker_prj = @yr_marker_prj + 1
					end
						
				if MONTH( dateadd(m,@i_prj, @st_prj) ) = 8--@mthstart
				begin
				set @prjyr_marker_prj = @prjyr_marker_prj + 1
				end
						
				insert into @temp_pr_ESTINDINF ( point)
					select 
						--dateadd(m,@i_prj, @st_prj)
						case 
									when @i_prj = 0 then @st_date_ESTINDINF
									else
									--	dateadd(m,@i, @st) 
									DATEADD(mm, DATEDIFF(mm, 0, dateadd(m,@i_prj, @st_PRJ) ), 0)
									end
					set @i_prj = @i_prj + 1
					
				end						


				delete from @temp_pr_ESTINDINF
				where id > @month_between_prj
				
				-- ensure date profile doesn't exceed end date for project
				delete from @temp_pr_ESTINDINF
				where point > @ed_prj


					update @temp_pr_ESTINDINF
							set point = @ST_date_ESTINDINF
							where id = ( Select MIN(id) from @temp_pr_ESTINDINF )

					update @temp_pr_ESTINDINF
								set point = @ED_date_ESTINDINF
								where id = ( Select MAX(id) from @temp_pr_ESTINDINF )
				--SELECT @ftei_ESTINDINF, * FROM @temp_pr_ESTINDINF

				--	RESET TABLE CONTENT
				update @temp_pr_ESTINDINF
					set 
					EQUIPamount = 0,
					EQUIPtotaldays = 0,
					EQUIPdayrate = 0,
					EQUIPdayrateNOTEMP = 0

					declare @fundperc float
					set @fundperc = (

									select top 1 
									calcFundPerc
									/*
									case
															when finaladjust IS null then 100
															when finaladjust = '' then 100
															else finaladjust end
															*/
														
									 from @resprof where 
									ROWID = @rowid and 
									stafftypedesc = @stafftypedesc and
									PRJ_ID = @prj_id

									/*

										 SELECT distinct 
										 MAX(ISNULL(FteProjectIncomeAdjustment.IncomeTemplatePercentage,0)) AS IncomeTemplatePercentage           
										 FROM FteProjectIncomeAdjustment   
										 Where Project= @prj_id
										 */
									)

										update @temp_pr_ESTINDINF
										set 
										EQUIPamount = B.Cost,
										EQUIPtotaldays = b.totdays,
										EQUIPdayrate = B.COSTdayrate,
										EQUIPdayrateNOTEMP = B.COSTdayrateNOTEMP
										--SELECT * 
										FROM
										 @temp_pr_ESTINDINF A
												INNER JOIN (

												SELECT COIN AS ProjPropOtherDetails,StartDate,EndDate,
												ISNULL(AMOUNT,0) * ( @fundperc / 100 ) Cost 
												,DATEDIFF(d, StartDate, EndDate)+
													( case
																											when 
																												(
																													(
																													 dbo.IsLeapYearLkup(DATEPART(YEAR,StartDate)) = 1 
																													)
																													Or
																													(
																													 dbo.IsLeapYearLkup(DATEPART(YEAR,EndDate)) = 1 
																													) 
																												)
																												and dbo.februarybetweendates(StartDate, EndDate) = 1
																												then 0
																												else 1 end )
												
												 as totdays,
												( AMOUNT / (DATEDIFF(d, StartDate, EndDate)+
												( case
																											when 
																												(
																													(
																													 dbo.IsLeapYearLkup(DATEPART(YEAR,StartDate)) = 1 
																													)
																													Or
																													(
																													 dbo.IsLeapYearLkup(DATEPART(YEAR,EndDate)) = 1 
																													) 
																												)
																												and dbo.februarybetweendates(StartDate, EndDate) = 1
																												then 0
																												else 1 end )
												
												) )  * ( @fundperc / 100 ) as COSTdayrate,
											( AMOUNT / (DATEDIFF(d, StartDate, EndDate)+
												( case
																											when 
																												(
																													(
																													 dbo.IsLeapYearLkup(DATEPART(YEAR,StartDate)) = 1 
																													)
																													Or
																													(
																													 dbo.IsLeapYearLkup(DATEPART(YEAR,EndDate)) = 1 
																													) 
																												)
																												and dbo.februarybetweendates(StartDate, EndDate) = 1
																												then 0
																												else 1 end )
												
												) )   as COSTdayrateNOTEMP
												 FROM dbo.CostIndirectIncome  C     
											left outer join [dbo].[LUP_Expenditure] d
												on C.ExpType = d.id       
								   WHERE C.Project=@prj_id  AND   C.UseInflation=1 

														 and CoIn = @rowid
														and d.Description = @stafftypedesc
															) B ON A.point = B.StartDate

			




						--	select '@temp_pr_ESTINDINF', * from @temp_pr_ESTINDINF


				
				set @i = 1

				
				set @maxi = ( select MAX(id) from @temp_pr_ESTINDINF )

					declare @ESTINDINFamout float
					declare @ESTINDINFtotaldays int
					declare @ESTINDINFdayrate float
					declare @ESTINDINFdayrateNOTEMP float

					declare @ESTINDINFamoutCARRYOVER float
					declare @ESTINDINFtotaldaysCARRYOVER int
					declare @ESTINDINFdayrateCARRYOVER float
					declare @ESTINDINFdayrateNOTEMPCARRYOVER float

					
				
				while @i <= @maxi
				begin

						if ( select isnull(EQUIPamount,0) from @temp_pr_ESTINDINF where id = @i ) = 0
						begin
							update @temp_pr_ESTINDINF
								set 
										EQUIPamount = @ESTINDINFamoutCARRYOVER,
										EQUIPtotaldays = @ESTINDINFtotaldaysCARRYOVER,
										EQUIPdayrate = @ESTINDINFdayrateCARRYOVER,
										EQUIPdayrateNOTEMP = @ESTINDINFdayrateNOTEMPCARRYOVER

									where id = @i
						end


						if ( select isnull(EQUIPamount,0) from @temp_pr_ESTINDINF where id = @i ) <> 0
						begin

							set @ESTINDINFamout = ( select EQUIPamount from @temp_pr_ESTINDINF where id = @i )
							set @ESTINDINFtotaldays = ( select EQUIPtotaldays from @temp_pr_ESTINDINF where id = @i )
							set @ESTINDINFdayrate = ( select EQUIPdayrate from @temp_pr_ESTINDINF where id = @i )
							set @ESTINDINFdayrateNOTEMP = ( select EQUIPdayrateNOTEMP from @temp_pr_ESTINDINF where id = @i )


							update @temp_pr_ESTINDINF
								set 
									EQUIPamount = @ESTINDINFamout,
										EQUIPtotaldays = @ESTINDINFtotaldays,
										EQUIPdayrate = @ESTINDINFdayrate,
										EQUIPdayrateNOTEMP = @ESTINDINFdayrateNOTEMP

									where id = @i

								set @ESTINDINFamoutCARRYOVER = @ESTINDINFamout
								set @ESTINDINFtotaldaysCARRYOVER = @ESTINDINFtotaldays
								set @ESTINDINFdayrateCARRYOVER = @ESTINDINFdayrate
								set @ESTINDINFdayrateNOTEMPCARRYOVER = @ESTINDINFdayrateNOTEMP


						end
						
					--	select 'first', @i, * from @temp_pr_ESTINDINF

						set @i = @i + 1

				end


				--	select 'sdfsdfsdf', @i, * from @temp_pr_ESTINDINF

					--	select * from @temp_pr_ESTINDINF

		/*
					select 'results', b.daycalc * a.EQUIPdayrate as newrate, a.*, b.daycalc * a.EQUIPdayrate
					from 
						UOE_T1_CSV_DATA b
						left outer join
								@temp_pr_ESTINDINF a
									on a.point = b.point
										WHERE PRJ_ID = @prj_id
										AND ROWID = @rowid
										AND StaffTypeDesc = @stafftypedesc
				*/
			
				update UOE_T1_CSV_DATA
				
				set 
				
				SPONVAL = (b.daycalc * a.EQUIPdayrate),
				subincome =  case 
						when ( select TOP 1 sponsorcont from @resprof 
							where PRJ_ID = @prj_id
									AND ROWID = @rowid
									AND StaffTypeDesc = @stafftypedesc ) = 0 then 0
									else
								
								(b.daycalc * a.EQUIPdayrateNOTEMP)  end
				
										/*
											*
										( case
											when isnull(finaladjust,'') = '' then 100 else 
											finaladjust end / 100
											 )
											 */

				from 
				
					UOE_T1_CSV_DATA 
				
					b
					left outer join
							@temp_pr_ESTINDINF a
								on a.point = b.point
									WHERE PRJ_ID = @prj_id
									AND ROWID = @rowid
									AND StaffTypeDesc = @stafftypedesc

			--	ORDER BY a.POINT


				set @ftei_ESTINDINF = @ftei_ESTINDINF + 1

			end


	 -------------------	ESTATE INDIRECT INFRA PROFILE UPDATE














	 --	submitted income
	
	 update UOE_T1_CSV_DATA 
	 set 
		subincome = 
				sponval * ( 
				
				--B.FundedPercentage / 100 )
				c.calcFundPerc / 100 )

				
				from
				UOE_T1_CSV_DATA a
					inner join (
								 SELECT --A.Project,A.Inflation,A.Charge_Type,            
									--A.VAT_Type,A.Vat_Value,
									A.ItemID_In_Project AS ID,
									--A.ItemIn_Table AS ElementType,A.CostCategory,             
									--B.Description InflationDesc,B.Effective_Date AS InflationEffectiveDate,C.ICT_Class_Description ChargeTypeDesc,             
									D.Description CostCategoryDesc,
									--ISNULL(E.Description,' ') JesCategoryDesc ,F.DESCRIPTION VATDESC ,  
									--ISNULL(D.ExceptionalRule,0) ExceptionalRule,ISNULL(DiscretionaryPoint,1) AS DiscretionaryPoint,Funder,
									ISNULL(FundedPercentage,0) FundedPercentage
									-- ,ISNULL(IncomeTemplatePercentage,0) IncomeTemplatePercentage       
									FROM Project_Income_Adjustment A LEFT JOIN Lup_Inflation_Factor B ON             
									A.Inflation=B.ID LEFT JOIN LUP_FIX_ICT_CLASS C ON             
									A.Charge_Type=C.ICT_CLASS_ID             
									LEFT JOIN Lup_Element_Categories D ON A.CostCategory=D.ID              
									LEFT JOIN Lup_Fix_Jes_Categories E ON D.JesCategory=E.ID             
									LEFT JOIN LUP_FIX_VAT F ON A.VAT_Type=F.ID            
									WHERE A.Project=  @prj_id
									) b
										on a.ROWID = b.id
										inner join @resprof c
											on c.ROWID = a.ROWID
										WHERE a.StaffTypeDesc
											NOT IN (
											'Estate Lab',
											'Indirect',
											'Infrastructure Lab'
											)
										

---------	BALANCE VALUES



		declare @rowidBALANCE int
		declare @stafftypedescBALANCE varchar(300)

		declare @ii int
		set @ii = 1

		declare @maxii int
		set @maxii = ( select MAX(recid) from @resprof )


		declare @tempBALANCE table (
							id int identity(1,1),
							result_id int,
							point datetime,
							rowid int,
							val float,
							[Basicval] float,
							[NIval] float,
							[Superval] float,
							[Sponval] float,
							OAval float,
							SponsorCont float
							)

		while @ii <= @maxii
		begin

			set @rowidBALANCE = ( select rowid from @resprof where recid = @ii )
			set @stafftypedescBALANCE = ( select StaffTypeDesc from @resprof where recid = @ii )

		--	select @rowidBALANCE , @stafftypedescBALANCE
							
							delete from @tempBALANCE
							
							insert into @tempBALANCE
								select id, point, ROWID, 
								CONVERT(DECIMAL(20,2),val), 
								CONVERT(DECIMAL(20,2),Basicval), 
								CONVERT(DECIMAL(20,2),NIval), 
								CONVERT(DECIMAL(20,2),Superval), 
								CONVERT(DECIMAL(20,2),Sponval), 
								CONVERT(DECIMAL(20,2),OAval), 
								CONVERT(DECIMAL(20,2),subincome) from 
										UOE_T1_CSV_DATA
										where PRJ_ID = @prj_id
										and ROWID = @rowidBALANCE
										and StaffTypeDesc = @stafftypedescBALANCE
											order by ROWID, StaffTypeDesc, point

									declare @val_DIFFBALANCE float = 0
									declare @basicval_DIFFBALANCE float = 0
									declare @nival_DIFFBALANCE float = 0
									declare @superval_DIFFBALANCE float = 0
									declare @sponval_DIFFBALANCE float = 0
									declare @oaval_DIFFBALANCE float = 0

									declare @sponcontval_DIFFBALANCE float = 0

								--	select * from @tempBALANCE

								declare @tempBALANCEi int
								set @tempBALANCEi = 1

								declare @maxtempi int
								set @maxtempi = ( select MAX(id) from @tempBALANCE )

							--	select @tempBALANCEi, @maxtempi

								while @tempBALANCEi <= @maxtempi
								begin

								
												if @tempBALANCEi = @maxtempi
												begin

													--declare @valtotROUND DECIMAL(20,2)
													set @valtotROUND = ( SELECT CONVERT(DECIMAL(20,2),Total) FROM @resprof WHERE recid = @ii )
													set @val_DIFFBALANCE = ( ( Select SUM(convert(decimal(20,2),val)) from @tempBALANCE ) - @valtotROUND )
					
													--	select @valtotROUND, @val_DIFFBALANCE, ( Select SUM(convert(decimal(20,2),val)) from @tempBALANCE )
										
													--declare @basictotROUND DECIMAL(20,2)
													set @basictotROUND = ( SELECT CONVERT(DECIMAL(20,2),basicTotal) FROM @resprof WHERE recid = @ii )
													set @basicval_DIFFBALANCE = ( ( Select SUM(convert(decimal(20,2),basicval)) from @tempBALANCE ) - @basictotROUND )

													--	select @basictotROUND, @basicval_DIFFBALANCE, ( Select SUM(convert(decimal(20,2),basicval)) from @tempBALANCE )

													--declare @nitotROUND DECIMAL(20,2)
													set @nitotROUND = ( SELECT CONVERT(DECIMAL(20,2),niTotal) FROM @resprof WHERE recid = @ii )
													set @nival_DIFFBALANCE = ( ( Select SUM(convert(decimal(20,2),nival)) from @tempBALANCE ) - @nitotROUND )

													--	select @nitotROUND, @nival_DIFFBALANCE, ( Select SUM(convert(decimal(20,2),nival)) from @tempBALANCE )

													--declare @supertotROUND DECIMAL(20,2)
													set @supertotROUND = ( SELECT CONVERT(DECIMAL(20,2),superTotal) FROM @resprof WHERE recid = @ii )
													set @superval_DIFFBALANCE = ( ( Select SUM(convert(decimal(20,2),superval)) from @tempBALANCE ) - @supertotROUND )

													--	select @supertotROUND, @superval_DIFFBALANCE, ( Select SUM(convert(decimal(20,2),superval)) from @tempBALANCE )

													--declare @spontotROUND DECIMAL(20,2)
													set @spontotROUND = ( SELECT 
																				case
																							when finaladjust IS null  then CONVERT(DECIMAL(20,2),[SponsorCONT]) 
																							when finaladjust = ''  then CONVERT(DECIMAL(20,2),[SponsorCONT]) 
																					else
													
																						CONVERT(DECIMAL(20,2),SponTotal) /100  *
																							( case
																							--when finaladjust = 0   then 0
																							when finaladjust IS null  then 100 
																							when finaladjust = ''  then 100 
																							
																							else 
																							finaladjust end 
																								)
																								end
																							FROM @resprof WHERE recid = @ii )


													set @sponval_DIFFBALANCE = ( ( Select SUM(convert(decimal(20,2),Sponval)) from @tempBALANCE ) - @spontotROUND )

													--	select @spontotROUND, @sponval_DIFFBALANCE, ( Select SUM(convert(decimal(20,2),Sponval)) from @tempBALANCE )

													declare @oatotROUND DECIMAL(20,2)
													set @oatotROUND = ( SELECT CONVERT(DECIMAL(20,2),Other_Allowances) FROM @resprof WHERE recid = @ii )
													set @oaval_DIFFBALANCE = ( ( Select SUM(convert(decimal(20,2),OAval)) from @tempBALANCE ) - @oatotROUND )


													declare @sponsorconttotROUND DECIMAL(20,2)
													set @sponsorconttotROUND = ( SELECT CONVERT(DECIMAL(20,2),SponsorCONT) FROM @resprof WHERE recid = @ii )
													set @sponcontval_DIFFBALANCE = ( ( Select SUM(convert(decimal(20,2),SponsorCont)) from @tempBALANCE ) - @sponsorconttotROUND )

													
												
										/*		
										update UOE_T1_CSV_DATA
										set 
										val = convert(decimal(20,2),val - @val_DIFFBALANCE),
										basicval = convert(decimal(20,2),basicval - @basicval_DIFFBALANCE),
										NIval = convert(decimal(20,2),NIval - @nival_DIFFBALANCE),
										Superval = convert(decimal(20,2),Superval - @superval_DIFFBALANCE),
										Sponval = convert(decimal(20,2),Sponval - @sponval_DIFFBALANCE),
										OAval = convert(decimal(20,2),OAval - @oaval_DIFFBALANCE),
										subincome = convert(decimal(20,2),subincome - @sponcontval_DIFFBALANCE)
										where PRJ_ID = @prj_id
										and ROWID = @rowidBALANCE
										and id = ( select result_id from @tempBALANCE
													where id = @maxtempi )
											*/	
										end

									set @tempBALANCEi = @tempBALANCEi + 1

								end


							set @ii = @ii + 1
			
		end 





-- select * from @resprof

---------	BALANCE VALUES

		
								
								
end




