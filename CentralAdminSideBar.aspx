<%@ page language="vb" autoeventwireup="false" inherits="PFACT.CentralAdminSideBar, pFACT_Deploy" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>pFACT</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<%=styleSheetVar%>
	</HEAD>
	<body Class="SideBar">
		<form id="Form1" method="post" runat="server">
			<INPUT id="hdnOtherVars" style="Z-INDEX: 101; LEFT: 8px; POSITION: absolute; TOP: 8px"
				type="hidden" name="hdnOtherVars" runat="server"><INPUT id="hdnArrTextVals" style="Z-INDEX: 102; LEFT: 8px; POSITION: absolute; TOP: 40px"
				type="hidden" name="hdnArrTextVals" runat="server">
		</form>
		<script type="text/javascript" src="menubarCentralAdmin.js"></script>
		<script type="text/javascript" src="menuFunctions.js"></script>
	</body>
</HTML>
<style>A { COLOR: #ffffff; TEXT-DECORATION: none }
	A:hover { TEXT-DECORATION: underline }
	DIV { PADDING-RIGHT: 0px; PADDING-LEFT: 0px; FONT-WEIGHT: normal; FONT-SIZE: 12px; PADDING-BOTTOM: 0px; COLOR: #ffffff; PADDING-TOP: 0px; FONT-FAMILY: Verdana, sans-serif; BACKGROUND-COLOR: #000099; layer-background-color: #000000 }
	DIV.menuMain { LEFT: 30px; VISIBILITY: visible; WIDTH: 150px; HEIGHT: 20px }
	DIV.menuDropDown { LEFT: 50px; VISIBILITY: hidden; WIDTH: 100px; POSITION: absolute; TOP: 30px; HEIGHT: 85px }
		</style>
	
<style>
.alertcontainer {
   position: relative;
}

td {
font-size:13px;
font-family: Arial;
}

.alerttag {
   float: left;
   position: absolute;
   border: 2px solid black;
   left: 5px;
   top: 400px;
   width: 100px;
   height: 60px;
   z-index: 1000;
   padding: 5px;
   
   font-size: 10px; 
}

.alerttagPAC {
   float: left;
   position: absolute;
   border: 2px solid black;
   left: 5px;
   top: 500px;
   width: 100px;
   height: 60px;
   z-index: 1000;
   padding: 5px;

   font-size: 10px; 
}


.alerttaghelp {
   float: left;
   position: absolute;
   border: 2px solid black;
   left: 5px;
   top: 600px;
   width: 100px;
   height: 40px;
   z-index: 1000;
   padding: 5px;

   font-size: 10px; 
}

</style>
<!--
<div class="alertcontainer">

	<div id="alertuserdisplay" class="alerttag" > 
<table class="stat">

<tr>
<td>
<div style="display:inline;"><a href="mrfreport.asp" target="pfactrepwin" >MRF Report</a></div>
</td>
</tr>

<tr>
<td>
<div style="display:inline;"><a href="pooltechnicians.asp" target="pfactrepwin" >Tech Report</a></div>
</td>
</tr>

<tr>
<td>
<div style="display:inline;"><a href="report1650.asp" target="pfactrepwin" >1650</a></div>
</td>
</tr>

</table>	

	</div>
	

<div id="alertuserdisplay" class="alerttagPAC" > 
<table class="stat">	
<tr>
<td>
<div style="display:inline;"><a href="http://vmpac:90/PAC/micrm/BUDGET/" target="_newpac" >PAC Budget System</a></div>
</td>
</tr>
</table>
</div>		
	
<div id="alertuserdisplay" class="alerttaghelp" > 
<table class="stat">	
<tr>
<td>
<div style="display:inline;"><a href="help.asp" target="pfactrepwin" >Help</a></div>
</td>
</tr>
</table>
</div>		
	
</div>	
-->