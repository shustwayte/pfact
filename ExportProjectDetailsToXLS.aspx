<%@ page language="VB" autoeventwireup="false" inherits="PFACT.ExportProjectDetailsToXLS, pFACT_Deploy" %>
<%@ Register Assembly="pFACT.CustomControl" Namespace="BbyB.pFACT.Controls" TagPrefix="pFACT" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>PFACT</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR"/>
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE"/>
		<meta content="JavaScript" name="vs_defaultClientScript"/>
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema"/>
		<link id="LinkStyle" href="" type="text/css" rel="stylesheet" runat="server"/>
		<script src="ClientScripts.js" type="text/javascript" ></script>
		<script language="javascript" type="text/javascript" >
		function SetDefaults()
			{
				//Required window size
				windowWidth =1024;
				windowHeight= 700;	
	
				//If IE then, calculate centre screen by screen max size,
				if (screen)
				{
					x = (screen.availWidth - windowWidth)/2;
					y = (screen.availHeight - windowHeight)/2;
					//Give offset to roughly align within the main frame.
					//x = x + 50;
					y = y + 5;
				}

				window.resizeTo(windowWidth, windowHeight)
				window.moveTo(x, y)				
				self.focus();
							
			}
			function cancelClicked()
			{
			    self.close();
			}
			</script>
</head>
<body class="Body" onload="SetDefaults();"onbeforeunload="HandleOnClose();">
    <form id="ExportToXLS" runat="server">
    <div>
		<input id="txtImagePath" style="Z-INDEX: 104; LEFT: 8px; POSITION: absolute; TOP: 8px" 	type="hidden" name="txtImagePath" runat="server"/>
     <table width="990px">
        <tr>
			<td style="HEIGHT: 23px" align="center" colspan="2">
			    <asp:label id="lblMessage" runat="server" CssClass="ErrorLabel" Height="9px" Width="323px"/>
			</td>
		</tr>
		<tr align="center" >
			<td style="HEIGHT: 23px" align="left">
			    <asp:label id="lblIncomeRule" runat="server" CssClass="LabelCaption" Height="9px" Width="323px" Text="Income Rule"/>
			</td>
			<td align="right">
			    <IMG id="Close" onmouseover="javascript: toggleImage('Close',ExportToXLS.txtImagePath.value+'Close_mover.jpg')"
							style="WIDTH: 72px; CURSOR: hand; HEIGHT: 29px" onclick="javascript: cancelClicked();" onmouseout="javascript: toggleImage('Close',ExportToXLS.txtImagePath.value+'Close.jpg')"
							height="24" alt="" width="62" align="absBottom" runat="server">
			</td>
		</tr>
		<tr>
		    <td colspan="2">
                <pFACT:ExportProjectDetailsToXLS ID="ExportProjectDetailsToXLS" runat="server" />
		    </td>
		</tr>
    </table>
    </div>
    </form>
</body>
</html>

